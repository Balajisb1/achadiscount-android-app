package com.application.ads.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfflineStoreListviewAdapter extends BaseAdapter {

    List<OfflineStoreData> categorydata;
    Context c;
    LayoutInflater inflater;

    public OfflineStoreListviewAdapter(Context c, List<OfflineStoreData> categorydata) {
        // TODO Auto-generated constructor stub
        this.categorydata = categorydata;
        this.c = c;
        //	Log.e("SIze ", categorydata.size()+"");
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categorydata.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return categorydata.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.activity_brand_list_detail, null);
        if (categorydata.get(position).getDist() <= 10) {
            ImageView offlineofferimage = (ImageView) view.findViewById(R.id.offlineofferimage);
            TextView offlineoffername = (TextView) view.findViewById(R.id.offlineoffername);
            TextView offlineofferdesc = (TextView) view.findViewById(R.id.offlineofferdesc);

            offlineoffername.setText(categorydata.get(position).getOffer());
            offlineofferdesc.setText(categorydata.get(position).getDescription());
            //Log.e("img path", "http://offline.achadiscount.in/admin/uploads/"+categorydata.get(position).getImage());
            Picasso.with(c).load(DBConnection.OFFLINE_EXTRAIMG + categorydata.get(position).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(offlineofferimage);

        }
        return view;
    }

}
