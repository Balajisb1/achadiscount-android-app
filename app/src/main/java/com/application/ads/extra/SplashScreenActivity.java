package com.application.ads.extra;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;

import com.application.ads.NavigationActivity;
import com.application.ads.R;
import com.application.ads.data.MyApplication;
import com.application.ads.utils.InternetDetector;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by devel-73 on 29/9/16.
 */

public class SplashScreenActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    //GPSCurrentLocation gpsCurrentLocation;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    boolean gpsStatus = false;
    double latitude, longitude;
    MyApplication myApplication;
    private android.app.Fragment fragment = null;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        createLocationRequest();
        buildGoogleApiClient();
        super.onCreate(savedInstanceState);

        //Hide actionBar
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();

        setContentView(R.layout.activity_splash);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);
        myApplication = MyApplication.getInstance();

        checkInternet();


//        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));

    }


    public void checkInternet() {


        if (InternetDetector.getInstance(this).isOnline(this))

        {

            new Thread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    try {
                        Thread.sleep(1500);
                        Intent intent = new Intent(SplashScreenActivity.this, NavigationActivity.class);
                        finish();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } else

        {
            //showAlert("You are offline..Please Check Internet Connection...!");
            new Thread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    try {
                        Thread.sleep(1500);
                        Intent intent = new Intent(SplashScreenActivity.this, NavigationActivity.class);
                        finish();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

    }

    public void showAlert(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SplashScreenActivity.this);

        alertDialog.setTitle(msg);

        alertDialog.setPositiveButton("Retry",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        checkInternet();
                    }
                });

        alertDialog.setNegativeButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        finish();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (checkGPS()) {


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                latitude = mLastLocation.getLatitude();

                longitude = mLastLocation.getLongitude();

                handleNewLocation(mLastLocation);

	            /*mLatitudeText.setText(String.format("%s: %f", mLatitudeLabel,
                        mLastLocation.getLatitude()));

	            mLongitudeText.setText(String.format("%s: %f", mLongitudeLabel,
	                    mLastLocation.getLongitude()));*/
            } else {
                //        Toast.makeText(this, "No location Detected", Toast.LENGTH_LONG).show();
                mGoogleApiClient.connect();
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i("THIS", "Connection suspended");
        mGoogleApiClient.connect();
    }

    public void handleNewLocation(Location location) {
        myApplication.setMyLatitude(location.getLatitude() + "");
        myApplication.setMyLongitude(location.getLongitude() + "");
        //          Toast.makeText(this, "Lat "+latitude+", Longitude"+longitude, Toast.LENGTH_LONG).show();
        latitude = Double.parseDouble(myApplication.getMyLatitude());
        longitude = Double.parseDouble(myApplication.getMyLongitude());


        Log.e("latitute", latitude + "");
        Log.e("longitude", longitude + "");

        if (myApplication.getMyLongitude() != null && myApplication.getMyLongitude() != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("THIS", "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(SplashScreenActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        if (gpsStatus) {
            mGoogleApiClient.connect();
        }
    }

    public boolean checkGPS() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gpsStatus = false;
            return true;
        } else {
//            showGPSDisabledAlertToUser();
            return false;
        }
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

}
