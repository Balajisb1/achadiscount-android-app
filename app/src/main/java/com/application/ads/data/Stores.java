package com.application.ads.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Stores implements Parcelable {

    public static final Parcelable.Creator<Stores> CREATOR = new Creator<Stores>() {

        @Override
        public Stores[] newArray(int size) {
            return new Stores[size];
        }

        @Override
        public Stores createFromParcel(Parcel source) {
            return new Stores(source);
        }
    };
    private String product_price;
    private String affliate_name;
    private String affliate_logo;
    private String store_id;
    private String product_url;
    private String pp_id;
    private String offline_price;
    private String offline_offer;
    private String offline_storename;
    private String offline_storelogo;
    private String latitude;
    private String longtitude;
    private String address;
    private List<Stores> offlinestores;
    private List<Stores> storeslist;
    private String productid;
    private String stockAvail;
    private String storerating;
    private List<ProductData> productDatas;

    public Stores(Parcel source) {
        setAddress(source.readString());
        setOffline_storelogo(source.readString());
        setOffline_storename(source.readString());
        setLatitude(source.readString());
        setLongtitude(source.readString());
    }

    public Stores() {
        // TODO Auto-generated constructor stub
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getAffliate_name() {
        return affliate_name;
    }

    public void setAffliate_name(String affliate_name) {
        this.affliate_name = affliate_name;
    }

    public String getAffliate_logo() {
        return affliate_logo;
    }

    public void setAffliate_logo(String affliate_logo) {
        this.affliate_logo = affliate_logo;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getPp_id() {
        return pp_id;
    }

    public void setPp_id(String pp_id) {
        this.pp_id = pp_id;
    }

    public String getOffline_storelogo() {
        return offline_storelogo;
    }

    public void setOffline_storelogo(String offline_storelogo) {
        this.offline_storelogo = offline_storelogo;
    }

    public String getOffline_storename() {
        return offline_storename;
    }

    public void setOffline_storename(String offline_storename) {
        this.offline_storename = offline_storename;
    }

    public String getOffline_offer() {
        return offline_offer;
    }

    public void setOffline_offer(String offline_offer) {
        this.offline_offer = offline_offer;
    }

    public String getOffline_price() {
        return offline_price;
    }

    public void setOffline_price(String offline_price) {
        this.offline_price = offline_price;
    }

    public List<Stores> getStores() {
        return offlinestores;
    }

    public void setStores(List<Stores> offlinestores) {
        this.offlinestores = offlinestores;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getAddress());
        dest.writeString(getOffline_storelogo());
        dest.writeString(getOffline_storename());
        dest.writeString(getLatitude());
        dest.writeString(getLongtitude());

    }

    public List<Stores> getStoreslist() {
        return storeslist;
    }

    public void setStoreslist(List<Stores> storeslist) {
        this.storeslist = storeslist;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public List<ProductData> getProductDatas() {
        return productDatas;
    }

    public void setProductDatas(List<ProductData> productDatas) {
        this.productDatas = productDatas;
    }

    public String getStockAvail() {
        return stockAvail;
    }

    public void setStockAvail(String stockAvail) {
        this.stockAvail = stockAvail;
    }

    public String getStorerating() {
        return storerating;
    }

    public void setStorerating(String storerating) {
        this.storerating = storerating;
    }


}
