package com.application.ads.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.application.ads.R;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PopupAdapter implements InfoWindowAdapter {
    Context context;
    String imgurl;
    String newpopup;
    List<OfflineStoreData> categorydata;
    private View popup = null;
    private LayoutInflater inflater = null;

    public PopupAdapter(Context context, List<OfflineStoreData> categorydata) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.categorydata = categorydata;
        this.newpopup = newpopup;
        this.imgurl = imgurl;
    }


    @Override
    public View getInfoWindow(Marker marker) {
        return (null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        marker.hideInfoWindow();
        popup = inflater.inflate(R.layout.activity_offline_offer_homepage, null);
        ImageView tv = (ImageView) popup.findViewById(R.id.offerimghome);
        String markertitle = marker.getTitle();

        for (int i = 0; i < categorydata.size(); i++) {

            if (markertitle.equalsIgnoreCase(categorydata.get(i).getBrandname())) {
                Picasso.with(context).load(DBConnection.OFFLINE_EXTRAIMG + categorydata.get(i).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(tv);

            } else {
                marker.hideInfoWindow();
            }
        }

    
   /* popup.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(newpopup!=null){
			Intent intent=new Intent(context,OfflineOfferDetails.class);
			intent.putExtra("id",newpopup);
			context.startActivity(intent);
			}
		}
	});*/
//    tv.setText(marker.getSnippet());

        return (popup);
    }
}