package com.application.ads.data;

import com.google.android.gms.common.api.GoogleApiClient;

public class MyApplication {

    private static MyApplication mInstance;

    private String tempcatid;

    private GoogleApiClient mGoogleApiClient;

    private String myLatitude;

    private String myLongitude;
    private String productName;

    protected MyApplication() {

    }

    public static synchronized MyApplication getInstance() {
        if (null == mInstance) {
            mInstance = new MyApplication();
        }
        return mInstance;
    }

    public String getTempcatid() {
        return tempcatid;
    }

    public void setTempcatid(String tempcatid) {
        this.tempcatid = tempcatid;
    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        this.mGoogleApiClient = mGoogleApiClient;
    }

    public String getMyLatitude() {
        return myLatitude;
    }

    public void setMyLatitude(String myLatitude) {
        this.myLatitude = myLatitude;
    }

    public String getMyLongitude() {
        return myLongitude;
    }

    public void setMyLongitude(String myLongitude) {
        this.myLongitude = myLongitude;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

}
