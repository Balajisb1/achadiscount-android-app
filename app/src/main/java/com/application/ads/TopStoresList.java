package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.TopStoresAdapter;
import com.application.ads.data.ProductData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TopStoresList extends Activity {

    final String urlLoadTopStores = DBConnection.BASEURL + "load_top_store.php";

    JSONObject object;
    JSONParser parser;
    //	AutoCompleteTextView searchedittext;
    SharedPreferences preferences;
    SessionManager manager;
    ImageButton searchbtn;
    TopStoresAdapter topStoresAdapter;
    GridView storesgrid;

    List<ProductData> productList;
    //	LinearLayout linearStoreList;
    SwipeRefreshLayout mSwipeRefreshLayout;


    Button btnTopStoreAll, btnTopStoreA, btnTopStoreB, btnTopStoreC, btnTopStoreD,
            btnTopStoreE, btnTopStoreF, btnTopStoreG, btnTopStoreH,
            btnTopStoreI, btnTopStoreJ, btnTopStoreK, btnTopStoreL,
            btnTopStoreM, btnTopStoreN, btnTopStoreO, btnTopStoreP,
            btnTopStoreQ, btnTopStoreR, btnTopStoreS, btnTopStoreT,
            btnTopStoreU, btnTopStoreV, btnTopStoreW, btnTopStoreX,
            btnTopStoreY, btnTopStoreZ;
    int numOfCount = 2;
    String pronames[] = new String[]{};
    List<String> names;
    TextView toolbartitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_cashback_stores);
        getActionBar().setTitle("Coupon Discounts");
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Top Stores/All");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        manager = new SessionManager(TopStoresList.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        storesgrid = (GridView) findViewById(R.id.storesgrid);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        /*searchedittext=(AutoCompleteTextView)findViewById(R.id.searchstore);

		Set<String> set = preferences.getStringSet("key", null);
		names=new ArrayList<String>(set);
		
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(TopStoresList.this, android.R.layout.simple_list_item_1, names);
		searchedittext.setAdapter(adapter);
		
		searchbtn=(ImageButton)findViewById(R.id.searchbtn);
		searchbtn.setOnClickListener(new OnClickListener() {

			
			@Override
			public void onClick(View v) {
				if(searchedittext.getText().toString().equals("") || searchedittext.getText().toString().equals(null)){
					Toast.makeText(getApplicationContext(), "Please provide search term", 5000).show();
				}
				else{					
				Intent intent=new Intent(TopStoresList.this,CouponList.class);
				intent.putExtra("storeName",searchedittext.getText().toString());
				searchedittext.setText("");
				startActivity(intent);
				}
			}
		});*/
		
		
		/*getActionBar().setTitle("Top Stores List");*/
        getActionBar().setTitle("");
        parser = new JSONParser();


        btnTopStoreAll = (Button) findViewById(R.id.btnTopStoreAll);
        btnTopStoreAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadTopCashBack("").execute();
              /*  new LoadTopCashBack("").execute();
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("start_letter", type));*/


           /*     String url=urlLoadTopStores+"?start_letter=";
                volleyJsonObjectRequest(url);*/


                toolbartitle.setText("Top Stores/All");
            }
        });
        btnTopStoreA = (Button) findViewById(R.id.btnTopStoreA);
        btnTopStoreA.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("A").execute();
/*
                String url=urlLoadTopStores+"?start_letter=A";
                volleyJsonObjectRequest(url);
*/

                toolbartitle.setText("Top Stores/A");
            }
        });
        btnTopStoreB = (Button) findViewById(R.id.btnTopStoreB);
        btnTopStoreB.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("B").execute();
/*
                String url=urlLoadTopStores+"?start_letter=B";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/B");
            }
        });

        btnTopStoreC = (Button) findViewById(R.id.btnTopStoreC);
        btnTopStoreC.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("C").execute();
/*
                String url=urlLoadTopStores+"?start_letter=C";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/C");
            }
        });
        btnTopStoreD = (Button) findViewById(R.id.btnTopStoreD);
        btnTopStoreD.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("D").execute();
/*
                String url=urlLoadTopStores+"?start_letter=D";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/D");
            }
        });
        btnTopStoreE = (Button) findViewById(R.id.btnTopStoreE);
        btnTopStoreE.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("E").execute();
/*
                String url=urlLoadTopStores+"?start_letter=E";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/E");
            }
        });
        btnTopStoreF = (Button) findViewById(R.id.btnTopStoreF);
        btnTopStoreF.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("F").execute();
/*
                String url=urlLoadTopStores+"?start_letter=F";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/F");
            }
        });
        btnTopStoreG = (Button) findViewById(R.id.btnTopStoreG);
        btnTopStoreG.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("G").execute();
/*
                String url=urlLoadTopStores+"?start_letter=G";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/G");
            }
        });
        btnTopStoreH = (Button) findViewById(R.id.btnTopStoreH);
        btnTopStoreH.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("H").execute();
/*
                String url=urlLoadTopStores+"?start_letter=H";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/H");
            }
        });
        btnTopStoreI = (Button) findViewById(R.id.btnTopStoreI);
        btnTopStoreI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("I").execute();
/*
                String url=urlLoadTopStores+"?start_letter=I";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/I");
            }
        });
        btnTopStoreJ = (Button) findViewById(R.id.btnTopStoreJ);
        btnTopStoreJ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("J").execute();
/*
                String url=urlLoadTopStores+"?start_letter=J";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/J");
            }
        });
        btnTopStoreK = (Button) findViewById(R.id.btnTopStoreK);
        btnTopStoreK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("K").execute();
/*
                String url=urlLoadTopStores+"?start_letter=K";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/K");
            }
        });
        btnTopStoreL = (Button) findViewById(R.id.btnTopStoreL);
        btnTopStoreL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("L").execute();
/*
                String url=urlLoadTopStores+"load_top_store.php"+"?start_letter=L";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/L");
            }
        });
        btnTopStoreM = (Button) findViewById(R.id.btnTopStoreM);
        btnTopStoreM.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("M").execute();
/*
                String url=urlLoadTopStores+"load_top_store.php"+"?start_letter=M";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/M");
            }
        });
        btnTopStoreN = (Button) findViewById(R.id.btnTopStoreN);
        btnTopStoreN.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("N").execute();
/*
                String url=urlLoadTopStores+"?start_letter=N";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/N");
            }
        });
        btnTopStoreO = (Button) findViewById(R.id.btnTopStoreO);
        btnTopStoreO.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("O").execute();
/*
                String url=urlLoadTopStores+"?start_letter=O";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/O");
            }
        });
        btnTopStoreP = (Button) findViewById(R.id.btnTopStoreP);
        btnTopStoreP.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("P").execute();
/*
                String url=urlLoadTopStores+"?start_letter=P";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/P");
            }
        });
        btnTopStoreQ = (Button) findViewById(R.id.btnTopStoreQ);
        btnTopStoreQ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("Q").execute();
/*
                String url=urlLoadTopStores+"?start_letter=Q";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/Q");
            }
        });
        btnTopStoreR = (Button) findViewById(R.id.btnTopStoreR);
        btnTopStoreR.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("R").execute();
/*
                String url=urlLoadTopStores+"?start_letter=R";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/R");
            }
        });
        btnTopStoreS = (Button) findViewById(R.id.btnTopStoreS);
        btnTopStoreS.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("S").execute();
/*
                String url=urlLoadTopStores+"?start_letter=S";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/S");
            }
        });
        btnTopStoreT = (Button) findViewById(R.id.btnTopStoreT);
        btnTopStoreT.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("T").execute();
/*
                String url=urlLoadTopStores+"?start_letter=T";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/T");
            }
        });
        btnTopStoreU = (Button) findViewById(R.id.btnTopStoreU);
        btnTopStoreU.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("U").execute();
/*
                String url=urlLoadTopStores+"?start_letter=U";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/U");
            }
        });
        btnTopStoreV = (Button) findViewById(R.id.btnTopStoreV);
        btnTopStoreV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("V").execute();
/*
                String url=urlLoadTopStores+"?start_letter=V";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/V");
            }
        });
        btnTopStoreW = (Button) findViewById(R.id.btnTopStoreW);
        btnTopStoreW.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("W").execute();
/*
                String url=urlLoadTopStores+"?start_letter=W";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/W");
            }
        });
        btnTopStoreX = (Button) findViewById(R.id.btnTopStoreX);
        btnTopStoreX.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("X").execute();
/*
                String url=urlLoadTopStores+"?start_letter=X";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/X");
            }
        });
        btnTopStoreY = (Button) findViewById(R.id.btnTopStoreY);
        btnTopStoreY.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("Y").execute();
/*
                String url=urlLoadTopStores+"?start_letter=Y";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/Y");
            }
        });
        btnTopStoreZ = (Button) findViewById(R.id.btnTopStoreZ);
        btnTopStoreZ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadTopCashBack("Z").execute();
/*
                String url=urlLoadTopStores+"?start_letter=Z";
                volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Top Stores/Z");
            }
        });


        storesgrid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
              /*  start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;*/
                int topRowVerticalPosition = (storesgrid == null || storesgrid.getChildCount() == 0) ? 0 : storesgrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                /*
				 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                // Log.e("totalItemCount", totalcount+"");


            }

        });
        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new LoadTopCashBack("").execute();
/*

                String url=urlLoadTopStores+"?start_letter=";
                volleyJsonObjectRequest(url);
*/
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        new LoadTopCashBack("").execute();
/*
        String url=urlLoadTopStores+"?start_letter=";
        volleyJsonObjectRequest(url);
*/

		/*searchedittext.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
	//			Toast.makeText(getApplicationContext(), searchedittext.getText().toString(), 5000).show();
				Intent intent=new Intent(TopStoresList.this,CouponList.class);
				intent.putExtra("storeName",searchedittext.getText().toString());
				searchedittext.setText("");
				startActivity(intent);			
			}
		});*/
    }


    public void volleyJsonObjectRequest(String url) {
        productList = new ArrayList<ProductData>();
        productList.clear();

        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);


                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {

                            JSONArray array = jsonObject.getJSONArray("top_stores");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object2 = array.getJSONObject(i);
                                ProductData data = new ProductData();
                                // data.setProId(object1.getString(""));
                                data.setProName(object2.getString("name"));
                                data.setProCashbackType(object2.getString("cashback_type"));
                                data.setProType(object2.getString("type"));
                                data.setProCashbackAmount(object2.getString("amount_percentage"));
                                data.setProImagePath(object2.getString("img_logo"));
                                data.setProCoupons(object2.getString("coupons"));
                                productList.add(data);


                            }

                            topStoresAdapter = new TopStoresAdapter(TopStoresList.this, productList);
                            storesgrid.setAdapter(topStoresAdapter);

						/*for(int j=0;j<productList.size();j++){
							//String names[j]=productList.get(j).getProName();
							names.add(productList.get(j).getProName());
							//Log.e("Length is :",productList.get(j).getProName());
						}
						pronames=names.toArray(new String[]{});
						Log.e("Length is :",""+pronames.length);
						ArrayAdapter<String> adapter=new ArrayAdapter<String>(TopStoresList.this, android.R.layout.simple_list_item_1, pronames);
						searchedittext.setAdapter(adapter);
*/
                            //		fillAllLayout();

                        } else {
                            Toast.makeText(TopStoresList.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(TopStoresList.this, "Something goes wrong..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

	/*public void fillAllLayout() {
		

		int numOfTyms = productList.size() / numOfCount;
		int remaing = productList.size() % numOfCount;

		int c = 0;
		for (int i = 0; i < numOfTyms; i++) {
			LinearLayout layout = new LinearLayout(TopStoresList.this);
			LayoutParams layoutParams = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layout.setLayoutParams(layoutParams);
			layout.setOrientation(LinearLayout.HORIZONTAL);
			
			layout.setGravity(Gravity.CENTER);
			for (int j = 0; j < numOfCount; j++) {
				c = c + 1;
				fillProductsLayout(productList.get(c - 1), layout);
			}
			linearStoreList.addView(layout);
		}

		if (remaing > 0) {
			LinearLayout layout = new LinearLayout(TopStoresList.this);
			LayoutParams layoutParams = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			
			layout.setGravity(Gravity.CENTER);
			
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setLayoutParams(layoutParams);
			
			
			for (int j = 0; j < remaing; j++) {
				c = c + 1;
				fillProductsLayout1(productList.get(c - 1), layout);
			}
			linearStoreList.addView(layout);
		}

	}

	public void fillProductsLayout(final ProductData data, LinearLayout layout) {

		View view = getLayoutInflater().inflate(R.layout.top_cashback_detail,
				null);
		
		Button btnTopCashBackViewStore=(Button)view.findViewById(R.id.btnTopCashBackViewStore);
		btnTopCashBackViewStore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(TopStoresList.this,CouponList.class);
				intent.putExtra("storeName",data.getProName());
				startActivity(intent);				
			}
		});
		

		TextView txtCashbackTypeLogo = (TextView) view
				.findViewById(R.id.txtCashbackTypeLogo);
		
		ImageView imgTopCashBackLogo = (ImageView) view
				.findViewById(R.id.imgTopCashBackLogo);
		
		TextView txtTopCashBackName = (TextView) view
				.findViewById(R.id.txtTopCashBackName);
		
		TextView txtTopCashBackDescription = (TextView) view
				.findViewById(R.id.txtTopCashBackDescription);
		
		txtTopCashBackName.setText(data.getProName());

		String details="Earn ";
		if (data.getProType().equals("Flat")) {
			details = "Earn Up to Rs." + data.getProCashbackAmount() + " cashback ";
		} else {
			details = "Earn Up to " + data.getProCashbackAmount() + "% cashback ";
		}
		if (Integer.parseInt(data.getProCoupons().trim()) > 0) {
			details = details + "& " + Integer.parseInt(data.getProCoupons())
					+ " coupons";
		}
		details=details+" From Ullasita";
		txtTopCashBackDescription.setText(details);
		loader.DisplayImage(data.getProImagePath(), imgTopCashBackLogo);
		
		
		if(data.getProCashbackType().equals("featured")){
			txtCashbackTypeLogo.setText("FEATURE");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#ED0C0C"));
		}else if(data.getProCashbackType().equals("offers")){
			txtCashbackTypeLogo.setText("OFFER");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#125DDE"));
		}else{
			txtCashbackTypeLogo.setText("THIS WEEK");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#a5d16c"));
		}
		
		
		
		
		layout.addView(view);
	}
	
	
	public void fillProductsLayout1(final ProductData data, LinearLayout layout1) {

		View view = getLayoutInflater().inflate(R.layout.activity_store_detail_linear,
				null);
		
		
		
		
		Button btnTopCashBackViewStore=(Button)view.findViewById(R.id.btnTopCashBackViewStore);
		btnTopCashBackViewStore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(TopStoresList.this,CouponList.class);
				intent.putExtra("storeName",data.getProName());
				startActivity(intent);				
			}
		});
		

		TextView txtCashbackTypeLogo = (TextView) view
				.findViewById(R.id.txtCashbackTypeLogo);
		
		ImageView imgTopCashBackLogo1 = (ImageView) view
				.findViewById(R.id.imgTopCashBackLogo);
		
		TextView txtTopCashBackName1 = (TextView) view
				.findViewById(R.id.txtTopCashBackName);
		
		TextView txtTopCashBackDescription1 = (TextView) view
				.findViewById(R.id.txtTopCashBackDescription);
		
		txtTopCashBackName1.setText(data.getProName());

		String details="Earn ";
		if (data.getProType().equals("Flat")) {
			details = "Earn Up to Rs." + data.getProCashbackAmount() + " cashback ";
		} else {
			details = "Earn Up to " + data.getProCashbackAmount() + "% cashback ";
		}
		if (Integer.parseInt(data.getProCoupons().trim()) > 0) {
			details = details + "& " + Integer.parseInt(data.getProCoupons())
					+ " coupons";
		}
		details=details+" From Ullasita";
		txtTopCashBackDescription1.setText(details);
		loader.DisplayImage(data.getProImagePath(), imgTopCashBackLogo1);
		
		
		if(data.getProCashbackType().equals("featured")){
			txtCashbackTypeLogo.setText("FEATURE");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#ED0C0C"));
		}else if(data.getProCashbackType().equals("offers")){
			txtCashbackTypeLogo.setText("OFFER");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#125DDE"));
		}else{
			txtCashbackTypeLogo.setText("THIS WEEK");
			txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#a5d16c"));
		}
		
		
		
		
		layout1.addView(view);
	}*/

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadTopCashBack extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        String type;

        public LoadTopCashBack(String type) {
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(TopStoresList.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("start_letter", type));
            object = parser.makeHttpRequest(TopStoresList.this, urlLoadTopStores, "GET",
                    param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            productList = new ArrayList<ProductData>();
            productList.clear();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {

                        JSONArray array = object
                                .getJSONArray("top_stores");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object2 = array.getJSONObject(i);
                            ProductData data = new ProductData();
                            // data.setProId(object1.getString(""));
                            data.setProName(object2.getString("name"));
                            data.setProCashbackType(object2.getString("cashback_type"));
                            data.setProType(object2.getString("type"));
                            data.setProCashbackAmount(object2.getString("amount_percentage"));
                            data.setProImagePath(object2.getString("img_logo"));
                            data.setProCoupons(object2.getString("coupons"));
                            productList.add(data);


                        }

                        topStoresAdapter = new TopStoresAdapter(TopStoresList.this, productList);
                        storesgrid.setAdapter(topStoresAdapter);

						/*for(int j=0;j<productList.size();j++){
							//String names[j]=productList.get(j).getProName();
							names.add(productList.get(j).getProName());
							//Log.e("Length is :",productList.get(j).getProName());
						}
						pronames=names.toArray(new String[]{});
						Log.e("Length is :",""+pronames.length);
						ArrayAdapter<String> adapter=new ArrayAdapter<String>(TopStoresList.this, android.R.layout.simple_list_item_1, pronames);
						searchedittext.setAdapter(adapter);
*/
                        //		fillAllLayout();

                    } else {
                        Toast.makeText(TopStoresList.this,
                                "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(TopStoresList.this,
                        "Something goes wrong..", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }

    }
}
