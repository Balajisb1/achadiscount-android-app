package com.application.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by devel-73 on 6/7/17.
 */

public class CouponGoToStore extends Activity {

    private WebView couponredirectweb;
    private String couponId="";
    private String storeId="";
    private RequestQueue queue;
    private SessionManager manager;
    private SharedPreferences preferences;
    private Context mContext;
    private String urlRedirect= DBConnection.BASEURL+"visit_shop";
    ProgressDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_gotostore);
        initVariables();
        couponredirectweb=(WebView)findViewById(R.id.couponredirectweb);
        couponredirectweb.getSettings().setJavaScriptEnabled(true);
        couponredirectweb.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        couponredirectweb.getSettings().setJavaScriptEnabled(true);
        couponredirectweb.getSettings().setUseWideViewPort(true);
        couponredirectweb.getSettings().setLoadWithOverviewMode(true);
        couponredirectweb.getSettings().setLoadsImagesAutomatically(true);
        couponredirectweb.setHorizontalScrollBarEnabled(false);
        couponredirectweb.setVerticalScrollBarEnabled(true);
        couponredirectweb.getSettings().setCacheMode(
                android.webkit.WebSettings.LOAD_DEFAULT);
        couponredirectweb.getSettings().setUseWideViewPort(true);
        couponredirectweb.getSettings().setPluginState(WebSettings.PluginState.ON);
        couponredirectweb.getSettings().setDomStorageEnabled(true);
        //socialwebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        couponredirectweb.setWebViewClient(new myWebClient());
        loadRedirectURL();
    }

    private void initVariables() {
        mContext=CouponGoToStore.this;
        manager=new SessionManager(mContext);
        queue= Volley.newRequestQueue(mContext);
        preferences=getSharedPreferences(SessionManager.PREF_NAME,SessionManager.PRIVATE_MODE);
        try{
            couponId=getIntent().getStringExtra("coupon_id");
            storeId=getIntent().getStringExtra("store_id");
        }
        catch (Exception e){
            Log.e("Exception is",e.toString());
        }
    }

    private void loadRedirectURL() {
        dialog=ProgressDialog.show(mContext,"","Redirecting",true,false);
        String urlval=urlRedirect+"?storeid="+storeId+"&couponid="+couponId+"&user_id="+preferences.getString(SessionManager.KEY_USERID,"");
        StringRequest request=new StringRequest(Request.Method.GET, urlval, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String redirectUrl=jsonObject.getString("redirect_url");
                    couponredirectweb.loadUrl(redirectUrl);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }


    public class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //     view.loadUrl(url);

            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            dialog.dismiss();
            //            Toast.makeText(SocialActivity.this, url, 5000).show();
            super.onPageFinished(view, url);
        }


        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl) {
            try {
                webView.stopLoading();
            } catch (Exception e) {
            }
            try {
                webView.clearView();
            } catch (Exception e) {
            }
            if (webView.canGoBack()) {
                webView.goBack();
            }
            webView.loadUrl("about:blank");
          /*  AlertDialog alertDialog = new Builder(SocialActivity.this).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Please Check Your Internet Connection..!");


            alertDialog.setButton("Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(getIntent());
                }
            });
            alertDialog.show();
            super.onReceivedError(webView, errorCode, description, failingUrl);*/

        }


    }


    @Override
    public void onBackPressed() {
        if (couponredirectweb.canGoBack()) {
            couponredirectweb.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
