package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CountryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccountSetting extends Activity {

    TextView txtAccountSettingEmail;

    EditText edtAccountSettingFirstName, edtAccountSettingLastName,
            edtAccountSettingStreet, edtAccountSettingCity,
            edtAccountSettingState, edtAccountSettingZipCode,
            edtAccountSettingCountry, edtAccountSettingContactNo;

    Button btnAccountSettingSubmit;
    SessionManager manager;
    List<CountryData> countryDatas;

    JSONParser parser;
    int countryid;
    LinearLayout countrylinear;
    Dialog dialog;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    private String urlUpdateProfile = DBConnection.BASEURL + "update_profile.php";
    private String urlViewProfile = DBConnection.BASEURL + "view_profile.php";
    private String urlLoadAllCountries = DBConnection.BASEURL + "load_all_countries.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acc_setting);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Account Settings");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        manager = new SessionManager(AccountSetting.this);
        dialog = new Dialog(AccountSetting.this);
        dialog.setContentView(R.layout.activity_country_text);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        txtAccountSettingEmail = (TextView) findViewById(R.id.txtAccountSettingEmail);

        edtAccountSettingFirstName = (EditText) findViewById(R.id.edtAccountSettingFirstName);
        edtAccountSettingLastName = (EditText) findViewById(R.id.edtAccountSettingLastName);
        edtAccountSettingStreet = (EditText) findViewById(R.id.edtAccountSettingStreet);
        edtAccountSettingCity = (EditText) findViewById(R.id.edtAccountSettingCity);
        edtAccountSettingState = (EditText) findViewById(R.id.edtAccountSettingState);
        edtAccountSettingZipCode = (EditText) findViewById(R.id.edtAccountSettingZipCode);
        edtAccountSettingCountry = (EditText) findViewById(R.id.edtAccountSettingCountry);
        edtAccountSettingContactNo = (EditText) findViewById(R.id.edtAccountSettingContactNo);

        btnAccountSettingSubmit = (Button) findViewById(R.id.btnAccountSettingSubmit);
        btnAccountSettingSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateComponents()) {
                    //new UpdateProfile().execute();

                    String firstName = edtAccountSettingFirstName.getText().toString().trim().replace(" ", "%20");
                    String lastName = edtAccountSettingLastName.getText().toString().trim();
                    String street = edtAccountSettingStreet.getText().toString().trim();
                    String city = edtAccountSettingCity.getText().toString().trim();
                    String state = edtAccountSettingState.getText().toString().trim();
                    String zipCode = edtAccountSettingZipCode.getText().toString().trim();
                    String contactNo = edtAccountSettingContactNo.getText().toString().trim();

				/*	List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("first_name",firstName));
					param.add(new BasicNameValuePair("last_name",lastName));
					param.add(new BasicNameValuePair("street",street));
					param.add(new BasicNameValuePair("city", city));
					param.add(new BasicNameValuePair("state",state));
					param.add(new BasicNameValuePair("zip_code",zipCode));
					param.add(new BasicNameValuePair("country",countryid+""));
					param.add(new BasicNameValuePair("contact_no",contactNo));
					param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
					object = parser.makeHttpRequest(urlUpdateProfile, "GET", param);
					*/

                    String url = urlUpdateProfile + "?first_name=" + firstName + "&last_name=" + lastName
                            + "&street=" + street + "&city=" + city + "&state=" + state + "&zip_code=" + zipCode + "&country=" + countryid + "&contact_no=" + contactNo +
                            "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "0");
                    volleyJsonObjectRequest(url);


                }
            }
        });

        edtAccountSettingCountry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.setTitle("Select Country");
                dialog.show();
            }
        });

//new LoadAllCountries().execute();

		/*List<NameValuePair> param=new ArrayList<NameValuePair>();
        JSONObject object=parser.makeHttpRequest(urlLoadAllCountries, "GET", param);*/


        String url = urlLoadAllCountries;
        volleyJsonObjectRequest1(url);

        String url1 = urlViewProfile + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "");
        volleyJsonObjectRequest2(url1);


    }

    public void volleyJsonObjectRequest1(String url) {


        Log.i("URL", url);

        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject object = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {

                        if (object.getInt("success") == 1) {

                            JSONArray jsonArray = object.getJSONArray("countries");
                            countryDatas = new ArrayList<CountryData>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                CountryData data = new CountryData();
                                data.setId(jsonObject.getInt("id"));
                                data.setName(jsonObject.getString("name"));
                                countryDatas.add(data);
                            }
                            fillAllCountries(countryDatas);


                        } else {
                            Toast.makeText(AccountSetting.this, "Failed to load all countries", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);
    }


    public void volleyJsonObjectRequest(String url) {
        Log.i("URL", url);
        progressDialog = new ProgressDialog(AccountSetting.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(AccountSetting.this,
                                    "Updated Successfully..", Toast.LENGTH_LONG)
                                    .show();
                        } else {
                            Toast.makeText(AccountSetting.this,
                                    "Updating Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AccountSetting.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();

/*
        List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
		Log.e("params", param.toString());


		object = parser.makeHttpRequest(urlViewProfile, "GET", param)*/
        ;
//		String url=urlViewProfile+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
//		volleyJsonObjectRequest2(url);
    }

    public void volleyJsonObjectRequest2(String url) {

        final ProgressDialog dialog = new ProgressDialog(AccountSetting.this);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONObject object2 = jsonObject.getJSONObject("profile_details");
                            if (object2.getString("first_name") == null || object2.getString("first_name") == "null") {
                                edtAccountSettingFirstName.setText("");
                            } else {
                                edtAccountSettingFirstName.setText(object2.getString("first_name"));
                            }

                            if (object2.getString("last_name") == null || object2.getString("last_name") == "null") {
                                edtAccountSettingLastName.setText("");
                            } else {
                                edtAccountSettingLastName.setText(object2.getString("last_name"));
                            }


                            if (object2.getString("street") == null || object2.getString("street") == "null") {
                                edtAccountSettingStreet.setText("");
                            } else {
                                edtAccountSettingStreet.setText(object2.getString("street"));
                            }


                            if (object2.getString("city") == null || object2.getString("city") == "null") {
                                edtAccountSettingCity.setText("");
                            } else {
                                edtAccountSettingCity.setText(object2.getString("city"));
                            }


                            if (object2.getString("state") == null || object2.getString("state") == "null") {
                                edtAccountSettingState.setText("");
                            } else {
                                edtAccountSettingState.setText(object2.getString("state"));
                            }


                            if (object2.getString("zipcode") == null || object2.getString("zipcode") == "null") {
                                edtAccountSettingZipCode.setText("");
                            } else {
                                edtAccountSettingZipCode.setText(object2.getString("zipcode"));
                            }

						/*if(object2.getString("country")==null||object2.getString("country")=="null"){
                            edtAccountSettingCountry.setText("");
						}else{
							edtAccountSettingCountry.setText(object2.getString("country"));
						}*/

                            if (object2.getString("country") == null || object2.getString("country") == "null") {
                                edtAccountSettingCountry.setText("");
                            } else {
                                edtAccountSettingCountry.setText(object2.getString("country"));
                                countryid = object2.getInt("countryid");
                            }

                            if (object2.getString("contact_no") == null || object2.getString("contact_no") == "null") {
                                edtAccountSettingContactNo.setText("");
                            } else {
                                edtAccountSettingContactNo.setText(object2.getString("contact_no"));
                            }

                            txtAccountSettingEmail.setText(object2.getString("email"));
                        } else {
                            Toast.makeText(AccountSetting.this,
                                    "Loading Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AccountSetting.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
			/*case R.id.action_home:
				Intent intent=new Intent(AccountSetting.this,NavigationActivity.class);
				startActivity(intent);
				finish();
				break;
			case R.id.action_compare:
				Intent intent2=new Intent(AccountSetting.this,ProductDetailsActivity.class);
				intent2.putExtra("parent_child_id", preferences.getString(SessionManager.KEY_COMP_ID, "20003"));
				startActivity(intent2);
				break;

			case R.id.action_coupons:
				Intent intent1=new Intent(AccountSetting.this,CouponNavigation.class);
				startActivity(intent1);
				break;
			case R.id.action_offline:
				Intent intent3=new Intent(AccountSetting.this,OfflineHomeNavigation.class);
				startActivity(intent3);
				break;
			case R.id.logout:
				manager.logoutUser();
				invalidateOptionsMenu();
				break;
			case R.id.myaccount:
				if(manager.checkLogin()){
				Intent intent5=new Intent(AccountSetting.this,AccountTitle.class);
				startActivity(intent5);
				}
				break;
*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validateComponents() {

        if (edtAccountSettingFirstName.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for FirstName..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingLastName.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for LastName..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingStreet.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for Street..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingCity.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for City..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingState.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for State..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingZipCode.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for ZipCode..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingCountry.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for Country..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAccountSettingContactNo.getText().toString().trim().length() <= 0) {
            Toast.makeText(AccountSetting.this, "Please fill value for ContactNo..", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }

    }

    public void fillAllCountries(final List<CountryData> countryDatas) {

        countrylinear = (LinearLayout) dialog.findViewById(R.id.countrylinear);
        for (int i = 0; i < countryDatas.size(); i++) {
            final int m = i;
            View view = getLayoutInflater().inflate(R.layout.activity_country_name_text, null);
            TextView countryname = (TextView) view.findViewById(R.id.countryname);
            countryname.setText(countryDatas.get(i).getName());
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    countryid = countryDatas.get(m).getId();
                    edtAccountSettingCountry.setText(countryDatas.get(m).getName());
                    dialog.dismiss();
                }
            });
            countrylinear.addView(view);
        }


    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }
	
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main,menu);
		String userid = preferences
				.getString(SessionManager.KEY_USERID, "");
		if (userid == null || userid == "") {
			MenuItem menuItem=menu.findItem(R.id.logout);
			menuItem.setVisible(false);
		}
		//invalidateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}*/

    class LoadAllCountries extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(AccountSetting.this, "", "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            JSONObject object = parser.makeHttpRequest(AccountSetting.this, urlLoadAllCountries, "GET", param);
            return object.toString();
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {

                        JSONArray jsonArray = object.getJSONArray("countries");
                        countryDatas = new ArrayList<CountryData>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            CountryData data = new CountryData();
                            data.setId(jsonObject.getInt("id"));
                            data.setName(jsonObject.getString("name"));
                            countryDatas.add(data);
                        }
                        fillAllCountries(countryDatas);

                    } else {
                        Toast.makeText(AccountSetting.this, "Failed to load all countries", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    Log.e("Exc", e.toString());
                }
            }
        }
    }

    class UpdateProfile extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        private String firstName = edtAccountSettingFirstName.getText().toString().trim().replace(" ", "%20");
        private String lastName = edtAccountSettingLastName.getText().toString().trim();
        private String street = edtAccountSettingStreet.getText().toString().trim();
        private String city = edtAccountSettingCity.getText().toString().trim();
        private String state = edtAccountSettingState.getText().toString().trim();
        private String zipCode = edtAccountSettingZipCode.getText().toString().trim();
        private String contactNo = edtAccountSettingContactNo.getText().toString().trim();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AccountSetting.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("first_name", firstName));
            param.add(new BasicNameValuePair("last_name", lastName));
            param.add(new BasicNameValuePair("street", street));
            param.add(new BasicNameValuePair("city", city));
            param.add(new BasicNameValuePair("state", state));
            param.add(new BasicNameValuePair("zip_code", zipCode));
            param.add(new BasicNameValuePair("country", countryid + ""));
            param.add(new BasicNameValuePair("contact_no", contactNo));
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(AccountSetting.this, urlUpdateProfile, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        Toast.makeText(AccountSetting.this,
                                "Updated Successfully..", Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(AccountSetting.this,
                                "Updating Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(AccountSetting.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    class LoadProfileDetails extends AsyncTask<String, String, String> {


        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            Log.e("params", param.toString());


            object = parser.makeHttpRequest(AccountSetting.this, urlViewProfile, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        JSONObject object2 = object1.getJSONObject("profile_details");
                        if (object2.getString("first_name") == null || object2.getString("first_name") == "null") {
                            edtAccountSettingFirstName.setText("");
                        } else {
                            edtAccountSettingFirstName.setText(object2.getString("first_name"));
                        }

                        if (object2.getString("last_name") == null || object2.getString("last_name") == "null") {
                            edtAccountSettingLastName.setText("");
                        } else {
                            edtAccountSettingLastName.setText(object2.getString("last_name"));
                        }


                        if (object2.getString("street") == null || object2.getString("street") == "null") {
                            edtAccountSettingStreet.setText("");
                        } else {
                            edtAccountSettingStreet.setText(object2.getString("street"));
                        }


                        if (object2.getString("city") == null || object2.getString("city") == "null") {
                            edtAccountSettingCity.setText("");
                        } else {
                            edtAccountSettingCity.setText(object2.getString("city"));
                        }


                        if (object2.getString("state") == null || object2.getString("state") == "null") {
                            edtAccountSettingState.setText("");
                        } else {
                            edtAccountSettingState.setText(object2.getString("state"));
                        }


                        if (object2.getString("zipcode") == null || object2.getString("zipcode") == "null") {
                            edtAccountSettingZipCode.setText("");
                        } else {
                            edtAccountSettingZipCode.setText(object2.getString("zipcode"));
                        }

						/*if(object2.getString("country")==null||object2.getString("country")=="null"){
							edtAccountSettingCountry.setText("");
						}else{
							edtAccountSettingCountry.setText(object2.getString("country"));
						}*/

                        if (object2.getString("country") == null || object2.getString("country") == "null") {
                            edtAccountSettingCountry.setText("");
                        } else {
                            edtAccountSettingCountry.setText(object2.getString("country"));
                            countryid = object2.getInt("countryid");
                        }

                        if (object2.getString("contact_no") == null || object2.getString("contact_no") == "null") {
                            edtAccountSettingContactNo.setText("");
                        } else {
                            edtAccountSettingContactNo.setText(object2.getString("contact_no"));
                        }

                        txtAccountSettingEmail.setText(object2.getString("email"));

                    } else {
                        Toast.makeText(AccountSetting.this,
                                "Loading Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(AccountSetting.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }


}
