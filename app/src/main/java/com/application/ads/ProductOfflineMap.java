package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.MyApplication;
import com.application.ads.data.Stores;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class ProductOfflineMap extends Activity {

    double mylongitude, mylatitude;
    int rangeinkm;
    TextView toname;
    SharedPreferences preferences;
    MyApplication myApplication;
    double latitude, longitude;
    private GoogleMap googleMap;
//	List<Stores> storeslist;

    public static double distanceBetweenUsers(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts decimal degrees to radians						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offlinemap);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Product Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        toname = (TextView) findViewById(R.id.toname);
        toname.setText(getIntent().getStringExtra("name").toString());
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        myApplication = MyApplication.getInstance();

        latitude = Double.parseDouble(getIntent().getStringExtra("latitude"));
        longitude = Double.parseDouble(getIntent().getStringExtra("longitude"));
        //storeslist=getIntent().getParcelableArrayListExtra("storeslist");
        initilizeMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    private void initilizeMap() {

        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            //	calculateDistance(storeslist);

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(ProductOfflineMap.this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        googleMap.clear();
        mylatitude = Double.parseDouble(myApplication.getMyLatitude());
        mylongitude = Double.parseDouble(myApplication.getMyLongitude());
        MarkerOptions marker1 = new MarkerOptions().position(new LatLng(mylatitude, mylongitude)).title("ME");
        marker1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_RED));
        googleMap.addMarker(marker1);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mylatitude,
                        mylongitude)).zoom(10).build();


        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));


        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title(getIntent().getStringExtra("name") + "," + getIntent().getStringExtra("address"));
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        googleMap.addMarker(marker);


    }

    private void calculateDistance(List<Stores> storeslist) {


        for (int i = 0; i < storeslist.size(); i++) {
            Log.e("user_lat", storeslist.get(i).getLatitude() + "");
            Log.e("user_longi", storeslist.get(i).getLongtitude() + "");
        }
        rangeinkm = 10;

        try {

            googleMap.clear();
            mylatitude = Double.parseDouble(preferences.getString(SessionManager.KEY_LATITUDE, "0.0"));
            mylongitude = Double.parseDouble(preferences.getString(SessionManager.KEY_LONGITUDE, "0.0"));
            MarkerOptions marker1 = new MarkerOptions().position(new LatLng(mylatitude, mylongitude)).title("ME");
            marker1.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));
            googleMap.addMarker(marker1);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mylatitude,
                            mylongitude)).zoom(15).build();


            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            for (int i = 0; i < storeslist.size(); i++) {
                double distance = distanceBetweenUsers(mylatitude, mylongitude, Double.parseDouble(storeslist.get(i).getLatitude()), Double.parseDouble(storeslist.get(i).getLongtitude()), "K");
                if (distance <= rangeinkm) {
                    MarkerOptions marker = new MarkerOptions().position(
                            new LatLng(Double.parseDouble(storeslist.get(i).getLatitude()), Double.parseDouble(storeslist.get(i).getLongtitude()))).title(storeslist.get(i).getAddress());
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                    googleMap.addMarker(marker);
                    //		googleMap.addPolyline(new PolylineOptions().geodesic(true).add(new LatLng(mylatitude, mylongitude)).add(new LatLng(Double.parseDouble(storeslist.get(i).getLatitude()), Double.parseDouble(storeslist.get(i).getLongtitude()))));
                }
                System.out.println(distance + " Kilometers\n");
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }


    }


}
