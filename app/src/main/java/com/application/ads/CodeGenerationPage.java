package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

public class CodeGenerationPage extends Activity {

    TextView txtProductCode;

    ImageView imgProductLogo;
    ProgressBar codegenprogress;
    long Delay = 10000;

    TextView txtShopDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_generate);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Coupons");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        /*getActionBar().setTitle(getIntent().getStringExtra("name"));*/
        getActionBar().setTitle("");

        imgProductLogo = (ImageView) findViewById(R.id.imgProductLogo);
        txtShopDetails = (TextView) findViewById(R.id.txtShopDetails);
        txtProductCode = (TextView) findViewById(R.id.txtProductCode);
        codegenprogress = (ProgressBar) findViewById(R.id.codegenprogress);
        codegenprogress.setVisibility(View.VISIBLE);
        txtShopDetails.setText("Shop Normally At " + getIntent().getStringExtra("name") + ". Your Cashback Will Automatically Get Added In Your All Discount Sale Account Within 72 Hours.");
        String code = getIntent().getStringExtra("code");
        if (code.trim().length() > 0) {
            txtProductCode.setText(code);

        } else {
            txtProductCode.setText("Your Deal is Acivated");
            //	codegenprogress.setVisibility(View.INVISIBLE);
        }


        Timer RunSplash = new Timer();
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                Intent myIntent = new Intent(CodeGenerationPage.this,
                        OfferPage.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        codegenprogress.setVisibility(View.INVISIBLE);
                    }
                });
                myIntent.putExtra("offer_page", getIntent().getStringExtra("offer_page"));
                startActivity(myIntent);

            }
        };

        RunSplash.schedule(ShowSplash, Delay);
        Picasso.with(this).load(getIntent().getStringExtra("logo")).placeholder(R.drawable.adslogo).resize(144, 0).into(imgProductLogo);

        txtProductCode.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
