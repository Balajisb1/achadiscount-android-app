package com.application.ads;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductGotoStore extends Activity {

    public static String urlRedirectPage = DBConnection.BASEURL + "visit_product";
    JSONParser parser;
    ProgressBar progressBar;
    SharedPreferences preferences;
    SessionManager manager;
    WebView productwebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Product's Store");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        productwebview = (WebView) findViewById(R.id.productwebview);


        //Improve webview performance
        WebSettings mywebSettings = productwebview.getSettings();
        mywebSettings.setJavaScriptEnabled(true);
        productwebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        productwebview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        productwebview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        productwebview.getSettings().setAppCacheEnabled(true);
        productwebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mywebSettings.setDomStorageEnabled(true);
        mywebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mywebSettings.setUseWideViewPort(true);
        mywebSettings.setSavePassword(true);
        mywebSettings.setSaveFormData(true);
        mywebSettings.setEnableSmoothTransition(true);

       // productwebview.setWebViewClient(new myWebClient());
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        manager = new SessionManager(ProductGotoStore.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);

        new ExecuteProductDetails().execute();

        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        Log.e("user_id", userid);
    /*    if (userid == null || userid == "") {
            userid = "ADS230000";
        } else {
            int user_id = Integer.parseInt(userid);
            userid = "ADS23" + String.format("%04d", user_id);
        }*/

	/*	List<NameValuePair> param = new ArrayList<NameValuePair>();

		param.add(new BasicNameValuePair("user_id", userid));
		param.add(new BasicNameValuePair("product_id", getIntent()
				.getStringExtra("product_id")));
		param.add(new BasicNameValuePair("store_id", getIntent()
				.getStringExtra("store_id")));
		param.add(new BasicNameValuePair("pp_id", getIntent()
				.getStringExtra("pp_id")));*/

        String url = urlRedirectPage + "?user_id=" + userid + "&product_id=" + getIntent().getStringExtra("product_id")
                + "&store_id=" + getIntent().getStringExtra("store_id") + "&pp_id=" + getIntent().getStringExtra("pp_id");

        //volleyJsonObjectRequest(url);
    }

    public void volleyJsonObjectRequest(String url) {


        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.

                        Log.i("response", response);
                        String redirecturl;
                        if (response.trim().length() > 0) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getInt("success") == 1) {
                                    redirecturl = object.getString("redirect_url");
                                    Log.i("redirect url", redirecturl);
                                    //productwebview.loadDataWithBaseURL(redirecturl, "", "text/html", "utf-8", null);
                                    //	productwebview.setWebViewClient(new myWebClient());

                                    productwebview.loadUrl(redirecturl);
                                    finish();

                                }

                            } catch (Exception e) {
                                Log.e("error", e + "");
                            }

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//				mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

	/*	Log.i("URL",url);
        final RequestQueue queue = Volley.newRequestQueue(this);
		CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
			@Override
			public void onResponse(NetworkResponse response) {
				try {
					final String jsonString = new String(response.data,HttpHeaderParser.parseCharset(response.headers));


					Log.i("jsonString", jsonString);
					JSONObject jsonObject = new JSONObject(jsonString);
					String redirecturl;

					if (jsonString.toString().trim().length() > 0) {

						if (jsonObject.getInt("success") == 1) {
							redirecturl = jsonObject.getString("redirect_url");
							Log.i("redirect url", redirecturl);
							//productwebview.loadDataWithBaseURL(redirecturl, "", "text/html", "utf-8", null);
							//	productwebview.setWebViewClient(new myWebClient());

							productwebview.loadUrl(redirecturl);
							finish();

						}
					}
					else
					{

					}


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
				} catch (UnsupportedEncodingException | JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				//Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
			}
		});
		queue.add(cacheRequest);*/

    }

	/*@Override
	protected void onResume() {
		super.onResume();
//		new ExecuteProductDetails().execute();

//		Intent intent=new Intent(ProductGotoStore.this,ProductSoloDetailsWithCompare.class);
//		startActivity(intent);

	}
	@Override
	public void onBackPressed() {
		// your code.
//		new ExecuteProductDetails().execute();

		Intent intent=new Intent(ProductGotoStore.this,ProductSoloDetailsWithCompare.class);
		startActivity(intent);
		finish();
	}*/

    public void viewInBrowser(Context context, String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        Log.e("ViewBrowser", url);
        if (null != intent.resolveActivity(context.getPackageManager())) {
            Log.e("ViewBrowser=======", url);
//			context.startActivity(intent);
            productwebview.loadUrl(url);
        }

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteProductDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(ProductGotoStore.this, "",
                    "Loading...", true, false);
        }

        @Override
        protected String doInBackground(String... params) {

            String userid = preferences.getString(SessionManager.KEY_USERID, "");
            Log.e("user_id", userid);
         /*   if (userid == null || userid == "") {
                userid = "ADS230000";
            } else {
                int user_id = Integer.parseInt(userid);
                userid = "ADS23" + String.format("%04d", user_id);
            }*/

            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("user_id", userid));
            param.add(new BasicNameValuePair("productid", getIntent()
                    .getStringExtra("product_id")));
            param.add(new BasicNameValuePair("pp_id", getIntent()
                    .getStringExtra("pp_id")));
            Log.e("param", param.toString());
            JSONObject object = parser.makeHttpRequest(ProductGotoStore.this, urlRedirectPage, "GET",
                    param);
            // Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            String redirecturl;
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        redirecturl = object.getString("redirect_url");

                        Log.e("redirect url", redirecturl.trim().replace(" ", "%20").replace("'", " "));
                        productwebview.loadUrl(redirecturl.trim().replace(" ", "%20").replace("'", " "));
						finish();

					/*	try {
						*//*	Intent viewIntent =	new Intent("android.intent.action.VIEW",Uri.parse(redirecturl.trim().replace(" ","%20").replace("'","")));
							startActivity(viewIntent);*//*

                            productwebview.loadUrl(redirecturl);


						} catch (android.content.ActivityNotFoundException anfe) {
                            productwebview.loadUrl(redirecturl);
//							viewInBrowser(ProductGotoStore.this,redirecturl.trim().replace(" ","%20").replace("'",""));
						}*/

                    }

                } catch (Exception e) {
                    Log.e("error", e + "");
                }

            }

        }
    }

    public class myWebClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.e("Webview Error", "" + error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }


        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl) {
            try {
                webView.stopLoading();
            } catch (Exception e) {
            }
            try {
                webView.clearView();
            } catch (Exception e) {
            }
            if (webView.canGoBack()) {
                webView.goBack();
            }
            webView.loadUrl("about:blank");
            AlertDialog alertDialog = new AlertDialog.Builder(ProductGotoStore.this).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Please Check Your Internet Connection..!");


            alertDialog.setButton("Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(getIntent());
                }
            });
            alertDialog.show();
            super.onReceivedError(webView, errorCode, description, failingUrl);
        }
    }
}
