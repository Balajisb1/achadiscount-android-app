package com.application.ads.data;

public class SubCategoryData {

    private String subcat_id;
    private String subcat_name;
    private String subcat_img;
    private String catId;
    private String catName;

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getSubcat_name() {
        return subcat_name;
    }

    public void setSubcat_name(String subcat_name) {
        this.subcat_name = subcat_name;
    }

    public String getSubcat_img() {
        return subcat_img;
    }

    public void setSubcat_img(String subcat_img) {
        this.subcat_img = subcat_img;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }


}
