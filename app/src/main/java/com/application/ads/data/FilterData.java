package com.application.ads.data;

import java.util.ArrayList;
import java.util.List;

public class FilterData {

    private String filterHeadingId;
    private String filterHeadingName;
    private ArrayList<OptionData> options;

    private String brandid;
    private String brandname;
    private String brandimage;
    private String branddesc;
    private String brandlogo;
    private String categoryname;
    private String categoryid;
    private String categoryimage;
    private String categoryicon;
    private boolean selected;

    private List<FilterData> filterBrandDatas;
    private List<FilterData> filterCategoryDatas;

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getBrandimage() {
        return brandimage;
    }

    public void setBrandimage(String brandimage) {
        this.brandimage = brandimage;
    }

    public String getBranddesc() {
        return branddesc;
    }

    public void setBranddesc(String branddesc) {
        this.branddesc = branddesc;
    }

    public String getBrandlogo() {
        return brandlogo;
    }

    public void setBrandlogo(String brandlogo) {
        this.brandlogo = brandlogo;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryimage() {
        return categoryimage;
    }

    public void setCategoryimage(String categoryimage) {
        this.categoryimage = categoryimage;
    }

    public String getCategoryicon() {
        return categoryicon;
    }

    public void setCategoryicon(String categoryicon) {
        this.categoryicon = categoryicon;
    }

    public String getFilterHeadingId() {
        return filterHeadingId;
    }

    public void setFilterHeadingId(String filterHeadingId) {
        this.filterHeadingId = filterHeadingId;
    }

    public String getFilterHeadingName() {
        return filterHeadingName;
    }

    public void setFilterHeadingName(String filterHeadingName) {
        this.filterHeadingName = filterHeadingName;
    }

    public ArrayList<OptionData> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionData> options) {
        this.options = options;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<FilterData> getFilterCategoryDatas() {
        return filterCategoryDatas;
    }

    public void setFilterCategoryDatas(List<FilterData> filterCategoryDatas) {
        this.filterCategoryDatas = filterCategoryDatas;
    }

    public List<FilterData> getFilterBrandDatas() {
        return filterBrandDatas;
    }

    public void setFilterBrandDatas(List<FilterData> filterBrandDatas) {
        this.filterBrandDatas = filterBrandDatas;
    }

}
