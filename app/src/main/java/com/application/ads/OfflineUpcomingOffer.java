package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.OfflineStoreAdapter;
import com.application.ads.adapter.OfflineStoreListviewAdapter;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class OfflineUpcomingOffer extends Activity {

    public static String urlLoadMainCategories = "https://achadiscount.in/offline/admin/index.php/json/getupcommingoffer";
    SessionManager manager;
    SharedPreferences preferences;
    ListView offlinestorelist;
    GridView offlinestoregrid;
    JSONParser parser;
    List<OfflineStoreData> categorydata;
    OfflineStoreAdapter adapter;
    OfflineStoreListviewAdapter listviewAdapter;
    LinearLayout linearGridList, linearMap, linearmylocation;
    TextView gridlisttext, mapgridtext, mylocationtext;
    RelativeLayout maprelative;
    double distorderby[];
    Double mylatitude, mylongitude;
    LatLng latLng;
    LinearLayout storedetailslinear, linearOFflineFilter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    /* For google map */
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_upcoming_stores);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Upcoming Offers");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        offlinestorelist = (ListView) findViewById(R.id.offlinestorelist);
        offlinestoregrid = (GridView) findViewById(R.id.offlinestoregrid);

        linearGridList = (LinearLayout) findViewById(R.id.linearGridList);
        linearMap = (LinearLayout) findViewById(R.id.linearMap);
        linearmylocation = (LinearLayout) findViewById(R.id.linearmylocation);
        linearOFflineFilter = (LinearLayout) findViewById(R.id.linearOFflineFilter);
        maprelative = (RelativeLayout) findViewById(R.id.maprelative);
        gridlisttext = (TextView) findViewById(R.id.gridlisttext);
        mapgridtext = (TextView) findViewById(R.id.mapgridtext);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                String url = urlLoadMainCategories + "?category=" + "&brand=" + "&lat=" + preferences.getString(SessionManager.KEY_LATITUDE, "0.0") + "" + "&long=" + preferences.getString(SessionManager.KEY_LONGITUDE, "0.0");
                volleyJsonObjectRequest(url);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        categorydata = new ArrayList<OfflineStoreData>();


        linearOFflineFilter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(OfflineUpcomingOffer.this, OfflineFilterActivity.class);
                startActivity(intent);
            }
        });


        linearmylocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OfflineUpcomingOffer.this, OfflineMap.class);
                startActivity(intent);
            }
        });

        storedetailslinear = (LinearLayout) findViewById(R.id.storedetailslinear);
        parser = new JSONParser();
        manager = new SessionManager(OfflineUpcomingOffer.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);

        linearGridList.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                    gridlisttext.setText("List");
                    if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {
                        mapgridtext.setText("Map");
                    }
                    offlinestoregrid.setVisibility(View.VISIBLE);
                    offlinestorelist.setVisibility(View.GONE);
                    maprelative.setVisibility(View.GONE);
                } else {
                    gridlisttext.setText("Grid");
                    if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {
                        mapgridtext.setText("Map");
                    }
                    offlinestoregrid.setVisibility(View.GONE);
                    offlinestorelist.setVisibility(View.VISIBLE);
                    maprelative.setVisibility(View.GONE);
                }

            }
        });


//	new ExecuteLoadMainCategories().execute();

	/*	List<NameValuePair> param=new ArrayList<NameValuePair>();
        //?category=&brand=&lat=9.9252007&long=78.11977539999998
		param.add(new BasicNameValuePair("category", ""));
		param.add(new BasicNameValuePair("brand", ""));
		param.add(new BasicNameValuePair("lat", preferences.getString(SessionManager.KEY_LATITUDE, "0.0")));
		param.add(new BasicNameValuePair("long", preferences.getString(SessionManager.KEY_LONGITUDE, "0.0")));
		//JSONObject jsonObject=parser.getJSONFromUrl(urlLoadMainCategories);*/

        String url = urlLoadMainCategories + "?category=" + "&brand=" + "&lat=" + preferences.getString(SessionManager.KEY_LATITUDE, "0.0") + "" + "&long=" + preferences.getString(SessionManager.KEY_LONGITUDE, "0.0");
        volleyJsonObjectRequest(url);

        linearMap.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {
                    mapgridtext.setText("Map");
                    if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                        gridlisttext.setText("List");
                    }

                    offlinestoregrid.setVisibility(View.VISIBLE);
                    maprelative.setVisibility(View.GONE);
                    offlinestorelist.setVisibility(View.GONE);

                } else {
                    mapgridtext.setText("Grid");
                    if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                        gridlisttext.setText("List");
                    }
                    //gridlisttext.setText("Grid");
                    offlinestoregrid.setVisibility(View.GONE);
                    maprelative.setVisibility(View.VISIBLE);
                    offlinestorelist.setVisibility(View.GONE);
                    initilizeMap(categorydata);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    googleMap.getUiSettings().setCompassEnabled(true);
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    googleMap.getUiSettings().setZoomGesturesEnabled(true);
                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    //googleMap.set
                }


            }
        });


        offlinestoregrid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(OfflineUpcomingOffer.this, OfflineOfferDetails.class);
                intent.putExtra("id", categorydata.get(position).getBrand());
                intent.putExtra("desc", categorydata.get(position).getDescription());
                startActivity(intent);

            }
        });


        offlinestorelist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(OfflineUpcomingOffer.this, OfflineOfferDetails.class);
                intent.putExtra("id", categorydata.get(position).getBrand());
                intent.putExtra("desc", categorydata.get(position).getDescription());
                startActivity(intent);

            }
        });

        //initilizeMap();
    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray jsonArray = jsonObject.getJSONArray("queryresult");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);
                            OfflineStoreData data = new OfflineStoreData();
                            data.setId(object1.getString("id"));
                            data.setImage(object1.getString("image"));
                            data.setBrand(object1.getString("brand"));
                            data.setOffer(object1.getString("offer"));
                            data.setDescription(object1.getString("description"));
                            data.setBrandname(object1.getString("brandname"));
                            data.setMaplat(object1.getDouble("maplat"));
                            data.setMaplong(object1.getDouble("maplong"));
                            data.setDist(object1.getDouble("dist"));
                            categorydata.add(data);
                        }
                    } else {
                        Toast.makeText(OfflineUpcomingOffer.this, "No data found", Toast.LENGTH_SHORT).show();
                    }
                    adapter = new OfflineStoreAdapter(OfflineUpcomingOffer.this, categorydata);
                    listviewAdapter = new OfflineStoreListviewAdapter(OfflineUpcomingOffer.this, categorydata);
                    offlinestorelist.setAdapter(listviewAdapter);
                    offlinestoregrid.setAdapter(adapter);

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void initilizeMap(List<OfflineStoreData> categorydata) {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(OfflineUpcomingOffer.this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        googleMap.clear();
        mylatitude = Double.parseDouble(preferences.getString(
                SessionManager.KEY_LATITUDE, "0.0"));
        mylongitude = Double.parseDouble(preferences.getString(
                SessionManager.KEY_LONGITUDE, "0.0"));
        MarkerOptions marker1 = new MarkerOptions().position(
                new LatLng(mylatitude, mylongitude)).title("ME");
        marker1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_RED));
        googleMap.addMarker(marker1);
        googleMap.addCircle(new CircleOptions().fillColor(Color.parseColor("#20ff0000")).radius(10000).center(new LatLng(mylatitude, mylongitude)).strokeColor(Color.TRANSPARENT));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mylatitude, mylongitude)).zoom(12).build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        placeAllStores(categorydata);


    }

    public void placeAllStores(final List<OfflineStoreData> categorydata) {

/*		for(int i=0;i<categorydata.size();i++){
			distorderby[i]=categorydata.get(i).getDist();
		}
		List<OfflineStoreData> templist=new ArrayList<OfflineStoreData>();
*/
        Log.e("dist", categorydata.size() + "");

        for (int i = 0; i < categorydata.size(); i++) {
            final int m = i;
            Log.e("dist", categorydata.get(i).getDist() + "");
            storedetailslinear.removeAllViews();
            if (categorydata.get(i).getDist() <= 10) {
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(categorydata.get(i).getMaplat(),
                                categorydata.get(i).getMaplong())).title(categorydata.get(i).getBrandname());
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                googleMap.addMarker(marker);

                View view = getLayoutInflater().inflate(R.layout.activity_offline_categories, null);
                ImageView categoryimg = (ImageView) view.findViewById(R.id.categoryimg);
                TextView categoryname = (TextView) view.findViewById(R.id.categoryname);
                Picasso.with(OfflineUpcomingOffer.this).load(DBConnection.OFFLINE_EXTRAIMG + categorydata.get(i).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);

                categoryname.setText(categorydata.get(i).getBrandname());
                view.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OfflineUpcomingOffer.this, OfflineOfferDetails.class);
                        intent.putExtra("id", categorydata.get(m).getBrand());
                        startActivity(intent);
                    }
                });

                storedetailslinear.addView(view);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteLoadMainCategories extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineUpcomingOffer.this, "", "Loading..", true, false);

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            //?category=&brand=&lat=9.9252007&long=78.11977539999998
            param.add(new BasicNameValuePair("category", ""));
            param.add(new BasicNameValuePair("brand", ""));
            param.add(new BasicNameValuePair("lat", preferences.getString(SessionManager.KEY_LATITUDE, "0.0")));
            param.add(new BasicNameValuePair("long", preferences.getString(SessionManager.KEY_LONGITUDE, "0.0")));
            //JSONObject jsonObject=parser.getJSONFromUrl(urlLoadMainCategories);

            JSONObject object = parser.makeHttpRequest(OfflineUpcomingOffer.this, urlLoadMainCategories, "GET", param);
            return object.toString();


        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();

            try {
							/*	JSONArray array=new JSONArray(result);
								for (int i = 0; i < array.length(); i++) {
								        JSONObject object1 = array.getJSONObject(i);
									StoreData data=new StoreData();
									data.setStoreId(object1.getString("id"));
									data.setName(object1.getString("name"));
									data.setImgLogo(object1.getString("image"));
									categorydata.add(data);
								}
*/

                JSONObject jsonObject = new JSONObject(result);


                JSONArray jsonArray = jsonObject.getJSONArray("queryresult");
                if (jsonArray.length() > 0) {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object1 = jsonArray.getJSONObject(i);
                        OfflineStoreData data = new OfflineStoreData();
                        data.setId(object1.getString("id"));
                        data.setImage(object1.getString("image"));
                        data.setBrand(object1.getString("brand"));
                        data.setOffer(object1.getString("offer"));
                        data.setDescription(object1.getString("description"));
                        data.setBrandname(object1.getString("brandname"));
                        data.setMaplat(object1.getDouble("maplat"));
                        data.setMaplong(object1.getDouble("maplong"));
                        data.setDist(object1.getDouble("dist"));
                        categorydata.add(data);
                    }
                } else {
                    Toast.makeText(OfflineUpcomingOffer.this, "No data found", Toast.LENGTH_SHORT).show();
                }
                adapter = new OfflineStoreAdapter(OfflineUpcomingOffer.this, categorydata);
                listviewAdapter = new OfflineStoreListviewAdapter(OfflineUpcomingOffer.this, categorydata);
                offlinestorelist.setAdapter(listviewAdapter);
                offlinestoregrid.setAdapter(adapter);


            } catch (Exception e) {
                // TODO: handle exception
                Log.e("offline exception", e.toString());
            }
        }
    }


}
