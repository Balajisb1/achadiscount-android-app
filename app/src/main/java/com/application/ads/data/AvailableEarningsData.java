package com.application.ads.data;

public class AvailableEarningsData {

    private String totalEarnings;
    private String cashBackEarnings;
    private String paidEarnings;
    private String availableEarnings;
    private String withdrawEarnings;
    private String refferalEarnings;

    public String getTotalEarnings() {
        return totalEarnings;
    }

    public void setTotalEarnings(String totalEarnings) {
        this.totalEarnings = totalEarnings;
    }

    public String getCashBackEarnings() {
        return cashBackEarnings;
    }

    public void setCashBackEarnings(String cashBackEarnings) {
        this.cashBackEarnings = cashBackEarnings;
    }

    public String getPaidEarnings() {
        return paidEarnings;
    }

    public void setPaidEarnings(String paidEarnings) {
        this.paidEarnings = paidEarnings;
    }

    public String getAvailableEarnings() {
        return availableEarnings;
    }

    public void setAvailableEarnings(String availableEarnings) {
        this.availableEarnings = availableEarnings;
    }

    public String getWithdrawEarnings() {
        return withdrawEarnings;
    }

    public void setWithdrawEarnings(String withdrawEarnings) {
        this.withdrawEarnings = withdrawEarnings;
    }

    public String getRefferalEarnings() {
        return refferalEarnings;
    }

    public void setRefferalEarnings(String refferalEarnings) {
        this.refferalEarnings = refferalEarnings;
    }


}