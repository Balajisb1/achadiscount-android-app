package com.application.ads.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.CouponGoToStore;
import com.application.ads.LoginActivity;
import com.application.ads.R;
import com.application.ads.data.CouponsData;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by devel-73 on 6/7/17.
 */

public class CouponsAdapter extends BaseAdapter {

    private Context mContext;
    private List<CouponsData> dataList;
    private TextView txtNewCouponStoreName, txtNewCouponStoreOffer;
    private Button btnNewCouponShowCode, btnAbtStore;
    private ImageView imgNewCouponCouponImage;
    private SessionManager manager;

    public CouponsAdapter(Context mContext, List<CouponsData> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
        manager = new SessionManager(mContext);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.coupon_details, null, false);
        txtNewCouponStoreName = (TextView) view.findViewById(R.id.txtNewCouponStoreName);
        txtNewCouponStoreOffer = (TextView) view.findViewById(R.id.txtNewCouponStoreOffer);
        btnNewCouponShowCode = (Button) view.findViewById(R.id.btnNewCouponShowCode);
        btnAbtStore = (Button) view.findViewById(R.id.btnAbtStore);
        imgNewCouponCouponImage = (ImageView) view.findViewById(R.id.imgNewCouponCouponImage);

        txtNewCouponStoreName.setText(dataList.get(position).getOfferName());
        txtNewCouponStoreOffer.setText(dataList.get(position).getTitle());
        if (dataList.get(position).getOfferType().trim().equalsIgnoreCase("Promotion")) {
            btnNewCouponShowCode.setText("Activate Deal");
        } else {
            btnNewCouponShowCode.setText("Show Code");
        }
        btnAbtStore.setText("About " + dataList.get(position).getOfferName());

        Picasso.with(mContext).load("https://achadiscount.in/uploads/affiliates/" + dataList.get(position).getAffiliateImg()).placeholder(R.drawable.logo).into(imgNewCouponCouponImage);
        btnNewCouponShowCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (manager.isLoggedIn()) {
                    Intent intent = new Intent(mContext, CouponGoToStore.class);
                    intent.putExtra("coupon_id", "" + dataList.get(position).getCouponId());
                    intent.putExtra("store_id", "" + dataList.get(position).getAffiliateId());
                    mContext.startActivity(intent);
                }else{
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("coupon_id", "" + dataList.get(position).getCouponId());
                    intent.putExtra("store_id", "" + dataList.get(position).getAffiliateId());
                    mContext.startActivity(intent);
                }
            }
        });

        btnAbtStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(dataList.get(position).getAffiliateDesc())) {
                    showDescriptionDialog(dataList.get(position).getOfferName(), dataList.get(position).getAffiliateDesc());
                }else{
                    showDescriptionDialog(dataList.get(position).getOfferName(), "No description avail for this store");
                }
            }
        });

        return view;
    }

    private void showDescriptionDialog(String offerName, String affiliateDesc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("About " + offerName);
        builder.setMessage(Html.fromHtml(affiliateDesc));
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
