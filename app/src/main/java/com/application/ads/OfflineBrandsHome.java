package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.OfflineBrandAdapter;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class OfflineBrandsHome extends Activity {

    public static String urlOfflineBrands = "https://achadiscount.in/offline/admin/index.php/json/getbrandlist";
    JSONParser parser;
    SessionManager manager;
    SharedPreferences preferences;
    List<OfflineStoreData> offlineStoreDatas;
    ListView offlinebrandslist;
    OfflineBrandAdapter brandAdapter;
    //pageno=1&maxrow=10
    int pagenum = 1;
    int maxrow = 10;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_brands);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        offlinebrandslist = (ListView) findViewById(R.id.offlinebrandslist);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Brands");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");


        offlineStoreDatas = new ArrayList<OfflineStoreData>();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        manager = new SessionManager(OfflineBrandsHome.this);
        parser = new JSONParser();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        offlinebrandslist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(OfflineBrandsHome.this, OfflineBrandDetails.class);
                intent.putExtra("id", offlineStoreDatas.get(position).getId());
                startActivity(intent);
            }
        });


        brandAdapter = new OfflineBrandAdapter(offlineStoreDatas, OfflineBrandsHome.this);
        offlinebrandslist.setAdapter(brandAdapter);


        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
//                new ExecuteBrandDetails(pagenum, maxrow).execute();

                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("pageno", pagenum + ""));
                param.add(new BasicNameValuePair("maxrow", maxrow + ""));


                String url = urlOfflineBrands + "?pageno=" + pagenum + "&maxrow=" + maxrow;

                volleyJsonObjectRequest(url);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        offlinebrandslist.setOnScrollListener(new OnScrollListener() {
            int start;
            int visible;
            int totalItemCount;


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
            /*	Log.e("Totoal", totalItemCount+"");
                Log.e("firstvisible", firstVisibleItem+"");
				Log.e("visiblecount", visibleItemCount+"");*/

                this.totalItemCount = totalItemCount;
                this.start = firstVisibleItem;
                this.visible = visibleItemCount;

                int topRowVerticalPosition = (offlinebrandslist == null || offlinebrandslist.getChildCount() == 0) ? 0 : offlinebrandslist.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == SCROLL_STATE_IDLE && start + visible == totalItemCount) {
                    //		Toast.makeText(getApplicationContext(), "End of page reaced", 5000).show();
                    pagenum++;
                    maxrow = maxrow + totalItemCount;
//                    new ExecuteBrandDetails(pagenum, maxrow).execute();
                    String url = urlOfflineBrands + "?pageno=" + pagenum + "&maxrow=" + maxrow;

                    volleyJsonObjectRequest(url);
                }
            }
        });


//        new ExecuteBrandDetails(pagenum, maxrow).execute();
        String url = urlOfflineBrands + "?pageno=" + pagenum + "&maxrow=" + maxrow;

        volleyJsonObjectRequest(url);

    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);


                    JSONArray array = jsonObject.getJSONArray("queryresult");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object1 = array.getJSONObject(i);

                        OfflineStoreData data = new OfflineStoreData();
                        data.setId(object1.getString("id"));
                        data.setImage(object1.getString("image"));
                        data.setDescription(object1.getString("description"));
                        data.setBrandname(object1.getString("name"));
                        offlineStoreDatas.add(data);
                    }
                    brandAdapter.notifyDataSetChanged();

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteBrandDetails extends AsyncTask<String, String, String> {


        ProgressDialog dialog;
        private int pagenum, maxrow;

        ExecuteBrandDetails(int pagenum, int maxresults) {
            this.pagenum = pagenum;
            this.maxrow = maxresults;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineBrandsHome.this, "", "Loading brands..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("pageno", pagenum + ""));
            param.add(new BasicNameValuePair("maxrow", maxrow + ""));
            JSONObject object = parser.makeHttpRequest(OfflineBrandsHome.this, urlOfflineBrands, "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                JSONObject object = new JSONObject(result);

                JSONArray array = object.getJSONArray("queryresult");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = array.getJSONObject(i);

                    OfflineStoreData data = new OfflineStoreData();
                    data.setId(object1.getString("id"));
                    data.setImage(object1.getString("image"));
                    data.setDescription(object1.getString("description"));
                    data.setBrandname(object1.getString("name"));
                    offlineStoreDatas.add(data);
                }
                brandAdapter.notifyDataSetChanged();
            } catch (Exception exception) {
                Log.e("error", exception.toString());
            }

        }


    }


}
