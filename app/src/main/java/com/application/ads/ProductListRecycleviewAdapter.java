package com.application.ads;

/**
 * Created by devel-91 on 18/10/16.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CompareProducts;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.application.ads.MyWishListActivity.preferences;

public class ProductListRecycleviewAdapter extends RecyclerView.Adapter<ProductListRecycleViewHolder> {

    List<ProductDetails> productdetailslist;
    Context context;
    LayoutInflater inflater;
    SessionManager manager;
    private ProgressDialog dialog;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";


    public ProductListRecycleviewAdapter(Context context, List<ProductDetails> productdetailslist) {
        this.context = context;
        this.productdetailslist = productdetailslist;
        manager = new SessionManager(context);
    }


    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return (null != productdetailslist ? productdetailslist.size() : 0);
    }


    @Override
    public ProductListRecycleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // TODO Auto-generated method stub
        View v = LayoutInflater.from(context).inflate(R.layout.activity_product_grid_details, null);
        ProductListRecycleViewHolder mh = new ProductListRecycleViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final ProductListRecycleViewHolder viewHolder, int position) {

        final int m = position;
        viewHolder.prodcompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();  // Here we get the position that we have set for the checkbox using setTag.

                if (!CompareProducts.isAlreadyInCompare(productdetailslist.get(position))) {
                    if (CompareProducts.getProducts().size() >= 2) {
                        Toast.makeText(context, "Maximum compare 2 products", Toast.LENGTH_SHORT).show();
                    } else {
                        if (CompareProducts.addToCompare(productdetailslist.get(position))) {
                            viewHolder.prodcompare.setImageResource(R.drawable.added_compare);
                            productdetailslist.get(position).setSelected(true);
                        }
                    }
                } else {
                    if (CompareProducts.removeFromCompare(productdetailslist.get(position))) {
                        viewHolder.prodcompare.setImageResource(R.drawable.prod_compare);

                        productdetailslist.get(position).setSelected(false);
                    }
                }
            }
        });

        viewHolder.imgProductListingDetailFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                if (manager.checkLogin()) {
                    // Remove from wish list
                    if (Integer.parseInt(productdetailslist.get(pos)
                            .getProWishListStatus().trim()) > 0) {
                        addToWishList(productdetailslist.get(pos), 1,viewHolder);
                    } else {
                        addToWishList(productdetailslist.get(pos), 0,viewHolder);
                    }
                } else {
                    context.startActivity(new Intent(context,
                            LoginActivity.class));
                }

            }
        });


        viewHolder.prodcompare.setTag(position);
        viewHolder.imgProductListingDetailFavourites.setTag(position);

        if (productdetailslist.size() > 0) {


            if (productdetailslist.get(position).isSelected()) {
                for (int i = 0; i < CompareProducts.productdetails.size(); i++) {
                    if (CompareProducts.productdetails.get(i).getProduct_id().equalsIgnoreCase(productdetailslist.get(position).getProduct_id())) {
                        viewHolder.prodcompare
                                .setImageResource(R.drawable.added_compare);
                    } else {
                        viewHolder.prodcompare
                                .setImageResource(R.drawable.prod_compare);
                    }
                }
            } else {
                viewHolder.prodcompare
                        .setImageResource(R.drawable.prod_compare);
            }

            viewHolder.prodrating.setRating(Float.parseFloat(productdetailslist.get(position).getRating()));
            viewHolder.prodprice.setText("Rs." + productdetailslist.get(position).getProduct_price());
            viewHolder.prodname.setText(productdetailslist.get(position).getProduct_name());
        /*viewHolder.prodminprice.setText("Rs."
                + productdetailslist.get(position).getMrp());*/
            if (productdetailslist.get(position).getProWishListStatus().equals(null)
                    || productdetailslist.get(position).getProWishListStatus()
                    .equals("")) {
                Log.e("", "Null");
            } else {
                if (Integer.parseInt(productdetailslist.get(position)
                        .getProWishListStatus().trim()) > 0) {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.filled_heart);
                } else {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.heart);
                }

            }
        }

        viewHolder.prodimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });


        viewHolder.prodname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });

        if (productdetailslist.size() > 0) {
//            Log.i("Product Image", DBConnection.PRODUCT_IMGURL + productdetailslist.get(position).getProduct_image());

            Picasso.with(context).load(DBConnection.PRODUCT_IMGURL
                    + productdetailslist.get(m).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(viewHolder.prodimg);

        }

    }


    private void addToWishList(final ProductDetails productdetails, final int i, final ProductListRecycleViewHolder viewHolder) {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = urlAddOrRemoveProductsFromWishList + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0") + "&product_id=" + productdetails.getProduct_id() + "&wish_list_status=" + i;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {

                            if (i == 0) {
                                Toast.makeText(context, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
                                productdetailslist.clear();

                                productdetails.setProWishListStatus("1");
                                productdetailslist.add(productdetails);
                            } else if (i == 1) {
                                Toast.makeText(context, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
                            }

                        } else {
                            Toast.makeText(context,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }
}





