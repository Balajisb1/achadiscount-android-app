package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.application.ads.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class BankPayment extends Activity {


    EditText edtBankDetailAccountHolder, edtBankDetailBankName,
            edtBankDetailBranchName, edtBankDetailAccountNumber,
            edtBankDetailIFSCCode, edtBankDetailPasswordConfirm;

    Button btnAccountSettingSubmit;

    JSONParser parser;
    SharedPreferences preferences;
    SessionManager manager;
    private String urlUpdateBankDetail = DBConnection.BASEURL + "update_bankdetails.php";
    private String urlGetBankDetail = DBConnection.BASEURL + "get_bank_details.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_details);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Bank Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        manager = new SessionManager(BankPayment.this);

        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        edtBankDetailAccountHolder = (EditText) findViewById(R.id.edtBankDetailAccountHolder);
        edtBankDetailBankName = (EditText) findViewById(R.id.edtBankDetailBankName);
        edtBankDetailBranchName = (EditText) findViewById(R.id.edtBankDetailBranchName);
        edtBankDetailAccountNumber = (EditText) findViewById(R.id.edtBankDetailAccountNumber);
        edtBankDetailIFSCCode = (EditText) findViewById(R.id.edtBankDetailIFSCCode);
        edtBankDetailPasswordConfirm = (EditText) findViewById(R.id.edtBankDetailPasswordConfirm);
        btnAccountSettingSubmit = (Button) findViewById(R.id.btnAccountSettingSubmit);
        btnAccountSettingSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateComponents()) {
                    new UpdateBankDetails().execute();


                    String accntHolder = edtBankDetailAccountHolder.getText().toString().trim();
                    String bankName = edtBankDetailBankName.getText().toString().trim();
                    String branchName = edtBankDetailBranchName
                            .getText().toString().trim();
                    String accntNumber = edtBankDetailAccountNumber
                            .getText().toString().trim();
                    String ifscCode = edtBankDetailIFSCCode
                            .getText().toString().trim();
        /*			List<NameValuePair> param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair("account_holder",accntHoldezzr));
					param.add(new BasicNameValuePair("bank_name",bankName));
					param.add(new BasicNameValuePair("branch_name", branchName));
					param.add(new BasicNameValuePair("account_number",accntNum_ber ));
					param.add(new BasicNameValuePair("ifsc_code", ifscCode));
					param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
					object = parser.makeHttpRequest(urlUpdateBankDetail, "GET", param);*/

/*
					String url=urlUpdateBankDetail+"?account_holder="+accntHolder+"&bank_name="+bankName
							+"&branch_name="+branchName+"&account_number="+accntNumber+"&ifsc_code="+ifscCode+"&user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
					volleyJsonObjectRequest(url);
*/

                }
            }
        });

        //new LoadBankDetails().execute();

	/*	List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));

		object = parser.makeHttpRequest(urlGetBankDetail, "GET", param);
*/
/*
		String url=urlGetBankDetail+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
		volleyJsonObjectRequest1(url);
*/

    }


    public void volleyJsonObjectRequest1(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONObject object2 = jsonObject.getJSONObject("bank_details");
                            edtBankDetailAccountHolder.setText(object2.getString("account_holder"));
                            edtBankDetailAccountNumber.setText(object2.getString("account_number"));
                            edtBankDetailBankName.setText(object2.getString("bank_name"));
                            edtBankDetailBranchName.setText(object2.getString("branch_name"));
                            edtBankDetailIFSCCode.setText(object2.getString("ifsc_code"));
                        } else {
                            Toast.makeText(BankPayment.this,
                                    "Loading Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(BankPayment.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(BankPayment.this,
                                    "Updated Successfully..", Toast.LENGTH_LONG)
                                    .show();
                        } else {
                            Toast.makeText(BankPayment.this,
                                    "Updating Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(BankPayment.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    /*	@Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.main,menu);
            String userid = preferences
                    .getString(SessionManager.KEY_USERID, "");
            if (userid == null || userid == "") {
                MenuItem menuItem=menu.findItem(R.id.logout);
                menuItem.setVisible(false);
            }
            //invalidateOptionsMenu();
            return super.onCreateOptionsMenu(menu);
        }
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validateComponents() {

        if (edtBankDetailAccountHolder.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for Account Holder..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtBankDetailAccountNumber.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for Account Number..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtBankDetailBankName.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for Bank Name..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtBankDetailBranchName.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for BranchName..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtBankDetailIFSCCode.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for IFSC Code..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtBankDetailPasswordConfirm.getText().toString().trim().length() <= 0) {
            Toast.makeText(BankPayment.this, "Please fill value for Password to Confirm..", Toast.LENGTH_LONG).show();
            return false;
        } else if (!Utils.encryptToMD5(edtBankDetailPasswordConfirm.getText().toString().trim()).equals(preferences.getString(SessionManager.KEY_PASSWORD, ""))) {
            Toast.makeText(BankPayment.this, "Password Doesn't match..", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadBankDetails().execute();
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class UpdateBankDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        private String accntHolder = edtBankDetailAccountHolder.getText().toString().trim();
        private String bankName = edtBankDetailBankName.getText().toString().trim();
        private String branchName = edtBankDetailBranchName
                .getText().toString().trim();
        private String accntNumber = edtBankDetailAccountNumber
                .getText().toString().trim();
        private String ifscCode = edtBankDetailIFSCCode
                .getText().toString().trim();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(BankPayment.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("account_holder", accntHolder));
            param.add(new BasicNameValuePair("bank_name", bankName));
            param.add(new BasicNameValuePair("branch_name", branchName));
            param.add(new BasicNameValuePair("account_number", accntNumber));
            param.add(new BasicNameValuePair("ifsc_code", ifscCode));
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(BankPayment.this, urlUpdateBankDetail, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        Toast.makeText(BankPayment.this,
                                "Updated Successfully..", Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(BankPayment.this,
                                "Updating Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(BankPayment.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    class LoadBankDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(BankPayment.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));

            object = parser.makeHttpRequest(BankPayment.this, urlGetBankDetail, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        JSONObject object2 = object1.getJSONObject("bank_details");
                        edtBankDetailAccountHolder.setText(object2.getString("account_holder"));
                        edtBankDetailAccountNumber.setText(object2.getString("account_number"));
                        edtBankDetailBankName.setText(object2.getString("bank_name"));
                        edtBankDetailBranchName.setText(object2.getString("branch_name"));
                        edtBankDetailIFSCCode.setText(object2.getString("ifsc_code"));
                    } else {
                        Toast.makeText(BankPayment.this,
                                "Loading Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(BankPayment.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }
}