package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.MyApplication;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import info.androidramp.gearload.Loading;

public class OfflineOfferDetails extends Activity {

    public static String urlLoadSingleOfferDetails = "https://achadiscount.in/offline/admin/index.php/json/getsinglebrandforapp";
    private final int interval = 3000; // 3 Second
    ImageView brandimage;
    TextView brandname;
    LinearLayout addresslinear, offerbannerlinear;
    JSONParser parser;
    SessionManager manager;
    SharedPreferences preferences;
    MyApplication myApplication;
    String storedesc;
    TextView txtlistdedicatedesc;
    Loading loading;
    List<OfflineStoreData> categorydata, storeaddress, offerbanner;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_dedicate);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Offer Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        parser = new JSONParser();
        storedesc = getIntent().getStringExtra("desc").toString();

        txtlistdedicatedesc = (TextView) findViewById(R.id.txtlistdedicatedesc);
        txtlistdedicatedesc.setText(storedesc);
        myApplication = MyApplication.getInstance();
        manager = new SessionManager(OfflineOfferDetails.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        brandimage = (ImageView) findViewById(R.id.brandimage);
        brandname = (TextView) findViewById(R.id.brandname);
        addresslinear = (LinearLayout) findViewById(R.id.addresslinear);
        offerbannerlinear = (LinearLayout) findViewById(R.id.offerbannerlinear);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

//        new ExecuteLoadAllAddress().execute();

      /*  List<NameValuePair> param = new ArrayList<NameValuePair>();
        Log.e("id is:", getIntent().getStringExtra("id") + " id");
        param.add(new BasicNameValuePair("id", getIntent().getStringExtra("id")));
        param.add(new BasicNameValuePair("lat", myApplication.getMyLatitude()));
        param.add(new BasicNameValuePair("long", myApplication.getMyLongitude()));*/

        String url = urlLoadSingleOfferDetails + "?id=" + getIntent().getStringExtra("id") + "&lat=" + myApplication.getMyLatitude()
                + "&long=" + myApplication.getMyLongitude();
        volleyJsonObjectRequest(url);

        //mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                String url = urlLoadSingleOfferDetails + "?id=" + getIntent().getStringExtra("id") + "&lat=" + myApplication.getMyLatitude()
                        + "&long=" + myApplication.getMyLongitude();
                volleyJsonObjectRequest(url);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        loading = (Loading) findViewById(R.id.loading);

        loading.Start();
        Runnable runnable = new Runnable() {
            public void run() {

//				pd.dismiss();
                loading.Cancel();


            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);
    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    storeaddress = new ArrayList<OfflineStoreData>();
                    categorydata = new ArrayList<OfflineStoreData>();
                    offerbanner = new ArrayList<OfflineStoreData>();

                    if (jsonString.toString().trim().length() > 0) {

                        JSONObject object1 = jsonObject.getJSONObject("brand");
                        OfflineStoreData data = new OfflineStoreData();
                        data.setId(object1.getString("id"));
                        data.setImage(object1.getString("logo"));
                        data.setOfferimage(object1.getString("image"));
                        data.setDescription(object1.getString("description"));
                        data.setBrandname(object1.getString("name"));
                        categorydata.add(data);


                        JSONArray jsonArray2 = jsonObject.getJSONArray("locations");
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            JSONObject object = jsonArray2.getJSONObject(j);
                            OfflineStoreData data2 = new OfflineStoreData();
                            data2.setLocationid(object.getString("id"));
                            data2.setLocationcity(object.getString("city"));
                            data2.setLocationlat(object.getString("lat"));
                            data2.setLocationlong(object.getString("long"));
                            data2.setLocationaddr(object.getString("address1"));
                            data2.setDist(object.getDouble("dist"));
                            storeaddress.add(data2);
                        }

                        JSONArray jsonArray3 = jsonObject.getJSONArray("offerbanner");
                        for (int j = 0; j < jsonArray3.length(); j++) {
                            JSONObject object = jsonArray3.getJSONObject(j);
                            OfflineStoreData data2 = new OfflineStoreData();
                            data2.setOfferbannerid(object.getString("id"));
                            data2.setOfferbannerbannername(object.getString("banner"));
                            data2.setOfferbannerbrand(object.getString("brand"));
                            data2.setOfferbannerdescription(object.getString("description"));
                            data2.setOfferbannerimage(object.getString("image"));
                            data2.setOfferbanneroffer(object.getString("offer"));
                            offerbanner.add(data2);
                        }


                        fillAllOfflineOfferBanner(offerbanner);

                        fillAllBrandDetails(categorydata, storeaddress);


                        //fillAllBrandDetails(categorydata,storeaddress);
                    } else {
                        Toast.makeText(OfflineOfferDetails.this, "No addresses found", Toast.LENGTH_SHORT).show();
                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void fillAllOfflineOfferBanner(List<OfflineStoreData> offlinebanner) {
        offerbannerlinear.removeAllViews();
        for (int i = 0; i < offlinebanner.size(); i++) {

            View view = getLayoutInflater().inflate(R.layout.activity_offline_offer_banner_image, null);

            ImageView categoryimg = (ImageView) view.findViewById(R.id.offlineofferbanner);
            TextView offername = (TextView) view.findViewById(R.id.offername);
            offername.setText(offlinebanner.get(i).getOfferbanneroffer() + "..");
            Picasso.with(OfflineOfferDetails.this).load(DBConnection.OFFLINE_EXTRAIMG + offlinebanner.get(i).getOfferbannerbannername()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);

            offerbannerlinear.addView(view);
        }
    }

    public void fillAllBrandDetails(final List<OfflineStoreData> categorydata, List<OfflineStoreData> storeaddress) {

        for (int i = 0; i < categorydata.size(); i++) {
            final int position = i;
            brandname.setText(categorydata.get(i).getBrandname());
            Picasso.with(OfflineOfferDetails.this).load(DBConnection.OFFLINE_EXTRAIMG + categorydata.get(i).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(brandimage);


            brandimage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OfflineOfferDetails.this, OfflineBrandDetails.class);
                    intent.putExtra("id", categorydata.get(position).getId());
                    startActivity(intent);
                }
            });

			/*loader.DisplayImage(DBConnection.OFFLINE_EXTRAIMG+categorydata.get(i).getOfferimage(), offerimage);*/
        }
        addresslinear.removeAllViews();
        for (int j = 0; j < storeaddress.size(); j++) {
            final int m = j;
            final List<OfflineStoreData> storeaddressdetails = storeaddress;
            if (storeaddress.get(j).getDist() <= 10) {
                View view = getLayoutInflater().inflate(R.layout.activity_offline_address_text, null);
                TextView offlineaddressname = (TextView) view.findViewById(R.id.offlineaddressname);
                offlineaddressname.setText(storeaddress.get(j).getLocationaddr().toString());
                offlineaddressname.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OfflineOfferDetails.this, OfflineOfferMap.class);
                        intent.putExtra("lat", storeaddressdetails.get(m).getLocationlat());
                        intent.putExtra("long", storeaddressdetails.get(m).getLocationlong());
                        intent.putExtra("address", storeaddressdetails.get(m).getLocationaddr());
                        intent.putExtra("city", storeaddressdetails.get(m).getLocationcity());
                        intent.putExtra("img_url", offerbanner.get(0).getOfferbannerbannername());
                        startActivity(intent);
                    }
                });
                addresslinear.addView(view);
            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteLoadAllAddress extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineOfferDetails.this, "", "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            Log.e("id is:", getIntent().getStringExtra("id") + " id");
            param.add(new BasicNameValuePair("id", getIntent().getStringExtra("id")));
            param.add(new BasicNameValuePair("lat", myApplication.getMyLatitude()));
            param.add(new BasicNameValuePair("long", myApplication.getMyLongitude()));
            /*JSONArray array=parser.makeHttpRequest1(urlLoadSingleOfferDetails, "GET", param);
            return array.toString();*/
            JSONObject object = parser.makeHttpRequest(OfflineOfferDetails.this, urlLoadSingleOfferDetails, "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            storeaddress = new ArrayList<OfflineStoreData>();
            categorydata = new ArrayList<OfflineStoreData>();
            offerbanner = new ArrayList<OfflineStoreData>();
            try {
                if (result.trim().length() > 0) {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject object1 = jsonObject.getJSONObject("brand");
                    OfflineStoreData data = new OfflineStoreData();
                    data.setId(object1.getString("id"));
                    data.setImage(object1.getString("logo"));
                    data.setOfferimage(object1.getString("image"));
                    data.setDescription(object1.getString("description"));
                    data.setBrandname(object1.getString("name"));
                    categorydata.add(data);


                    JSONArray jsonArray2 = jsonObject.getJSONArray("locations");
                    for (int j = 0; j < jsonArray2.length(); j++) {
                        JSONObject object = jsonArray2.getJSONObject(j);
                        OfflineStoreData data2 = new OfflineStoreData();
                        data2.setLocationid(object.getString("id"));
                        data2.setLocationcity(object.getString("city"));
                        data2.setLocationlat(object.getString("lat"));
                        data2.setLocationlong(object.getString("long"));
                        data2.setLocationaddr(object.getString("address1"));
                        data2.setDist(object.getDouble("dist"));
                        storeaddress.add(data2);
                    }

                    JSONArray jsonArray3 = jsonObject.getJSONArray("offerbanner");
                    for (int j = 0; j < jsonArray3.length(); j++) {
                        JSONObject object = jsonArray3.getJSONObject(j);
                        OfflineStoreData data2 = new OfflineStoreData();
                        data2.setOfferbannerid(object.getString("id"));
                        data2.setOfferbannerbannername(object.getString("banner"));
                        data2.setOfferbannerbrand(object.getString("brand"));
                        data2.setOfferbannerdescription(object.getString("description"));
                        data2.setOfferbannerimage(object.getString("image"));
                        data2.setOfferbanneroffer(object.getString("offer"));
                        offerbanner.add(data2);
                    }


                    fillAllOfflineOfferBanner(offerbanner);

                    fillAllBrandDetails(categorydata, storeaddress);


                    //fillAllBrandDetails(categorydata,storeaddress);
                } else {
                    Toast.makeText(OfflineOfferDetails.this, "No addresses found", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception exception) {
                Log.e("except", exception.toString());
            }
        }

    }


}
