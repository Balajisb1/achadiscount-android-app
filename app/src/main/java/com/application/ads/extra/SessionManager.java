package com.application.ads.extra;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.Toast;

import com.application.ads.LoginActivity;
import com.application.ads.data.MyApplication;
import com.application.ads.utils.FileCache;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SessionManager {
    public static final int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "Achadiscount";
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USERID = "userId";
    public static final String KEY_CONTACTNO = "contactno";
    public static final String KEY_USERNAME = "userName";
    public static final String KEY_EMAIL = "emailId";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_RANDOMCODE = "random_code";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_COMP_ID = "comp_id";
    public static final String KEY_TYPE = "type";
    SharedPreferences pref;
    Editor editor;
    Context _context;
    FileCache fileCache;
    MyApplication myApplication;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        myApplication = MyApplication.getInstance();

    }

    public void createCompareId(String id) {
        editor.putString(SessionManager.KEY_COMP_ID, id);
        editor.commit();
    }

	/*
     * public void initializelocation(String latitude, String longtitude) {
	 * editor.putString(KEY_LATITUDE, latitude); editor.putString(KEY_LONGITUDE,
	 * longtitude); editor.commit(); }
	 */

    public void createLoginSession(String userId, String email, String password, String userName, String randomcode, String contactno,String type) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERID, userId);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_USERNAME, userName);
        editor.putString(KEY_RANDOMCODE, randomcode);
        editor.putString(KEY_CONTACTNO, contactno);
        editor.putString(KEY_TYPE, type);
        editor.commit();
    }

    public boolean checkLogin() {

        Log.i("Check Login", "Check login");
        if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, LoginActivity.class);
            _context.startActivity(i);


        }

        return isLoggedIn();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USERID, pref.getString(KEY_USERID, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        return user;
    }

    public void setStoreDetails(List<String> listOfExistingScores) {
        Set<String> set = new HashSet<String>();
        set.addAll(listOfExistingScores);
        editor.putStringSet("key", set);
        editor.commit();
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();

        if (FacebookSdk.isInitialized()) {
            LoginManager.getInstance().logOut();
        }
		/*
		 * myApplication.setmGoogleApiClient(buildGoogleAPIClient());
		 * callGoogleSignOut();
		 */
		/* Toast.makeText(_context, "Signout Called", Toast.LENGTH_SHORT).show(); */
        Toast.makeText(_context, "User Logged out Successfully", Toast.LENGTH_SHORT).show();
		/*
		 * Intent intent = new Intent(_context, LoginActivity.class);
		 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
		 * Intent.FLAG_ACTIVITY_NEW_TASK);
		 * intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		 * intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		 * _context.startActivity(intent);
		 */
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
