package com.application.ads.data;

public class CashbackData {


    private String cashbackDateAdded;
    private String cashbackStoreName;
    private String cashbackAmount;
    private String cashbackStatus;

    public String getCashbackStatus() {
        return cashbackStatus;
    }

    public void setCashbackStatus(String cashbackStatus) {
        this.cashbackStatus = cashbackStatus;
    }

    public String getCashbackAmount() {
        return cashbackAmount;
    }

    public void setCashbackAmount(String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }

    public String getCashbackStoreName() {
        return cashbackStoreName;
    }

    public void setCashbackStoreName(String cashbackStoreName) {
        this.cashbackStoreName = cashbackStoreName;
    }

    public String getCashbackDateAdded() {
        return cashbackDateAdded;
    }

    public void setCashbackDateAdded(String cashbackDateAdded) {
        this.cashbackDateAdded = cashbackDateAdded;
    }
}
