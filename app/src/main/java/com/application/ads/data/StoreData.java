package com.application.ads.data;

import java.util.ArrayList;

public class StoreData {

    private String storeId;
    private String name;
    private String type;
    private String amountPercentage;
    private String coupons;
    private String imgLogo;
    private String termsConditions;
    private String howToGetThisOffer;
    private String desc;
    private String reviewsCount;
    private ArrayList<CouponData> datas;
    private CouponData counponData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmountPercentage() {
        return amountPercentage;
    }

    public void setAmountPercentage(String amountPercentage) {
        this.amountPercentage = amountPercentage;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public String getImgLogo() {
        return imgLogo;
    }

    public void setImgLogo(String imgLogo) {
        this.imgLogo = imgLogo;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getHowToGetThisOffer() {
        return howToGetThisOffer;
    }

    public void setHowToGetThisOffer(String howToGetThisOffer) {
        this.howToGetThisOffer = howToGetThisOffer;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<CouponData> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<CouponData> datas) {
        this.datas = datas;
    }

    public CouponData getCounponData() {
        return counponData;
    }

    public void setCounponData(CouponData counponData) {
        this.counponData = counponData;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

}
