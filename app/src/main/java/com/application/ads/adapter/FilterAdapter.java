package com.application.ads.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.SpecificationData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterAdapter extends BaseExpandableListAdapter {
    public static List<String> filterids;
    int newGroupPosition;
    SpecificationData specdata;
    ViewHolder viewHolder = null;
    private Activity _context;
    private List<SpecificationData> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<SpecificationData>> _listDataChild;

    public FilterAdapter(Activity context,
                         List<SpecificationData> listDataHeader,
                         HashMap<String, List<SpecificationData>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        if (filterids != null) {
            if (filterids.size() == 0) {
                filterids = new ArrayList<String>();
            }
        } else {
            filterids = new ArrayList<String>();
        }
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(
                this._listDataHeader.get(groupPosition))
                .get(childPosititon);

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {




			/*if (convertView == null) {
                viewHolder = new ViewHolder();
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.activity_filter_child, null);

			viewHolder.txtListChild = (CheckBox)convertView.findViewById(R.id.filterchild);
			convertView.setTag(viewHolder);
            convertView.setTag(R.id.filterchild, viewHolder.txtListChild);

			}
			else {
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
			viewHolder.txtListChild.setText(subcatdata.getOptionName());
			viewHolder.txtListChild.setTag(this._listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition));
			*/

        final SpecificationData catdata = (SpecificationData) _listDataHeader.get((groupPosition));


        final SpecificationData subcatdata = (SpecificationData) _listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition);


        if (convertView == null) {
            LayoutInflater inflator = _context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.activity_filter_child, null);
            viewHolder = new ViewHolder();
            //viewHolder.text = (TextView) convertView.findViewById(R.id.label);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.filterchild);
		            /*viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		                @Override
		                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		                	CheckBox cb=(CheckBox)buttonView;
		                	SpecificationData specdata=(SpecificationData)cb.getTag();

		                	if(cb.isChecked()==true){
		                		specdata.setSelected(cb.isChecked());
			                	subcatdata.setSelected(cb.isChecked());

		                		if(!filterids.contains(_listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId())){
			                		filterids.add(_listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString());
			                	}
		               // 		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
		                	}else{
		                		if(filterids.contains(_listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId())){
			                		filterids.remove(_listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString());
			                	}
		                //		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
		                	}

		                	Toast.makeText(_context, filterids.toString(), 5000).show();
		                }

		            });*/

         /*   viewHolder.checkbox.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Log.e("newgroupposition", catdata.getParentId() + "");
                    CheckBox cb = (CheckBox) v;
                    specdata = (SpecificationData) cb.getTag();
                    specdata.setSelected(cb.isChecked());
                    subcatdata.setSelected(cb.isChecked());
                    if (cb.isChecked() == true) {

                        if (!filterids.contains(subcatdata.getParentId().toString() + "_" + specdata.getOptionId())) {
                            filterids.add(subcatdata.getParentId().toString() + "_" + specdata.getOptionId().toString());
                        }
                        // 		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                    } else {
                        if (filterids.contains(subcatdata.getParentId().toString() + "_" + specdata.getOptionId())) {
                            filterids.remove(subcatdata.getParentId().toString() + "_" + specdata.getOptionId().toString());
                        }
                        //		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                    }

                    //    	Toast.makeText(_context, filterids.toString(), 5000).show();


                }
            });*/

            convertView.setTag(viewHolder);
            //     convertView.setTag(R.id.label, viewHolder.text);
            convertView.setTag(R.id.check, viewHolder.checkbox);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();


            convertView.setTag(R.id.check, viewHolder.checkbox);
        }


        viewHolder.checkbox.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.e("newgroupposition", subcatdata.getParentId() + "");
                CheckBox cb = (CheckBox) v;
                specdata = (SpecificationData) cb.getTag();
                specdata.setSelected(cb.isChecked());
                subcatdata.setSelected(cb.isChecked());
                String s=(specdata.getOptionId().toString().trim().contains("megapixel"))?specdata.getOptionId().toString().trim().replaceAll(" megapixel","MP"):specdata.getOptionId().toString().trim();
                if (cb.isChecked() == true) {
                    if (!filterids.contains(subcatdata.getParentId().toString() + "_" +s)) {
                        filterids.add(subcatdata.getParentId().toString() + "_" + s);
                    }
                } else {
                    if (filterids.contains(subcatdata.getParentId().toString() + "_" + s)) {
                        filterids.remove(subcatdata.getParentId().toString() + "_" + s);
                    }
                    //		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                }
                //    	Toast.makeText(_context, filterids.toString(), 5000).show();
            }
        });


        if (filterids != null) {
            for (String filterid : filterids) {
                // Log.e("filterid",filterid);
                //   Log.e("par text",subcatdata.getParentId().toString() + "_" +  _listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId());
                String s=(_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim().contains("megapixel"))?_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim().replaceAll(" megapixel","MP"):_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim();
                if (filterid.equalsIgnoreCase(subcatdata.getParentId().toString() + "_" +s)) {
                    viewHolder.checkbox.setChecked(true);
                }
            }

            if (filterids.size() > 0) {
                String s=(_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim().contains("megapixel"))?_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim().replaceAll(" megapixel","MP"):_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId().toString().trim();
                if (filterids.contains(subcatdata.getParentId().toString() + "_" + s)) {
                    subcatdata.setSelected(true);
                    //viewHolder.checkbox.setChecked(true);
                    Log.e("par text", subcatdata.getParentId().toString() + "_" + s);
                }
            }
        }

        viewHolder.checkbox.setTag(_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition)); // This line is important.

        viewHolder.checkbox.setText(subcatdata.getOptionId());
        viewHolder.checkbox.setChecked(subcatdata.isSelected());


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (this._listDataHeader.get(groupPosition).getSpecificationDataslist()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        if (_listDataHeader.get(groupPosition).getParentId() != null) {
            return Long.parseLong(_listDataHeader.get(groupPosition).getParentId());
        } else {
            return 0;
        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        SpecificationData specificationData = (SpecificationData) _listDataHeader.get(groupPosition);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflator = _context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.activity_filter_parent, null);


            viewHolder.lblListHeader = (TextView) convertView
                    .findViewById(R.id.filterparent);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        newGroupPosition = groupPosition;
        viewHolder.lblListHeader.setTag(groupPosition);
        viewHolder.lblListHeader.setText(specificationData.getParentName());
        TextView collapseparent = (TextView) convertView.findViewById(R.id.collapseparent);
        if (isExpanded) {
            collapseparent.setText("-");
        } else {
            collapseparent.setText("+");
        }
		    /*ViewHolder viewHolder = null;
	        if (convertView == null) {
	            LayoutInflater inflator = _context.getLayoutInflater();
	            convertView = inflator.inflate(R.layout.activity_filter_child, null);
	            viewHolder = new ViewHolder();
	            //viewHolder.text = (TextView) convertView.findViewById(R.id.label);
	            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.filterchild);
	            viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	                @Override
	                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	                	CheckBox cb=(CheckBox)buttonView;
	                	SpecificationData specdata=(SpecificationData)cb.getTag();
	                	specdata.setSelected(cb.isChecked());
	                	filterids.add(specdata.getOptionId().toString());
	                	Toast.makeText(_context, filterids.toString(), 5000).show();
	                }

	            });

	            convertView.setTag(viewHolder);
	       //     convertView.setTag(R.id.label, viewHolder.text);
	            convertView.setTag(R.id.check, viewHolder.checkbox);
	            } else {
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
	        viewHolder.checkbox.setTag(_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition)); // This line is important.

	        viewHolder.checkbox.setText(subcatdata.getOptionName());
	        viewHolder.checkbox.setChecked(subcatdata.isSelected());
*/

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
        protected TextView lblListHeader;
        protected TextView collapseparent;
    }

}
