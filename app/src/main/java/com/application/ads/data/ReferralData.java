package com.application.ads.data;

public class ReferralData {

    private String dateJoined;
    private String refferalName;
    private String refferalAccountStatus;


    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getRefferalName() {
        return refferalName;
    }

    public void setRefferalName(String refferalName) {
        this.refferalName = refferalName;
    }

    public String getRefferalAccountStatus() {
        return refferalAccountStatus;
    }

    public void setRefferalAccountStatus(String refferalAccountStatus) {
        this.refferalAccountStatus = refferalAccountStatus;
    }

}