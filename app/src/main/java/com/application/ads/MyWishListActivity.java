package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.FavoriteProductsAdapter;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MyWishListActivity extends Activity {

    public static JSONParser parser;
    public static SharedPreferences preferences;

    public static SessionManager manager;
    public static String urlLoadFavouriteList = DBConnection.BASEURL + "load_fav_product_list.php";
    public static List<ProductDetails> productDatas;
    public static String urlRemoveFromFav = DBConnection.BASEURL
            + "add_to_wishlist.php";
    //	LinearLayout grdViewFavouriteStoreList;
    //GridView favprodgrid;
    public static ListView favprodlist;
    public static FavoriteProductsAdapter adapter;
    int rowWiseCount = 2;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fav_coupons_list);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("My Wishlist");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        productDatas = new ArrayList<ProductDetails>();
        manager = new SessionManager(MyWishListActivity.this);

        //	favprodgrid=(GridView)findViewById(R.id.favprodgrid);
        favprodlist = (ListView) findViewById(R.id.favprodlist);


        favprodlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(MyWishListActivity.this, ProductOverView.class);
                intent.putExtra("parent_child_id", productDatas.get(position).getParent_child_id());
                intent.putExtra("prod_name", productDatas.get(position).getProduct_name());
                intent.putExtra("prod_img", productDatas.get(position).getProduct_image());
                intent.putExtra("prod_price", productDatas.get(position).getMrp());
                intent.putExtra("prod_rating", 0.0);
                intent.putExtra("prod_id", productDatas.get(position).getProduct_id());
                intent.putExtra("pp_id", "");
                intent.putExtra("prod_desc", "");

                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

	
	
	
	/*private void fillStoreCouponsDetails(List<StoreData> storesList) {
		ImageView imgNewCouponStoreImage,imgNewCouponCouponImage,imgNewCouponRemove;
		TextView txtNewCouponStoreName,txtNewCouponStoreOffer,txtNewCouponCouponOffer;
		Button btnNewCouponShowCode;
		
		int totSize=storesList.size();
		int totCount=totSize/rowWiseCount;
		int remaining=totSize%rowWiseCount;
		
		int c=0;
		
		grdViewFavouriteStoreList.removeAllViews();
		
		for(int i=0;i<totCount;i++){
			LinearLayout layout=new LinearLayout(MyWishListActivity.this);
			LayoutParams params=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setLayoutParams(params);
			for(int j=0;j<rowWiseCount;j++){
				final StoreData data=storesList.get(c);
				View view=getLayoutInflater().inflate(R.layout.fav_coupon_list_detail,null);
				txtNewCouponStoreName=(TextView)view.findViewById(R.id.txtNewCouponStoreName);
				txtNewCouponStoreOffer=(TextView)view.findViewById(R.id.txtNewCouponStoreOffer);
				txtNewCouponCouponOffer=(TextView)view.findViewById(R.id.txtNewCouponCouponOffer);
				imgNewCouponStoreImage=(ImageView)view.findViewById(R.id.imgNewCouponStoreImage);
				imgNewCouponCouponImage=(ImageView)view.findViewById(R.id.imgNewCouponCouponImage);
				btnNewCouponShowCode=(Button)view.findViewById(R.id.btnNewCouponShowCode);
				btnNewCouponShowCode.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Log.e("Store_names==",data.getName());
						Intent intent=new Intent(MyWishListActivity.this,CouponList.class);
						intent.putExtra("storeName",data.getName());
						startActivity(intent);
					}
				});

				txtNewCouponStoreName.setText(data.getName());
				if(data.getType().trim().equals("Percentage")){
					txtNewCouponStoreOffer.setText("+ Earn upto "+data.getAmountPercentage()+"% Cashback");	
				}else{
					txtNewCouponStoreOffer.setText("+ Earn upto Rs."+data.getAmountPercentage()+" Cashback");
				}
				txtNewCouponCouponOffer.setText(data.getCounponData().getTitle());
				loader.DisplayImage(data.getImgLogo(),imgNewCouponStoreImage);
				loader.DisplayImage(data.getCounponData().getImgName(),imgNewCouponCouponImage);
				
				imgNewCouponRemove=(ImageView)view.findViewById(R.id.imgNewCouponRemove);
				imgNewCouponRemove.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						new RemoveFromFavouriteList(data.getStoreId(),data.getCounponData().getCouponId()).execute();
					}
				});
				layout.addView(view);
				c+=1;
			}
			grdViewFavouriteStoreList.addView(layout);
		}
		
		
		
		if(remaining>0){
			LinearLayout layout=new LinearLayout(MyWishListActivity.this);
			LayoutParams params=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setLayoutParams(params);
			for(int j=0;j<remaining;j++){
				final StoreData data=storesList.get(c);
				View view=getLayoutInflater().inflate(R.layout.fav_coupon_list_detail,null);
				txtNewCouponStoreName=(TextView)view.findViewById(R.id.txtNewCouponStoreName);
				txtNewCouponStoreOffer=(TextView)view.findViewById(R.id.txtNewCouponStoreOffer);
				txtNewCouponCouponOffer=(TextView)view.findViewById(R.id.txtNewCouponCouponOffer);
				imgNewCouponStoreImage=(ImageView)view.findViewById(R.id.imgNewCouponStoreImage);
				imgNewCouponCouponImage=(ImageView)view.findViewById(R.id.imgNewCouponCouponImage);
				btnNewCouponShowCode=(Button)view.findViewById(R.id.btnNewCouponShowCode);
				btnNewCouponShowCode.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Log.e("Store_names==",data.getName());
						Intent intent=new Intent(MyWishListActivity.this,CouponList.class);
						intent.putExtra("storeName",data.getName());
						startActivity(intent);
					}
				});

				txtNewCouponStoreName.setText(data.getName());
				if(data.getType().trim().equals("Percentage")){
					txtNewCouponStoreOffer.setText("+ Earn upto "+data.getAmountPercentage()+"% Cashback");	
				}else{
					txtNewCouponStoreOffer.setText("+ Earn upto Rs."+data.getAmountPercentage()+" Cashback");
				}
				txtNewCouponCouponOffer.setText(data.getCounponData().getTitle());
				loader.DisplayImage(data.getImgLogo(),imgNewCouponStoreImage);
				loader.DisplayImage(data.getCounponData().getImgName(),imgNewCouponCouponImage);
				imgNewCouponRemove=(ImageView)view.findViewById(R.id.imgNewCouponRemove);
				imgNewCouponRemove.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						new RemoveFromFavouriteList(data.getStoreId(),data.getCounponData().getCouponId()).execute();
					}
				});
				layout.addView(view);
				c+=1;
			}
			grdViewFavouriteStoreList.addView(layout);
		}
	}
	*/
	
	
	
/*	class RemoveFromFavouriteList extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		JSONObject object;
		private String store_id,coupon_id;
		
		public RemoveFromFavouriteList(String store_id,String coupon_id) {
			this.store_id=store_id;
			this.coupon_id=coupon_id;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(MyWishListActivity.this);
			dialog.setMessage("Loading..");
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"")));
			param.add(new BasicNameValuePair("coupon_id",coupon_id));
			param.add(new BasicNameValuePair("store_id",store_id));
			Log.e("Fav params ==============",param.toString());
			Log.e("Fav url ==============",urlRemoveFromFav);
			object = parser.makeHttpRequest(urlRemoveFromFav, "GET", param);
			if (object != null) {
				return object.toString();
			} else {
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result.trim().length() > 0) {
				try {
					object = new JSONObject(result);
					if (object.getInt("success") == 1) {
						Toast.makeText(MyWishListActivity.this, "Successfully removed from your wishlist..",Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(MyWishListActivity.this, "No Data Found..",Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			dialog.dismiss();
			new LoadAllCouponList().execute();
		}

	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    productDatas = new ArrayList<ProductDetails>();
                    productDatas.clear();
                    Context context = MyWishListActivity.this;
                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("products");
                            for (int m = 0; m < array.length(); m++) {
                                JSONObject object1 = array.getJSONObject(m);
                                ProductDetails productDetails = new ProductDetails();
                                productDetails.setProduct_id(object1.getString("product_id"));
                                productDetails.setProduct_image(object1.getString("product_image"));
                                productDetails.setParent_child_id(object1.getString("parent_child_id"));
                                productDetails.setProduct_name(object1.getString("product_name"));
                                productDetails.setProduct_url(object1.getString("product_url"));
                                productDetails.setMrp(object1.getString("min_price"));
                                productDetails.setProWishListStatus(object1.getString("wishlist_status"));
                                productDatas.add(productDetails);
                            }

                        } else {
                            Toast.makeText(context, "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                        adapter = new FavoriteProductsAdapter(context, productDatas);
                        favprodlist.setAdapter(adapter);
                    } else {

                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //new LoadAllCouponList(MyWishListActivity.this).execute();
        loadCouponList();
/*
		List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));*/

/*
		String url=urlLoadFavouriteList+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
		volleyJsonObjectRequest(url);
*/

    }


    public void loadCouponList(){
        productDatas.clear();
        dialog = new ProgressDialog(MyWishListActivity.this);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue=Volley.newRequestQueue(MyWishListActivity.this);
        String url=urlLoadFavouriteList+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
        StringRequest request=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                if (response.trim().length() > 0) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getInt("success") == 1) {
                            JSONArray array = object.getJSONArray("products");
                            for (int m = 0; m < array.length(); m++) {
                                JSONObject object1 = array.getJSONObject(m);
                                ProductDetails productDetails = new ProductDetails();
                                productDetails.setProduct_id(object1.getString("product_id"));
                                productDetails.setProduct_image(object1.getString("product_image"));
                                productDetails.setParent_child_id(object1.getString("parent_child_id"));
                                productDetails.setProduct_name(object1.getString("product_name"));
                                productDetails.setProduct_url(object1.getString("product_url"));
                                productDetails.setMrp(object1.getString("min_price"));
                                productDetails.setProWishListStatus(object1.getString("wishlist_status"));
                                productDatas.add(productDetails);
                            }

                        } else {
                            Toast.makeText(MyWishListActivity.this, "No Data Found..",
                                    Toast.LENGTH_LONG).show();
                        }
                        adapter = new FavoriteProductsAdapter(MyWishListActivity.this, productDatas);
                        favprodlist.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);


    }

    public static class LoadAllCouponList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        Context context;

        public LoadAllCouponList(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(context, urlLoadFavouriteList, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            productDatas = new ArrayList<ProductDetails>();
            productDatas.clear();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray array = object.getJSONArray("products");
                        for (int m = 0; m < array.length(); m++) {
                            JSONObject object1 = array.getJSONObject(m);
                            ProductDetails productDetails = new ProductDetails();
                            productDetails.setProduct_id(object1.getString("product_id"));
                            productDetails.setProduct_image(object1.getString("product_image"));
                            productDetails.setParent_child_id(object1.getString("parent_child_id"));
                            productDetails.setProduct_name(object1.getString("product_name"));
                            productDetails.setProduct_url(object1.getString("product_url"));
                            productDetails.setMrp(object1.getString("min_price"));
                            productDetails.setProWishListStatus(object1.getString("wishlist_status"));
                            productDatas.add(productDetails);
                        }

                    } else {
                        Toast.makeText(context, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                    adapter = new FavoriteProductsAdapter(context, productDatas);
                    favprodlist.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }

    }

    public class AddToWishList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        ProductDetails data;

        String wishListStatus;
        JSONObject object;
        JSONParser parser;
        List<ProductDetails> productListing;
        SharedPreferences preferences;
        Context context;
        String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
                + "add_to_wishlist.php";

        public AddToWishList(String wishListStatus, ProductDetails data, Context context) {
            this.wishListStatus = wishListStatus;
            this.data = data;
            this.context = context;
            preferences = context.getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
            parser = new JSONParser();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("product_id", data.getProduct_id()));
            param.add(new BasicNameValuePair("wish_list_status", wishListStatus));
            Log.e("param", urlAddOrRemoveProductsFromWishList + param.toString());
            object = parser.makeHttpRequest(context, urlAddOrRemoveProductsFromWishList, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            productListing = new ArrayList<ProductDetails>();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        if (Integer.parseInt(wishListStatus.trim()) == 0) {
                            Toast.makeText(context, "Products added to Wish List..", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, "Products removed from Wish List..", Toast.LENGTH_LONG).show();
                        }
                        productDatas.clear();
                        //new LoadAllCouponList(context).execute();
                        loadCouponList();
						/*String url=urlLoadFavouriteList+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0")+MyWishListActivity.this;
						volleyJsonObjectRequest(url);*/


                    } else {
                        Toast.makeText(context,
                                "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context,
                        "Something goes wrong..", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }
}
