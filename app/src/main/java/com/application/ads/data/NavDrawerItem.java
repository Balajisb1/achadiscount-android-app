package com.application.ads.data;

public class NavDrawerItem {

    private int headingId;
    private String heading;
    private String subHeading;
    private String subHeadImg;
    private int subHeadingId;
    private int productsCount;


    public NavDrawerItem(String heading, int headingId, int productsCount) {
        this.headingId = headingId;
        this.heading = heading;
        this.subHeading = null;
        this.productsCount = productsCount;
    }

    public NavDrawerItem(String heading, String subHeading, int subHeadingId, String subHeadImg) {
        this.setSubHeading(subHeading);
        this.heading = heading;
        this.subHeadingId = subHeadingId;
        this.setSubHeadImg(subHeadImg);
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }


    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public int getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(int productsCount) {
        this.productsCount = productsCount;
    }

    public String getSubHeadImg() {
        return subHeadImg;
    }

    public void setSubHeadImg(String subHeadImg) {
        this.subHeadImg = subHeadImg;
    }

    public int getSubHeadingId() {
        return subHeadingId;
    }

    public void setSubHeadingId(int subHeadingId) {
        this.subHeadingId = subHeadingId;
    }

    public int getHeadingId() {
        return headingId;
    }

    public void setHeadingId(int headingId) {
        this.headingId = headingId;
    }

}