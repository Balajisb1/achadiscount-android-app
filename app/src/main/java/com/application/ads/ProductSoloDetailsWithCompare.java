package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.ProductGridAdapter;
import com.application.ads.data.CompareProducts;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.ReviewData;
import com.application.ads.data.SpecificationData;
import com.application.ads.data.Stores;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductSoloDetailsWithCompare extends Activity {
    public static String urlAddReviews = DBConnection.BASEURL + "add_user_prouct_reviews.php";
    public static String urlLoadProductDetailWithCompare = DBConnection.BASEURL + "product_details.php";
    public static String urlLoadProductDailyPrice = DBConnection.BASEURL + "product_daily_price.php";
    LinearLayout sellerlinear, relatedlinear, reviewlinear, reviewalllinear;
    ImageView prodimage;
    TextView prodname, prodprice, prodshortdesc;
    RatingBar prodrating;
    LineGraphSeries<DataPoint> series;
    String prod_id;
    JSONParser parser;
    List<ProductDetails> productdetailslist;
    int catposition;
    // GridView releatedgrid;
    ProductGridAdapter adapter;
    ProductDetails productdetails;
    // List<Stores> offlinestores;
    List<ReviewData> reviewDatas;
    List<ProductDetails> relatedproducts;
    SessionManager manager;
    Button writereview;
    double cashbackprice;
    Button sellerbtn, relatedbtn, reviewbtn, specbtn;
    SharedPreferences preferences;
    LinearLayout sellerslinear, productspeclinear;
    Dialog reviewdialog;
    TextView noofreviewers;
    ImageView imgProductListingDetailFavourites;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private List<ProductDetails> productDailyDetailsList;
    private ProgressDialog dialog;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";
    private boolean isRelatedAdd = false;
    private List<ProductDetails> prodCommList;
    private DecimalFormat format = new DecimalFormat("##.00");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_alldetails);
        getActionBar()
                .setTitle("Online/Offline Price Comparison With Cashback");
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        relatedproducts = new ArrayList<ProductDetails>();
        imgProductListingDetailFavourites = (ImageView) findViewById(R.id.imgProductListingDetailFavourites);
        toolbartitle.setText("Product Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        sellerlinear = (LinearLayout) findViewById(R.id.sellerlinear);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        productDailyDetailsList = new ArrayList<>();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        // Log.e("Temp List size:", ProductOverView.tempList.size()+"");

        sellerbtn = (Button) findViewById(R.id.sellerbtn);
        relatedbtn = (Button) findViewById(R.id.relatedbtn);
        reviewbtn = (Button) findViewById(R.id.reviewbtn);
        specbtn = (Button) findViewById(R.id.specbtn);
        writereview = (Button) findViewById(R.id.writereview);
        manager = new SessionManager(ProductSoloDetailsWithCompare.this);
        productspeclinear = (LinearLayout) findViewById(R.id.productspeclinear);
        sellerslinear = (LinearLayout) findViewById(R.id.sellerslinear);
        relatedlinear = (LinearLayout) findViewById(R.id.relatedlinear);
        reviewlinear = (LinearLayout) findViewById(R.id.reviewlinear);
        reviewalllinear = (LinearLayout) findViewById(R.id.reviewalllinear);
        // releatedgrid=(GridView)findViewById(R.id.releatedgrid);
        prod_id = getIntent().getStringExtra("prod_id");
        parser = new JSONParser();
        productdetails = new ProductDetails();
        reviewdialog = new Dialog(ProductSoloDetailsWithCompare.this);

        prodprice = (TextView) findViewById(R.id.prodprice);
        prodname = (TextView) findViewById(R.id.prodname);
        prodshortdesc = (TextView) findViewById(R.id.prodshortdesc);
        prodimage = (ImageView) findViewById(R.id.prodimage);
        prodrating = (RatingBar) findViewById(R.id.prodrating);
        noofreviewers = (TextView) findViewById(R.id.noofreviewers);
        /*
         * LayerDrawable stars = (LayerDrawable)
		 * prodrating.getProgressDrawable();
		 * stars.getDrawable(2).setColorFilter(Color.YELLOW,
		 * PorterDuff.Mode.SRC_ATOP);
		 */

        // prodrating.setRating((float)getIntent().getDoubleExtra("prod_rating",0.0));
        prodshortdesc.setText(getIntent().getStringExtra("prod_desc"));

        Picasso.with(ProductSoloDetailsWithCompare.this).load(DBConnection.PRODUCT_IMGURL
                + getIntent().getStringExtra("prod_img")).placeholder(R.drawable.adslogo).resize(144, 0).into(prodimage);


        writereview.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                reviewdialog.setContentView(R.layout.activity_user_reviews);
                Button postuserrating = (Button) reviewdialog
                        .findViewById(R.id.postuserrating);
                final EditText userratingreview = (EditText) reviewdialog
                        .findViewById(R.id.userratingreview);
                final RatingBar userrating = (RatingBar) reviewdialog
                        .findViewById(R.id.userrating);
                LayerDrawable drawable = (LayerDrawable) userrating
                        .getProgressDrawable();
                drawable.getDrawable(2).setColorFilter(Color.YELLOW,
                        PorterDuff.Mode.SRC_ATOP);

                postuserrating.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (validateComponents(userratingreview.getText()
                                .toString(), userrating.getRating())) {

                            new ExecuteAddReview(userratingreview.getText().toString(), userrating.getRating()).execute();

                         /*   List<NameValuePair> param = new ArrayList<NameValuePair>();
                            param.add(new BasicNameValuePair("usercomment", reviewcomment));
                            param.add(new BasicNameValuePair("rating", reviewrating + ""));
                            param.add(new BasicNameValuePair("productid", prod_id));
                            param.add(new BasicNameValuePair("userid", preferences.getString(
                                    SessionManager.KEY_USERID, "1")));*/
/*

                            String reviewcomment=userratingreview.getText().toString();
                            String reviewrating=String.valueOf(userrating.getRating());

                            String url=urlAddReviews+"?usercomment="+reviewcomment+"&rating="+reviewrating+"&productid="+prod_id
                                    +"&userid="+ preferences.getString(SessionManager.KEY_USERID, "1");
                            volleyJsonObjectRequest(url);
*/


                        }
                    }
                });
                reviewdialog.setTitle("Review");
                if (manager.checkLogin()) {
                    reviewdialog.show();
                }

            }
        });

        imgProductListingDetailFavourites
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (manager.checkLogin()) {
                            // Remove from wish list
                            if (Integer.parseInt(productdetailslist.get(0)
                                    .getProWishListStatus().trim()) > 0) {

                                Toast.makeText(ProductSoloDetailsWithCompare.this, "It is already added in wishlist", Toast.LENGTH_SHORT).show();
                               /* new AddToWishList("1", productdetailslist
                                        .get(0),
                                        imgProductListingDetailFavourites,
                                        ProductSoloDetailsWithCompare.this)
                                        .execute(); */// Remove from wish
                            } else {
                                addToWishList(productdetailslist.get(0), 0, 0, imgProductListingDetailFavourites, 0);


                            }
                        } else {
                            startActivity(new Intent(
                                    ProductSoloDetailsWithCompare.this,
                                    LoginActivity.class));
                        }

                    }
                });

        sellerbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                sellerbtn.setBackgroundColor(Color.parseColor("#f32440"));
                sellerbtn.setTextColor(getResources().getColor(R.color.WHITE));

                relatedbtn.setBackgroundResource(R.drawable.border_red);
                relatedbtn.setTextColor(getResources().getColor(R.color.PINK));

                specbtn.setBackgroundResource(R.drawable.border_red);
                specbtn.setTextColor(getResources().getColor(R.color.PINK));

                reviewbtn.setBackgroundResource(R.drawable.border_red);
                reviewbtn.setTextColor(getResources().getColor(R.color.PINK));
                sellerslinear.setVisibility(View.VISIBLE);
                productspeclinear.setVisibility(View.GONE);
                relatedlinear.setVisibility(View.GONE);
                reviewlinear.setVisibility(View.GONE);

            }
        });

        specbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                sellerbtn.setBackgroundResource(R.drawable.border_red);
                sellerbtn.setTextColor(getResources().getColor(R.color.PINK));

                relatedbtn.setBackgroundResource(R.drawable.border_red);
                relatedbtn.setTextColor(getResources().getColor(R.color.PINK));

                specbtn.setBackgroundColor(Color.parseColor("#f32440"));
                specbtn.setTextColor(getResources().getColor(R.color.WHITE));

                reviewbtn.setBackgroundResource(R.drawable.border_red);
                reviewbtn.setTextColor(getResources().getColor(R.color.PINK));
                sellerslinear.setVisibility(View.GONE);
                productspeclinear.setVisibility(View.VISIBLE);
                relatedlinear.setVisibility(View.GONE);
                reviewlinear.setVisibility(View.GONE);

            }
        });

        relatedbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                sellerbtn.setBackgroundResource(R.drawable.border_red);
                sellerbtn.setTextColor(getResources().getColor(R.color.PINK));

                relatedbtn.setBackgroundColor(Color.parseColor("#f32440"));
                relatedbtn.setTextColor(getResources().getColor(R.color.WHITE));

                specbtn.setBackgroundResource(R.drawable.border_red);
                specbtn.setTextColor(getResources().getColor(R.color.PINK));

                reviewbtn.setBackgroundResource(R.drawable.border_red);
                reviewbtn.setTextColor(getResources().getColor(R.color.PINK));
                sellerslinear.setVisibility(View.GONE);
                productspeclinear.setVisibility(View.GONE);
                relatedlinear.setVisibility(View.VISIBLE);
                reviewlinear.setVisibility(View.GONE);

            }
        });

        reviewbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                sellerbtn.setBackgroundResource(R.drawable.border_red);
                sellerbtn.setTextColor(getResources().getColor(R.color.PINK));

                relatedbtn.setBackgroundResource(R.drawable.border_red);
                relatedbtn.setTextColor(getResources().getColor(R.color.PINK));

                specbtn.setBackgroundResource(R.drawable.border_red);
                specbtn.setTextColor(getResources().getColor(R.color.PINK));

                reviewbtn.setBackgroundColor(Color.parseColor("#f32440"));
                reviewbtn.setTextColor(getResources().getColor(R.color.WHITE));
                sellerslinear.setVisibility(View.GONE);
                productspeclinear.setVisibility(View.GONE);
                relatedlinear.setVisibility(View.GONE);
                reviewlinear.setVisibility(View.VISIBLE);

            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                productdetailslist.clear();
                reviewDatas.clear();
                relatedproducts.clear();

                productspeclinear.removeAllViews();

//                new ExecuteLoadProductDetails(getIntent().getStringExtra("prod_id")).execute();

               /* List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("product_id", productid));
                param.add(new BasicNameValuePair("user_id", preferences.getString(
                        SessionManager.KEY_USERID, "")));*/

                String productid = getIntent().getStringExtra("prod_id");

                String url = urlLoadProductDetailWithCompare + "?product_id=" + productid + "&user_id=" + preferences.getString(
                        SessionManager.KEY_USERID, "");

                volleyJsonObjectRequest1(url);


                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,

                android.R.color.holo_red_light);


        // Toast.makeText(getApplicationContext(),
        // getIntent().getStringExtra("pp_id"),Toast.LENGTH_SHORT ).show();


//        new ExecuteLoadProductDetails(getIntent().getStringExtra("prod_id")).execute();


        String productid = getIntent().getStringExtra("prod_id");

        String url = urlLoadProductDetailWithCompare + "?product_id=" + productid + "&user_id=" + preferences.getString(
                SessionManager.KEY_USERID, "");

        volleyJsonObjectRequest1(url);


       /* new ExecuteProductPrice().execute();

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("product_id", prod_id));*/

        String urlPrice = urlLoadProductDailyPrice + "?product_id=" + prod_id;
        volleyJsonObjectRequest2(urlPrice);


    }

    public void volleyJsonObjectRequest2(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(response);
                    productDailyDetailsList = new ArrayList<>();

                    if (response.toString().trim().length() > 0) {

                        if (jsonObject.length() > 0) {

                            JSONArray jsonArray = jsonObject.getJSONArray(prod_id);
                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    ProductDetails details = new ProductDetails();
                                    details.setProductDailyPrice(object.getString("price"));
                                    details.setProductDailyDate(object.getString("date"));
                                    productDailyDetailsList.add(details);
                                }

                            }
                        }
                        makeGraph();
                    } else {

                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }


    public void volleyJsonObjectRequest1(String url) {

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Loading", true, false);

        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressDialog.dismiss();
                    //final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(response);

                    productdetails = new ProductDetails();
                    prodCommList = new ArrayList<>();

                    productdetailslist = new ArrayList<ProductDetails>();
                    // offlinestores=new ArrayList<Stores>();
                    reviewDatas = new ArrayList<ReviewData>();
                    List<Stores> storeslist = new ArrayList<Stores>();
                    relatedproducts = new ArrayList<ProductDetails>();
                    // Toast.makeText(ProductSoloDetailsWithCompare.this,
                    // result.trim().length()+" Length", Toast.LENGTH_SHORT).show();

                    Log.i("JSON STRING", response);

                    if (response.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONArray subcatarray = jsonObject.getJSONArray("products");
                            for (int i = 0; i < subcatarray.length(); i++) {
                                JSONObject subcatobj = subcatarray.getJSONObject(i);
                                productdetails.setProduct_id(subcatobj
                                        .getString("product_id"));
                                productdetails.setProduct_image(subcatobj
                                        .getString("product_image"));
                                productdetails.setKey_feature(subcatobj
                                        .getString("key_feature"));
                                productdetails.setProduct_name(subcatobj
                                        .getString("product_name"));
                                productdetails.setProWishListStatus(subcatobj.getString("wishlist_status"));
                                // productdetails.setProd_desc(subcatobj.getString("description"));

                                JSONArray storesarray = jsonObject
                                        .getJSONArray("stores");

                                for (int j = 0; j < storesarray.length(); j++) {
                                    JSONObject subcatobj1 = storesarray
                                            .getJSONObject(j);
                                    Stores storedetails = new Stores();
                                    storedetails.setStore_id(subcatobj1
                                            .getString("store_id"));
                                    storedetails.setProduct_price(subcatobj1
                                            .getString("product_price"));
                                    storedetails.setAffliate_logo(subcatobj1
                                            .getString("affiliate_logo"));
                                    storedetails.setAffliate_name(subcatobj1
                                            .getString("affiliate_name"));
                                    storedetails.setProduct_url(subcatobj1
                                            .getString("product_url"));
                                    storedetails.setPp_id(subcatobj1
                                            .getString("pp_id"));
                                    storedetails.setStockAvail(subcatobj1
                                            .getString("stock"));
                                    storedetails.setStorerating(subcatobj1
                                            .getString("rating"));
                                    if (!subcatobj1.getString("product_price").equalsIgnoreCase("0")) {
                                        storeslist.add(storedetails);
                                    }
                                }

                                JSONArray reviewarray = jsonObject
                                        .getJSONArray("reviews");

                                if (reviewarray.length() > 0) {

                                    for (int j = 0; j < reviewarray.length(); j++) {
                                        JSONObject subcatobj1 = reviewarray
                                                .getJSONObject(j);
                                        ReviewData data = new ReviewData();
                                        data.setUserName(subcatobj1
                                                .getString("user_name"));
                                        data.setRating(subcatobj1
                                                .getString("rating"));
                                        data.setComments(subcatobj1
                                                .getString("review_title"));
                                        data.setTotalReviews(subcatobj1
                                                .getString("total_reviews"));
                                        data.setOverallreview(subcatobj1
                                                .getString("over_all_rating"));
                                        reviewDatas.add(data);
                                    /*
                                     * Stores storedetails=new Stores();
									 * storedetails
									 * .setStore_id(subcatobj1.getString
									 * ("store_id"));
									 * storedetails.setProduct_price
									 * (subcatobj1.getString("product_price"));
									 * storedetails
									 * .setAffliate_logo(subcatobj1.getString
									 * ("affiliate_logo"));
									 * storedetails.setAffliate_name
									 * (subcatobj1.getString("affiliate_name"));
									 * storedetails
									 * .setProduct_url(subcatobj1.getString
									 * ("product_url"));
									 * storedetails.setPp_id(subcatobj1
									 * .getString("pp_id"));
									 * storeslist.add(storedetails);
									 */

                                    }

                                    fillAllReviews();
                                }
                            /*
                             * JSONArray offlinestoresarray=object.getJSONArray(
							 * "offlinestores");
							 *
							 * if(offlinestoresarray.length()>0){
							 *
							 * for (int j = 0; j < offlinestoresarray.length();
							 * j++) { JSONObject subcatobj1 =
							 * offlinestoresarray.getJSONObject(j); Stores
							 * storedetails1=new Stores();
							 * storedetails1.setOffline_price
							 * (subcatobj1.getString("price"));
							 * storedetails1.setOffline_offer
							 * (subcatobj1.getString("offline_offer"));
							 * JSONArray
							 * offlinestorearray1=subcatobj1.getJSONArray
							 * ("storedetails"); for(int
							 * ij=0;ij<offlinestorearray1.length();ij++){ Stores
							 * storedetails=new Stores(); JSONObject
							 * object2=offlinestorearray1.getJSONObject(j);
							 * storedetails
							 * .setOffline_storename(object2.getString
							 * ("store_name"));
							 * storedetails.setOffline_storelogo
							 * (object2.getString("store_logo"));
							 * storedetails.setLatitude
							 * (object2.getString("latitude"));
							 * storedetails.setLongtitude
							 * (object2.getString("longtitude"));
							 * storedetails.setAddress
							 * (object2.getString("address"));
							 * offlinestores.add(storedetails);
							 * data1.setCatId(object1.getString("category_id"));
							 * data1
							 * .setCatName(object1.getString("category_name"));
							 *
							 * } storedetails1.setStores(offlinestores); } }
							 */
                                // ProductOverView.tempList.size()
                                JSONArray cashbackarray = jsonObject
                                        .getJSONArray("cashback");
                                if (cashbackarray.length() > 0) {
                                    for (int j = 0; j < cashbackarray.length(); j++) {
                                        JSONObject subcatobj2 = cashbackarray
                                                .getJSONObject(j);
                                    /*
                                     * String categoryid=subcatobj2.getString(
									 * "cashback_catid");
									 * productdetails.setCategory_id
									 * (Arrays.asList
									 * (categoryid.split("\\s*,\\s*"))); String
									 * categorycashper
									 * =subcatobj2.getString("cashback_perc");
									 * productdetails
									 * .setCategory_per(Arrays.asList
									 * (categorycashper.split("\\s*,\\s*")));
									 */
                                        ProductDetails data = new ProductDetails();
                                        data.setWeget(subcatobj2
                                                .getString("weget"));
                                        data.setWegive(subcatobj2
                                                .getString("wegive"));
                                        data.setStore_id(subcatobj2
                                                .getString("store_id"));

                                        prodCommList.add(data);

                                    }
                                }
                                if (jsonObject.has("relproducts")) {
                                    JSONArray relproductsarray = jsonObject
                                            .getJSONArray("relproducts");
                                    if (relproductsarray.length() > 0) {
                                        for (int j = 0; j < relproductsarray
                                                .length(); j++) {
                                            JSONObject subcatobj4 = relproductsarray
                                                    .getJSONObject(j);
                                            ProductDetails relproddetails = new ProductDetails();
                                            relproddetails.setPp_id(subcatobj4
                                                    .getString("pp_id"));
                                            relproddetails
                                                    .setProduct_price(subcatobj4
                                                            .getString("product_price"));
                                            relproddetails.setRating(subcatobj4
                                                    .getString("rating"));
                                            relproddetails
                                                    .setProduct_name(subcatobj4
                                                            .getString("prod_name"));
                                            relproddetails
                                                    .setProduct_parent(subcatobj4
                                                            .getString("parent_child_id"));
                                            relproddetails.setMrp(subcatobj4
                                                    .getString("mrp"));
                                            relproddetails.setProduct_id(subcatobj4
                                                    .getString("product_id"));
                                            relproddetails
                                                    .setProduct_image(subcatobj4
                                                            .getString("product_image"));
                                            relproddetails
                                                    .setKey_feature(subcatobj4
                                                            .getString("key_feature"));
                                            relproddetails
                                                    .setProWishListStatus(subcatobj4
                                                            .getString("wishlist_status"));
                                            relproddetails
                                                    .setParent_child_id(subcatobj4.getString("parent_child_id"));
                                            relatedproducts.add(relproddetails);
                                        }
                                    }
                                }
                                ArrayList<SpecificationData> specifications = new ArrayList<SpecificationData>();
                                if (jsonObject.has("specification")) {
                                    JSONArray array1 = jsonObject
                                            .getJSONArray("specification");
                                    for (int ij = 0; ij < array1.length(); ij++) {
                                        JSONObject specObject = array1
                                                .getJSONObject(ij);
                                        SpecificationData data1 = new SpecificationData();
                                        data1.setOptionId(specObject
                                                .getString("specid"));
                                        data1.setOptionName(specObject
                                                .getString("specification"));
                                        data1.setParentId(specObject
                                                .getString("parant_id"));
                                        data1.setParentName(specObject
                                                .getString("parant_name"));
                                        data1.setValue(specObject
                                                .getString("values"));
                                        specifications.add(data1);
                                    }
                                    productdetails
                                            .setSpecificationList(specifications);
                                }
                                productdetails.setStorelist(storeslist);
                                productdetailslist.add(productdetails);
                            }

                            if (productdetailslist.get(0).getProWishListStatus()
                                    .equals(null)
                                    || productdetailslist.get(0)
                                    .getProWishListStatus().equals("")) {
                                Log.e("", "Null");
                            } else {
                                if (Integer.parseInt(productdetailslist.get(0)
                                        .getProWishListStatus().trim()) > 0) {
                                    imgProductListingDetailFavourites
                                            .setImageResource(R.drawable.filled_heart);
                                } else {
                                    imgProductListingDetailFavourites
                                            .setImageResource(R.drawable.un_fillled_heart);
                                }

                            }
                       /* shareProdUrl.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                shareMessage(productdetailslist.get(0).getProduct_url());
                            }
                        });
*/
                            if (storeslist != null) {
                                for (Stores stores : storeslist) {
                                    if (!stores.getProduct_price().equalsIgnoreCase("0")) {
                                        prodprice.setText("Rs. "
                                                + stores.getProduct_price());
                                        break;
                                    }
                                }
                            }
                            if (productdetailslist != null) {
                                if (productdetailslist.size() > 0) {
                                    prodname.setText(productdetailslist.get(0)
                                            .getProduct_name());
                                } else {

                                    prodname.setText(getIntent().getStringExtra(
                                            "prod_name"));
                                }
                            }
                            String noofreviews = "";
                            if (reviewDatas.size() > 0) {
                                prodrating.setRating(Float.parseFloat(reviewDatas
                                        .get(0).getOverallreview()));
                                noofreviews = reviewDatas.get(0).getTotalReviews()
                                        + " Reviews";
                            } else {
                                prodrating.setRating(0F);
                                noofreviews = "0 Reviews";
                            }
                            noofreviewers.setText(noofreviews);
                            fillProductRelated();

                            fillSellers(storeslist);


                            fillAllOfflineProducts();

                            productspeclinear.removeAllViews();
                           /* Log.e("Prod Size", productdetails
                                    .getSpecificationList().size() + "");*/
                            if (productdetails.getSpecificationList().size() > 0) {
                                for (int i = 0; i < productdetails
                                        .getSpecificationList().size(); i++) {
                                    final SpecificationData data1 = productdetails
                                            .getSpecificationList().get(i);
                                    View view = getLayoutInflater().inflate(
                                            R.layout.activity_product_spec, null);
                                    TextView txtProCompareSpecTitle = (TextView) view
                                            .findViewById(R.id.txtProCompareSpecTitle);
                                    TextView txtProCompareSpecProductName1 = (TextView) view
                                            .findViewById(R.id.txtProCompareSpecProductName1);
                                    TextView txtProCompareSpecProductValue1 = (TextView) view
                                            .findViewById(R.id.txtProCompareSpecProductValue1);
                                    txtProCompareSpecTitle.setText(data1
                                            .getParentName());
                                    txtProCompareSpecProductName1.setText(data1
                                            .getOptionName());
                                    txtProCompareSpecProductValue1.setText(data1
                                            .getValue());
                                    productspeclinear.addView(view);
                                }
                            }

						/*
                         * adapter=new
						 * ProductGridAdapter(ProductSoloDetailsWithCompare
						 * .this, relatedproducts);
						 * releatedgrid.setAdapter(adapter);
						 */

						/*
                         * for(int i=0;i<offlinestores.size();i++){
						 * //Log.e("Addrs",
						 * storeslist12.get(i).getStoreslist().get
						 * (i).getAddress()); final int m=i; final List<Stores>
						 * offlinestoreslist=offlinestores; Log.e("Addrs_new",
						 * offlinestoreslist.get(m).getAddress()); View
						 * view=getLayoutInflater
						 * ().inflate(R.layout.activity_productsellers, null);
						 * ImageView
						 * affliatelogo=(ImageView)view.findViewById(R.
						 * id.affliatelogo); TextView
						 * productprice=(TextView)view
						 * .findViewById(R.id.productprice);
						 *
						 * Button
						 * gotostore=(Button)view.findViewById(R.id.gotostore);
						 * gotostore.setText("View Store");
						 * gotostore.setOnClickListener(new OnClickListener() {
						 *
						 * @Override public void onClick(View v) { Intent
						 * intent=new Intent(ProductSoloDetailsWithCompare.this,
						 * ProductOfflineMap.class);
						 * intent.putParcelableArrayListExtra
						 * ("storeslist",(ArrayList<? extends Parcelable>)
						 * offlinestoreslist); startActivity(intent); Intent
						 * intent=new Intent(ProductSoloDetailsWithCompare.this,
						 * ProductGotoStore.class);
						 * intent.putExtra("product_id",
						 * productdetailss.getProduct_id());
						 * intent.putExtra("store_id",
						 * storeslisst.get(m).getStore_id());
						 * intent.putExtra("pp_id",
						 * storeslisst.get(m).getPp_id());
						 * startActivity(intent); //
						 * Toast.makeText(ProductSoloDetailsWithCompare
						 * .this,storeslisst.get(m).getStore_id(),Toast.LENGTH_SHORT).show();
						 * } }); productprice.setText("Rs."+storeslist12.get(i).
						 * getOffline_price()); //
						 * Toast.makeText(getApplicationContext(),
						 * DBConnection.PRODUCT_IMGURL
						 * +storeslist.get(i).getAffliate_logo(), Toast.LENGTH_SHORT).show();
						 * loader
						 * .DisplayImage(DBConnection.OFFLINE_IMGURL+offlinestores
						 * .get(i).getOffline_storelogo(), affliatelogo);
						 * sellerlinear.addView(view); }
						 */

						/*
                         * String parent_child_id=getIntent().getStringExtra(
						 * "parent_child_id");
						 *
						 * for(int
						 * k=0;k<productdetails.getCategory_id().size();k++){
						 * Log.e("Size is",
						 * productdetails.getCategory_id().size()+"");
						 * Log.e("cat id ",
						 * productdetailslist.get(k).getCategory_id().get(k));
						 * Log.e("parent_child_id ", parent_child_id);
						 *
						 *
						 * if(productdetailslist.get(k).getCategory_id().get(k).
						 * equals(parent_child_id)){
						 * Toast.makeText(getApplicationContext(),
						 * "Position is "+k, Toast.LENGTH_SHORT).show(); catposition=k; break;
						 * } }
						 */
                            // String
                            // cashbackper=productdetails.getCategory_per().get(catposition);
                            // Toast.makeText(getApplicationContext(), cashbackper,
                            // Toast.LENGTH_SHORT).show();

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(ProductSoloDetailsWithCompare.this,
                                    "No data found", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void fillSellers(List<Stores> storeslist) {
        sellerlinear.removeAllViews();
        for (int i = 0; i < storeslist.size(); i++) {
            final List<Stores> storeslisst = storeslist;
            final int m = i;
            // Toast.makeText(this, ""+prodCommList.size(), Toast.LENGTH_SHORT).show();
//
            View view = getLayoutInflater().inflate(
                    R.layout.activity_productsellers, null);
            ImageView affliatelogo = (ImageView) view
                    .findViewById(R.id.affliatelogo);
            TextView productprice = (TextView) view
                    .findViewById(R.id.productprice);
            TextView productpriceNew = (TextView) view
                    .findViewById(R.id.productpriceNew);
            TextView cashbacktext = (TextView) view
                    .findViewById(R.id.cashbacktext);
            Button gotostore = (Button) view
                    .findViewById(R.id.gotostore);
            RatingBar storerating = (RatingBar) view
                    .findViewById(R.id.storerating);
            LayerDrawable drawable = (LayerDrawable) storerating
                    .getProgressDrawable();
            drawable.getDrawable(2).setColorFilter(
                    Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

            storerating.setRating(Float.parseFloat(storeslist
                    .get(i).getStorerating()));

            Log.i("OUTTTTT", "" + storeslist.get(i).getStockAvail().toString().equals("1"));
            if (storeslist.get(i).getStockAvail().toString().equals("1")) {
                gotostore.setText("Go to Store");
            } else if (storeslist.get(i).getStockAvail().toString().equals("0")) {
                gotostore.setText("Go to Store");
            } else if (storeslist.get(i).getStockAvail().toString().equals("2")) {
                gotostore.setText("Out of Stock");
            } else {
//                                    gotostore.setText("Out of Stock");
            }
            gotostore.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!manager.isLoggedIn()) {
                        Intent intent = new Intent(
                                ProductSoloDetailsWithCompare.this,
                                LoginActivity.class);
                        intent.putExtra("product_id",
                                productdetails.getProduct_id());
                        intent.putExtra("store_id", storeslisst
                                .get(m).getStore_id());
                        intent.putExtra("pp_id", storeslisst
                                .get(m).getPp_id());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(ProductSoloDetailsWithCompare.this, ProductGotoStore.class);
                        intent.putExtra("product_id", productdetails.getProduct_id());
                        intent.putExtra("store_id", storeslisst.get(m).getStore_id());
                        intent.putExtra("pp_id", storeslisst.get(m).getPp_id());
                        startActivity(intent);
                    }
                    // Toast.makeText(ProductSoloDetailsWithCompare.this,storeslisst.get(m).getStore_id(),Toast.LENGTH_SHORT).show();
                }
            });
            productprice.setText("Rs."
                    + storeslist.get(i).getProduct_price());
            // Toast.makeText(getApplicationContext(),
            // DBConnection.PRODUCT_IMGURL+storeslist.get(i).getAffliate_logo(),
            // Toast.LENGTH_SHORT).show();
            Picasso.with(ProductSoloDetailsWithCompare.this).load(DBConnection.AFFLIATE_IMGURL
                    + storeslist.get(i).getAffliate_logo()).placeholder(R.drawable.adslogo).resize(144, 0).into(affliatelogo);

            //final ProductDetails productdetailss = prodCommList.get(i);
            /*Log.e("pro store Id",productdetailss.getStore_id()+"avsd");
            Log.e("store Id",storeslist+"efgj");
            if (productdetailss.getStore_id() != null) {*/
            Double ppPrice = 0.0;
            Double weGive = 0.0;
            for (int ij = 0; ij < prodCommList.size(); ij++) {
                if (storeslist.get(i).getStore_id().trim().equals(prodCommList.get(ij).getStore_id().toString())) {
                    ppPrice = Double.parseDouble(storeslist.get(i).getProduct_price());
                    weGive = Double.parseDouble(prodCommList.get(ij).getWegive());
                    break;
                }


            }

            //  Log.e("Is avail",storeslist.get(i).getStore_id().contains(productdetailss.getStore_id().toString().trim())+"efgj");




                  /*  if (productdetailss.getStore_id()
                        .toString()
                        .equalsIgnoreCase(
                                storeslist.get(i).getStore_id())) {*/

                    /*cashbackprice = Double.parseDouble(storeslist
                            .get(i).getProduct_price())
                            * (Double.parseDouble(productdetails
                            .getWeget()) / 100);

                    cashbackprice=cashbackprice*(50/100);
*/
            Log.e("procommlist ", weGive + "");
            cashbackprice = (ppPrice * weGive) / 100;

            productpriceNew.setText("Rs." + format.format(Double.parseDouble(storeslist.get(i).getProduct_price()) - cashbackprice) + "* (Approx.)");
            Log.e("CB price", Math.round(cashbackprice)
                    + "");
            if (Math.round(cashbackprice) > 0) {
                cashbacktext.setText("+ Upto Rs."
                        + format.format(cashbackprice)
                        + " Cashback");
            } else {
                cashbacktext
                        .setText("No Cashback Available");
            }
                /*} else {
                    cashbacktext.setText("No Cashback Available");
                }*/
            sellerlinear.addView(view);
        }

    }


    public boolean validateComponents(String userratingtext, float userrating) {
        if (userrating <= 0.0) {
            Toast.makeText(ProductSoloDetailsWithCompare.this,
                    "Review Rating is Mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(userratingtext)) {
            Toast.makeText(ProductSoloDetailsWithCompare.this,
                    "Review Description Mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }

    }

    public void fillAllOfflineProducts() {

        if (ProductOverView.tempList.size() > 0) {
            for (int i = 0; i < ProductOverView.tempList.size(); i++) {
                final int m = i;
                View view = getLayoutInflater().inflate(
                        R.layout.activity_productsellers, null);
                ImageView affliatelogo = (ImageView) view
                        .findViewById(R.id.affliatelogo);
                TextView productprice = (TextView) view
                        .findViewById(R.id.productprice);
                TextView cashbacktext = (TextView) view
                        .findViewById(R.id.cashbacktext);

                Button gotostore = (Button) view.findViewById(R.id.gotostore);
                gotostore.setText("View Store");
                gotostore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                ProductSoloDetailsWithCompare.this,
                                ProductOfflineMap.class);
                        // intent.putParcelableArrayListExtra("storeslist",(ArrayList<?
                        // extends Parcelable>) ProductOverView.tempList);

                        intent.putExtra("latitude", ProductOverView.tempList
                                .get(m).getLatitude());
                        intent.putExtra("longitude", ProductOverView.tempList
                                .get(m).getLongtitude());
                        intent.putExtra("address", ProductOverView.tempList
                                .get(m).getAddress());
                        intent.putExtra("name", ProductOverView.tempList.get(m)
                                .getOffline_storename());

                        startActivity(intent);
                    }
                });
                productprice.setText("Rs."
                        + ProductOverView.tempList.get(i).getOffline_price());
                if (!ProductOverView.tempList.get(i).getOffline_offer()
                        .toString().equalsIgnoreCase(null)
                        || ProductOverView.tempList.get(i).getOffline_offer()
                        .toString().equalsIgnoreCase("")) {
                    if (ProductOverView.tempList.get(i)
                            .getOffline_offer() != "") {
                        cashbacktext.setText(ProductOverView.tempList.get(i)
                                .getOffline_offer() + "% Offer");
                    } else {
                        cashbacktext.setText("No offers available");
                    }

                } else {
                    cashbacktext.setText("No offers available");
                }
                // Toast.makeText(getApplicationContext(),
                // DBConnection.PRODUCT_IMGURL+storeslist.get(i).getAffliate_logo(),
                // Toast.LENGTH_SHORT).show();

                Picasso.with(ProductSoloDetailsWithCompare.this).load(DBConnection.OFFLINE_IMGURL
                        + ProductOverView.tempList.get(i)
                        .getOffline_storelogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(affliatelogo);

                sellerlinear.addView(view);
            }
        }

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(response);

                    if (response.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(ProductSoloDetailsWithCompare.this,
                                    "Review posted successfully", Toast.LENGTH_SHORT).show();
                            reviewdialog.dismiss();
                        } else {
                            Toast.makeText(ProductSoloDetailsWithCompare.this,
                                    "Failed to Review", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void fillAllReviews() {
        reviewalllinear.removeAllViews();

        if (reviewDatas.size() > 0) {
            for (int i = 0; i < reviewDatas.size(); i++) {
                View view = getLayoutInflater().inflate(
                        R.layout.activity_user_all_reviews, null);
                TextView ratingusername = (TextView) view
                        .findViewById(R.id.ratingusername);
                TextView ratinguserreview = (TextView) view
                        .findViewById(R.id.ratinguserreview);
                RatingBar userratingall = (RatingBar) view
                        .findViewById(R.id.userratingall);
                LayerDrawable drawable = (LayerDrawable) userratingall
                        .getProgressDrawable();
                drawable.getDrawable(2).setColorFilter(Color.YELLOW,
                        PorterDuff.Mode.SRC_ATOP);

                ratingusername.setText(reviewDatas.get(i).getUserName());
                ratinguserreview.setText(reviewDatas.get(i).getComments());
                userratingall.setRating(Float.parseFloat(reviewDatas.get(i)
                        .getRating()));
                reviewalllinear.addView(view);
            }
        }
    }

    public void fillProductRelated() {
        relatedlinear.removeAllViews();

        for (int i = 0; i < relatedproducts.size(); i++) {
            final int position = i;
            View view = getLayoutInflater().inflate(
                    R.layout.activity_product_grid_details, null);
            ImageView prodimg = (ImageView) view.findViewById(R.id.prodimg);
            TextView prodprice = (TextView) view.findViewById(R.id.prodprice);
            // TextView
            // prodminprice=(TextView)view.findViewById(R.id.prodminprice);
            TextView prodname = (TextView) view.findViewById(R.id.prodname);
            final ImageView prodcompare = (ImageView) view
                    .findViewById(R.id.prodcomparegrid);
            final ImageView gridFav = (ImageView) view
                    .findViewById(R.id.imgProductListingDetailFavourites);

            RatingBar prodrating = (RatingBar) view
                    .findViewById(R.id.prodrating);
			/*
			 * LayerDrawable stars = (LayerDrawable)
			 * prodrating.getProgressDrawable();
			 * stars.getDrawable(2).setColorFilter(Color.YELLOW,
			 * PorterDuff.Mode.SRC_ATOP);
			 */
            prodrating.setRating(Float.parseFloat(relatedproducts.get(i)
                    .getRating()));
            prodprice
                    .setText("Rs." + relatedproducts.get(i).getProduct_price());
            prodname.setText(relatedproducts.get(i).getProduct_name());
            // prodminprice.setText("Rs."+relatedproducts.get(i).getMrp());
            Picasso.with(ProductSoloDetailsWithCompare.this).load(DBConnection.PRODUCT_IMGURL
                    + relatedproducts.get(i).getProduct_image()).placeholder(R.drawable.placeholder).resize(144, 0).into(prodimg);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(
                            ProductSoloDetailsWithCompare.this,
                            ProductOverView.class);
                    intent.putExtra("parent_child_id",
                            relatedproducts.get(position).getParent_child_id());
                    intent.putExtra("prod_name", relatedproducts.get(position)
                            .getProduct_name());
                    intent.putExtra("prod_img", relatedproducts.get(position)
                            .getProduct_image());
                    intent.putExtra("prod_price", relatedproducts.get(position)
                            .getProduct_price());
                    intent.putExtra("prod_rating", relatedproducts
                            .get(position).getRating());
                    intent.putExtra("prod_id", relatedproducts.get(position)
                            .getProduct_id());
                    intent.putExtra("pp_id", relatedproducts.get(position)
                            .getPp_id());
                    intent.putExtra("prod_desc", relatedproducts.get(position)
                            .getKey_feature().toString().trim());
                    startActivity(intent);
                }
            });


            if (relatedproducts.get(position).isSelected()) {
                for (int j = 0; j < CompareProducts.productdetails.size(); j++) {
                    if (CompareProducts.productdetails.get(i).getProduct_id().equalsIgnoreCase(relatedproducts.get(position).getProduct_id())) {
                        prodcompare
                                .setImageResource(R.drawable.added_compare);
                    } else {
                        prodcompare
                                .setImageResource(R.drawable.prod_compare);
                    }
                }
            } else {
                prodcompare
                        .setImageResource(R.drawable.prod_compare);
            }

            if (relatedproducts.get(position).getProWishListStatus()
                    .equals(null)
                    || relatedproducts.get(position).getProWishListStatus()
                    .equals("")) {
                Log.e("", "Null");
            } else {
                if (Integer.parseInt(relatedproducts.get(position)
                        .getProWishListStatus().trim()) > 0) {
                    gridFav
                            .setImageResource(R.drawable.filled_heart);
                } else {
                    gridFav
                            .setImageResource(R.drawable.un_fillled_heart);
                }

            }

            gridFav
                    .setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (manager.checkLogin()) {

                                // Remove from wish list
                                if (relatedproducts.get(position).getProWishListStatus().equalsIgnoreCase("0")) {

                                    addToWishList(relatedproducts.get(position), 0, 2, gridFav, position);
                                    // list
                                } else {

                                    addToWishList(relatedproducts.get(position), 1, 2, gridFav, position);

                                    // list
                                }
                            } else {
                                startActivity(new Intent(
                                        ProductSoloDetailsWithCompare.this,
                                        com.application.ads.LoginActivity.class));
                            }

                        }
                    });

            prodcompare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ProductDetails productdetails = relatedproducts
                            .get(position);
                    if (!CompareProducts.isAlreadyInCompare(relatedproducts
                            .get(position))) {
                        if (CompareProducts.getProducts().size() >= 2) {
                            Toast.makeText(ProductSoloDetailsWithCompare.this, "Maximum compare 2 products",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            if (CompareProducts.addToCompare(relatedproducts
                                    .get(position))) {
                                prodcompare
                                        .setImageResource(R.drawable.added_compare);
								/*Toast.makeText(context,
										"Successfully added to compare list",
										Toast.LENGTH_LONG).show();*/
                                relatedproducts.get(position).setSelected(true);
                            }
                        }
                    } else {
                        if (CompareProducts.removeFromCompare(relatedproducts
                                .get(position))) {
                            prodcompare
                                    .setImageResource(R.drawable.prod_compare);
							/*Toast.makeText(context,
									"Successfully removed from compare list",
									Toast.LENGTH_LONG).show();*/
                            relatedproducts.get(position).setSelected(false);
                        }
                    }
                    // Toast.makeText(ProductSoloDetailsWithCompare.this,
                    // CompareProducts.productdetails.size()+"", Toast.LENGTH_SHORT).show();
                }
            });
            relatedlinear.addView(view);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }


    private void addToWishList(ProductDetails productdetails, final int i, final int i1, final ImageView gridFav, final int position) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(ProductSoloDetailsWithCompare.this);
        String url = urlAddOrRemoveProductsFromWishList + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0") + "&product_id=" + productdetails.getProduct_id() + "&wish_list_status=" + i;

        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {
                            if (i1 == 2) {
                                if (i == 0) {
                                    Toast.makeText(ProductSoloDetailsWithCompare.this, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                    gridFav.setImageResource(R.drawable.filled_heart);
                                    relatedproducts.get(position).setProWishListStatus("1");
                                } else if (i == 1) {
                                    Toast.makeText(ProductSoloDetailsWithCompare.this, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                    gridFav.setImageResource(R.drawable.heart);
                                    relatedproducts.get(position).setProWishListStatus("0");

                                }

                            } else {
                                if (i == 0) {
                                    Toast.makeText(ProductSoloDetailsWithCompare.this, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                    imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
                                } else if (i == 1) {
                                    Toast.makeText(ProductSoloDetailsWithCompare.this, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                    imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
                                }
                            }

                        } else {
                            Toast.makeText(ProductSoloDetailsWithCompare.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProductSoloDetailsWithCompare.this,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:
                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // productdetailslist.clear();
        fillProductRelated();

    }

    public void shareMessage(String msg) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(sharingIntent, "Acha Discount"));
    }

    public void makeGraph() {

        GraphView graph = (GraphView) findViewById(R.id.graph);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd");

        for (int i = 0; i < productDailyDetailsList.size(); i++) {
            String data = productDailyDetailsList.get(i).getProductDailyDate();


            String r = data.replace("Date.UTC(", "");
            String s = r.replace(")", "");
            String sa = s.split(",")[2];


            Log.i("price", productDailyDetailsList.get(i).getProductDailyPrice());
            Log.i("s value", sa);


            series = new LineGraphSeries<>(new DataPoint[]{
                    new DataPoint(0, 1),
                    new DataPoint(Double.parseDouble(sa), Double.parseDouble(productDailyDetailsList.get(i).getProductDailyPrice())),

            });

        }
        graph.addSeries(series);


    }

    public class ExecuteLoadProductDetails extends
            AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String productid;

        public ExecuteLoadProductDetails(String productid) {
            this.productid = productid;
            Log.e("productid", productid);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(ProductSoloDetailsWithCompare.this,
                    "", "Loading..", true, false);
            dialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("product_id", productid));
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            Log.e("param", param.toString());
            JSONObject object = parser.makeHttpRequest(ProductSoloDetailsWithCompare.this,
                    urlLoadProductDetailWithCompare, "GET", param);
            // Log.e("Object", object.toString());
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();
            productdetails = new ProductDetails();
            prodCommList = new ArrayList<>();
            productdetailslist = new ArrayList<ProductDetails>();
            // offlinestores=new ArrayList<Stores>();
            reviewDatas = new ArrayList<ReviewData>();
            List<Stores> storeslist = new ArrayList<Stores>();
            relatedproducts = new ArrayList<ProductDetails>();
            // Toast.makeText(ProductSoloDetailsWithCompare.this,
            // result.trim().length()+" Length", Toast.LENGTH_SHORT).show();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray subcatarray = object.getJSONArray("products");
                        for (int i = 0; i < subcatarray.length(); i++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(i);
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productdetails.setProduct_image(subcatobj
                                    .getString("product_image"));
                            productdetails.setKey_feature(subcatobj
                                    .getString("key_feature"));
                            productdetails.setProduct_name(subcatobj
                                    .getString("product_name"));
                            productdetails.setProWishListStatus(subcatobj.getString("wishlist_status"));
                            // productdetails.setProd_desc(subcatobj.getString("description"));

                            JSONArray storesarray = object
                                    .getJSONArray("stores");

                            for (int j = 0; j < storesarray.length(); j++) {
                                JSONObject subcatobj1 = storesarray
                                        .getJSONObject(j);
                                Stores storedetails = new Stores();
                                storedetails.setStore_id(subcatobj1
                                        .getString("store_id"));
                                storedetails.setProduct_price(subcatobj1
                                        .getString("product_price"));
                                storedetails.setAffliate_logo(subcatobj1
                                        .getString("affiliate_logo"));
                                storedetails.setAffliate_name(subcatobj1
                                        .getString("affiliate_name"));
                                storedetails.setProduct_url(subcatobj1
                                        .getString("product_url"));
                                storedetails.setPp_id(subcatobj1
                                        .getString("pp_id"));
                                storedetails.setStockAvail(subcatobj1
                                        .getString("stock"));
                                storedetails.setStorerating(subcatobj1
                                        .getString("rating"));
                                storeslist.add(storedetails);
                            }

                            JSONArray reviewarray = object
                                    .getJSONArray("reviews");

                            if (reviewarray.length() > 0) {

                                for (int j = 0; j < reviewarray.length(); j++) {
                                    JSONObject subcatobj1 = reviewarray
                                            .getJSONObject(j);
                                    ReviewData data = new ReviewData();
                                    data.setUserName(subcatobj1
                                            .getString("user_name"));
                                    data.setRating(subcatobj1
                                            .getString("rating"));
                                    data.setComments(subcatobj1
                                            .getString("review_title"));
                                    data.setTotalReviews(subcatobj1
                                            .getString("total_reviews"));
                                    data.setOverallreview(subcatobj1
                                            .getString("over_all_rating"));
                                    reviewDatas.add(data);
                                    /*
                                     * Stores storedetails=new Stores();
									 * storedetails
									 * .setStore_id(subcatobj1.getString
									 * ("store_id"));
									 * storedetails.setProduct_price
									 * (subcatobj1.getString("product_price"));
									 * storedetails
									 * .setAffliate_logo(subcatobj1.getString
									 * ("affiliate_logo"));
									 * storedetails.setAffliate_name
									 * (subcatobj1.getString("affiliate_name"));
									 * storedetails
									 * .setProduct_url(subcatobj1.getString
									 * ("product_url"));
									 * storedetails.setPp_id(subcatobj1
									 * .getString("pp_id"));
									 * storeslist.add(storedetails);
									 */

                                }

                                fillAllReviews();
                            }
                            /*
                             * JSONArray offlinestoresarray=object.getJSONArray(
							 * "offlinestores");
							 *
							 * if(offlinestoresarray.length()>0){
							 *
							 * for (int j = 0; j < offlinestoresarray.length();
							 * j++) { JSONObject subcatobj1 =
							 * offlinestoresarray.getJSONObject(j); Stores
							 * storedetails1=new Stores();
							 * storedetails1.setOffline_price
							 * (subcatobj1.getString("price"));
							 * storedetails1.setOffline_offer
							 * (subcatobj1.getString("offline_offer"));
							 * JSONArray
							 * offlinestorearray1=subcatobj1.getJSONArray
							 * ("storedetails"); for(int
							 * ij=0;ij<offlinestorearray1.length();ij++){ Stores
							 * storedetails=new Stores(); JSONObject
							 * object2=offlinestorearray1.getJSONObject(j);
							 * storedetails
							 * .setOffline_storename(object2.getString
							 * ("store_name"));
							 * storedetails.setOffline_storelogo
							 * (object2.getString("store_logo"));
							 * storedetails.setLatitude
							 * (object2.getString("latitude"));
							 * storedetails.setLongtitude
							 * (object2.getString("longtitude"));
							 * storedetails.setAddress
							 * (object2.getString("address"));
							 * offlinestores.add(storedetails);
							 * data1.setCatId(object1.getString("category_id"));
							 * data1
							 * .setCatName(object1.getString("category_name"));
							 *
							 * } storedetails1.setStores(offlinestores); } }
							 */
                            // ProductOverView.tempList.size()
                            JSONArray cashbackarray = object
                                    .getJSONArray("cashback");
                            if (cashbackarray.length() > 0) {
                                for (int j = 0; j < cashbackarray.length(); j++) {
                                    JSONObject subcatobj2 = cashbackarray
                                            .getJSONObject(j);
                                    /*
									 * String categoryid=subcatobj2.getString(
									 * "cashback_catid");
									 * productdetails.setCategory_id
									 * (Arrays.asList
									 * (categoryid.split("\\s*,\\s*"))); String
									 * categorycashper
									 * =subcatobj2.getString("cashback_perc");
									 * productdetails
									 * .setCategory_per(Arrays.asList
									 * (categorycashper.split("\\s*,\\s*")));
									 */
                                    ProductDetails data = new ProductDetails();
                                    data.setWeget(subcatobj2
                                            .getString("weget"));
                                    data.setWegive(subcatobj2
                                            .getString("wegive"));
                                    data.setStore_id(subcatobj2
                                            .getString("store_id"));

                                    prodCommList.add(data);
                                }
                            }
                            if (object.has("relproducts")) {
                                JSONArray relproductsarray = object
                                        .getJSONArray("relproducts");
                                if (relproductsarray.length() > 0) {
                                    for (int j = 0; j < relproductsarray
                                            .length(); j++) {
                                        JSONObject subcatobj4 = relproductsarray
                                                .getJSONObject(j);
                                        ProductDetails relproddetails = new ProductDetails();
                                        relproddetails.setPp_id(subcatobj4
                                                .getString("pp_id"));
                                        relproddetails
                                                .setProduct_price(subcatobj4
                                                        .getString("product_price"));
                                        relproddetails.setRating(subcatobj4
                                                .getString("rating"));
                                        relproddetails
                                                .setProduct_name(subcatobj4
                                                        .getString("prod_name"));
                                        relproddetails
                                                .setProduct_parent(subcatobj4
                                                        .getString("parent_child_id"));
                                        relproddetails.setMrp(subcatobj4
                                                .getString("mrp"));
                                        relproddetails.setProduct_id(subcatobj4
                                                .getString("product_id"));
                                        relproddetails
                                                .setProduct_image(subcatobj4
                                                        .getString("product_image"));
                                        relproddetails
                                                .setKey_feature(subcatobj4
                                                        .getString("key_feature"));
                                        relproddetails
                                                .setProWishListStatus(subcatobj4
                                                        .getString("wishlist_status"));
                                        relproddetails
                                                .setParent_child_id(subcatobj4.getString("parent_child_id"));
                                        relatedproducts.add(relproddetails);
                                    }
                                }
                            }
                            ArrayList<SpecificationData> specifications = new ArrayList<SpecificationData>();
                            if (object.has("specification")) {
                                JSONArray array1 = object
                                        .getJSONArray("specification");
                                for (int ij = 0; ij < array1.length(); ij++) {
                                    JSONObject specObject = array1
                                            .getJSONObject(ij);
                                    SpecificationData data1 = new SpecificationData();
                                    data1.setOptionId(specObject
                                            .getString("specid"));
                                    data1.setOptionName(specObject
                                            .getString("specification"));
                                    data1.setParentId(specObject
                                            .getString("parant_id"));
                                    data1.setParentName(specObject
                                            .getString("parant_name"));
                                    data1.setValue(specObject
                                            .getString("values"));
                                    specifications.add(data1);
                                }
                                productdetails
                                        .setSpecificationList(specifications);
                            }
                            productdetails.setStorelist(storeslist);
                            productdetailslist.add(productdetails);
                        }

                        if (productdetailslist.get(0).getProWishListStatus()
                                .equals(null)
                                || productdetailslist.get(0)
                                .getProWishListStatus().equals("")) {
                            Log.e("", "Null");
                        } else {
                            if (Integer.parseInt(productdetailslist.get(0)
                                    .getProWishListStatus().trim()) > 0) {
                                imgProductListingDetailFavourites
                                        .setImageResource(R.drawable.filled_heart);
                            } else {
                                imgProductListingDetailFavourites
                                        .setImageResource(R.drawable.un_fillled_heart);
                            }

                        }
                       /* shareProdUrl.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                shareMessage(productdetailslist.get(0).getProduct_url());
                            }
                        });
*/
                        if (storeslist != null) {
                            if (storeslist.size() > 0) {
                                prodprice.setText("Rs."
                                        + storeslist.get(0).getProduct_price());
                            }
                        }
                        if (productdetailslist != null) {
                            if (productdetailslist.size() > 0) {
                                prodname.setText(productdetailslist.get(0)
                                        .getProduct_name());
                            } else {

                                prodname.setText(getIntent().getStringExtra(
                                        "prod_name"));
                            }
                        }
                        String noofreviews = "";
                        if (reviewDatas.size() > 0) {
                            prodrating.setRating(Float.parseFloat(reviewDatas
                                    .get(0).getOverallreview()));
                            noofreviews = reviewDatas.get(0).getTotalReviews()
                                    + " Reviews";
                        } else {
                            prodrating.setRating(0F);
                            noofreviews = "0 Reviews";
                        }
                        noofreviewers.setText(noofreviews);
                        fillProductRelated();


                        for (int i = 0; i < storeslist.size(); i++) {
                            final List<Stores> storeslisst = storeslist;
                            final int m = i;
                            final ProductDetails productdetailss = productdetails;
                            View view = getLayoutInflater().inflate(
                                    R.layout.activity_productsellers, null);
                            ImageView affliatelogo = (ImageView) view
                                    .findViewById(R.id.affliatelogo);
                            TextView productprice = (TextView) view
                                    .findViewById(R.id.productprice);
                            TextView cashbacktext = (TextView) view
                                    .findViewById(R.id.cashbacktext);
                            Button gotostore = (Button) view
                                    .findViewById(R.id.gotostore);
                            RatingBar storerating = (RatingBar) view
                                    .findViewById(R.id.storerating);
                            LayerDrawable drawable = (LayerDrawable) storerating
                                    .getProgressDrawable();
                            drawable.getDrawable(2).setColorFilter(
                                    Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                            storerating.setRating(Float.parseFloat(storeslist
                                    .get(i).getStorerating()));

                            if (storeslist.get(i).getStockAvail().toString()
                                    .equals("1")) {
                                gotostore.setText("Go to Store");
                            } else if (storeslist.get(i).getStockAvail().toString().equals("0")) {
                                gotostore.setText("Go to Store");
                            } else if (storeslist.get(i).getStockAvail().toString().equals("2")) {
                                gotostore.setText("Out of Stock");
                            } else {
//                                gotostore.setText("Out of Stock");
                            }
                            gotostore.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!manager.isLoggedIn()) {
                                        Intent intent = new Intent(
                                                ProductSoloDetailsWithCompare.this,
                                                LoginActivity.class);
                                        intent.putExtra("product_id",
                                                productdetailss.getProduct_id());
                                        intent.putExtra("store_id", storeslisst
                                                .get(m).getStore_id());
                                        intent.putExtra("pp_id", storeslisst
                                                .get(m).getPp_id());
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(
                                                ProductSoloDetailsWithCompare.this,
                                                ProductGotoStore.class);
                                        intent.putExtra("product_id",
                                                productdetailss.getProduct_id());
                                        intent.putExtra("store_id", storeslisst
                                                .get(m).getStore_id());
                                        intent.putExtra("pp_id", storeslisst
                                                .get(m).getPp_id());
                                        startActivity(intent);
                                    }
                                    // Toast.makeText(ProductSoloDetailsWithCompare.this,storeslisst.get(m).getStore_id(),Toast.LENGTH_SHORT).show();
                                }
                            });
                            productprice.setText("Rs."
                                    + storeslist.get(i).getProduct_price());
                            // Toast.makeText(getApplicationContext(),
                            // DBConnection.PRODUCT_IMGURL+storeslist.get(i).getAffliate_logo(),
                            // Toast.LENGTH_SHORT).show();
                            Picasso.with(ProductSoloDetailsWithCompare.this).load(DBConnection.AFFLIATE_IMGURL
                                    + storeslist.get(i).getAffliate_logo()).placeholder(R.drawable.adslogo).resize(144, 0).into(affliatelogo);


                            if (productdetailss.getStore_id() != null) {

                                if (productdetailss.getStore_id()
                                        .toString()
                                        .equalsIgnoreCase(
                                                storeslist.get(i).getStore_id())) {

                                   /* cashbackprice = Double.parseDouble(storeslist
                                            .get(i).getProduct_price())
                                            * (Double.parseDouble(productdetails
                                            .getWeget()) / 100);*/

                                    cashbackprice = Double.parseDouble(storeslist
                                            .get(i).getProduct_price())
                                            * (Double.parseDouble(prodCommList.get(0)
                                            .getWegive()) / 100);

                                    Log.e("CB price", Math.round(cashbackprice)
                                            + "");
                                    if (Math.round(cashbackprice) > 0) {
                                        cashbacktext.setText("+ Upto Rs."
                                                + cashbackprice
                                                + " Cashback");
                                    } else {
                                        cashbacktext
                                                .setText("No Cashback Available");
                                    }
                                } else {
                                    cashbacktext.setText("No Cashback Available");
                                }

                            } else {
                                cashbacktext.setText("No Cashback Available");
                            }
                            sellerlinear.addView(view);
                        }

                        fillAllOfflineProducts();

                        productspeclinear.removeAllViews();
                        Log.e("Prod Size", productdetails
                                .getSpecificationList().size() + "");
                        if (productdetails.getSpecificationList().size() > 0) {
                            for (int i = 0; i < productdetails
                                    .getSpecificationList().size(); i++) {
                                final SpecificationData data1 = productdetails
                                        .getSpecificationList().get(i);
                                View view = getLayoutInflater().inflate(
                                        R.layout.activity_product_spec, null);
                                TextView txtProCompareSpecTitle = (TextView) view
                                        .findViewById(R.id.txtProCompareSpecTitle);
                                TextView txtProCompareSpecProductName1 = (TextView) view
                                        .findViewById(R.id.txtProCompareSpecProductName1);
                                TextView txtProCompareSpecProductValue1 = (TextView) view
                                        .findViewById(R.id.txtProCompareSpecProductValue1);
                                txtProCompareSpecTitle.setText(data1
                                        .getParentName());
                                txtProCompareSpecProductName1.setText(data1
                                        .getOptionName());
                                txtProCompareSpecProductValue1.setText(data1
                                        .getValue());
                                productspeclinear.addView(view);
                            }
                        }

						/*
						 * adapter=new
						 * ProductGridAdapter(ProductSoloDetailsWithCompare
						 * .this, relatedproducts);
						 * releatedgrid.setAdapter(adapter);
						 */

						/*
						 * for(int i=0;i<offlinestores.size();i++){
						 * //Log.e("Addrs",
						 * storeslist12.get(i).getStoreslist().get
						 * (i).getAddress()); final int m=i; final List<Stores>
						 * offlinestoreslist=offlinestores; Log.e("Addrs_new",
						 * offlinestoreslist.get(m).getAddress()); View
						 * view=getLayoutInflater
						 * ().inflate(R.layout.activity_productsellers, null);
						 * ImageView
						 * affliatelogo=(ImageView)view.findViewById(R.
						 * id.affliatelogo); TextView
						 * productprice=(TextView)view
						 * .findViewById(R.id.productprice);
						 *
						 * Button
						 * gotostore=(Button)view.findViewById(R.id.gotostore);
						 * gotostore.setText("View Store");
						 * gotostore.setOnClickListener(new OnClickListener() {
						 *
						 * @Override public void onClick(View v) { Intent
						 * intent=new Intent(ProductSoloDetailsWithCompare.this,
						 * ProductOfflineMap.class);
						 * intent.putParcelableArrayListExtra
						 * ("storeslist",(ArrayList<? extends Parcelable>)
						 * offlinestoreslist); startActivity(intent); Intent
						 * intent=new Intent(ProductSoloDetailsWithCompare.this,
						 * ProductGotoStore.class);
						 * intent.putExtra("product_id",
						 * productdetailss.getProduct_id());
						 * intent.putExtra("store_id",
						 * storeslisst.get(m).getStore_id());
						 * intent.putExtra("pp_id",
						 * storeslisst.get(m).getPp_id());
						 * startActivity(intent); //
						 * Toast.makeText(ProductSoloDetailsWithCompare
						 * .this,storeslisst.get(m).getStore_id(),Toast.LENGTH_SHORT).show();
						 * } }); productprice.setText("Rs."+storeslist12.get(i).
						 * getOffline_price()); //
						 * Toast.makeText(getApplicationContext(),
						 * DBConnection.PRODUCT_IMGURL
						 * +storeslist.get(i).getAffliate_logo(), Toast.LENGTH_SHORT).show();
						 * loader
						 * .DisplayImage(DBConnection.OFFLINE_IMGURL+offlinestores
						 * .get(i).getOffline_storelogo(), affliatelogo);
						 * sellerlinear.addView(view); }
						 */

						/*
						 * String parent_child_id=getIntent().getStringExtra(
						 * "parent_child_id");
						 *
						 * for(int
						 * k=0;k<productdetails.getCategory_id().size();k++){
						 * Log.e("Size is",
						 * productdetails.getCategory_id().size()+"");
						 * Log.e("cat id ",
						 * productdetailslist.get(k).getCategory_id().get(k));
						 * Log.e("parent_child_id ", parent_child_id);
						 *
						 *
						 * if(productdetailslist.get(k).getCategory_id().get(k).
						 * equals(parent_child_id)){
						 * Toast.makeText(getApplicationContext(),
						 * "Position is "+k, Toast.LENGTH_SHORT).show(); catposition=k; break;
						 * } }
						 */
                        // String
                        // cashbackper=productdetails.getCategory_per().get(catposition);
                        // Toast.makeText(getApplicationContext(), cashbackper,
                        // Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ProductSoloDetailsWithCompare.this,
                                "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    Log.e("Error", e + "");
                }


            }
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    private class ExecuteAddReview extends AsyncTask<String, String, String> {
        String reviewcomment;
        float reviewrating;
        ProgressDialog dialog;

        public ExecuteAddReview(String usercomment, float userrating) {
            this.reviewcomment = usercomment;
            this.reviewrating = userrating;

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = new ProgressDialog(ProductSoloDetailsWithCompare.this);
            dialog.setMessage("Loading..");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("usercomment", reviewcomment));
            param.add(new BasicNameValuePair("rating", reviewrating + ""));
            param.add(new BasicNameValuePair("productid", prod_id));
            param.add(new BasicNameValuePair("userid", preferences.getString(
                    SessionManager.KEY_USERID, "1")));

            JSONObject jsonObject = parser.makeHttpRequest(ProductSoloDetailsWithCompare.this, urlAddReviews,
                    "GET", param);

            return jsonObject.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.trim().length() > 0) {

                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Toast.makeText(ProductSoloDetailsWithCompare.this,
                                "Review submitted successfully and waiting for admin apporval..!", Toast.LENGTH_SHORT).show();
                        reviewdialog.dismiss();
                    } else {
                        Toast.makeText(ProductSoloDetailsWithCompare.this,
                                "Failed to Review", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e("Except", e.toString());
                }

            }

        }

    }

    private class ExecuteProductPrice extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("product_id", prod_id));
            JSONObject object = parser.makeHttpRequest(ProductSoloDetailsWithCompare.this, urlLoadProductDailyPrice, "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            productDailyDetailsList = new ArrayList<>();
            try {
                if (s.trim().length() > 0) {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.length() > 0) {

                        JSONArray jsonArray = jsonObject.getJSONArray(prod_id);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ProductDetails details = new ProductDetails();
                                details.setProductDailyPrice(object.getString("price"));
                                details.setProductDailyDate(object.getString("date"));
                                productDailyDetailsList.add(details);
                            }

                        }
                    }
                    makeGraph();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
