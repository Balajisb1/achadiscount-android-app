package com.application.ads;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.CouponGridAdapter;
import com.application.ads.data.CouponData;
import com.application.ads.data.ProductData;
import com.application.ads.data.StoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.ExpandableHeightGridView;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.analytics.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class TopCouponsFragment extends Fragment {
    public static String toolbartitle;
    final String urlLoadTopCoupons = DBConnection.BASEURL + "load_couponlist.php";
    JSONObject object;
    JSONParser parser;
    ImageButton searchbtn;
    Button btnTopStores, btnTopCashBackOffers, btnSalableCoupon;
    List<ProductData> productList;
    Tracker t;
    CouponGridAdapter adapter;
    ExpandableHeightGridView coupongrid;
    List<StoreData> storesList;
    int rowWiseCount = 2;
    Button btnTopCouponAll, btnTopCouponA, btnTopCouponB, btnTopCouponC,
            btnTopCouponD, btnTopCouponE, btnTopCouponF, btnTopCouponG,
            btnTopCouponH, btnTopCouponI, btnTopCouponJ, btnTopCouponK,
            btnTopCouponL, btnTopCouponM, btnTopCouponN, btnTopCouponO,
            btnTopCouponP, btnTopCouponQ, btnTopCouponR, btnTopCouponS,
            btnTopCouponT, btnTopCouponU, btnTopCouponV, btnTopCouponW,
            btnTopCouponX, btnTopCouponY, btnTopCouponZ;
    int numOfCount = 2;
    AutoCompleteTextView searchedittext;
    SharedPreferences preferences;
    SessionManager manager;
    List<String> names;
    Bundle bundle;
    SwipeRefreshLayout mSwipeRefreshLayout;


   /* public TopCouponsFragment(TextView toolbartitle) {
        this.toolbartitle = toolbartitle;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_coupon_grid, null);
        bundle = getArguments();
        toolbartitle = bundle.getString("toolbartitle");
        toolbartitle = "Coupons/All";
        coupongrid = (ExpandableHeightGridView) view
                .findViewById(R.id.coupongrid);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);


        storesList = new ArrayList<StoreData>();
        btnTopStores = (Button) view.findViewById(R.id.btnTopStores);
        btnTopCashBackOffers = (Button) view
                .findViewById(R.id.btnTopCashBackOffers);
        btnSalableCoupon = (Button) view.findViewById(R.id.btnSalableCoupon);

        btnSalableCoupon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SalableCoupons.class);
                startActivity(intent);

            }
        });
        btnTopCashBackOffers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),
                        TopCashbackOffers.class);
                startActivity(intent);

            }
        });
        btnTopStores.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TopStoresList.class);
                startActivity(intent);
            }
        });

        parser = new JSONParser();

        storesList = new ArrayList<StoreData>();

        manager = new SessionManager(getActivity());
        preferences = getActivity().getSharedPreferences(
                SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        btnTopCouponAll = (Button) view.findViewById(R.id.btnTopCouponAll);
        btnTopCouponAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadAllCouponList("").execute();
//                List<NameValuePair> param = new ArrayList<NameValuePair>();
//                param.add(new BasicNameValuePair("store_name", startLetter));

             /*   String url=urlLoadTopCoupons+"?store_name=";
                volleyJsonObjectRequest(url);*/
                toolbartitle = "Coupons/All";
            }
        });

        btnTopCouponA = (Button) view.findViewById(R.id.btnTopCouponA);
        btnTopCouponA.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("A").execute();
/*
                String url=urlLoadTopCoupons+"?store_name=A";
                volleyJsonObjectRequest(url);
*/
                toolbartitle = "Coupons/A";
            }
        });
        btnTopCouponB = (Button) view.findViewById(R.id.btnTopCouponB);
        btnTopCouponB.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("B").execute();
/*
                String url=urlLoadTopCoupons+"?store_name=B";
                volleyJsonObjectRequest(url);
*/
                toolbartitle = "Coupons/B";
            }
        });

        btnTopCouponC = (Button) view.findViewById(R.id.btnTopCouponC);
        btnTopCouponC.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("C").execute();
  /*              String url=urlLoadTopCoupons+"?store_name=C";
                volleyJsonObjectRequest(url);
  */
                toolbartitle = "Coupons/C";
            }
        });
        btnTopCouponD = (Button) view.findViewById(R.id.btnTopCouponD);
        btnTopCouponD.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("D").execute();
   /*             String url=urlLoadTopCoupons+"?store_name=D";
                volleyJsonObjectRequest(url);
   */
                toolbartitle = "Coupons/D";
            }
        });
        btnTopCouponE = (Button) view.findViewById(R.id.btnTopCouponE);
        btnTopCouponE.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("E").execute();
 /*               String url=urlLoadTopCoupons+"?store_name=E";
                volleyJsonObjectRequest(url);
 */
                toolbartitle = "Coupons/E";
            }
        });
        btnTopCouponF = (Button) view.findViewById(R.id.btnTopCouponF);
        btnTopCouponF.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("F").execute();
      /*          String url=urlLoadTopCoupons+"?store_name=F";
                volleyJsonObjectRequest(url);
      */
                toolbartitle = "Coupons/F";
            }
        });
        btnTopCouponG = (Button) view.findViewById(R.id.btnTopCouponG);
        btnTopCouponG.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("G").execute();
      /*          String url=urlLoadTopCoupons+"?store_name=G";
                volleyJsonObjectRequest(url);
      */
                toolbartitle = "Coupons/G";
            }
        });
        btnTopCouponH = (Button) view.findViewById(R.id.btnTopCouponH);
        btnTopCouponH.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("H").execute();
      /*          String url=urlLoadTopCoupons+"?store_name=H";
                volleyJsonObjectRequest(url);
      */
                toolbartitle = "Coupons/H";
            }
        });
        btnTopCouponI = (Button) view.findViewById(R.id.btnTopCouponI);
        btnTopCouponI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("I").execute();
      /*          String url=urlLoadTopCoupons+"?store_name=I";
                volleyJsonObjectRequest(url);
      */
                toolbartitle = "Coupons/I";
            }
        });
        btnTopCouponJ = (Button) view.findViewById(R.id.btnTopCouponJ);
        btnTopCouponJ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("J").execute();
      /*          String url=urlLoadTopCoupons+"?store_name=J";
                volleyJsonObjectRequest(url);
      */
                toolbartitle = "Coupons/J";
            }
        });
        btnTopCouponK = (Button) view.findViewById(R.id.btnTopCouponK);
        btnTopCouponK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("K").execute();
/*
                String url=urlLoadTopCoupons+"?store_name=K";
                volleyJsonObjectRequest(url);
*/
                toolbartitle = "Coupons/K";
            }
        });
        btnTopCouponL = (Button) view.findViewById(R.id.btnTopCouponL);
        btnTopCouponL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("L").execute();
/*
                String url=urlLoadTopCoupons+"?store_name=L";
                volleyJsonObjectRequest(url);
*/
                toolbartitle = "Coupons/L";
            }
        });
        btnTopCouponM = (Button) view.findViewById(R.id.btnTopCouponM);
        btnTopCouponM.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("M").execute();
/*
                String url=urlLoadTopCoupons+"?store_name=M";
                volleyJsonObjectRequest(url);
*/
                toolbartitle = "Coupons/M";
            }
        });
        btnTopCouponN = (Button) view.findViewById(R.id.btnTopCouponN);
        btnTopCouponN.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("N").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=N";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/N";
            }
        });
        btnTopCouponO = (Button) view.findViewById(R.id.btnTopCouponO);
        btnTopCouponO.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("O").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=O";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/O";
            }
        });
        btnTopCouponP = (Button) view.findViewById(R.id.btnTopCouponP);
        btnTopCouponP.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("P").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=P";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/P";
            }
        });
        btnTopCouponQ = (Button) view.findViewById(R.id.btnTopCouponQ);
        btnTopCouponQ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Q").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=Q";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/Q";
            }
        });
        btnTopCouponR = (Button) view.findViewById(R.id.btnTopCouponR);
        btnTopCouponR.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("R").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=R";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/R";
            }
        });
        btnTopCouponS = (Button) view.findViewById(R.id.btnTopCouponS);
        btnTopCouponS.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("S").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=S";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/S";
            }
        });
        btnTopCouponT = (Button) view.findViewById(R.id.btnTopCouponT);
        btnTopCouponT.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("T").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=T";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/T";
            }
        });
        btnTopCouponU = (Button) view.findViewById(R.id.btnTopCouponU);
        btnTopCouponU.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("U").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=U";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/U";
            }
        });
        btnTopCouponV = (Button) view.findViewById(R.id.btnTopCouponV);
        btnTopCouponV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("V").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=V";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/V";
            }
        });
        btnTopCouponW = (Button) view.findViewById(R.id.btnTopCouponW);
        btnTopCouponW.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("W").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=W";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/W";
            }
        });
        btnTopCouponX = (Button) view.findViewById(R.id.btnTopCouponX);
        btnTopCouponX.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("X").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=x";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/X";
            }
        });
        btnTopCouponY = (Button) view.findViewById(R.id.btnTopCouponY);
        btnTopCouponY.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Y").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=Y";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/Y";
            }
        });
        btnTopCouponZ = (Button) view.findViewById(R.id.btnTopCouponZ);
        btnTopCouponZ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Z").execute();
        /*        String url=urlLoadTopCoupons+"?store_name=Z";
                volleyJsonObjectRequest(url);
        */
                toolbartitle = "Coupons/Z";
            }
        });


        coupongrid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
              /*  start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;*/
                int topRowVerticalPosition = (coupongrid == null || coupongrid.getChildCount() == 0) ? 0 : coupongrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                /*
                 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                // Log.e("totalItemCount", totalcount+"");


            }

        });
        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new LoadAllCouponList("").execute();
                /*String url=urlLoadTopCoupons+"?store_name=";
                volleyJsonObjectRequest(url);
                */
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        new LoadAllCouponList("").execute();
        return view;
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    storesList.clear();

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            storesList = new ArrayList<StoreData>();

                            JSONArray array = jsonObject.getJSONArray("coupons");
                            for (int m = 0; m < array.length(); m++) {
                                JSONObject object1 = array.getJSONObject(m);
                                JSONObject object2 = object1
                                        .getJSONObject("store_detail");
                                StoreData storeData = new StoreData();
                                storeData.setStoreId(object2.getString("id"));
                                storeData.setName(object2.getString("name"));
                                storeData.setType(object2.getString("type"));
                                storeData.setAmountPercentage(object2
                                        .getString("amount_percentage"));
                                storeData.setCoupons(object2.getString("coupons"));
                                storeData.setImgLogo(object2.getString("img_logo"));
                                storeData.setTermsConditions(object2
                                        .getString("terms_and_conditions"));
                                storeData.setHowToGetThisOffer(object2
                                        .getString("how_to_get_this_offer"));
                                storeData.setDesc(object2.getString("desc"));

                                JSONObject object3 = object1
                                        .getJSONObject("coupon_detail");
                                CouponData data = new CouponData();
                                data.setOfferName(object3.getString("offer_name"));
                                data.setTitle(object3.getString("title"));
                                data.setDescription(object3
                                        .getString("description"));
                                data.setOfferPage(object3.getString("offer_page"));
                                data.setCode(object3.getString("code"));
                                data.setExclusive(object3.getString("exclusive"));
                                data.setFeatured(object3.getString("featured"));
                                data.setImgName(object3.getString("coupon_image"));
                                data.setRemainingDays(object3
                                        .getString("remaining_days"));

                                storeData.setCounponData(data);

                                storesList.add(storeData);
                            }


                        } else {
                            Toast.makeText(getActivity(), "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                        adapter = new CouponGridAdapter(getActivity(),
                                storesList);
                        coupongrid.setAdapter(adapter);

                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


    class LoadAllCouponList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        String startLetter;

        public LoadAllCouponList(String startLetter) {
            this.startLetter = startLetter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("store_name", startLetter));
            object = parser.makeHttpRequest(getActivity(), urlLoadTopCoupons, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            storesList.clear();

            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        storesList = new ArrayList<StoreData>();

                        JSONArray array = object.getJSONArray("coupons");
                        for (int m = 0; m < array.length(); m++) {
                            JSONObject object1 = array.getJSONObject(m);
                            JSONObject object2 = object1
                                    .getJSONObject("store_detail");
                            StoreData storeData = new StoreData();
                            storeData.setStoreId(object2.getString("id"));
                            storeData.setName(object2.getString("name"));
                            storeData.setType(object2.getString("type"));
                            storeData.setAmountPercentage(object2
                                    .getString("amount_percentage"));
                            storeData.setCoupons(object2.getString("coupons"));
                            storeData.setImgLogo(object2.getString("img_logo"));
                            storeData.setTermsConditions(object2
                                    .getString("terms_and_conditions"));
                            storeData.setHowToGetThisOffer(object2
                                    .getString("how_to_get_this_offer"));
                            storeData.setDesc(object2.getString("desc"));

                            JSONObject object3 = object1
                                    .getJSONObject("coupon_detail");
                            CouponData data = new CouponData();
                            data.setOfferName(object3.getString("offer_name"));
                            data.setTitle(object3.getString("title"));
                            data.setDescription(object3
                                    .getString("description"));
                            data.setOfferPage(object3.getString("offer_page"));
                            data.setCode(object3.getString("code"));
                            data.setExclusive(object3.getString("exclusive"));
                            data.setFeatured(object3.getString("featured"));
                            data.setImgName(object3.getString("coupon_image"));
                            data.setRemainingDays(object3
                                    .getString("remaining_days"));

                            storeData.setCounponData(data);

                            storesList.add(storeData);
                        }


                    } else {
                        Toast.makeText(getActivity(), "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                    adapter = new CouponGridAdapter(getActivity(),
                            storesList);
                    coupongrid.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.e("err", e + "");
                }
            }
            dialog.dismiss();
        }
    }
}
