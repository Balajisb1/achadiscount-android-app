package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.MyApplication;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.Stores;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.androidramp.gearload.Loading;

public class ProductOverView extends Activity {

    public static String urlLoadProductDetailWithCompare = DBConnection.BASEURL + "product_details.php";
    public static List<Stores> tempList;
    TextView prod_parent_name, prodprice, availstore;
    ImageView prodimg;
    Button shopnow;
    LinearLayout speclinear;
    int store_count = 0;
    MyApplication myApplication;
    SharedPreferences preferences;
    JSONParser parser;
    List<Stores> offlinestores;
    SessionManager manager;
    List<Stores> storeslist12;
    List<ProductDetails> productList;
    ImageView imgProductListingDetailFavourites;
    SwipeRefreshLayout mSwipeRefreshLayout;

    Loading loading;
    private ProgressDialog dialog;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_overview);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        myApplication = MyApplication.getInstance();
        imgProductListingDetailFavourites = (ImageView) findViewById(R.id.imgProductListingDetailFavourites);
        toolbartitle.setText("Product Overview");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        speclinear = (LinearLayout) findViewById(R.id.speclinear);
        prod_parent_name = (TextView) findViewById(R.id.prod_parent_name);
        prodprice = (TextView) findViewById(R.id.prodprice);
        availstore = (TextView) findViewById(R.id.availstore);
        prodimg = (ImageView) findViewById(R.id.prodimg);
        shopnow = (Button) findViewById(R.id.shopnow);
        shopnow.setVisibility(View.GONE);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        manager = new SessionManager(ProductOverView.this);
        tempList = new ArrayList<Stores>();
        parser = new JSONParser();

        imgProductListingDetailFavourites.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (manager.checkLogin()) {
                    // Remove from wish list
                    if (Integer.parseInt(productList.get(0)
                            .getProWishListStatus().trim()) > 0) {
                        Toast.makeText(ProductOverView.this, "It is already added in wishlist", Toast.LENGTH_SHORT).show();
//                        new AddToWishList("1", productList.get(0), imgProductListingDetailFavourites, ProductOverView.this).execute(); // Remove from wish
                    } else {

                        addToWishList(productList.get(0), 0);
                    }
                } else {
                    startActivity(new Intent(ProductOverView.this,
                            LoginActivity.class));
                }

            }
        });


        shopnow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductOverView.this,
                        ProductSoloDetailsWithCompare.class);
                intent.putExtra("prod_id", productList.get(0).getProduct_id());

                intent.putExtra("prod_img", productList.get(0)
                        .getProduct_image());

                intent.putExtra("prod_name", productList.get(0)
                        .getProduct_name());
                intent.putExtra("prod_rating", productList.get(0).getRating()
                        + "");
                intent.putExtra("prod_desc", productList.get(0).getProd_desc()
                        .split(",")[0]);
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
//                new ExecuteLoadProductDetails(getIntent().getStringExtra("prod_id")).execute();
/*
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("product_id", productid));
                param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "")));*/

                speclinear.removeAllViews();
                String productid = getIntent().getStringExtra("prod_id");

                String url = urlLoadProductDetailWithCompare + "?product_id=" + productid + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");

                volleyJsonObjectRequest(url);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


//        new ExecuteLoadProductDetails(getIntent().getStringExtra("prod_id")).execute();

        String productid = getIntent().getStringExtra("prod_id");

        String url = urlLoadProductDetailWithCompare + "?product_id=" + productid + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");

        volleyJsonObjectRequest(url);


        loading = (Loading) findViewById(R.id.loading);

        loading.Start();
    }


    private void addToWishList(ProductDetails productdetails, final int i) {
        dialog = new ProgressDialog(ProductOverView.this);
        dialog.setMessage("Adding..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(ProductOverView.this);
        String url = urlAddOrRemoveProductsFromWishList + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0") + "&product_id=" + productdetails.getProduct_id() + "&wish_list_status=" + i;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {

                            if (i == 0) {
                                Toast.makeText(ProductOverView.this, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
                            } else if (i == 1) {
                                Toast.makeText(ProductOverView.this, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
                            }


                        } else {
                            Toast.makeText(ProductOverView.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProductOverView.this,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    // final String jsonString = new String(response, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(response);
                    loading.Cancel();
                    offlinestores = new ArrayList<Stores>();
                    List<Stores> storeslist = new ArrayList<Stores>();
                    storeslist12 = new ArrayList<Stores>();
                    productList = new ArrayList<ProductDetails>();

                    if (response.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray storesarray = jsonObject.getJSONArray("stores");
                            for (int j = 0; j < storesarray.length(); j++) {
                                JSONObject subcatobj1 = storesarray
                                        .getJSONObject(j);
                                Stores storedetails = new Stores();
                                storedetails.setStore_id(subcatobj1
                                        .getString("store_id"));
                                storedetails.setProduct_price(subcatobj1
                                        .getString("product_price"));
                                storedetails.setAffliate_logo(subcatobj1
                                        .getString("affiliate_logo"));
                                storedetails.setAffliate_name(subcatobj1
                                        .getString("affiliate_name"));
                                storedetails.setProduct_url(subcatobj1
                                        .getString("product_url"));
                                storedetails
                                        .setPp_id(subcatobj1.getString("pp_id"));
                                storedetails.setStockAvail(subcatobj1
                                        .getString("stock"));
                                storedetails.setStorerating(subcatobj1
                                        .getString("rating"));
                                storeslist.add(storedetails);
                            }

                            JSONArray productssarray = jsonObject
                                    .getJSONArray("products");
                            for (int j = 0; j < productssarray.length(); j++) {
                                JSONObject subcatobj1 = productssarray
                                        .getJSONObject(j);
                                ProductDetails storedetails = new ProductDetails();
                                storedetails.setProduct_id(subcatobj1
                                        .getString("product_id"));
                                storedetails.setProduct_name(subcatobj1
                                        .getString("product_name"));
                                storedetails.setProduct_image(subcatobj1
                                        .getString("product_image"));
                                storedetails.setProd_desc(subcatobj1
                                        .getString("key_feature"));
                                storedetails.setProduct_url(subcatobj1
                                        .getString("product_url"));
                                storedetails.setRating(subcatobj1
                                        .getString("rating"));
                                storedetails.setProWishListStatus(subcatobj1.getString("wishlist_status"));
                                productList.add(storedetails);
                            }

                            JSONArray offlinestoresarray = jsonObject
                                    .getJSONArray("offlinestores");

                            if (offlinestoresarray.length() > 0) {

                                for (int j = 0; j < offlinestoresarray.length(); j++) {
                                    JSONObject subcatobj1 = offlinestoresarray
                                            .getJSONObject(j);
                                    Stores storedetails1 = new Stores();
                                    storedetails1.setOffline_price(subcatobj1
                                            .getString("price"));
                                    storedetails1.setOffline_offer(subcatobj1
                                            .getString("offline_offer"));

								/*
                                 * JSONArray
								 * offlinestorearray2=subcatobj1.getJSONArray
								 * ("offlinestoredetails"); for(int
								 * ij=0;ij<offlinestorearray2.length();ij++){
								 * Stores storedetails=new Stores(); JSONObject
								 * object2=offlinestorearray2.getJSONObject(j);
								 * storedetails
								 * .setStore_id(object2.getString("store_id"));
								 * storedetails
								 * .setOffline_storename(object2.getString
								 * ("store_name"));
								 * storedetails.setOffline_storelogo
								 * (object2.getString("store_logo"));
								 * storesdetails.add(storedetails);
								 * data1.setCatId
								 * (object1.getString("category_id"));
								 * data1.setCatName
								 * (object1.getString("category_name"));
								 *
								 * }
								 */

                                    JSONArray offlinestorearray1 = subcatobj1
                                            .getJSONArray("storedetails");
                                    for (int ij = 0; ij < offlinestorearray1
                                            .length(); ij++) {
                                        JSONObject object2 = offlinestorearray1
                                                .getJSONObject(ij);
                                        Stores storedetails = new Stores();
                                        storedetails.setStore_id(object2
                                                .getString("store_id"));
                                        storedetails.setLatitude(object2
                                                .getString("latitude"));
                                        storedetails.setLongtitude(object2
                                                .getString("longtitude"));
                                        storedetails.setAddress(object2
                                                .getString("address"));
                                        storedetails.setOffline_storename(object2
                                                .getString("store_name"));
                                        storedetails.setOffline_storelogo(object2
                                                .getString("store_logo"));
                                        storedetails.setOffline_price(storedetails1
                                                .getOffline_price());
                                        storedetails.setOffline_offer(storedetails1
                                                .getOffline_offer());
                                        offlinestores.add(storedetails);

									/*
                                     * Log.e("lat",
									 * object2.getString("latitude"));
									 * Log.e("long",
									 * object2.getString("longtitude"));
									 */
                                    /*
                                     * data1.setCatId(object1.getString(
									 * "category_id"));
									 * data1.setCatName(object1.
									 * getString("category_name"));
									 */

                                    }

                                    // storedetails1.setStoreslist(storesdetails);
                                    storedetails1.setStores(offlinestores);
                                    storeslist12.add(storedetails1);

                                }
                            }

						/*
						 * Log.e("Online Stores size:", storeslist.size()+"");
						 * Log.e("Stores size:", storeslist12.size()+"");
						 * Log.e("Store details size:",
						 * offlinestores.size()+"");
						 */
                            if (offlinestores.size() > 0) {
                                calculateBetweenStoresDistance(offlinestores);
                            }
                            if (productList.get(0).getProWishListStatus().equals(null)
                                    || productList.get(0).getProWishListStatus()
                                    .equals("")) {
                                Log.e("", "Null");
                            } else {
                                if (Integer.parseInt(productList.get(0)
                                        .getProWishListStatus().trim()) > 0) {
                                    imgProductListingDetailFavourites
                                            .setImageResource(R.drawable.filled_heart);
                                } else {
                                    imgProductListingDetailFavourites
                                            .setImageResource(R.drawable.un_fillled_heart);
                                }

                            }

                            if (storeslist.size() > 0) {

                                for (Stores stores : storeslist) {
                                    if (!stores.getProduct_price().equalsIgnoreCase("0")) {
                                        prodprice.setText("Rs. "
                                                + stores.getProduct_price());
                                        break;
                                    }
                                }


                                store_count = storeslist.size() + tempList.size();
                                availstore.setText("Available at " + store_count
                                        + " Stores.");
                            } else {
                                prodprice.setText("Rs. 0.0");
                            }

                            Picasso.with(ProductOverView.this).load(DBConnection.PRODUCT_IMGURL
                                    + productList.get(0).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(prodimg);

                            prod_parent_name.setText(productList.get(0)
                                    .getProduct_name());
                            if (productList.get(0).getProd_desc().toString()
                                    .equals(null)) {
                            } else {
                                String proddesc[] = productList.get(0)
                                        .getProd_desc().toString().split(",");

                                if (productList.get(0).getProd_desc().toString()
                                        .length() > 0) {
                                    for (int i = 0; i < proddesc.length; i++) {
                                        View specview = getLayoutInflater()
                                                .inflate(
                                                        R.layout.activity_spec_title,
                                                        null);
                                        TextView spectitle = (TextView) specview
                                                .findViewById(R.id.spectitle);
                                        spectitle.setText(proddesc[i] + "");
                                        speclinear.addView(specview);
                                    }
                                }
                            }
                        }
                        shopnow.setVisibility(View.VISIBLE);
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);


    }

    private void calculateBetweenStoresDistance(List<Stores> offlinestores) {
        double mylatitude = Double.parseDouble(myApplication.getMyLatitude());
        double mylongitude = Double.parseDouble(myApplication.getMyLongitude());
        int rangeinkm = 10;
        Log.e("offline store size", offlinestores.size() + "");
        for (int j = 0; j < offlinestores.size(); j++) {

            if (offlinestores.get(j).getLatitude().equalsIgnoreCase("") || offlinestores.get(j).getLatitude().equalsIgnoreCase(null) && offlinestores.get(j).getLongtitude().equalsIgnoreCase("") || offlinestores.get(j).getLongtitude().equalsIgnoreCase(null)) {

            } else {

                double distance = ProductOfflineMap.distanceBetweenUsers(
                        mylatitude, mylongitude,
                        Double.parseDouble(offlinestores.get(j).getLatitude()),
                        Double.parseDouble(offlinestores.get(j).getLongtitude()),
                        "K");
                Log.e("Range Distance", distance + "");
                if (distance <= rangeinkm) {
                    Stores stores = new Stores();
                    stores.setLatitude(offlinestores.get(j).getLatitude());
                    stores.setLongtitude(offlinestores.get(j).getLongtitude());
                    stores.setAddress(offlinestores.get(j).getAddress());

                    stores.setOffline_offer(offlinestores.get(j).getOffline_offer());
                    stores.setOffline_price(offlinestores.get(j).getOffline_price());

                    stores.setOffline_storelogo(offlinestores.get(j)
                            .getOffline_storelogo());
                    stores.setOffline_storename(offlinestores.get(j)
                            .getOffline_storename());
                    tempList.add(stores);
                    // stores.setOffline_storelogo(storeslist12.get(i).get)
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    public class ExecuteLoadProductDetails extends
            AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String productid;

        public ExecuteLoadProductDetails(String productid) {
            this.productid = productid;
            Log.e("productid", productid);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(ProductOverView.this, "", "Loading..",
                    true, false);
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("product_id", productid));
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "")));
            Log.e("param", param.toString());
            JSONObject object = parser.makeHttpRequest(ProductOverView.this,
                    urlLoadProductDetailWithCompare, "GET", param);
            // Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();
            offlinestores = new ArrayList<Stores>();
            List<Stores> storeslist = new ArrayList<Stores>();
            storeslist12 = new ArrayList<Stores>();
            productList = new ArrayList<ProductDetails>();
            // List<Stores> storesdetails=new ArrayList<Stores>();
            loading.Cancel();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray storesarray = object.getJSONArray("stores");
                        for (int j = 0; j < storesarray.length(); j++) {
                            JSONObject subcatobj1 = storesarray
                                    .getJSONObject(j);
                            Stores storedetails = new Stores();
                            storedetails.setStore_id(subcatobj1
                                    .getString("store_id"));
                            storedetails.setProduct_price(subcatobj1
                                    .getString("product_price"));
                            storedetails.setAffliate_logo(subcatobj1
                                    .getString("affiliate_logo"));
                            storedetails.setAffliate_name(subcatobj1
                                    .getString("affiliate_name"));
                            storedetails.setProduct_url(subcatobj1
                                    .getString("product_url"));
                            storedetails
                                    .setPp_id(subcatobj1.getString("pp_id"));
                            storedetails.setStockAvail(subcatobj1
                                    .getString("stock"));
                            storedetails.setStorerating(subcatobj1
                                    .getString("rating"));
                            storeslist.add(storedetails);
                        }

                        JSONArray productssarray = object
                                .getJSONArray("products");
                        for (int j = 0; j < productssarray.length(); j++) {
                            JSONObject subcatobj1 = productssarray
                                    .getJSONObject(j);
                            ProductDetails storedetails = new ProductDetails();
                            storedetails.setProduct_id(subcatobj1
                                    .getString("product_id"));
                            storedetails.setProduct_name(subcatobj1
                                    .getString("product_name"));
                            storedetails.setProduct_image(subcatobj1
                                    .getString("product_image"));
                            storedetails.setProd_desc(subcatobj1
                                    .getString("key_feature"));
                            storedetails.setProduct_url(subcatobj1
                                    .getString("product_url"));
                            storedetails.setRating(subcatobj1
                                    .getString("rating"));
                            storedetails.setProWishListStatus(subcatobj1.getString("wishlist_status"));
                            productList.add(storedetails);
                        }

                        JSONArray offlinestoresarray = object
                                .getJSONArray("offlinestores");

                        if (offlinestoresarray.length() > 0) {

                            for (int j = 0; j < offlinestoresarray.length(); j++) {
                                JSONObject subcatobj1 = offlinestoresarray
                                        .getJSONObject(j);
                                Stores storedetails1 = new Stores();
                                storedetails1.setOffline_price(subcatobj1
                                        .getString("price"));
                                storedetails1.setOffline_offer(subcatobj1
                                        .getString("offline_offer"));

								/*
                                 * JSONArray
								 * offlinestorearray2=subcatobj1.getJSONArray
								 * ("offlinestoredetails"); for(int
								 * ij=0;ij<offlinestorearray2.length();ij++){
								 * Stores storedetails=new Stores(); JSONObject
								 * object2=offlinestorearray2.getJSONObject(j);
								 * storedetails
								 * .setStore_id(object2.getString("store_id"));
								 * storedetails
								 * .setOffline_storename(object2.getString
								 * ("store_name"));
								 * storedetails.setOffline_storelogo
								 * (object2.getString("store_logo"));
								 * storesdetails.add(storedetails);
								 * data1.setCatId
								 * (object1.getString("category_id"));
								 * data1.setCatName
								 * (object1.getString("category_name"));
								 *
								 * }
								 */

                                JSONArray offlinestorearray1 = subcatobj1
                                        .getJSONArray("storedetails");
                                for (int ij = 0; ij < offlinestorearray1
                                        .length(); ij++) {
                                    JSONObject object2 = offlinestorearray1
                                            .getJSONObject(ij);
                                    Stores storedetails = new Stores();
                                    storedetails.setStore_id(object2
                                            .getString("store_id"));
                                    storedetails.setLatitude(object2
                                            .getString("latitude"));
                                    storedetails.setLongtitude(object2
                                            .getString("longtitude"));
                                    storedetails.setAddress(object2
                                            .getString("address"));
                                    storedetails.setOffline_storename(object2
                                            .getString("store_name"));
                                    storedetails.setOffline_storelogo(object2
                                            .getString("store_logo"));
                                    storedetails.setOffline_price(storedetails1
                                            .getOffline_price());
                                    storedetails.setOffline_offer(storedetails1
                                            .getOffline_offer());
                                    offlinestores.add(storedetails);

									/*
									 * Log.e("lat",
									 * object2.getString("latitude"));
									 * Log.e("long",
									 * object2.getString("longtitude"));
									 */
									/*
									 * data1.setCatId(object1.getString(
									 * "category_id"));
									 * data1.setCatName(object1.
									 * getString("category_name"));
									 */

                                }

                                // storedetails1.setStoreslist(storesdetails);
                                storedetails1.setStores(offlinestores);
                                storeslist12.add(storedetails1);

                            }
                        }

						/*
						 * Log.e("Online Stores size:", storeslist.size()+"");
						 * Log.e("Stores size:", storeslist12.size()+"");
						 * Log.e("Store details size:",
						 * offlinestores.size()+"");
						 */
                        if (offlinestores.size() > 0) {
                            calculateBetweenStoresDistance(offlinestores);
                        }
                        if (productList.get(0).getProWishListStatus().equals(null)
                                || productList.get(0).getProWishListStatus()
                                .equals("")) {
                            Log.e("", "Null");
                        } else {
                            if (Integer.parseInt(productList.get(0)
                                    .getProWishListStatus().trim()) > 0) {
                                imgProductListingDetailFavourites
                                        .setImageResource(R.drawable.filled_heart);
                            } else {
                                imgProductListingDetailFavourites
                                        .setImageResource(R.drawable.un_fillled_heart);
                            }

                        }

                        if (storeslist.size() > 0) {
                            prodprice.setText("Rs. "
                                    + storeslist.get(0).getProduct_price());

                            store_count = storeslist.size() + tempList.size();
                            availstore.setText("Available at " + store_count
                                    + " Stores.");
                        } else {
                            prodprice.setText("Rs. 0.0");
                        }

                        Picasso.with(ProductOverView.this).load(DBConnection.PRODUCT_IMGURL
                                + productList.get(0).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(prodimg);

                        prod_parent_name.setText(productList.get(0)
                                .getProduct_name());


                        speclinear.removeAllViews();

                        if (productList.get(0).getProd_desc().toString()
                                .equals(null)) {
                        } else {
                            String proddesc[] = productList.get(0)
                                    .getProd_desc().toString().split(",");

                            if (productList.get(0).getProd_desc().toString()
                                    .length() > 0) {
                                for (int i = 0; i < proddesc.length; i++) {
                                    View specview = getLayoutInflater().inflate(R.layout.activity_spec_title, null);
                                    TextView spectitle = (TextView) specview
                                            .findViewById(R.id.spectitle);
                                    spectitle.setText(proddesc[i] + "");

                                    speclinear.addView(specview);
                                }
                            }
                        }
                    }

                } catch (Exception exception) {
                    //Log.e("Exception", exception + "");
                    exception.printStackTrace();
                }
            }
        }
    }

}
