package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class AddReferalActivity extends Activity {

    private final String urlInviteFriends = DBConnection.BASEURL + "invite_friends_mail.php";
    private final String urlReferFriendsHeader = DBConnection.BASEURL + "refer_friends.php";
    JSONParser parser;
    Button btnSendInvites;
    TextView referfriendscount;
    EditText edtReferFriendsLink, edtReferFriendsEmail;
    SharedPreferences preferences;
    SessionManager manager;

    ImageView inviteImage;
    TextView inviteDescription, buttonDetail, referFriend;
    EditText inviteText, inviteMailText;
    ImageView shareInvite;
    Button inviteFriends;

    String toMailId = "", shareBody;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		setContentView(R.layout.refer_friends);
        setContentView(R.layout.invite_layout);

        parser = new JSONParser();
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Refer Friends");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        manager = new SessionManager(AddReferalActivity.this);

//		referfriendscount=(TextView)findViewById(R.id.referfriendscount);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
//		edtReferFriendsLink=(EditText)findViewById(R.id.edtReferFriendsLink);
//		edtReferFriendsLink.setText(DBConnection.REFFERAL_URL+preferences.getString(SessionManager.KEY_RANDOMCODE,""));
//		edtReferFriendsEmail=(EditText)findViewById(R.id.edtReferFriendsEmail);
//		btnSendInvites=(Button)findViewById(R.id.btnSendInvites);

		/*
		btnSendInvites.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(edtReferFriendsEmail.getText().toString().trim().length()>0 && 
						Patterns.EMAIL_ADDRESS.matcher(edtReferFriendsEmail.getText().toString().trim()).matches()){


					new LoadProductDetails().execute();
					List<NameValuePair> param=new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair("from_email_id",preferences.getString(SessionManager.KEY_EMAIL,"")));
					param.add(new BasicNameValuePair("to_email_id",toMailId));
					param.add(new BasicNameValuePair("user_name",preferences.getString(SessionManager.KEY_USERNAME,"3232")));
					param.add(new BasicNameValuePair("rand_code",preferences.getString(SessionManager.KEY_RANDOMCODE,"23232323")));
					System.out.println("Param is "+param.toString());
					JSONObject object=parser.makeHttpRequest(urlInviteFriends,"GET",param);


					String toMailId=edtReferFriendsEmail.getText().toString();

					String url=urlInviteFriends+"?from_email_id="+preferences.getString(SessionManager.KEY_EMAIL,"")+"&to_email_id="+toMailId
							+"&user_name="+preferences.getString(SessionManager.KEY_USERNAME,"3232")+"&rand_code="+preferences.getString(SessionManager.KEY_RANDOMCODE,"23232323");
					volleyJsonObjectRequest(url);

				}else{
					Toast.makeText(AddReferalActivity.this,"Fill value for Email ..",Toast.LENGTH_LONG).show();
				}
			}
		});
		*/
//		new LoadReferralCount().execute();
//		String toMailId=edtReferFriendsEmail.getText().toString();

		/*List<NameValuePair> param=new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"")));
		System.out.println("Param is "+param.toString());
		JSONObject object=parser.makeHttpRequest(urlReferFriendsHeader,"GET",param);*/

		/*String url=urlReferFriendsHeader+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"");
		volleyJsonObjectRequest1(url);*/

        new LoadReferralCount().execute();

        inviteImage = (ImageView) findViewById(R.id.imgInvite);
        inviteDescription = (TextView) findViewById(R.id.txtInviteDescription);

        inviteText = (EditText) findViewById(R.id.inviteCodeText);
        shareInvite = (ImageView) findViewById(R.id.imageShare);
        inviteFriends = (Button) findViewById(R.id.btninviteFriends);
        referFriend = (TextView) findViewById(R.id.referFriends);
        inviteMailText = (EditText) findViewById(R.id.editText);

        inviteText.setText(DBConnection.REFFERAL_URL + preferences.getString(SessionManager.KEY_RANDOMCODE, ""));


        toMailId = inviteMailText.getText().toString();


        inviteFriends.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inviteMailText.getText().toString().trim().length() > 0 &&
                        Patterns.EMAIL_ADDRESS.matcher(inviteMailText.getText().toString().trim()).matches()) {

                    new LoadProductDetails().execute();

                } else {
                    Toast.makeText(AddReferalActivity.this, "Fill value for Email ..", Toast.LENGTH_LONG).show();
                }
            }
        });

        shareBody = inviteDescription.getText().toString() + "\n" + inviteText.getText().toString();

        Log.i("URL", shareBody);


        shareInvite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Friend Referral - Save Money on Shopping");

                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


    }


    public void volleyJsonObjectRequest1(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            referFriend.setText("Friends Referred :" + jsonObject.getInt("count"));
                        } else {
                            referFriend.setText("Friends Referred :0");
                        }
                    } else {
                        Toast.makeText(AddReferalActivity.this, "Something goes wrong..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            edtReferFriendsEmail.setText("");
                            Toast.makeText(AddReferalActivity.this, "Mail has been send to the user..", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(AddReferalActivity.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AddReferalActivity.this, "Something goes wrong..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadReferralCount extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddReferalActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "")));
            System.out.println("Param is " + param.toString());
            JSONObject object = parser.makeHttpRequest(AddReferalActivity.this, urlReferFriendsHeader, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        referFriend.setText("Friends Referred :" + object.getInt("count"));
                    } else {
                        referFriend.setText("Friends Referred :0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(AddReferalActivity.this, "Something goes wrong..", Toast.LENGTH_LONG).show();
            }
            //	dialog.dismiss();
        }
    }

    class LoadProductDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        private String toMailId = inviteMailText.getText().toString();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddReferalActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("from_email_id", preferences.getString(SessionManager.KEY_EMAIL, "")));
            param.add(new BasicNameValuePair("to_email_id", toMailId));
            param.add(new BasicNameValuePair("user_name", preferences.getString(SessionManager.KEY_USERNAME, "3232")));
            param.add(new BasicNameValuePair("rand_code", preferences.getString(SessionManager.KEY_RANDOMCODE, "23232323")));
            System.out.println("Param is " + param.toString());
            JSONObject object = parser.makeHttpRequest(AddReferalActivity.this, urlInviteFriends, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        inviteMailText.setText("");
                        Toast.makeText(AddReferalActivity.this, "Mail has been send to the user..", Toast.LENGTH_LONG).show();
                    } else if (object.getInt("success") == 2) {
                        inviteMailText.setText("");
                        Toast.makeText(AddReferalActivity.this, "Some account is already registered", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(AddReferalActivity.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(AddReferalActivity.this, "Something goes wrong..", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }
}
