package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.CompareProducts;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

public class CompareProductListActivity extends Activity {

    LinearLayout linearCompareProductList;

    SharedPreferences preferences;
    SessionManager manager;

    Button btnCompareProductListCompare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_compare_list);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Compare Products");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        manager = new SessionManager(CompareProductListActivity.this);
        linearCompareProductList = (LinearLayout) findViewById(R.id.linearCompareProductList);

        updateRow();

        btnCompareProductListCompare = (Button) findViewById(R.id.btnCompareProductListCompare);
        btnCompareProductListCompare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CompareProducts.productdetails.size() == 2) {
                    Intent intent = new Intent(CompareProductListActivity.this,
                            CompareProductsActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(CompareProductListActivity.this, "Need Minimum 2 products to compare", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateRow() {
        linearCompareProductList.removeAllViews();
        if (CompareProducts.productdetails != null
                && CompareProducts.productdetails.size() > 0) {
            for (int i = 0; i < CompareProducts.productdetails.size(); i++) {
                final int m = i;
                View view = getLayoutInflater().inflate(
                        R.layout.activity_product_compare_list_details, null);
                TextView txtCompareProductListDetailName = (TextView) view
                        .findViewById(R.id.txtCompareProductListDetailName);
                txtCompareProductListDetailName
                        .setText(CompareProducts.productdetails.get(i)
                                .getProduct_name());
                ImageView imgCompareProductListDetailRemove = (ImageView) view
                        .findViewById(R.id.imgCompareProductListDetailRemove);
                imgCompareProductListDetailRemove
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CompareProducts
                                        .removeFromCompare(CompareProducts.productdetails
                                                .get(m));
                                updateRow();
                            }
                        });
                ImageView imgCompareProductListDetailProduct = (ImageView) view
                        .findViewById(R.id.imgCompareProductListDetailProduct);
                Picasso.with(CompareProductListActivity.this).load(DBConnection.PRODUCT_IMGURL
                        + CompareProducts.productdetails.get(i)
                        .getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgCompareProductListDetailProduct);


                TextView txtCompareProductListDetailPrice = (TextView) view
                        .findViewById(R.id.txtCompareProductListDetailPrice);
                txtCompareProductListDetailPrice.setText("Rs."
                        + CompareProducts.productdetails.get(i)
                        .getProduct_price() + "");
                Button btnCompareProductListDetailView = (Button) view
                        .findViewById(R.id.btnCompareProductListDetailView);
                btnCompareProductListDetailView
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Toast.makeText(getApplicationContext(),
                                // CompareProducts.productdetails.get(m).getProduct_id(),
                                // 5000).show();
                                Intent intent = new Intent(
                                        CompareProductListActivity.this,
                                        ProductOverView.class);
                                intent.putExtra("prod_id",
                                        CompareProducts.productdetails.get(m)
                                                .getProduct_id());
                                intent.putExtra("prod_img",
                                        CompareProducts.productdetails.get(m)
                                                .getProduct_image());
                                intent.putExtra("prod_desc",
                                        CompareProducts.productdetails.get(m)
                                                .getKey_feature());
                                intent.putExtra("prod_raing",
                                        CompareProducts.productdetails.get(m)
                                                .getRating());
                                intent.putExtra("prod_name",
                                        CompareProducts.productdetails.get(m)
                                                .getProduct_name());
                                intent.putExtra("prod_price",
                                        CompareProducts.productdetails.get(m)
                                                .getProduct_price());
                                startActivity(intent);
                            }
                        });
                linearCompareProductList.addView(view);
            }
        } else {
            Toast.makeText(CompareProductListActivity.this,
                    "No products found..", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                CompareProducts.getProducts().clear();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        CompareProducts.getProducts().clear();
    }
}
