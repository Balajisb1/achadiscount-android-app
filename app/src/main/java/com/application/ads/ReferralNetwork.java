package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.ReferralData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ReferralNetwork extends Activity {

    JSONParser parser;
    List<ReferralData> referralDatas;
    TableLayout tblLayout;
    SessionManager manager;
    Spinner spnrUserRefferFriendsCount;
    List<ReferralData> list;
    EditText edtUserRefferFriendsFilter;
    int rowCount = 2;
    int currentPageNo = 1;
    LinearLayout linearPageNo;
    TextView txtPrevious;
    TextView noRecordsTxt;
    TextView txtNext, txtShowingResult;
    SharedPreferences preferences;
    private String urlLoadRefferalNetwork = DBConnection.BASEURL + "refferal_network.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_refferal_network);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Referral Network");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        /*getActionBar().setTitle("Refferal Network");*/
        getActionBar().setTitle("");
        manager = new SessionManager(ReferralNetwork.this);
        tblLayout = (TableLayout) findViewById(R.id.tblLayout);
        parser = new JSONParser();

        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        noRecordsTxt = (TextView) findViewById(R.id.noRecordsTxt);
        txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);

        linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

        txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (currentPageNo > 1) {
                        currentPageNo -= 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtNext = (TextView) findViewById(R.id.txtNext);
        txtNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (currentPageNo < (list.size() / rowCount)) {
                        currentPageNo += 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    } else if (currentPageNo == (list.size() / rowCount) && (list.size() % rowCount > 0)) {
                        currentPageNo += 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        edtUserRefferFriendsFilter = (EditText) findViewById(R.id.edtUserRefferFriendsFilter);
        edtUserRefferFriendsFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterOptionByEditText(edtUserRefferFriendsFilter.getText().toString().trim());
            }
        });

        spnrUserRefferFriendsCount = (Spinner) findViewById(R.id.spnrUserRefferFriendsCount);
        spnrUserRefferFriendsCount.setAdapter(new ArrayAdapter<String>(ReferralNetwork.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrUserRefferFriendsCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {
                    if (referralDatas != null) {
                        rowCount = Integer.parseInt(spnrUserRefferFriendsCount.getSelectedItem().toString());
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        new LoadReferralNetwork().execute();
        /*List<NameValuePair> param=new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));*/

//		String url=urlLoadRefferalNetwork+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
//
//		volleyJsonObjectRequest(url);
    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    referralDatas = new ArrayList<ReferralData>();
                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            noRecordsTxt.setVisibility(View.GONE);
                            JSONArray array = jsonObject.getJSONArray("referrals");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                ReferralData data = new ReferralData();
                                data.setDateJoined(object1.getString("date_added"));
                                data.setRefferalName(object1.getString("referral_email"));
                                data.setRefferalAccountStatus(object1.getString("status"));
                                referralDatas.add(data);
                            }
                            list = new ArrayList<ReferralData>();
                            list = referralDatas;
                            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                            createPageNo();
                        } else {
                            noRecordsTxt.setVisibility(View.VISIBLE);
                            Toast.makeText(ReferralNetwork.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ReferralNetwork.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void loadTableRows(int start, int end) {
        try {
            tblLayout.removeAllViews();


            if (list != null) {
                if (list.size() > 0) {
                    noRecordsTxt.setVisibility(View.GONE);

                    if (list.size() >= end) {
                        for (int i = start; i < end; i++) {
                            View view = getLayoutInflater().inflate(R.layout.user_refferal_network_table_row, null);
                            TextView txtRefferalDateJoined = (TextView) view.findViewById(R.id.txtRefferalDateJoined);
                            TextView txtRefferalRefferalName = (TextView) view.findViewById(R.id.txtRefferalRefferalName);
                            Button btnRefferalStatus = (Button) view.findViewById(R.id.btnRefferalStatus);
                            txtRefferalDateJoined.setText(referralDatas.get(i).getDateJoined());
                            txtRefferalRefferalName.setText(referralDatas.get(i).getRefferalName());
                            if (referralDatas.get(i).getRefferalAccountStatus().equalsIgnoreCase("1")) {
                                btnRefferalStatus.setText("Active");
                            } else if (referralDatas.get(i).getRefferalAccountStatus().equalsIgnoreCase("0")) {
                                btnRefferalStatus.setText("DeActive");
                            } else {
//						btnRefferalStatus.setText("DeActive");
                            }
                            tblLayout.addView(view);
                        }
                        String starting = String.valueOf(start + 1);
                        txtShowingResult.setText("Showing " + starting + " to " + end + " entries");
                    } else {
                        for (int i = start; i < list.size(); i++) {
                            View view = getLayoutInflater().inflate(R.layout.user_refferal_network_table_row, null);
                            TextView txtRefferalDateJoined = (TextView) view.findViewById(R.id.txtRefferalDateJoined);
                            TextView txtRefferalRefferalName = (TextView) view.findViewById(R.id.txtRefferalRefferalName);
                            Button btnRefferalStatus = (Button) view.findViewById(R.id.btnRefferalStatus);
                            txtRefferalDateJoined.setText(referralDatas.get(i).getDateJoined());
                            txtRefferalRefferalName.setText(referralDatas.get(i).getRefferalName());
                            btnRefferalStatus.setText(referralDatas.get(i).getRefferalAccountStatus());
                            if (referralDatas.get(i).getRefferalAccountStatus().equalsIgnoreCase("1")) {
                                btnRefferalStatus.setText("Active");
                            } else if (referralDatas.get(i).getRefferalAccountStatus().equalsIgnoreCase("0")) {
                                btnRefferalStatus.setText("DeActive");
                            } else {
//						btnRefferalStatus.setText("DeActive");
                            }
                            tblLayout.addView(view);
                        }
                        String starting = String.valueOf(start + 1);
                        txtShowingResult.setText("Showing " + starting + " to " + list.size() + " entries");
                    }
                } else {
                    noRecordsTxt.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void filterOption(int start, int end) {
        try {
            createPageNo();
            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void filterOptionByEditText(String text) {
        try {
            list = new ArrayList<ReferralData>();
            for (int i = 0; i < referralDatas.size(); i++) {
                if (referralDatas.get(i).getDateJoined().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                        referralDatas.get(i).getRefferalAccountStatus().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                        referralDatas.get(i).getRefferalName().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                    list.add(referralDatas.get(i));
                }
            }
            createPageNo();
            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createPageNo() {
        try {
            linearPageNo.removeAllViews();

            int numOfPage = list.size() / rowCount;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            params.setMargins(5, 5, 5, 5);

            for (int i = 0; i < numOfPage; i++) {
                final int j = i;
                TextView textView = new TextView(ReferralNetwork.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(i + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j + 1;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }

            if ((list.size() % rowCount) > 0) {
                final int j = numOfPage + 1;
                TextView textView = new TextView(ReferralNetwork.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(numOfPage + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadReferralNetwork extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ReferralNetwork.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(ReferralNetwork.this, urlLoadRefferalNetwork, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            referralDatas = new ArrayList<ReferralData>();
            if (result.toString().trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        noRecordsTxt.setVisibility(View.GONE);
                        JSONArray array = object.getJSONArray("referrals");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            ReferralData data = new ReferralData();
                            data.setDateJoined(object1.getString("date_added"));
                            data.setRefferalName(object1.getString("referral_email"));
                            data.setRefferalAccountStatus(object1.getString("status"));
                            referralDatas.add(data);
                        }
                        list = new ArrayList<ReferralData>();
                        list = referralDatas;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                        createPageNo();
                    } else {
                        noRecordsTxt.setVisibility(View.VISIBLE);
                        Toast.makeText(ReferralNetwork.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(ReferralNetwork.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
            }
        }

    }

}
