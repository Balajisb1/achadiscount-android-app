package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ads.extra.SessionManager;

public class AccountTitle extends Activity {

    LinearLayout linearAccountSetting, linearMyEarnings, linearAccountPayments,
            linearMissingCashback, linearReferFriends, linearOrders,
            linearTransactions, linearBankPayment;

    LinearLayout linearMyAccount, linearPaymentDetails, linearChangePassword, linearLogout;
    LinearLayout linearWishList, linearReferralNetwork;

    SharedPreferences preferences;
    boolean myAccount = false;
    boolean paymentSettings = false;

    SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myaccount_title);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("My Account");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        myAccount = true;
        paymentSettings = true;
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        manager = new SessionManager(AccountTitle.this);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.activity_othertoolbar);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        linearAccountSetting = (LinearLayout) findViewById(R.id.linearAccountSetting);
        linearAccountSetting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, AccountSetting.class));
            }
        });

        linearMyEarnings = (LinearLayout) findViewById(R.id.linearMyEarnings);
        linearMyEarnings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, MyEarnings.class));
            }
        });

        linearAccountPayments = (LinearLayout) findViewById(R.id.linearAccountPayments);
        linearAccountPayments.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, Payments.class));
            }
        });

        linearMissingCashback = (LinearLayout) findViewById(R.id.linearMissingCashback);
        linearMissingCashback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, MissingCashBack.class));
            }
        });

        linearReferralNetwork = (LinearLayout) findViewById(R.id.linearReferralNetwork);
        linearReferralNetwork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, ReferralNetwork.class));
            }
        });

        linearReferFriends = (LinearLayout) findViewById(R.id.linearReferFriends);
        linearReferFriends.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, AddReferalActivity.class));
            }
        });

        linearOrders = (LinearLayout) findViewById(R.id.linearOrders);
        linearOrders.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //	startActivity(new Intent(AccountTitle.this,Orders.class));
            }
        });

        linearTransactions = (LinearLayout) findViewById(R.id.linearTransactions);
        linearTransactions.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, Transactions.class));
            }
        });


        linearBankPayment = (LinearLayout) findViewById(R.id.linearBankPayment);
        linearBankPayment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, BankPayment.class));
            }
        });

		
		/*linearCheque=(LinearLayout)findViewById(R.id.linearCheque);
		linearCheque.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(AccountTitle.this,ChequeActivity.class));
			}
		});*/

        linearMyAccount = (LinearLayout) findViewById(R.id.linearMyAccount);
        linearMyAccount.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (myAccount) {
                    myAccount = false;
                } else {
                    myAccount = true;
                }

                if (myAccount) {
                    showMyAccountLayout();
                } else {
                    hideMyAccountLayout();
                }
            }
        });

        linearPaymentDetails = (LinearLayout) findViewById(R.id.linearPaymentDetails);
        linearPaymentDetails.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (paymentSettings) {
                    paymentSettings = false;
                } else {
                    paymentSettings = true;
                }

                if (paymentSettings) {
                    showPaymentSetting();
                } else {
                    hidePaymentSetting();
                }
            }
        });


        linearChangePassword = (LinearLayout) findViewById(R.id.linearChangePassword);
        linearChangePassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, ChangePassword.class));
            }
        });
		
		
		/*linearLogout=(LinearLayout)findViewById(R.id.linearLogout);
		linearLogout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ShowLogoutDialog();
			}
		});*/


        linearWishList = (LinearLayout) findViewById(R.id.linearWishList);
        linearWishList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, MyWishListActivity.class));
            }
        });


        linearReferralNetwork = (LinearLayout) findViewById(R.id.linearReferralNetwork);
        linearReferralNetwork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountTitle.this, ReferralNetwork.class));
            }
        });

        showMyAccountLayout();
        showPaymentSetting();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(AccountTitle.this, NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(AccountTitle.this, CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(AccountTitle.this, CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(AccountTitle.this, OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                finish();
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:
                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(AccountTitle.this, AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void hideMyAccountLayout() {
        linearAccountSetting.setVisibility(View.GONE);
        linearMyEarnings.setVisibility(View.GONE);
        linearAccountPayments.setVisibility(View.GONE);
        linearMissingCashback.setVisibility(View.GONE);
        linearReferFriends.setVisibility(View.GONE);
        linearOrders.setVisibility(View.GONE);
        linearReferralNetwork.setVisibility(View.GONE);
        linearTransactions.setVisibility(View.GONE);
    }


    public void showMyAccountLayout() {
        linearAccountSetting.setVisibility(View.VISIBLE);
        linearMyEarnings.setVisibility(View.VISIBLE);
        linearAccountPayments.setVisibility(View.VISIBLE);
        linearMissingCashback.setVisibility(View.VISIBLE);
        linearReferFriends.setVisibility(View.VISIBLE);
        linearOrders.setVisibility(View.GONE);
        linearReferralNetwork.setVisibility(View.VISIBLE);
        linearTransactions.setVisibility(View.VISIBLE);
    }

    public void showPaymentSetting() {
        linearBankPayment.setVisibility(View.VISIBLE);
		/*linearCheque.setVisibility(View.VISIBLE);*/
    }

    public void hidePaymentSetting() {
        linearBankPayment.setVisibility(View.GONE);
		/*linearCheque.setVisibility(View.GONE);*/
    }


    public void ShowLogoutDialog() {
        AlertDialog.Builder builder = new Builder(AccountTitle.this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure want to Logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                manager.logoutUser();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
