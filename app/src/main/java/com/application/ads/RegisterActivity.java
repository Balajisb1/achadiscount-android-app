package com.application.ads;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends Activity {
    public static String urlRegister = DBConnection.BASEURL + "register.php";
    TextView txtfirstname, txtlastname, txtmobile, txtemail, txtpassword,
            txtconfpassword;
    EditText edfirstname, edlastname, edmobile, edemail, edpassword,
            edconfpassword;
    Button butcreateaccnt, regsignin;
    JSONParser parser;
    private String referCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        edfirstname = (EditText) findViewById(R.id.firstname);
        edlastname = (EditText) findViewById(R.id.lastname);
        edmobile = (EditText) findViewById(R.id.mobile);
        edemail = (EditText) findViewById(R.id.email);
        edpassword = (EditText) findViewById(R.id.password);
        edconfpassword = (EditText) findViewById(R.id.confpassword);
        butcreateaccnt = (Button) findViewById(R.id.createaccnt);
        regsignin = (Button) findViewById(R.id.regsignin);

        regsignin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this,
                        LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        parser = new JSONParser();
        butcreateaccnt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (validateComponents()) {

                    new ExecuteRegisterTask().execute();
                    String firstName = edfirstname.getText().toString();
                    String lastName = edlastname.getText().toString();
                    String contactNo = edmobile.getText().toString();
                    String email = edemail.getText().toString();
                    String password = Utils.encryptToMD5(edpassword.getText().toString());
                /*
						List<NameValuePair> param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair("first_name", firstName));
					param.add(new BasicNameValuePair("last_name", lastName));
					param.add(new BasicNameValuePair("contact_no", contactNo));
					param.add(new BasicNameValuePair("email", email));
					param.add(new BasicNameValuePair("password", password));*/

//					String url=urlRegister+"?first_name="+firstName+"&last_name="+lastName
//							+"&contact_no="+contactNo+"&email="+email+"&password="+password;
//					volleyJsonObjectRequest(url);
                }

            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                referCode = uri.getQueryParameter("id");
                Log.i("REGISTER", referCode);
            }

        }
    }

    private boolean validateComponents() {

        if (edfirstname.getText().toString().equalsIgnoreCase("")
                || edfirstname.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this, "First name is mandatory",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (edlastname.getText().toString().equalsIgnoreCase("")
                || edlastname.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this, "Last name is mandatory",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (edmobile.getText().toString().equalsIgnoreCase("")
                || edmobile.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this, "Mobile number is mandatory",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (edemail.getText().toString().equalsIgnoreCase("")
                || edemail.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this, "Email is mandatory", Toast.LENGTH_SHORT)
                    .show();
            return false;
        } else if (edpassword.getText().toString().equalsIgnoreCase("")
                || edpassword.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this, "Password is mandatory", Toast.LENGTH_SHORT)
                    .show();
            return false;
        } else if (!Utils.validatePassword(edpassword.getText().toString())) {
            Toast.makeText(
                    RegisterActivity.this,
                    "Password must contain Lowercase,uppercase,numeric & special Characters(@#$%)",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (edconfpassword.getText().toString().equalsIgnoreCase("")
                || edconfpassword.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(RegisterActivity.this,
                    "Confirm Password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!edconfpassword.getText().toString()
                .equals(edpassword.getText().toString())) {
            Toast.makeText(RegisterActivity.this,
                    "Password and confirm password must be same", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }


    public void volleyJsonObjectRequest(String url) {

        Log.i("URL", url);
        RequestQueue queue = Volley.newRequestQueue(this);


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.

                        if (response.trim().length() > 0) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getInt("success") == 1) {
                                    clearComponents();
                                    Toast.makeText(RegisterActivity.this,
                                            "Register Successful", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(RegisterActivity.this,
                                            object.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);


	/*	Log.i("URL",url);
		final RequestQueue queue = Volley.newRequestQueue(this);
		CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
			@Override
			public void onResponse(NetworkResponse response) {
				try {
					final String jsonString = new String(response.data,HttpHeaderParser.parseCharset(response.headers));
					JSONObject jsonObject = new JSONObject(jsonString);
					if (jsonString.toString().trim().length() > 0) {
						if (jsonObject.getInt("success") == 1) {
							clearComponents();
							Toast.makeText(RegisterActivity.this,
									"Register Successful", Toast.LENGTH_SHORT).show();
							finish();
						} else {
							Toast.makeText(RegisterActivity.this,
									jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
						}
					}
					else
					{

					}

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
				} catch (UnsupportedEncodingException | JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				//Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
			}
		});
		queue.add(cacheRequest);
*/
    }

    public void clearComponents() {

        edfirstname.setText("");
        edlastname.setText("");
        edmobile.setText("");
        edemail.setText("");
        edpassword.setText("");
        edconfpassword.setText("");

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteRegisterTask extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        private String firstName = edfirstname.getText().toString();
        private String lastName = edlastname.getText()
                .toString();
        private String contactNo = edmobile.getText()
                .toString();
        private String emailtxt = edemail.getText()
                .toString();
        private String password = Utils
                .encryptToMD5(edpassword.getText().toString());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(RegisterActivity.this, "",
                    "Loading ...", true, false);
        }

        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("first_name", firstName));
            param.add(new BasicNameValuePair("last_name", lastName));
            param.add(new BasicNameValuePair("contact_no", contactNo));
            param.add(new BasicNameValuePair("email", emailtxt));
            param.add(new BasicNameValuePair("password", password));
            param.add(new BasicNameValuePair("referCode", referCode));
            Log.e("params are", param.toString());
            JSONObject object = parser.makeHttpRequest(RegisterActivity.this, urlRegister, "GET",
                    param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        clearComponents();
                        Toast.makeText(RegisterActivity.this,
                                "Register Successful", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this,
                                object.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                }
            }

        }
    }

}
