package com.application.ads;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.CategoryAdapter;
import com.application.ads.adapter.SubCategoryAdapter;
import com.application.ads.data.BannerData;
import com.application.ads.data.CategoryData;
import com.application.ads.data.MobileBrandsData;
import com.application.ads.data.MyApplication;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.StoreData;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.ExpandableHeightGridView;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.application.ads.utils.InternetDetector;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    public static String urlLoadMainCategories = DBConnection.BASEURL + "load_main_categories.php";
    public static String urlSearchProducts = DBConnection.BASEURL + "search_products.php";
    public static String urlLoadPopularBrands = DBConnection.BASEURL + "load_popular_brands.php";
    public static String urlLoadOfflineOfferProducts = "https://achadiscount.in/offline/admin/index.php/json/getofferlist";
    LinearLayout mostpopularcategorieslayout, popularmobilephones,
            comparepricesfromlayout; // offerslayout
    ScrollView scrollview;
    ExpandableHeightGridView categoriesgrid;
    LinearLayout floatinglinear;
    ImageView arrowupdown;
    List<StoreData> storesList;
    //	GPSTracker tracker;
    //	MyApplication application;
    SubCategoryAdapter subcatadapter;
    Button viewproduct;
    List<String> produnctname;
    ArrayAdapter<String> prodadapter;
    JSONParser parser;
    List<CategoryData> categorydata;
    List<OfflineStoreData> offlinecategorydata;
    LinearLayout onlinelinear;
    List<MobileBrandsData> mobilebranddata;
    List<BannerData> bannerDatas;
    CategoryAdapter adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    List<SubCategoryData> subcatlist;
    Dialog dialog;
    ListView subcatdialog;
    LinearLayout couponslinear, offlinelinear, offlineofferlinear;
    TextView brandmore;
    SessionManager manager;
    List<ProductDetails> productdetailslist, productsdetails;
    boolean arrowupdownid = false;
    SharedPreferences preferences;
    LinearLayout offlineimglinear;
    AutoCompleteTextView searchtext;
    ImageView searchbtn;
    int status;
    MyApplication myApplication;
    Double latitude, longitude;
    int demotest = 0;
    RequestQueue queue;
    private JSONObject mainCategoryJSonObject;
    private String searchProdkey = "";

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle("");
        View view = getActivity().getLayoutInflater().inflate(
                R.layout.activity_home, null);
        myApplication = MyApplication.getInstance();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        couponslinear = (LinearLayout) view.findViewById(R.id.couponslinear);
        offlinelinear = (LinearLayout) view.findViewById(R.id.offlinelinear);
        offlineimglinear = (LinearLayout) view
                .findViewById(R.id.offlineimglinear);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        offlineofferlinear = (LinearLayout) view
                .findViewById(R.id.offlineofferlinear);
        productsdetails = new ArrayList<ProductDetails>();
        preferences = getActivity().getSharedPreferences(
                SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        categorydata = new ArrayList<CategoryData>();
        offlinecategorydata = new ArrayList<OfflineStoreData>();
        brandmore = (TextView) view.findViewById(R.id.brandmore);
        //tracker = new GPSTracker(getActivity());
        manager = new SessionManager(getActivity());
        arrowupdown = (ImageView) view.findViewById(R.id.arrowupdown);
        floatinglinear = (LinearLayout) view.findViewById(R.id.floatinglinear);
        storesList = new ArrayList<StoreData>();
        productdetailslist = new ArrayList<ProductDetails>();
        onlinelinear = (LinearLayout) view.findViewById(R.id.onlinelinear);
        parser = new JSONParser();
        bannerDatas = new ArrayList<BannerData>();
        mobilebranddata = new ArrayList<MobileBrandsData>();
        subcatlist = new ArrayList<SubCategoryData>();

        arrowupdown.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!arrowupdownid) {
                    floatinglinear.setVisibility(View.GONE);
                    arrowupdown.setImageResource(R.drawable.arrow_up);
                    arrowupdownid = true;
                } else {
                    floatinglinear.setVisibility(View.VISIBLE);
                    arrowupdown.setImageResource(R.drawable.arrow_down);
                    arrowupdownid = false;
                }
            }
        });

		/*
         * //dialog=new Dialog(HomeActivity.this);
		 * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		 * dialog.setContentView(R.layout.subcat_dialog);
		 * subcatdialog=(ListView)dialog.findViewById(R.id.subcatdialog);
		 */

        brandmore.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity(),
                        OnlineBrandListing.class);
                startActivity(intent);
            }
        });


        categoriesgrid = (ExpandableHeightGridView) view
                .findViewById(R.id.categoriesgrid);
        categoriesgrid.setExpanded(true);
        // offerslayout=(LinearLayout)view.findViewById(R.id.offersforyou);
        mostpopularcategorieslayout = (LinearLayout) view
                .findViewById(R.id.mostpopularcategorieslayout);
        comparepricesfromlayout = (LinearLayout) view
                .findViewById(R.id.comparepricesfromlayout);
        popularmobilephones = (LinearLayout) view
                .findViewById(R.id.popularmobilephones);
        scrollview = (ScrollView) view.findViewById(R.id.scrollview);


        categoriesgrid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(getActivity(),
                        OnlineHomeNavigation.class);
                myApplication.setTempcatid(position + "");
                /*
                 * intent.putParcelableArrayListExtra("productsdetails",
				 * (ArrayList<? extends Parcelable>) productsdetails);
				 * intent.putExtra("parent_id", categorydata.get(position)
				 * .getCatid()); intent.putExtra("parent_name",
				 * categorydata.get(position) .getCatname());
				 */
                startActivity(intent);

            }
        });

        onlinelinear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity(),
                        OnlineHomeNavigation.class);

				/*
                 * intent.putParcelableArrayListExtra("productsdetails",
				 * (ArrayList<? extends Parcelable>) productsdetails);
				 * intent.putExtra("parent_id", categorydata.get(0)
				 * .getCatid()); intent.putExtra("parent_name",
				 * categorydata.get(0) .getCatname());
				 */
                startActivity(intent);
                // Toast.makeText(getActivity(), productsdetails.size()+"",
                // Toast.LENGTH_SHORT).show();
            }
        });
        couponslinear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),
                        CouponNavigation.class);
                startActivity(intent);
            }
        });

        offlinelinear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),
                        OfflineHomeNavigation.class);
                startActivity(intent);
            }
        });


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                demotest = 1;
                Log.i("Swipe refresh", "Swipe Refresh");
//                new ExecuteLoadMainCategories().execute();
                CacheRequest cacheRequest = new CacheRequest(0, urlLoadMainCategories, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {

                            Log.i("Swipe refresh1111", "Swipe Refresh1111");
                            final String jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);
                            loadCategories(jsonObject);
//                            Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                queue.add(cacheRequest);


                CacheRequest cacheRequest1 = new CacheRequest(0, urlLoadPopularBrands, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            final String jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);


                            if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                                //    Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                                if (queue.getCache() != null) {
                                    queue.getCache().remove(jsonString);
                                }
                                fillAllBrands(jsonObject, queue);
                            } else {
                                //   Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                                fillAllBrands(jsonObject, queue);

                            }


                            //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                queue.add(cacheRequest1);


                String url = urlLoadOfflineOfferProducts + "?category=&brand=&lat=" + myApplication.getMyLatitude() + "&long=" + myApplication.getMyLongitude();
                Log.i("URL", url);
                CacheRequest cacheRequest2 = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            final String jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);

                            if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                                //  Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                                if (queue.getCache() != null) {
                                    queue.getCache().remove(jsonString);
                                }

                                fillOfflineOffers(jsonObject);
                            } else {
                                Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                                fillOfflineOffers(jsonObject);

                            }


                            //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                        } catch (UnsupportedEncodingException | JSONException e) {
                            Toast.makeText(getActivity(), "Exception No Internet is there", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //     Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                queue.add(cacheRequest2);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

		/*
         * scrollview.post(new Runnable() {
		 * 
		 * @Override public void run() { // TODO Auto-generated method stub
		 * scrollview.fullScroll(ScrollView.FOCUSABLES_TOUCH_MODE); } });
		 */

		/*
         * for(int i=0;i<3;i++){ View
		 * v=getLayoutInflater().inflate(R.layout.activity_offersforyou,
		 * null,true); offerslayout.addView(v); }
		 * 
		 * for(int i=0;i<5;i++){ View
		 * v=getLayoutInflater().inflate(R.layout.activity_mostpopularcategories
		 * , null,true); mostpopularcategorieslayout.addView(v); }
		 * 
		 * for(int i=0;i<7;i++){ View
		 * v=getLayoutInflater().inflate(R.layout.activity_comparepricesfrom,
		 * null,true); comparepricesfromlayout.addView(v); }
		 */

		/*if (!tracker.getIsGPSTrackingEnabled()) {
			tracker.showSettingsAlert();
		} else {

			for (int i = 0; i < 50; i++) {

				if (tracker.latitude == 0.0 || tracker.longitude == 0.0) {
					tracker = new GPSTracker(getActivity());

				}
			}*/


        Log.e("lat", myApplication.getMyLatitude() + "");
        Log.e("lang", myApplication.getMyLongitude() + "");

        // manager.initializelocation(tracker.latitude+"",
        // tracker.longitude+"");

        searchtext = (AutoCompleteTextView) view.findViewById(R.id.searchtext);


        prodadapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                produnctname);
        searchtext.setAdapter(prodadapter);

        searchtext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("Text", s.toString());
                if (!TextUtils.isEmpty(s)) {
                    //new ExecuteSearchProducts(s.toString()).execute();
                    searchProducts(s.toString());
                }
            }
        });

        searchbtn = (ImageView) view.findViewById(R.id.searchbtn);
        searchbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (searchtext.getText().toString().equals("")
                        || searchtext.getText().toString().equals(null)) {
                    Toast.makeText(getActivity(), "Please provide search term",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String name = searchtext.getText().toString();
                    // Log.e("name", name);

                    for (int i = 0; i < productsdetails.size(); i++) {
                        // Log.e("prod name",
                        // productsdetails.get(i).getProduct_name());
                        if (name.equalsIgnoreCase(productsdetails.get(i)
                                .getProduct_name())) {
                            // Toast.makeText(getActivity(),
                            // productsdetails.get(i).getProduct_id(),
                            // Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(),
                                    ProductOverView.class);
                            intent.putExtra("prod_id", productsdetails.get(i)
                                    .getProduct_id());
                            searchtext.setText("");
                            startActivity(intent);
                            status = 1;
                        } else {
                            status = 0;
                            // Toast.makeText(getActivity(),
                            // "No data found..Try different keyword",
                            // Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (status == 0) {

                        searchProdkey = searchtext.getText().toString().trim();

                        Intent intent = new Intent(getActivity(), SearchProductDetailsActivity.class);
                        intent.putExtra("prodName", searchProdkey);
                        startActivity(intent);

//                        Toast.makeText(getActivity(), "No data found..Try different keyword",Toast.LENGTH_SHORT).show();
                        searchtext.setText("");
                    }
                }
            }
        });

        searchtext.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                // Toast.makeText(getApplicationContext(),
                // searchedittext.getText().toString(), Toast.LENGTH_SHORT).show();
				/*
				 * Intent intent = new Intent(getActivity(), CouponList.class);
				 * intent.putExtra("storeName", searchtext.getText()
				 * .toString()); searchtext.setText(""); startActivity(intent);
				 */

                String name = searchtext.getText().toString();
                // Log.e("name", name);
                for (int i = 0; i < productsdetails.size(); i++) {
                    // Log.e("prod name",
                    // productsdetails.get(i).getProduct_name());
                    if (name.equalsIgnoreCase(productsdetails.get(i)
                            .getProduct_name())) {
						/*
						 * Toast.makeText(getActivity(),
						 * productsdetails.get(i).getProduct_id(), Toast.LENGTH_SHORT)
						 * .show();
						 */
                        Intent intent = new Intent(getActivity(),
                                ProductOverView.class);

                        intent.putExtra("prod_id", productsdetails.get(i)
                                .getProduct_id());
                        searchtext.setText("");
                        startActivity(intent);
                    }
                }
            }
        });


        queue = Volley.newRequestQueue(getActivity());
        CacheRequest cacheRequest = new CacheRequest(0, urlLoadMainCategories, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {

                    Log.i("URLLOAD", urlLoadMainCategories);
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    Log.i("jsonString", jsonString);
                    JSONObject jsonObject = new JSONObject(jsonString);
                    loadCategories(jsonObject);
                    //    Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        })/*{
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", "Androidhive");
                params.put("email", "abc@androidhive.info");
                params.put("password", "password123");

                return params;
            }
        }*/;


        CacheRequest cacheRequest1 = new CacheRequest(0, urlLoadPopularBrands, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);


                    if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                        //    Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                        if (queue.getCache() != null) {
                            queue.getCache().remove(jsonString);
                        }
                        fillAllBrands(jsonObject, queue);
                    } else {
                        //   Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                        fillAllBrands(jsonObject, queue);

                    }


                    //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });


        // Add the request to the RequestQueue.


        String url = urlLoadOfflineOfferProducts + "?category=&brand=&lat=" + myApplication.getMyLatitude() + "&long=" + myApplication.getMyLongitude();
        Log.i("URL", url);
        CacheRequest cacheRequest2 = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                        //  Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                        if (queue.getCache() != null) {
                            queue.getCache().remove(jsonString);
                        }

                        fillOfflineOffers(jsonObject);
                    } else {
//                            Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                        fillOfflineOffers(jsonObject);
                    }
                    //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    Toast.makeText(getActivity(), "Exception No Internet is there", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //     Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(cacheRequest);
        queue.add(cacheRequest1);
        queue.add(cacheRequest2);

//        new ExecuteLoadMainCategories().execute();

        return view;
    }

    /*private void checkValues(final RequestQueue queue){

        if(myApplication.getMyLatitude()== null && myApplication.getMyLongitude()==null){
            checkValues(queue);
        }
        else{
            String url=urlLoadOfflineOfferProducts+"?category=&brand=&lat="+myApplication.getMyLatitude()+"&long="+myApplication.getMyLongitude();
            CacheRequest cacheRequest2 = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    try {
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        JSONObject jsonObject = new JSONObject(jsonString);

                        if(InternetDetector.getInstance(getActivity()).isOnline(getActivity()))
                        {
                            Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                            if(queue.getCache() !=null) {
                                queue.getCache().remove(jsonString);
                            }
                            fillOfflineOffers(jsonObject);
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                            fillOfflineOffers(jsonObject);

                        }


                        //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(cacheRequest2);
        }

    }*/


    public void loadCategories(JSONObject result) {
        try {

            Log.i("Load Categories", "Load Categories");
            if (result.getInt("success") == 1) {
                categorydata = new ArrayList<CategoryData>();
                productsdetails = new ArrayList<ProductDetails>();
                JSONArray array = result
                        .getJSONArray("main_categories");
                // Toast.makeText(HomeActivity.this, array.length()+"",
                // Toast.LENGTH_SHORT).show();

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = array.getJSONObject(i);
                    CategoryData data = new CategoryData();
                    data.setCatid(object1.getString("cate_id"));
                    data.setCatname(object1.getString("cate_name"));
                    data.setCatimg(object1.getString("cate_img"));
                    JSONArray array1 = object1
                            .getJSONArray("sub_categories");
                    for (int j = 0; j < array1.length(); j++) {

                        JSONObject object2 = array1.getJSONObject(j);
                        SubCategoryData data1 = new SubCategoryData();
                        data1.setSubcat_id(object2.getString("cate_id"));
                        data1.setSubcat_name(object2
                                .getString("cate_name"));
                        data1.setSubcat_img(object2
                                .getString("cate_img"));
                        subcatlist.add(data1);
                    }
                    data.setSubcatlist(subcatlist);
                    categorydata.add(data);
                }
                // placeCategory();
                adapter = new CategoryAdapter(getActivity(),
                        categorydata);
                categoriesgrid.setAdapter(adapter);
                if (demotest == 1) {
                    CacheRequest cacheRequest1 = new CacheRequest(0, urlLoadPopularBrands, new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            try {
                                final String jsonString = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers));
                                JSONObject jsonObject = new JSONObject(jsonString);


                                if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                                    //    Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                                    if (queue.getCache() != null) {
                                        queue.getCache().remove(jsonString);
                                    }
                                    fillAllBrands(jsonObject, queue);
                                } else {
                                    //   Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                                    fillAllBrands(jsonObject, queue);

                                }

                                //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                            } catch (UnsupportedEncodingException | JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });


                    // Add the request to the RequestQueue.


                    queue.add(cacheRequest1);

                    //  new ExecuteLoadPopularBrands().execute();
                }

            } else {
                Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                        .show();
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }


    public void fillAllBrands(JSONObject result, RequestQueue queue) {
        mobilebranddata.clear();
        bannerDatas.clear();
        productdetailslist.clear();
        bannerDatas = new ArrayList<BannerData>();
        mobilebranddata = new ArrayList<MobileBrandsData>();
        // Toast.makeText(HomeActivity.this, result.length()+"",
        // Toast.LENGTH_SHORT).show();
        if (result.length() > 0) {
            List<String> cataffdata = new ArrayList<String>();

            try {

                if (result.getInt("success") == 1) {
                    JSONArray array = result.optJSONArray("brands");
                    // Toast.makeText(HomeActivity.this, array.length()+"",
                    // Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < array.length(); i++) {
                        MobileBrandsData data = new MobileBrandsData();
                        JSONObject object1 = array.getJSONObject(i);
                        data.setMobile_id(object1.getString("id"));
                        data.setMobile_name(object1.getString("name"));
                        data.setMobile_image(object1.getString("image"));
                        data.setParent_id(object1.getString("parent_id"));
                        data.setParent_child_id(object1
                                .getString("parent_child_id"));
                        mobilebranddata.add(data);
                    }

                    JSONArray bannerarray = result.getJSONArray("banners");
                    // Toast.makeText(HomeActivity.this, array.length()+"",
                    // Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < bannerarray.length(); i++) {
                        BannerData data = new BannerData();
                        JSONObject object1 = bannerarray.getJSONObject(i);
                        data.setBannerid(object1.getString("banner_id"));
                        data.setBannerimage(object1
                                .getString("banner_image"));
                        data.setBannerUrl(object1.getString("banner_url"));
                        bannerDatas.add(data);
                    }

                    JSONArray array1 = result
                            .getJSONArray("affiliate_logos");
                    // Toast.makeText(HomeActivity.this, array.length()+"",
                    // Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < array1.length(); i++) {
                        JSONObject object11 = array1.getJSONObject(i);

                        cataffdata
                                .add(object11.getString("affiliate_logo"));
                    }


                    JSONArray subcatarray = result.getJSONArray("products");

                    for (int j = 0; j < subcatarray.length(); j++) {
                        JSONObject subcatobj = subcatarray.getJSONObject(j);
                        ProductDetails productdetails = new ProductDetails();
                        productdetails.setPp_id(subcatobj
                                .getString("pp_id"));
                        productdetails.setProduct_price(subcatobj
                                .getString("product_price"));
                        productdetails.setRating(subcatobj
                                .getString("rating"));
                        productdetails.setProduct_name(subcatobj
                                .getString("prod_name"));
                        productdetails.setMrp(subcatobj.getString("mrp"));
                        productdetails.setProduct_id(subcatobj
                                .getString("product_id"));
                        productdetails.setProduct_image(subcatobj
                                .getString("product_image"));
                        productdetails.setKey_feature(subcatobj
                                .getString("key_feature"));
                        productdetails.setParent_child_id(subcatobj
                                .getString("parent_child_id"));
                        productdetailslist.add(productdetails);
                    }

                    fillAllBanners(bannerDatas);

                    //new ExecuteLoadOfflineOffers().execute();
                    fillProducts(productdetailslist, queue);
                    popularmobilephones.removeAllViews();
                    for (int i = 0; i < mobilebranddata.size(); i++) {
                        final int m = i;
                        View v = getActivity().getLayoutInflater().inflate(
                                R.layout.activity_popularmobilephones,
                                null, true);

                        TextView popularimagename = (TextView) v
                                .findViewById(R.id.popularimgname);
                        ImageView popularimage = (ImageView) v
                                .findViewById(R.id.popularimg);
                        viewproduct = (Button) v
                                .findViewById(R.id.viewproduct);
                        popularimagename.setText(mobilebranddata.get(i)
                                .getMobile_name());

                        Log.e("Img url", DBConnection.BRAND_IMGURL
                                + mobilebranddata.get(i).getMobile_image());
                        Picasso.with(getActivity()).load(DBConnection.BRAND_IMGURL
                                + mobilebranddata.get(i).getMobile_image()).resize(144, 0).placeholder(R.drawable.placeholder).into(popularimage);

                        viewproduct
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub
                                        Intent intent = new Intent(
                                                getActivity(),
                                                BrandListActivity.class);
                                        intent.putExtra("brand_id",
                                                mobilebranddata.get(m)
                                                        .getMobile_id());
                                        intent.putExtra("brand_name",
                                                mobilebranddata.get(m)
                                                        .getMobile_name());
                                        intent.putExtra("parent_id",
                                                mobilebranddata.get(m)
                                                        .getParent_id());
                                        intent.putExtra(
                                                "parent_child_id",
                                                mobilebranddata
                                                        .get(m)
                                                        .getParent_child_id());
                                        startActivity(intent);

                                    }
                                });


                        popularmobilephones.addView(v);
                        // new ExecuteLoadMainCategories().execute();
                    }
                    comparepricesfromlayout.removeAllViews();
                    for (int j = 0; j < cataffdata.size(); j++) {
                        View v = getActivity().getLayoutInflater().inflate(
                                R.layout.activity_comparepricesfrom, null);
                        ImageView affliatelogo = (ImageView) v
                                .findViewById(R.id.affliatelogo);
                        Picasso.with(getActivity()).load(DBConnection.AFFLIATE_IMGURL + cataffdata.get(j)).resize(144, 0).placeholder(R.drawable.placeholder).into(affliatelogo);

                        comparepricesfromlayout.addView(v);
                    }


                } else {
                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                            .show();
                }


            } catch (Exception e) {
                // TODO: handle exception
                System.out.println(e);
            }
        }
    }

    private void fillOfflineOffers(JSONObject jsonObject) {

        Log.i("FILLOFRS11111111111", "FILLOFFLINEOFFERS111111111111");
        offlinecategorydata = new ArrayList<OfflineStoreData>();
        offlinecategorydata.clear();

        try {
				/*
				 * JSONArray array=new JSONArray(result); for (int i = 0; i <
				 * array.length(); i++) { JSONObject object1 =
				 * array.getJSONObject(i); StoreData data=new StoreData();
				 * data.setStoreId(object1.getString("id"));
				 * data.setName(object1.getString("name"));
				 * data.setImgLogo(object1.getString("image"));
				 * categorydata.add(data); }
				 */


            JSONArray jsonArray = jsonObject.getJSONArray("queryresult");
            Log.i("FILLOFRS2222222222", "FILLOFFLINEOFFERS22222222");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object1 = jsonArray.getJSONObject(i);
                OfflineStoreData data = new OfflineStoreData();
                data.setId(object1.getString("id"));
                data.setImage(object1.getString("image"));
                data.setBrand(object1.getString("brand"));
                data.setOffer(object1.getString("offer"));
                data.setDescription(object1.getString("description"));
                data.setBrandname(object1.getString("brandname"));
                data.setMaplat(object1.getDouble("maplat"));
                data.setMaplong(object1.getDouble("maplong"));
                data.setDist(object1.getDouble("dist"));
                offlinecategorydata.add(data);
            }

            Log.i("FILLOFFLINEOFFFERS", "FILLOFFLINEOFFERS");
            fillAllOfflineOffers(offlinecategorydata);
            // adapter=new OfflineStoreAdapter(, categorydata);
            // //offlinestorelist.setAdapter(adapter);
            // offlinestoregrid.setAdapter(adapter);

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("offline exception", e.toString());
        }
    }

    public void fillAllBanners(final List<BannerData> bannerDatas) {

        offlineimglinear.removeAllViews();

        for (int i = 0; i < bannerDatas.size(); i++) {
            final int m = i;
            View view = getActivity().getLayoutInflater().inflate(
                    R.layout.activity_banner_image, null);
            ImageView bannerimage = (ImageView) view
                    .findViewById(R.id.bannerimage);
            Picasso.with(getActivity()).load(DBConnection.BANNER_URL
                    + bannerDatas.get(i).getBannerimage()).placeholder(R.drawable.placeholder).into(bannerimage);

            bannerimage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    Uri uri = Uri.parse(bannerDatas.get(m).getBannerUrl());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });

            offlineimglinear.addView(view);
        }
    }

    public void fillProducts(final List<ProductDetails> productdetailslist, final RequestQueue queue) {
        mostpopularcategorieslayout.removeAllViews();

        for (int i = 0; i < productdetailslist.size(); i++) {
            final int m = i;
            View view = getActivity().getLayoutInflater().inflate(
                    R.layout.activity_popularmobileproducts, null);

            ImageView prodimg = (ImageView) view.findViewById(R.id.popularimg);
            TextView prodprice = (TextView) view
                    .findViewById(R.id.popularprice);
            TextView prodname = (TextView) view
                    .findViewById(R.id.popularimgname);
            RatingBar prodrating = (RatingBar) view
                    .findViewById(R.id.ratingBar);
            Button viewproduct = (Button) view.findViewById(R.id.viewproduct);
			/*LayerDrawable stars = (LayerDrawable) prodrating
					.getProgressDrawable();
			stars.getDrawable(2).setColorFilter(Color.YELLOW,
					PorterDuff.Mode.SRC_ATOP);*/

            prodrating.setRating(Float.parseFloat(productdetailslist.get(i).getRating()));
            prodprice.setText("Rs."
                    + productdetailslist.get(i).getProduct_price());
            prodname.setText(productdetailslist.get(i).getProduct_name());

            Picasso.with(getActivity()).load(DBConnection.PRODUCT_IMGURL
                    + productdetailslist.get(i).getProduct_image()).resize(144, 0).placeholder(R.drawable.placeholder).into(prodimg);
            /*
			loader.DisplayImage(DBConnection.PRODUCT_IMGURL
					+ productdetailslist.get(i).getProduct_image(), prodimg);*/
            viewproduct.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(),
                            ProductOverView.class);
                    intent.putExtra("parent_child_id", productdetailslist
                            .get(m).getParent_child_id());
                    intent.putExtra("prod_name", productdetailslist.get(m)
                            .getProduct_name());
                    intent.putExtra("prod_img", productdetailslist.get(m)
                            .getProduct_image());
                    intent.putExtra("prod_price", productdetailslist.get(m)
                            .getProduct_price());
                    intent.putExtra("prod_rating", productdetailslist.get(m)
                            .getRating());
                    intent.putExtra("prod_id", productdetailslist.get(m)
                            .getProduct_id());
                    intent.putExtra("pp_id", productdetailslist.get(m)
                            .getPp_id());
                    intent.putExtra("prod_desc", productdetailslist.get(m)
                            .getKey_feature().toString().trim());
                    startActivity(intent);
                }
            });

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(),
                            ProductOverView.class);
                    intent.putExtra("parent_child_id", productdetailslist
                            .get(m).getParent_child_id());
                    intent.putExtra("prod_name", productdetailslist.get(m)
                            .getProduct_name());
                    intent.putExtra("prod_img", productdetailslist.get(m)
                            .getProduct_image());
                    intent.putExtra("prod_price", productdetailslist.get(m)
                            .getProduct_price());
                    intent.putExtra("prod_rating", productdetailslist.get(m)
                            .getRating());
                    intent.putExtra("prod_id", productdetailslist.get(m)
                            .getProduct_id());
                    intent.putExtra("pp_id", productdetailslist.get(m)
                            .getPp_id());
                    intent.putExtra("prod_desc", productdetailslist.get(m)
                            .getKey_feature().toString().trim());
                    startActivity(intent);
                }
            });


            mostpopularcategorieslayout.addView(view);
        }


    }

	/*
	 * public void showSubCatDialog(List<SubCategoryData> subcategorydata,String
	 * parentname){
	 * 
	 * TextView cattile=(TextView)dialog.findViewById(R.id.cattile);
	 * cattile.setText(parentname); subcatadapter=new
	 * SubCategoryAdapter(HomeActivity.this, subcategorydata);
	 * subcatdialog.setAdapter(subcatadapter); dialog.show();
	 * 
	 * 
	 * 
	 * }
	 */

    private void fillAllOfflineOffers(final List<OfflineStoreData> offlinecategorydata) {

        Log.i("ALLFILLOFFLINEOFFFERS", "ALLFILLOFFLINEOFFERS");
        offlineofferlinear.removeAllViews();
        for (int i = 0; i < offlinecategorydata.size(); i++) {
            final int position = i;
            View view = getActivity().getLayoutInflater().inflate(
                    R.layout.activity_offline_offer_homepage, null);
            ImageView offerimage = (ImageView) view
                    .findViewById(R.id.offerimghome);
            offerimage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                    intent.putExtra("id", offlinecategorydata.get(position)
                            .getBrand());
                    intent.putExtra("desc", offlinecategorydata.get(position).getDescription());
                    startActivity(intent);
                }
            });

            Log.i("SPLASHHOTOFFER", DBConnection.OFFLINE_EXTRAIMG + offlinecategorydata.get(i).getImage());
            Picasso.with(getActivity()).load(DBConnection.OFFLINE_EXTRAIMG + offlinecategorydata.get(i).getImage()).placeholder(R.drawable.placeholder).resize(144, 0).into(offerimage);
            offlineofferlinear.addView(view);
            /*loader.DisplayImage(DBConnection.OFFLINE_EXTRAIMG
                    + offlinecategorydata.get(i).getImage(), offerimage);*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new ExecuteLoadPopularBrands().execute();
      /*  CacheRequest cacheRequest1 = new CacheRequest(0, urlLoadPopularBrands, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);


                    if (InternetDetector.getInstance(getActivity()).isOnline(getActivity())) {
                        //    Toast.makeText(getActivity(), "Internet is there", Toast.LENGTH_SHORT).show();
                        if (queue.getCache() != null) {
                            queue.getCache().remove(jsonString);
                        }
                        fillAllBrands(jsonObject, queue);
                    } else {
                        //   Toast.makeText(getActivity(), "No Internet is there", Toast.LENGTH_SHORT).show();
                        fillAllBrands(jsonObject, queue);

                    }


                    //     Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });



        queue.add(cacheRequest1);
*/

    }

    class ExecuteLoadMainCategories extends AsyncTask<String, String, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            return mainCategoryJSonObject;

        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            dialog.dismiss();
            List<SubCategoryData> subcatlist = new ArrayList<SubCategoryData>();
            // Toast.makeText(HomeActivity.this, result.length()+"",
            // Toast.LENGTH_SHORT).show();
            if (result.length() > 0) {
                try {

                    if (result.getInt("success") == 1) {
                        categorydata = new ArrayList<CategoryData>();
                        productsdetails = new ArrayList<ProductDetails>();
                        JSONArray array = result
                                .getJSONArray("main_categories");
                        // Toast.makeText(HomeActivity.this, array.length()+"",
                        // Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            CategoryData data = new CategoryData();
                            data.setCatid(object1.getString("cate_id"));
                            data.setCatname(object1.getString("cate_name"));
                            data.setCatimg(object1.getString("cate_img"));
                            JSONArray array1 = object1
                                    .getJSONArray("sub_categories");
                            for (int j = 0; j < array1.length(); j++) {

                                JSONObject object2 = array1.getJSONObject(j);
                                SubCategoryData data1 = new SubCategoryData();
                                data1.setSubcat_id(object2.getString("cate_id"));
                                data1.setSubcat_name(object2
                                        .getString("cate_name"));
                                data1.setSubcat_img(object2
                                        .getString("cate_img"));
                                subcatlist.add(data1);
                            }
                            data.setSubcatlist(subcatlist);
                            categorydata.add(data);
                        }
                        // placeCategory();
                        adapter = new CategoryAdapter(getActivity(),
                                categorydata);
                        categoriesgrid.setAdapter(adapter);
                        if (demotest == 1) {
                            //  new ExecuteLoadPopularBrands().execute();
                        }
					/*	JSONArray subcatarray = object.getJSONArray("products");

						for (int j = 0; j < subcatarray.length(); j++) {
							JSONObject subcatobj = subcatarray.getJSONObject(j);
							ProductDetails productdetails = new ProductDetails();
							productdetails.setProduct_name(subcatobj
									.getString("prod_name"));
							productdetails.setProduct_id(subcatobj
									.getString("product_id"));
							productsdetails.add(productdetails);
						}

						List<String> produnctname = new ArrayList<String>();
						for (int i = 0; i < productsdetails.size(); i++) {

							produnctname.add(productsdetails.get(i)
									.getProduct_name());
						}
						manager.setStoreDetails(produnctname);
						// Toast.makeText(getActivity(),
						// categorydata.get(0).getSubcatlist().get(0).getSubcat_id()+"",
						// Toast.LENGTH_SHORT).show();
						manager.createCompareId(categorydata.get(0)
								.getSubcatlist().get(0).getSubcat_id());
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								getActivity(),
								android.R.layout.simple_list_item_1,
								produnctname);
						searchtext.setAdapter(adapter);*/
                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }
            }
        }
    }

    class ExecuteLoadOfflineOffers extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
		/*	dialog = ProgressDialog.show(getActivity(), "", "Loading..", true,
					false);*/

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            // ?category=&brand=&lat=9.9252007&long=78.11977539999998
            param.add(new BasicNameValuePair("category", ""));
            param.add(new BasicNameValuePair("brand", ""));
            param.add(new BasicNameValuePair("lat", myApplication
                    .getMyLatitude()));
            param.add(new BasicNameValuePair("long", myApplication
                    .getMyLongitude()));
            // JSONObject
            // jsonObject=parser.getJSONFromUrl(urlLoadOfflineOfferProducts);
            JSONObject object = parser.makeHttpRequest(getActivity(),
                    urlLoadOfflineOfferProducts, "GET", param);
            return object.toString();

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //		dialog.dismiss();
            offlinecategorydata = new ArrayList<OfflineStoreData>();
            offlinecategorydata.clear();

            try {
				/*
				 * JSONArray array=new JSONArray(result); for (int i = 0; i <
				 * array.length(); i++) { JSONObject object1 =
				 * array.getJSONObject(i); StoreData data=new StoreData();
				 * data.setStoreId(object1.getString("id"));
				 * data.setName(object1.getString("name"));
				 * data.setImgLogo(object1.getString("image"));
				 * categorydata.add(data); }
				 */

                JSONObject jsonObject = new JSONObject(result);

                JSONArray jsonArray = jsonObject.getJSONArray("queryresult");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    OfflineStoreData data = new OfflineStoreData();
                    data.setId(object1.getString("id"));
                    data.setImage(object1.getString("image"));
                    data.setBrand(object1.getString("brand"));
                    data.setOffer(object1.getString("offer"));
                    data.setDescription(object1.getString("description"));
                    data.setBrandname(object1.getString("brandname"));
                    data.setMaplat(object1.getDouble("maplat"));
                    data.setMaplong(object1.getDouble("maplong"));
                    data.setDist(object1.getDouble("dist"));
                    offlinecategorydata.add(data);
                }

                fillAllOfflineOffers(offlinecategorydata);
                // adapter=new OfflineStoreAdapter(, categorydata);
                // //offlinestorelist.setAdapter(adapter);
                // offlinestoregrid.setAdapter(adapter);

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("offline exception", e.toString());
            }
        }
    }

    class ExecuteLoadPopularBrands extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			/*dialog = ProgressDialog.show(getActivity(), "", "Loading..", true,
					true);*/

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            JSONObject object = parser.makeHttpRequest(getActivity(), urlLoadPopularBrands,
                    "GET", param);
            // Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //		dialog.dismiss();

            mobilebranddata.clear();
            bannerDatas.clear();
            productdetailslist.clear();
            bannerDatas = new ArrayList<BannerData>();
            mobilebranddata = new ArrayList<MobileBrandsData>();
            // Toast.makeText(HomeActivity.this, result.length()+"",
            // Toast.LENGTH_SHORT).show();
            if (result.trim().length() > 0) {
                List<String> cataffdata = new ArrayList<String>();

                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray array = object.getJSONArray("brands");
                        // Toast.makeText(HomeActivity.this, array.length()+"",
                        // Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < array.length(); i++) {
                            MobileBrandsData data = new MobileBrandsData();
                            JSONObject object1 = array.getJSONObject(i);
                            data.setMobile_id(object1.getString("id"));
                            data.setMobile_name(object1.getString("name"));
                            data.setMobile_image(object1.getString("image"));
                            data.setParent_id(object1.getString("parent_id"));
                            data.setParent_child_id(object1
                                    .getString("parent_child_id"));
                            mobilebranddata.add(data);
                        }

                        JSONArray bannerarray = object.getJSONArray("banners");
                        // Toast.makeText(HomeActivity.this, array.length()+"",
                        // Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < bannerarray.length(); i++) {
                            BannerData data = new BannerData();
                            JSONObject object1 = bannerarray.getJSONObject(i);
                            data.setBannerid(object1.getString("banner_id"));
                            data.setBannerimage(object1
                                    .getString("banner_image"));
                            data.setBannerUrl(object1.getString("banner_url"));
                            bannerDatas.add(data);
                        }

                        JSONArray array1 = object
                                .getJSONArray("affiliate_logos");
                        // Toast.makeText(HomeActivity.this, array.length()+"",
                        // Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < array1.length(); i++) {
                            JSONObject object11 = array1.getJSONObject(i);

                            cataffdata
                                    .add(object11.getString("affiliate_logo"));
                        }

						/*
						 * JSONArray couparray=object.getJSONArray("coupons");
						 *
						 * for (int i = 0; i < couparray.length(); i++) {
						 * StoreData storeData = new StoreData(); JSONObject
						 * object1 = couparray.getJSONObject(i); CouponData data
						 * = new CouponData();
						 * data.setOfferName(object1.getString("offer_name"));
						 * data.setTitle(object1.getString("title"));
						 * data.setDescription(object1
						 * .getString("description"));
						 * data.setOfferPage(object1.getString("offer_page"));
						 * data.setCode(object1.getString("code"));
						 * data.setExclusive(object1.getString("exclusive"));
						 * data.setFeatured(object1.getString("featured"));
						 * data.setImgName(object1.getString("coupon_image"));
						 *
						 * storeData.setCounponData(data);
						 *
						 * storesList.add(storeData);
						 *
						 * } fillAllCoupons(storesList);
						 */

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setPp_id(subcatobj
                                    .getString("pp_id"));
                            productdetails.setProduct_price(subcatobj
                                    .getString("product_price"));
                            productdetails.setRating(subcatobj
                                    .getString("rating"));
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setMrp(subcatobj.getString("mrp"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productdetails.setProduct_image(subcatobj
                                    .getString("product_image"));
                            productdetails.setKey_feature(subcatobj
                                    .getString("key_feature"));
                            productdetails.setParent_child_id(subcatobj
                                    .getString("parent_child_id"));
                            productdetailslist.add(productdetails);
                        }

                        fillAllBanners(bannerDatas);

                        new ExecuteLoadOfflineOffers().execute();
                        //fillProducts(productdetailslist);
                        popularmobilephones.removeAllViews();
                        for (int i = 0; i < mobilebranddata.size(); i++) {
                            final int m = i;
                            View v = getActivity().getLayoutInflater().inflate(
                                    R.layout.activity_popularmobilephones,
                                    null, true);

                            TextView popularimagename = (TextView) v
                                    .findViewById(R.id.popularimgname);
                            ImageView popularimage = (ImageView) v
                                    .findViewById(R.id.popularimg);
                            viewproduct = (Button) v
                                    .findViewById(R.id.viewproduct);
                            popularimagename.setText(mobilebranddata.get(i)
                                    .getMobile_name());

                            Log.e("Img url", DBConnection.BRAND_IMGURL
                                    + mobilebranddata.get(i).getMobile_image());
                            Picasso.with(getActivity()).load(DBConnection.BRAND_IMGURL
                                    + mobilebranddata.get(i).getMobile_image()).resize(144, 0).placeholder(R.drawable.placeholder).into(popularimage);
							/*loader.DisplayImage(DBConnection.BRAND_IMGURL
									+ mobilebranddata.get(i).getMobile_image(),
									popularimage);*/
                            viewproduct
                                    .setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method stub
                                            Intent intent = new Intent(
                                                    getActivity(),
                                                    BrandListActivity.class);
                                            intent.putExtra("brand_id",
                                                    mobilebranddata.get(m)
                                                            .getMobile_id());
                                            intent.putExtra("brand_name",
                                                    mobilebranddata.get(m)
                                                            .getMobile_name());
                                            intent.putExtra("parent_id",
                                                    mobilebranddata.get(m)
                                                            .getParent_id());
                                            intent.putExtra(
                                                    "parent_child_id",
                                                    mobilebranddata
                                                            .get(m)
                                                            .getParent_child_id());
                                            startActivity(intent);
                                            // Toast.makeText(getActivity(),
                                            // mobilebranddata.get(m).getMobile_id().toString(),
                                            // Toast.LENGTH_SHORT).show();
                                        }
                                    });


                            // RatingBar ratingBar =
                            // (RatingBar)v.findViewById(R.id.ratingBar);
                            // LayerDrawable stars = (LayerDrawable)
                            // ratingBar.getProgressDrawable();
                            // stars.getDrawable(2).setColorFilter(Color.YELLOW,
                            // PorterDuff.Mode.SRC_ATOP);

                            popularmobilephones.addView(v);
                            // new ExecuteLoadMainCategories().execute();
                        }
                        comparepricesfromlayout.removeAllViews();
                        for (int j = 0; j < cataffdata.size(); j++) {
                            View v = getActivity().getLayoutInflater().inflate(
                                    R.layout.activity_comparepricesfrom, null);
                            ImageView affliatelogo = (ImageView) v
                                    .findViewById(R.id.affliatelogo);
                            Picasso.with(getActivity()).load(DBConnection.AFFLIATE_IMGURL + cataffdata.get(j)).resize(144, 0).placeholder(R.drawable.placeholder).into(affliatelogo);

                            comparepricesfromlayout.addView(v);
                        }

						/*
						 * JSONArray couponarray=object.getJSONArray("coupons");
						 *
						 * for(int j=0;j<couponarray.length();j++){ StoreData
						 * storeData = new StoreData(); JSONObject object3=
						 * couponarray.getJSONObject(j); //
						 * Toast.makeText(HomeActivity.this, array.length()+"",
						 * Toast.LENGTH_SHORT).show(); CouponData data = new CouponData();
						 * data.setOfferName(object3.getString("offer_name"));
						 * data.setTitle(object3.getString("title"));
						 * data.setDescription(object3
						 * .getString("description"));
						 * data.setOfferPage(object3.getString("offer_page"));
						 * data.setCode(object3.getString("code"));
						 * data.setExclusive(object3.getString("exclusive"));
						 * data.setFeatured(object3.getString("featured"));
						 * data.setImgName(object3.getString("coupon_image"));
						 *
						 * storeData.setCounponData(data);
						 *
						 * storesList.add(storeData);
						 *
						 *
						 * } Log.e("size", storesList.size()+"");
						 * fillAllCoupons(storesList);
						 */
						/*
						 * JSONArray couponarray=object.getJSONArray("coupons");
						 * for(int m=0;m<couponarray.length();m++){ JSONObject
						 * object1=couponarray.getJSONObject(m); JSONObject
						 * object2 = object1.getJSONObject("store_detail");
						 * StoreData storeData = new StoreData();
						 * storeData.setName(object2.getString("name"));
						 * storeData.setType(object2.getString("type"));
						 * storeData.setAmountPercentage(object2.getString(
						 * "amount_percentage"));
						 * storeData.setCoupons(object2.getString("coupons"));
						 * storeData.setImgLogo(object2.getString("img_logo"));
						 * storeData.setTermsConditions(object2.getString(
						 * "terms_and_conditions"));
						 * storeData.setHowToGetThisOffer
						 * (object2.getString("how_to_get_this_offer"));
						 * storeData.setDesc(object2.getString("desc"));
						 *
						 * JSONObject object3 =
						 * object1.getJSONObject("coupon_detail"); CouponData
						 * data = new CouponData();
						 * data.setOfferName(object3.getString("offer_name"));
						 * data.setTitle(object3.getString("title"));
						 * data.setDescription(object3
						 * .getString("description"));
						 * data.setOfferPage(object3.getString("offer_page"));
						 * data.setCode(object3.getString("code"));
						 * data.setExclusive(object3.getString("exclusive"));
						 * data.setFeatured(object3.getString("featured"));
						 * data.setImgName(object3.getString("coupon_image"));
						 *
						 *
						 * storeData.setCounponData(data);
						 *
						 * storesList.add(storeData);
						 *
						 * } fillAllCoupons(storesList);
						 */

                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }


                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }
            }
        }
    }

    class ExecuteSearchProducts extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String start_letter = "";


        public ExecuteSearchProducts(String start_letter) {
            this.start_letter = start_letter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("start_letter", start_letter));
            JSONObject object = parser.makeHttpRequest(getActivity(), urlSearchProducts,
                    "GET", param);

            Log.e("Object", urlSearchProducts + param.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //		dialog.dismiss();

            List<SubCategoryData> subcatlist = new ArrayList<SubCategoryData>();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        productsdetails = new ArrayList<ProductDetails>();

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productsdetails.add(productdetails);
                        }

                        produnctname = new ArrayList<String>();
                        for (int i = 0; i < productsdetails.size(); i++) {

                            produnctname.add(productsdetails.get(i)
                                    .getProduct_name());
                        }
                        manager.setStoreDetails(produnctname);

                        prodadapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_list_item_1,
                                produnctname);
                        searchtext.setAdapter(prodadapter);


                    } else {
                       /* Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();*/
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    }


    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


    private void searchProducts(String s) {
        String url = urlSearchProducts + "?start_letter=" + s;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.trim().length() > 0) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getInt("success") == 1) {
                            productsdetails = new ArrayList<ProductDetails>();

                            JSONArray subcatarray = object.getJSONArray("products");

                            for (int j = 0; j < subcatarray.length(); j++) {
                                JSONObject subcatobj = subcatarray.getJSONObject(j);
                                ProductDetails productdetails = new ProductDetails();
                                productdetails.setProduct_name(subcatobj
                                        .getString("prod_name"));
                                productdetails.setProduct_id(subcatobj
                                        .getString("product_id"));
                                productsdetails.add(productdetails);
                            }

                            produnctname = new ArrayList<String>();
                            for (int i = 0; i < productsdetails.size(); i++) {

                                produnctname.add(productsdetails.get(i)
                                        .getProduct_name());
                            }
                            manager.setStoreDetails(produnctname);

                            prodadapter = new ArrayAdapter<String>(
                                    getActivity(),
                                    android.R.layout.simple_list_item_1,
                                    produnctname);
                            searchtext.setAdapter(prodadapter);


                        } else {
                       /* Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();*/
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }

}
