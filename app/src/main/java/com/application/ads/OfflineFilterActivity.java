package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.OfflineFilterAdapter;
import com.application.ads.data.FilterData;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OfflineFilterActivity extends Activity {

    ExpandableListView explistview;
    OfflineFilterAdapter expadapter;
    List<String> listDataHeader;
    HashMap<String, List<FilterData>> listChildData;
    List<FilterData> filterBrandDatas, filterCategoryDatas;
    Button submitfilter;
    TextView filterback;
    JSONParser parser;
    String filterids;
    SharedPreferences preferences;
    SessionManager manager;
    private String urlCategoryFilter = "https://achadiscount.in/offline/admin/index.php/json/getcategoryforfilter";
    private String urlBrandFilter = "https://achadiscount.in/offline/admin/index.php/json/getbrandsforfilter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Filters");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        explistview = (ExpandableListView) findViewById(R.id.filterexp);
        filterback = (TextView) findViewById(R.id.filterback);
        parser = new JSONParser();
        manager = new SessionManager(OfflineFilterActivity.this);
        listDataHeader = new ArrayList<String>();
        filterBrandDatas = new ArrayList<FilterData>();
        filterCategoryDatas = new ArrayList<FilterData>();
        listChildData = new HashMap<String, List<FilterData>>();
        filterback.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setResult(2);
                finish();
            }
        });

        submitfilter = (Button) findViewById(R.id.submitfilter);
        submitfilter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(OfflineFilterActivity.this, OfflineFilterAdapter.offlinefilterids.toString(), 5000).show();

                for (int i = 0; i < OfflineFilterAdapter.offlinefilterids.size(); i++) {
                    if (i == 0) {
                        filterids = OfflineFilterAdapter.offlinefilterids.get(i);
                    } else {
                        filterids += "," + OfflineFilterAdapter.offlinefilterids.get(i);
                    }
                }
                Intent intent = new Intent(OfflineFilterActivity.this, OfflineHomeNavigation.class);
                intent.putExtra("filter_ids", filterids);
                setResult(1, intent);
                finish();
            }
        });

        new ExecuteBrandFilterActivity(urlCategoryFilter).execute();

        String url = urlCategoryFilter;
        volleyJsonObjectRequest(url);

    }


    public void volleyJsonObjectRequest(final String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONArray jsonArray = new JSONArray(response);

                    if (response.toString().trim().length() > 0) {


                        List<FilterData> tempFilterData = new ArrayList<FilterData>();
                        if (jsonArray.length() > 0) {
                            if (url.equalsIgnoreCase(urlBrandFilter)) {
                                filterBrandDatas = new ArrayList<FilterData>();
                                filterBrandDatas.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    FilterData filterData = new FilterData();
                                    filterData.setBrandid(jsonObject.getString("id"));
                                    filterData.setBranddesc(jsonObject.getString("description"));
                                    filterData.setBrandimage(jsonObject.getString("image"));
                                    filterData.setBrandlogo(jsonObject.getString("logo"));
                                    filterData.setBrandname(jsonObject.getString("name"));
                                    filterBrandDatas.add(filterData);
                                    //	filterData.setFilterDatas(filterBrandDatas);
                                }
                                listDataHeader.add("Brands");

                            } else {
                                //filterBrandDatas.clear();
                                filterCategoryDatas = new ArrayList<>();
                                filterCategoryDatas.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    FilterData filterData = new FilterData();
                                /*filterData.setCategoryid(jsonObject.getString("id"));
                                filterData.setCategoryimage(jsonObject.getString("image"));
								filterData.setCategoryicon(jsonObject.getString("icon"));*/
                                    filterData.setBrandid(jsonObject.getString("name"));
                                    filterData.setBrandname(jsonObject.getString("name"));
                                    filterCategoryDatas.add(filterData);
                                    //filterData.setFilterCategoryDatas(filterBrandDatas);
                                }

                                listDataHeader.add("Category");

//								new ExecuteBrandFilterActivity(urlBrandFilter).execute();

                                String url = urlBrandFilter;
                                volleyJsonObjectRequest(url);

                            }


                            expadapter = new OfflineFilterAdapter(OfflineFilterActivity.this, listDataHeader, filterBrandDatas, filterCategoryDatas);
                            explistview.setAdapter(expadapter);

							/*specificationData.setParentName(jsonObject.getString("cate_name"));
							specificationData.setParentId(jsonObject.getString("spec_id"));
							JSONArray options=jsonObject.getJSONArray("filter_ids");
							for(int j=0;j<options.length();j++){

								JSONObject jsonOptionObject=options.getJSONObject(j);
								SpecificationData specdata=new SpecificationData();
							specdata.setOptionId(jsonOptionObject.getString("filter_id"));
							specificationDatas.add(specdata);
							}
							specificationData.setSpecificationDataslist(specificationDatas);
							listDataHeader.add(specificationData);

							listChildData.put(specificationData.getParentName(), specificationDatas);
							expadapter=new FilterAdapter(OfflineFilterActivity.this, listDataHeader, listChildData);
							explistview.setAdapter(expadapter);*/

                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //super.onBackPressed();
        setResult(2);
        finish();
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    public class ExecuteBrandFilterActivity extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        private String url;

        public ExecuteBrandFilterActivity(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineFilterActivity.this, "", "Loading...", true, false);
        }


        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            JSONArray object = parser.makeHttpRequest1(OfflineFilterActivity.this, url, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();


            try {

                if (result.trim().length() > 0) {
                    JSONArray array = new JSONArray(result);
                    List<FilterData> tempFilterData = new ArrayList<FilterData>();
                    if (array.length() > 0) {
                        if (url.equalsIgnoreCase(urlBrandFilter)) {
                            filterBrandDatas = new ArrayList<FilterData>();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonObject = array.getJSONObject(i);
                                FilterData filterData = new FilterData();
                                filterData.setBrandid(jsonObject.getString("id"));
                                filterData.setBranddesc(jsonObject.getString("description"));
                                filterData.setBrandimage(jsonObject.getString("image"));
                                filterData.setBrandlogo(jsonObject.getString("logo"));
                                filterData.setBrandname(jsonObject.getString("name"));
                                filterBrandDatas.add(filterData);
                                //	filterData.setFilterDatas(filterBrandDatas);
                            }
                            listDataHeader.add("Brands");

                        } else {
                            //filterBrandDatas.clear();

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonObject = array.getJSONObject(i);
                                FilterData filterData = new FilterData();
								/*filterData.setCategoryid(jsonObject.getString("id"));
								filterData.setCategoryimage(jsonObject.getString("image"));
								filterData.setCategoryicon(jsonObject.getString("icon"));*/
                                filterData.setBrandid(jsonObject.getString("name"));
                                filterData.setBrandname(jsonObject.getString("name"));
                                filterCategoryDatas.add(filterData);
                                //filterData.setFilterCategoryDatas(filterBrandDatas);
                            }

                            listDataHeader.add("Category");
                            new ExecuteBrandFilterActivity(urlBrandFilter).execute();
                            /*String url = urlBrandFilter;
                            volleyJsonObjectRequest(url);*/

                        }

                        expadapter = new OfflineFilterAdapter(OfflineFilterActivity.this, listDataHeader, filterBrandDatas, filterCategoryDatas);
                        explistview.setAdapter(expadapter);

							/*specificationData.setParentName(jsonObject.getString("cate_name"));
							specificationData.setParentId(jsonObject.getString("spec_id"));
							JSONArray options=jsonObject.getJSONArray("filter_ids");
							for(int j=0;j<options.length();j++){

								JSONObject jsonOptionObject=options.getJSONObject(j);
								SpecificationData specdata=new SpecificationData();
							specdata.setOptionId(jsonOptionObject.getString("filter_id"));
							specificationDatas.add(specdata);
							}
							specificationData.setSpecificationDataslist(specificationDatas);
							listDataHeader.add(specificationData);

							listChildData.put(specificationData.getParentName(), specificationDatas);
							expadapter=new FilterAdapter(OfflineFilterActivity.this, listDataHeader, listChildData);
							explistview.setAdapter(expadapter);*/

                    }

                }

            } catch (Exception exception) {
                Log.e("Excep", exception + "");

            }

        }


    }
}
