package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.MyApplication;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class OfflineOfferMap extends Activity implements
        ConnectionCallbacks, OnConnectionFailedListener {

    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    SharedPreferences preferences;
    SessionManager manager;
    Double latitude, longitude;
    LatLng latLng;
    JSONParser parser;
    List<OfflineStoreData> categorydata;
    Button gothere;
    MyApplication myApplication;
    private GoogleMap googleMap;
    private PopupWindow mPopupWindow;
    private int mWidth;
    private int mHeight;
    private String imgurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        buildGoogleApiClient();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_offer_map);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Offers");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        myApplication = MyApplication.getInstance();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        gothere = (Button) findViewById(R.id.gothere);
        imgurl = getIntent().getStringExtra("img_url");
        manager = new SessionManager(OfflineOfferMap.this);
        parser = new JSONParser();
        categorydata = new ArrayList<OfflineStoreData>();

        initilizeMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //	googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        //	googleMap.getUiSettings().setMapToolbarEnabled(true);
        //googleMap.setInfoWindowAdapter(new PopupAdapter(OfflineOfferMap.this, imgurl,""));

        gothere.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                Log.i("LAT", myApplication.getMyLatitude());
                Log.i("LNG", myApplication.getMyLongitude());
                Log.i("addr", "" + latitude);
                Log.i("addr1", "" + longitude);
                String uri = "http://maps.google.com/maps?saddr=" + myApplication.getMyLatitude() + "," + myApplication.getMyLongitude() + "&daddr=" + latitude + "," + longitude + "";

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);

            }
        });
    }


    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(OfflineOfferMap.this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        googleMap.clear();
        /*mylatitude = Double.parseDouble(preferences.getString(
				SessionManager.KEY_LATITUDE, "0.0"));
		mylongitude = Double.parseDouble(preferences.getString(
				SessionManager.KEY_LONGITUDE, "0.0"));*/

        latitude = Double.parseDouble(getIntent().getStringExtra("lat"));
        longitude = Double.parseDouble(getIntent().getStringExtra("long"));
        MarkerOptions marker1 = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title(getIntent().getStringExtra("address"));
        marker1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_RED));

        googleMap.addMarker(marker1);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(12).build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(OfflineOfferMap.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
	    
	   /* @Override
	    protected void onStart() {
	        super.onStart();
	        mGoogleApiClient.connect();
	    }

	    @Override
	    protected void onStop() {
	        super.onStop();
	        if (mGoogleApiClient.isConnected()) {
	            mGoogleApiClient.disconnect();
	        }
	    }
*/

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();

            longitude = mLastLocation.getLongitude();
            myApplication.setMyLatitude(latitude + "");
            myApplication.setMyLongitude(longitude + "");
            Toast.makeText(this, "Lat " + latitude + ", Longitude" + longitude, Toast.LENGTH_LONG).show();
            latitude = Double.parseDouble(myApplication.getMyLatitude());
            longitude = Double.parseDouble(myApplication.getMyLongitude());

            Log.e("latitute", latitude + "");
            Log.e("longitude", longitude + "");
	        
	            /*mLatitudeText.setText(String.format("%s: %f", mLatitudeLabel,
	                    mLastLocation.getLatitude()));
	            mLongitudeText.setText(String.format("%s: %f", mLongitudeLabel,
	                    mLastLocation.getLongitude()));*/
        } else {
            Toast.makeText(this, "No location Detected", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("THIS", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i("THIS", "Connection suspended");
        mGoogleApiClient.connect();
    }


}
