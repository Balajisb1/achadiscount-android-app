package com.application.ads;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.CouponsAdapter;
import com.application.ads.data.CouponsData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.ExpandableHeightGridView;
import com.application.ads.extra.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devel-73 on 6/7/17.
 */

public class CouponsHome extends Fragment {

    final String urlLoadCoupons = DBConnection.BASEURL + "coupons";
    final String urlLoadCouponsMore = DBConnection.BASEURL + "store_ajax_coupon";
    private RequestQueue queue;
    private Context mContext;
    private SharedPreferences preferences;
    private SessionManager manager;
    private Button btnTopCouponAll, btnTopCouponA, btnTopCouponB, btnTopCouponC,
            btnTopCouponD, btnTopCouponE, btnTopCouponF, btnTopCouponG,
            btnTopCouponH, btnTopCouponI, btnTopCouponJ, btnTopCouponK,
            btnTopCouponL, btnTopCouponM, btnTopCouponN, btnTopCouponO,
            btnTopCouponP, btnTopCouponQ, btnTopCouponR, btnTopCouponS,
            btnTopCouponT, btnTopCouponU, btnTopCouponV, btnTopCouponW,
            btnTopCouponX, btnTopCouponY, btnTopCouponZ;
    private ExpandableHeightGridView coupongrid;
    private Button btnTopStores, btnTopCashBackOffers, btnSalableCoupon, btnAllCoupons;
    public static String toolbartitle;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView nocouponstext;
    private List<CouponsData> couponsDatas;
    private Button loadmore;
    private View view;
    private CouponsAdapter adapter;
    private Bundle bundle;
    private int i = 1;
    private ProgressDialog dialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_coupon_grid, null, false);
        initVariables();
        getCoupons();
        loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                getCouponsLoadMore();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                getCoupons();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        btnSalableCoupon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        SalableCoupons.class);
                startActivity(intent);

            }
        });
        btnTopCashBackOffers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        TopCashbackOffers.class);
                startActivity(intent);

            }
        });
        btnAllCoupons.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        CouponNavigation.class);
                startActivity(intent);

            }
        });
        btnTopStores.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        TopStoresList.class);
                startActivity(intent);
            }
        });

        btnTopCouponAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter=new CouponsAdapter(mContext,couponsDatas);
                coupongrid.setAdapter(adapter);
            }
        });
        btnTopCouponA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("A")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("B")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("C")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("D")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("E")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("F")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("G")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("H")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("I")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("J")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("K")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("L")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("M")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("N")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("O")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("P")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Q")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("R")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("S")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("T")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("U")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("W")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("X")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Y")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Z")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void initVariables() {
        mContext = getActivity();
        queue = Volley.newRequestQueue(mContext);
        manager = new SessionManager(mContext);
        preferences = mContext.getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        coupongrid = (ExpandableHeightGridView) view.findViewById(R.id.coupongrid);
        couponsDatas = new ArrayList<>();
        adapter = new CouponsAdapter(mContext, couponsDatas);
        coupongrid.setAdapter(adapter);
        loadmore = (Button) view.findViewById(R.id.loadmore);
        loadmore.setVisibility(View.VISIBLE);
        btnTopStores = (Button) view.findViewById(R.id.btnTopStores);
        btnTopCashBackOffers = (Button) view.findViewById(R.id.btnTopCashBackOffers);
        btnSalableCoupon = (Button) view.findViewById(R.id.btnSalableCoupon);
        btnAllCoupons = (Button) view.findViewById(R.id.btnAllCoupons);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        nocouponstext = (TextView) view.findViewById(R.id.nocouponstext);
        btnTopCouponAll = (Button) view.findViewById(R.id.btnTopCouponAll);
        btnTopCouponA = (Button) view.findViewById(R.id.btnTopCouponA);
        btnTopCouponB = (Button) view.findViewById(R.id.btnTopCouponB);
        btnTopCouponC = (Button) view.findViewById(R.id.btnTopCouponC);
        btnTopCouponD = (Button) view.findViewById(R.id.btnTopCouponD);
        btnTopCouponE = (Button) view.findViewById(R.id.btnTopCouponE);
        btnTopCouponF = (Button) view.findViewById(R.id.btnTopCouponF);
        btnTopCouponG = (Button) view.findViewById(R.id.btnTopCouponG);
        btnTopCouponH = (Button) view.findViewById(R.id.btnTopCouponH);
        btnTopCouponI = (Button) view.findViewById(R.id.btnTopCouponI);
        btnTopCouponJ = (Button) view.findViewById(R.id.btnTopCouponJ);
        btnTopCouponK = (Button) view.findViewById(R.id.btnTopCouponK);
        btnTopCouponL = (Button) view.findViewById(R.id.btnTopCouponL);
        btnTopCouponM = (Button) view.findViewById(R.id.btnTopCouponM);
        btnTopCouponN = (Button) view.findViewById(R.id.btnTopCouponN);
        btnTopCouponO = (Button) view.findViewById(R.id.btnTopCouponO);
        btnTopCouponP = (Button) view.findViewById(R.id.btnTopCouponP);
        btnTopCouponQ = (Button) view.findViewById(R.id.btnTopCouponQ);
        btnTopCouponR = (Button) view.findViewById(R.id.btnTopCouponR);
        btnTopCouponS = (Button) view.findViewById(R.id.btnTopCouponS);
        btnTopCouponT = (Button) view.findViewById(R.id.btnTopCouponT);
        btnTopCouponU = (Button) view.findViewById(R.id.btnTopCouponU);
        btnTopCouponV = (Button) view.findViewById(R.id.btnTopCouponV);
        btnTopCouponW = (Button) view.findViewById(R.id.btnTopCouponW);
        btnTopCouponX = (Button) view.findViewById(R.id.btnTopCouponX);
        btnTopCouponY = (Button) view.findViewById(R.id.btnTopCouponY);
        btnTopCouponZ = (Button) view.findViewById(R.id.btnTopCouponZ);
    }

    public void getCoupons() {
        couponsDatas.clear();
        dialog=ProgressDialog.show(mContext,"","Loading Coupons",true,false);
        StringRequest request = new StringRequest(Request.Method.GET, urlLoadCoupons, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Coupon Response", response);
                dialog.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.getJSONArray("coupons");
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            JSONObject object2 = object1.getJSONObject("coupons");
                            JSONObject object3 = object1.getJSONObject("store_details");
                            CouponsData data = new CouponsData();
                            data.setCouponId(object2.getString("coupon_id"));
                            data.setTitle(object2.getString("title"));
                            data.setOfferName(object2.getString("offer_name"));
                            data.setOfferType(object2.getString("type"));
                            data.setAffiliateId(object3.getString("affiliate_id"));
                            data.setAffiliateDesc(object3.getString("affiliate_desc"));
                            data.setAffiliateImg(object3.getString("affiliate_logo"));
                            couponsDatas.add(data);
                        }
                    } else {
                        nocouponstext.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error + "");
            }
        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }


    public void getCouponsLoadMore() {
        dialog=ProgressDialog.show(mContext,"","Loading Coupons",true,false);
        String url = urlLoadCouponsMore + "?cate_id=&pagenum=" + i + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.e("Coupon Response", response);
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.getJSONArray("coupons");
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            JSONObject object2 = object1.getJSONObject("coupons");
                            JSONObject object3 = object1.getJSONObject("store_details");
                            CouponsData data = new CouponsData();
                            data.setCouponId(object2.getString("coupon_id"));
                            data.setTitle(object2.getString("title"));
                            data.setOfferName(object2.getString("offer_name"));
                            data.setOfferType(object2.getString("type"));
                            data.setAffiliateId(object3.getString("affiliate_id"));
                            data.setAffiliateDesc(object3.getString("affiliate_desc"));
                            data.setAffiliateImg(object3.getString("affiliate_logo"));
                            couponsDatas.add(data);
                        }
                    } else {
                        nocouponstext.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error + "");
            }
        });
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }
}
