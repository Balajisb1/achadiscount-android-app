package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.FilterAdapter;
import com.application.ads.adapter.ProductDetailsAdapter;
import com.application.ads.adapter.ProductGridAdapter;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import info.androidramp.gearload.Loading;

public class SearchProductDetailsActivity extends Activity {

    public static String urlSearchProducts = DBConnection.BASEURL + "search_products.php";
    static ProductGridAdapter productgriddetailsadapter;
    private final int interval = 3000; // 3 Second
    // LinearLayout productdetaillistinglinear;
    ListView productlisting;
    GridView productgrid;
    ProductListRecycleviewAdapter mProductListRecycleviewAdapter;
    TextView prod_parent_name;
    JSONParser parser;
    List<ProductDetails> productdetailslist, productsdetails;
    TextView gridlisttext;
    boolean isLoadMore = false;
    SessionManager manager;
    LinearLayout linearFilter, linearGridList, searchBar;
    AutoCompleteTextView searchtext;
    ImageView searchbtn;
    int status;
    // Button filterbtn;
    ProductDetailsAdapter productdetailsadapter;
    SharedPreferences preferences;
    int minprice, maxprice;
    int start = 0;
    int count = 0;
    int totalcount;
    boolean loadingMore = false;
    Button compareprod;
    String filterids = "";
    int limitposition = 16;
    List<String> produnctname;
    ArrayAdapter<String> prodadapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    Loading loading;
    ProgressDialog pDialog;
    private String urlLoadAllProducts = DBConnection.BASEURL + "search_products_list.php";
    private LinearLayout sortby;
    private Dialog dialog;
    private ImageView sortImg;
    private RadioButton priceLowToHigh, priceHighToLow;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Product Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar()
                .setTitle("Online/Offline Price Comparison With Cashback");
        setContentView(R.layout.activity_product_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        productsdetails = new ArrayList<ProductDetails>();
        sortImg = (ImageView) findViewById(R.id.sortImg);

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.activity_dialog_linear);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


        priceLowToHigh = (RadioButton) dialog.findViewById(R.id.priceLowToHigh);
        priceHighToLow = (RadioButton) dialog.findViewById(R.id.priceHighToLow);


        productlisting = (ListView) findViewById(R.id.productlisting);
        productgrid = (GridView) findViewById(R.id.productgrid);
        mRecyclerView = (RecyclerView) findViewById(R.id.product_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(SearchProductDetailsActivity.this, 2));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        prod_parent_name = (TextView) findViewById(R.id.prod_parent_name);
        linearFilter = (LinearLayout) findViewById(R.id.linearFilter);
        linearGridList = (LinearLayout) findViewById(R.id.linearGridList);
        searchBar = (LinearLayout) findViewById(R.id.searchBar);
        sortby = (LinearLayout) findViewById(R.id.sortby);
        gridlisttext = (TextView) findViewById(R.id.gridlisttext);
        manager = new SessionManager(SearchProductDetailsActivity.this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        searchtext = (AutoCompleteTextView) findViewById(R.id.searchtext);

        prodadapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, produnctname);
        searchtext.setAdapter(prodadapter);


        sortby.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        searchtext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.e("Text", s.toString());
                new ExecuteSearchProducts(s.toString()).execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchbtn = (ImageView) findViewById(R.id.searchbtn);
        searchbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (searchtext.getText().toString().equals("")
                        || searchtext.getText().toString().equals(null)) {
                    Toast.makeText(SearchProductDetailsActivity.this,
                            "Please provide search term", Toast.LENGTH_SHORT).show();
                } else {
                    String name = searchtext.getText().toString();
                    // Log.e("name", name);

                    for (int i = 0; i < productsdetails.size(); i++) {
                        // Log.e("prod name",
                        // productsdetails.get(i).getProduct_name());
                        if (name.equalsIgnoreCase(productsdetails.get(i)
                                .getProduct_name())) {
                            // Toast.makeText(getActivity(),
                            // productsdetails.get(i).getProduct_id(),
                            // Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(
                                    SearchProductDetailsActivity.this,
                                    ProductOverView.class);
                            intent.putExtra("prod_id", productsdetails.get(i)
                                    .getProduct_id());
                            searchtext.setText("");
                            startActivity(intent);
                            status = 1;
                        } else {
                            status = 0;
                            // Toast.makeText(getActivity(),
                            // "No data found..Try different keyword",
                            // Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (status == 0) {
                        Toast.makeText(SearchProductDetailsActivity.this,
                                "No data found..Try different keyword",
                                Toast.LENGTH_SHORT).show();
                        searchtext.setText("");
                    }
                }
            }
        });

        searchtext.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                // Toast.makeText(getApplicationContext(),
                // searchedittext.getText().toString(), Toast.LENGTH_SHORT).show();
                /*
                 * Intent intent = new Intent(getActivity(), CouponList.class);
				 * intent.putExtra("storeName", searchtext.getText()
				 * .toString()); searchtext.setText(""); startActivity(intent);
				 */

                String name = searchtext.getText().toString();
                // Log.e("name", name);
                for (int i = 0; i < productsdetails.size(); i++) {
                    // Log.e("prod name",
                    // productsdetails.get(i).getProduct_name());
                    if (name.equalsIgnoreCase(productsdetails.get(i)
                            .getProduct_name())) {
                        /*
                         * Toast.makeText(getActivity(),
						 * productsdetails.get(i).getProduct_id(), Toast.LENGTH_SHORT)
						 * .show();
						 */
                        Intent intent = new Intent(SearchProductDetailsActivity.this,
                                ProductOverView.class);

                        intent.putExtra("prod_id", productsdetails.get(i)
                                .getProduct_id());
                        searchtext.setText("");
                        startActivity(intent);
                    }
                }
            }
        });

        linearGridList.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {

                    gridlisttext.setText("List");
                    productgrid.setVisibility(View.VISIBLE);
                    productlisting.setVisibility(View.GONE);
                } else {
                    gridlisttext.setText("Grid");
                    productgrid.setVisibility(View.GONE);
                    productlisting.setVisibility(View.VISIBLE);
                }

            }
        });
        linearFilter.setVisibility(View.INVISIBLE);
        sortby.setVisibility(View.INVISIBLE);
        searchBar.setVisibility(View.GONE);

        prod_parent_name.setText("Search Results for \"" + getIntent().getStringExtra("prodName").replace("%20", " ") + "\"");
        prod_parent_name.setVisibility(View.VISIBLE);

        linearFilter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchProductDetailsActivity.this,
                        FilterActivity.class);
                intent.putExtra("category_id", productdetailslist.get(0).getParent_child_id());
                intent.putExtra("status", "1");
                startActivityForResult(intent, 1);

            }
        });


        productdetailslist = new ArrayList<ProductDetails>();

        productdetailsadapter = new ProductDetailsAdapter(
                SearchProductDetailsActivity.this, productdetailslist);
        productlisting.setAdapter(productdetailsadapter);

        productgriddetailsadapter = new ProductGridAdapter(
                SearchProductDetailsActivity.this, productdetailslist);
        productgrid.setAdapter(productgriddetailsadapter);

        mProductListRecycleviewAdapter = new ProductListRecycleviewAdapter(SearchProductDetailsActivity.this, productdetailslist);
        mRecyclerView.setAdapter(mProductListRecycleviewAdapter);


        priceLowToHigh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 2;
                productdetailslist.clear();
                dialog.dismiss();
//                new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName"), 0, limitposition, filterids, "", "", "asc").execute();

                sortImg.setImageResource(R.drawable.sort_asc);
                String url = urlLoadAllProducts + "?prodName=" + getIntent().getStringExtra("prodName").replace(" ", "%20") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "0")
                        + "&start=" + 0 + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&min_price=&max_price&&sort=asc";
                volleyJsonObjectRequest(url);

            }
        });

        priceHighToLow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 3;
                dialog.dismiss();
                productdetailslist.clear();
                sortImg.setImageResource(R.drawable.sort);
//                new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName"), 0, limitposition, filterids, "", "", "desc").execute();

                String url = urlLoadAllProducts + "?prodName=" + getIntent().getStringExtra("prodName").replace(" ", "%20") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "0")
                        + "&start=" + 0 + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&min_price=&max_price&&sort=desc";
                volleyJsonObjectRequest(url);

            }
        });


        // productdetaillistinglinear=(LinearLayout)findViewById(R.id.productdetaillistinglinear);

		/*
         * productlisting.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { //Toast.makeText(getApplicationContext(),
		 * productdetailslist.get(position).getProduct_id(), Toast.LENGTH_SHORT).show();
		 * Intent intent=new
		 * Intent(ProductDetailsActivity.this,ProductOverView.class);
		 * intent.putExtra("prodName",
		 * productdetailslist.get(position).getParent_child_id());
		 * intent.putExtra("prod_name",
		 * productdetailslist.get(position).getProduct_name());
		 * intent.putExtra("prod_img",
		 * productdetailslist.get(position).getProduct_image());
		 * intent.putExtra("prod_price",
		 * productdetailslist.get(position).getProduct_price());
		 * intent.putExtra("prod_rating",
		 * productdetailslist.get(position).getRating());
		 * intent.putExtra("prod_id",
		 * productdetailslist.get(position).getProduct_id());
		 * intent.putExtra("pp_id",
		 * productdetailslist.get(position).getPp_id());
		 * intent.putExtra("prod_desc",
		 * productdetailslist.get(position).getKey_feature().toString().trim());
		 * startActivity(intent); } });
		 * 
		 * productgrid.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { Intent intent=new
		 * Intent(ProductDetailsActivity.this,ProductOverView.class);
		 * intent.putExtra("prodName",
		 * getIntent().getStringExtra("prodName"));
		 * intent.putExtra("prod_name",
		 * productdetailslist.get(position).getProduct_name());
		 * intent.putExtra("prod_img",
		 * productdetailslist.get(position).getProduct_image());
		 * intent.putExtra("prod_price",
		 * productdetailslist.get(position).getProduct_price());
		 * intent.putExtra("prod_rating",
		 * productdetailslist.get(position).getRating());
		 * intent.putExtra("prod_id",
		 * productdetailslist.get(position).getProduct_id());
		 * intent.putExtra("pp_id",
		 * productdetailslist.get(position).getPp_id());
		 * intent.putExtra("prod_desc",
		 * productdetailslist.get(position).getKey_feature().toString().trim());
		 * 
		 * startActivity(intent); } });
		 */

        productlisting.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;
                int topRowVerticalPosition = (productgrid == null || productgrid.getChildCount() == 0) ? 0 : productgrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // Log.e("totalItemCount", totalcount+"");

				/*
				 * for(int i=0;i<FilterAdapter.filterids.size();i++){ if(i==0){
				 * filterids=FilterAdapter.filterids.get(i); } else{
				 * filterids+=","+FilterAdapter.filterids.get(i); } }
				 */


                if ((start + count) == totalcount
                        && scrollState == SCROLL_STATE_IDLE) {
                    // Toast.makeText(getApplicationContext(),
                    // "End of file Reached", Toast.LENGTH_SHORT).show();
                    if (!isLoadMore) {
                        if (status == 2) {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "asc").execute();
                            isLoadMore = true;
                        /*    String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&&sort=asc";
                            volleyJsonObjectRequest(url);*/
                        } else if (status == 3) {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "desc").execute();
                            isLoadMore = true;
                          /*  String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&&sort=desc";
                            volleyJsonObjectRequest(url);*/
                        } else {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "").execute();
                            isLoadMore = true;
                          /*  String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&&sort=";
                            volleyJsonObjectRequest(url);*/
                        }

                    }
                }
            }

        });

        productgrid.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;
                int topRowVerticalPosition = (productgrid == null || productgrid.getChildCount() == 0) ? 0 : productgrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
				/*
				 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                Log.e("totalItemCount", totalcount + "");


                if ((start + count) == totalcount
                        && scrollState == SCROLL_STATE_IDLE) {
                    // Toast.makeText(getApplicationContext(),
                    // "End of file Reached", Toast.LENGTH_SHORT).show();
                    if (!isLoadMore) {
                        if (status == 2) {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "asc").execute();
                            isLoadMore = true;
                           /* String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&&sort=asc";
                            volleyJsonObjectRequest(url);*/
                        } else if (status == 3) {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "desc").execute();
                            isLoadMore = true;
                           /* String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=desc";
                            volleyJsonObjectRequest(url);*/
                        } else {
                            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), (totalcount++), limitposition, filterids, "", "", "").execute();
                            isLoadMore = true;
                           /* String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                                    +"&start="+(totalcount++) + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=";
                            volleyJsonObjectRequest(url);*/
                        }
                    }
                }
            }

        });
        mSwipeRefreshLayout.setEnabled(false);

        pDialog = new ProgressDialog(SearchProductDetailsActivity.this);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                productdetailslist.clear();
                productdetailsadapter.notifyDataSetChanged();
                productgriddetailsadapter.notifyDataSetChanged();


                pDialog.setMessage("Loading..");
                pDialog.setCanceledOnTouchOutside(false);
//                pDialog.show();

//                new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName"), 0, limitposition, filterids, "", "", "").execute();

                Log.i("STATUS", "" + status);
//                loading.Start();

                if (status == 0) {
                   /* String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                            +"&start="+0 + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=";
                    volleyJsonObjectRequest(url);*/

                    new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), 0, limitposition, filterids, "", "", "").execute();

                }

                if (status == 2) {
                    /*String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                            +"&start="+0 + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=asc";
                    volleyJsonObjectRequest(url);*/

                    new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), 0, limitposition, filterids, "", "", "asc").execute();

                }

                if (status == 3) {
                 /*   String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                            +"&start="+0 + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=desc";
                    volleyJsonObjectRequest(url);*/

                    new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), 0, limitposition, filterids, "", "", "desc").execute();

                }


                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


//        new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName"), 0, limitposition, filterids, "", "", "").execute();
//        https://achadiscount.in/android/load_products_list.php?prodName=20003&user_id=0&start=0&end=16&filter_ids=&min_price=&max_price=&sort=




      /*  if(SplashScreenActivity.appstartCount ==1)
        {
            Log.e("COUNTTTTT", ""+SplashScreenActivity.appstartCount);
            String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                    +"&start="+0+ ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=";
            volleyJsonObjectRequest1(url);

        }*/

        Intent intent = getIntent();
        String productKeyName = intent.getStringExtra("prodName");

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        System.out.println(dateFormat.format(new Date())); //2014/08/06


        String url = urlLoadAllProducts + "?prodName=" + getIntent().getStringExtra("prodName").replace(" ", "%20") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "0")
                + "&start=" + 0 + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&min_price=&max_price&sort=";
        volleyJsonObjectRequest(url);
//           volleyJsonObjectRequest1(url);


        // new
        // ExecuteLoadProductDetails(getIntent().getStringExtra("prodName"),0,limitposition,filterids,"","").execute();

        loading = (Loading) findViewById(R.id.loading);

        loading.Start();
        Runnable runnable = new Runnable() {
            public void run() {

//				pd.dismiss();
                loading.Cancel();

            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        isLoadMore = false;
        if (resultCode == 1) {
            for (int i = 0; i < FilterAdapter.filterids.size(); i++) {
                if (i == 0) {
                    this.filterids = FilterAdapter.filterids.get(i);
                } else {
                    this.filterids += "," + FilterAdapter.filterids.get(i);
                }
            }
            filterids = data.getStringExtra("filter_ids");
            productdetailslist.clear();

            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), 0, limitposition, data.getStringExtra("filter_ids"), "",
                    "", "").execute();

         /*   String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                    +"&start="+0 + ""+"&end="+limitposition+"&filter_ids="+filterids+"&min_price=&max_price&sort=";
            volleyJsonObjectRequest(url);*/


        } else {
            new ExecuteLoadProductDetails(getIntent().getStringExtra("prodName").replace(" ", "%20"), 0, limitposition, filterids, "", "", "").execute();


           /* String url=urlLoadAllProducts+"?prodName="+getIntent().getStringExtra("prodName")+"&user_id="+preferences.getString(SessionManager.KEY_USERID, "0")
                    +"&start="+0 + ""+"&end="+limitposition+"&filter_ids="+""+"&min_price=&max_price&sort=";
            volleyJsonObjectRequest(url);*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                if (FilterAdapter.filterids != null) {
                    FilterAdapter.filterids.clear();
                    filterids = "";
                }
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                if (FilterAdapter.filterids != null) {
                    FilterAdapter.filterids.clear();
                    filterids = "";
                }
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (FilterAdapter.filterids != null) {
            FilterAdapter.filterids.clear();
            filterids = "";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // productdetailslist.clear();
        /*productgriddetailsadapter.notifyDataSetChanged();
        productdetailsadapter.notifyDataSetChanged();*/

    }

    public void volleyJsonObjectRequest(String url) {

       /* */

        Log.i("URL", url);
        if (filterids != "") {
            //productdetailslist.clear();
            productdetailsadapter.notifyDataSetChanged();
            productgriddetailsadapter.notifyDataSetChanged();
            mProductListRecycleviewAdapter.notifyDataSetChanged();
        }
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        // Display the first 500 characters of the response string.
                        if (response.trim().length() > 0) {

                            try {

                                JSONObject object = new JSONObject(response);


                                if (object.getInt("success") == 1) {

                                    JSONArray subcatarray = object.getJSONArray("products");

                                    for (int j = 0; j < subcatarray.length(); j++) {
                                        JSONObject subcatobj = subcatarray.getJSONObject(j);
                                        ProductDetails productdetails = new ProductDetails();
                                        productdetails.setPp_id(subcatobj
                                                .getString("pp_id"));
                                        productdetails.setProduct_price(subcatobj
                                                .getString("product_price"));
                                        productdetails.setRating(subcatobj
                                                .getString("rating"));
                                        productdetails.setProduct_name(subcatobj
                                                .getString("prod_name"));
                                        productdetails.setProduct_parent(subcatobj
                                                .getString("parent_child_id"));
                                        productdetails.setMrp(subcatobj.getString("mrp"));
                                        productdetails.setProduct_id(subcatobj
                                                .getString("product_id"));
                                        productdetails.setProduct_image(subcatobj
                                                .getString("product_image"));
                                        productdetails.setKey_feature(subcatobj
                                                .getString("key_feature"));
                                        productdetails.setParent_child_id(subcatobj
                                                .getString("parent_child_id"));
                                        if (subcatobj.has("wishlist_status")) {
                                            productdetails.setProWishListStatus(subcatobj
                                                    .getString("wishlist_status"));
                                        }


                                        productdetailslist.add(productdetails);


                                    }


                                    Log.i("PRODUCTLISTSIZE", "" + productdetailslist.size());

                                    productdetailsadapter.notifyDataSetChanged();
                                    productgriddetailsadapter.notifyDataSetChanged();
                                    mProductListRecycleviewAdapter.notifyDataSetChanged();
                                    isLoadMore = false;


                                } else {
                                    isLoadMore = true;
                                    Toast.makeText(SearchProductDetailsActivity.this,
                                            "No data found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                // TODO: handle exception
                                System.out.println(e);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void volleyJsonObjectRequest1(String url) {
        Log.i("CACHEURL", url);


        if (filterids != "") {
            //productdetailslist.clear();
            productdetailsadapter.notifyDataSetChanged();
            productgriddetailsadapter.notifyDataSetChanged();
            mProductListRecycleviewAdapter.notifyDataSetChanged();


        }
        RequestQueue queue = Volley.newRequestQueue(this);

        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {


                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }

                try {

                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                    JSONObject object = new JSONObject(jsonString);

                    if (object.getInt("success") == 1) {

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setPp_id(subcatobj
                                    .getString("pp_id"));
                            productdetails.setProduct_price(subcatobj
                                    .getString("product_price"));
                            productdetails.setRating(subcatobj
                                    .getString("rating"));
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_parent(subcatobj
                                    .getString("parent_child_id"));
                            productdetails.setMrp(subcatobj.getString("mrp"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productdetails.setProduct_image(subcatobj
                                    .getString("product_image"));
                            productdetails.setKey_feature(subcatobj
                                    .getString("key_feature"));
                            productdetails.setParent_child_id(getIntent()
                                    .getStringExtra("parent_child_id"));
                            if (subcatobj.has("wishlist_status")) {
                                productdetails.setProWishListStatus(subcatobj
                                        .getString("wishlist_status"));
                            }


                            productdetailslist.add(productdetails);


                        }


                        Log.i("PRODUCTLISTSIZE", "" + productdetailslist.size());

                        productdetailsadapter.notifyDataSetChanged();
                        productgriddetailsadapter.notifyDataSetChanged();
                        mProductListRecycleviewAdapter.notifyDataSetChanged();
                        isLoadMore = false;


                    } else {
                        isLoadMore = true;
                        Toast.makeText(SearchProductDetailsActivity.this,
                                "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(cacheRequest);

    }

    public class ExecuteLoadProductDetails extends
            AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String parentchildid;
        int start, end;
        String min_price, max_price;
        private String filterids;
        private String sortOrder;

        public ExecuteLoadProductDetails(String parentchildid, int start,
                                         int end, String filterids, String minprice, String maxprice, String sortOrder) {
            this.parentchildid = parentchildid.replace("25", "");
            this.start = start;
            this.end = end;
            this.filterids = filterids;
            this.min_price = minprice;
            this.max_price = maxprice;
            this.sortOrder = sortOrder;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(SearchProductDetailsActivity.this, "",
                    "Loading..", true, false);

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            Log.i("PROD NAME", parentchildid);

            param.add(new BasicNameValuePair("prodName", parentchildid));
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("start", start + ""));
            param.add(new BasicNameValuePair("end", end + ""));
            param.add(new BasicNameValuePair("filter_ids", filterids));
            param.add(new BasicNameValuePair("min_price", min_price));
            param.add(new BasicNameValuePair("max_price", max_price));
            param.add(new BasicNameValuePair("sort", sortOrder));
            Log.e("Params", param.toString());
            JSONObject object = parser.makeHttpRequest(SearchProductDetailsActivity.this, urlLoadAllProducts, "GET", param);
            // Log.e("Object", object.toString());
            https:
//achadiscount.in/android/load_products_list.php?prodName=20003&user_id=0&start=0&end=16&filter_ids=&min_price=&max_price=&sort=
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            String count = "";
            if (filterids != "") {
                //productdetailslist.clear();
                productdetailsadapter.notifyDataSetChanged();
                productgriddetailsadapter.notifyDataSetChanged();
                mProductListRecycleviewAdapter.notifyDataSetChanged();
            }


            if (result.trim().length() > 0) {

                try {

                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setPp_id(subcatobj
                                    .getString("pp_id"));
                            productdetails.setProduct_price(subcatobj
                                    .getString("product_price"));
                            productdetails.setRating(subcatobj
                                    .getString("rating"));
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_parent(subcatobj
                                    .getString("parent_child_id"));
                            productdetails.setMrp(subcatobj.getString("mrp"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productdetails.setProduct_image(subcatobj
                                    .getString("product_image"));
                            productdetails.setKey_feature(subcatobj
                                    .getString("key_feature"));
                            productdetails.setParent_child_id(subcatobj
                                    .getString("parent_child_id"));
                            if (subcatobj.has("wishlist_status")) {
                                productdetails.setProWishListStatus(subcatobj
                                        .getString("wishlist_status"));
                            }


                            productdetailslist.add(productdetails);


                        }


                        productdetailsadapter.notifyDataSetChanged();
                        productgriddetailsadapter.notifyDataSetChanged();
                        mProductListRecycleviewAdapter.notifyDataSetChanged();
                        isLoadMore = false;


                    } else {
                        isLoadMore = true;
                        Toast.makeText(SearchProductDetailsActivity.this,
                                "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    }

    public class AddToWishList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        ProductDetails data;

        String wishListStatus;
        JSONObject object;
        JSONParser parser;
        List<ProductDetails> productListing;
        ImageView imgProductListingDetailFavourites;
        SharedPreferences preferences;
        Context context;
        String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
                + "add_to_wishlist.php";


        public AddToWishList(String wishListStatus, ProductDetails data,
                             ImageView imgProductListingDetailFavourites, Context context) {
            this.wishListStatus = wishListStatus;
            this.data = data;
            this.imgProductListingDetailFavourites = imgProductListingDetailFavourites;
            this.context = context;
            preferences = context.getSharedPreferences(
                    SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
            parser = new JSONParser();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("product_id", data.getProduct_id()));
            param.add(new BasicNameValuePair("wish_list_status", wishListStatus));
            Log.e("param",
                    urlAddOrRemoveProductsFromWishList + param.toString());
            object = parser.makeHttpRequest(SearchProductDetailsActivity.this, urlAddOrRemoveProductsFromWishList,
                    "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            productListing = new ArrayList<ProductDetails>();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        if (Integer.parseInt(wishListStatus.trim()) == 0) {
                            Toast.makeText(context,
                                    "Products added to Wish List..",
                                    Toast.LENGTH_LONG).show();
                            data.setProWishListStatus("1");
                            imgProductListingDetailFavourites
                                    .setImageResource(R.drawable.filled_heart);

                        } else {
                            Toast.makeText(context,
                                    "Products removed from Wish List..",
                                    Toast.LENGTH_LONG).show();
                            data.setProWishListStatus("0");
                            imgProductListingDetailFavourites
                                    .setImageResource(R.drawable.un_fillled_heart);

                        }
                    } else {
                        Toast.makeText(context, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "Something goes wrong..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    class ExecuteSearchProducts extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String start_letter;

        public ExecuteSearchProducts(String start_letter) {
            this.start_letter = start_letter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(SearchProductDetailsActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            // dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("start_letter", start_letter));
            JSONObject object = parser.makeHttpRequest(SearchProductDetailsActivity.this, urlSearchProducts,
                    "GET", param);

            Log.e("Object", urlSearchProducts + param.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // dialog.dismiss();

            List<SubCategoryData> subcatlist = new ArrayList<SubCategoryData>();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        productsdetails = new ArrayList<ProductDetails>();

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productsdetails.add(productdetails);
                        }

                        produnctname = new ArrayList<String>();
                        for (int i = 0; i < productsdetails.size(); i++) {

                            produnctname.add(productsdetails.get(i)
                                    .getProduct_name());
                        }
                        manager.setStoreDetails(produnctname);

                        prodadapter = new ArrayAdapter<String>(
                                SearchProductDetailsActivity.this,
                                android.R.layout.simple_list_item_1,
                                produnctname);
                        searchtext.setAdapter(prodadapter);

                    } else {
                        Toast.makeText(SearchProductDetailsActivity.this,
                                "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
