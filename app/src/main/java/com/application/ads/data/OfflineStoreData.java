package com.application.ads.data;

public class OfflineStoreData {

    private String id;
    private String brand;
    private String image;
    private String offer;
    private String description;
    private String favorite;
    private String brandname;
    private double maplat;
    private double maplong;
    private double dist;
    private String city;
    private String state;
    private String offerimage;
    private String locationid;
    private String locationcity;
    private String locationlat;
    private String locationlong;
    private String locationaddr;
    private String category;

    private String offerbannerid;
    private String offerbannerbrand;
    private String offerbannerimage;
    private String offerbannerbannername;
    private String offerbanneroffer;
    private String offerbannerdescription;
    private String address;

    public String getOfferbannerid() {
        return offerbannerid;
    }

    public void setOfferbannerid(String offerbannerid) {
        this.offerbannerid = offerbannerid;
    }

    public String getOfferbannerbrand() {
        return offerbannerbrand;
    }

    public void setOfferbannerbrand(String offerbannerbrand) {
        this.offerbannerbrand = offerbannerbrand;
    }

    public String getOfferbannerimage() {
        return offerbannerimage;
    }

    public void setOfferbannerimage(String offerbannerimage) {
        this.offerbannerimage = offerbannerimage;
    }

    public String getOfferbannerbannername() {
        return offerbannerbannername;
    }

    public void setOfferbannerbannername(String offerbannerbannername) {
        this.offerbannerbannername = offerbannerbannername;
    }

    public String getOfferbanneroffer() {
        return offerbanneroffer;
    }

    public void setOfferbanneroffer(String offerbanneroffer) {
        this.offerbanneroffer = offerbanneroffer;
    }

    public String getOfferbannerdescription() {
        return offerbannerdescription;
    }

    public void setOfferbannerdescription(String offerbannerdescription) {
        this.offerbannerdescription = offerbannerdescription;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public double getMaplat() {
        return maplat;
    }

    public void setMaplat(double maplat) {
        this.maplat = maplat;
    }

    public double getMaplong() {
        return maplong;
    }

    public void setMaplong(double maplong) {
        this.maplong = maplong;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOfferimage() {
        return offerimage;
    }

    public void setOfferimage(String offerimage) {
        this.offerimage = offerimage;
    }

    public String getLocationlong() {
        return locationlong;
    }

    public void setLocationlong(String locationlong) {
        this.locationlong = locationlong;
    }

    public String getLocationcity() {
        return locationcity;
    }

    public void setLocationcity(String locationcity) {
        this.locationcity = locationcity;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getLocationlat() {
        return locationlat;
    }

    public void setLocationlat(String locationlat) {
        this.locationlat = locationlat;
    }

    public String getLocationaddr() {
        return locationaddr;
    }

    public void setLocationaddr(String locationaddr) {
        this.locationaddr = locationaddr;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

}
