package com.application.ads.data;

import android.util.Log;

import java.util.ArrayList;

public class CompareProducts {

    public static ArrayList<ProductDetails> productdetails = new ArrayList<ProductDetails>();

    public static ArrayList<ProductDetails> getProducts() {
        return productdetails;
    }

    public static void setProducts(ArrayList<ProductDetails> products) {
        CompareProducts.productdetails = products;
    }

    public static boolean addToCompare(ProductDetails data) {
        if (CompareProducts.productdetails.size() == 0) {
            productdetails.add(data);
            return true;
        } else {
            Log.e("parent child id", productdetails.get(0).getParent_child_id());
            Log.e("parent id", data.getProduct_parent());
            if (data.getProduct_parent().trim()
                    .equals(productdetails.get(0).getParent_child_id().trim())) {
                productdetails.add(data);
                return true;
            } else {
                return false;
            }
        }
    }

    public static boolean isAlreadyInCompare(ProductDetails data) {
        boolean isIt = false;
        for (int i = 0; i < CompareProducts.productdetails.size(); i++) {
            if (data.getProduct_id()
                    .trim()
                    .equals(CompareProducts.productdetails.get(i)
                            .getProduct_id().trim())) {
                isIt = true;
                break;
            }
        }
        return isIt;
    }

    public static boolean removeFromCompare(ProductDetails data) {
        boolean isIt = false;
        for (int i = 0; i < CompareProducts.productdetails.size(); i++) {
            if (data.getProduct_id()
                    .trim()
                    .equals(CompareProducts.productdetails.get(i)
                            .getProduct_id().trim())) {
                CompareProducts.productdetails.remove(i);
                isIt = true;
                break;
            }
        }
        return isIt;
    }

}
