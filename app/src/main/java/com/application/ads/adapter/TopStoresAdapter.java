package com.application.ads.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.CouponList;
import com.application.ads.R;
import com.application.ads.data.ProductData;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TopStoresAdapter extends BaseAdapter {

    List<ProductData> productList;
    Context c;
    LayoutInflater inflater;
    SessionManager manager;

    public TopStoresAdapter(Context c, List<ProductData> productList) {
        // TODO Auto-generated constructor stub
        this.productList = productList;
        this.c = c;
        Log.e("SIze ", productList.size() + "");
        manager = new SessionManager(c);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.top_cashback_detail,
                null);
        final int m = position;

        Button btnTopCashBackViewStore = (Button) view.findViewById(R.id.btnTopCashBackViewStore);
        btnTopCashBackViewStore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, CouponList.class);
                intent.putExtra("storeName", productList.get(m).getProName());
                c.startActivity(intent);
            }
        });
        /*TextView txtCashbackTypeLogo = (TextView) view
                .findViewById(R.id.txtCashbackTypeLogo);*/

        ImageView imgTopCashBackLogo = (ImageView) view
                .findViewById(R.id.imgTopCashBackLogo);

        TextView txtTopCashBackName = (TextView) view
                .findViewById(R.id.txtTopCashBackName);

        TextView txtTopCashBackDescription = (TextView) view
                .findViewById(R.id.txtTopCashBackDescription);

        txtTopCashBackName.setText(productList.get(m).getProName());

        String details = "Earn ";
        if (productList.get(m).getProType().equals("Flat")) {
            details = "Earn Up to Rs." + productList.get(m).getProCashbackAmount() + " cashback ";
        } else {
            details = "Earn Up to " + productList.get(m).getProCashbackAmount() + "% cashback ";
        }
        if (Integer.parseInt(productList.get(m).getProCoupons().trim()) > 0) {
            details = details + "& " + Integer.parseInt(productList.get(m).getProCoupons())
                    + " coupons";
        }
        details = details + " From Acha Discount";
        txtTopCashBackDescription.setText(details);
        Picasso.with(c).load(productList.get(m).getProImagePath()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgTopCashBackLogo);

        return view;
    }

}
