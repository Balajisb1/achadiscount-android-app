package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.TopCashbackAdapter;
import com.application.ads.data.ProductData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TopCashbackOffers extends Activity {
    final String urlLoadTopCashbackProducts = DBConnection.BASEURL + "load_top_cashback.php";
    ImageButton searchbtn;
    JSONObject object;
    JSONParser parser;
    GridView cashbackgrid;
    TopCashbackAdapter topCashbackAdapter;
    List<ProductData> productList;
    Button btnTopCashBackAll, btnTopCashBackFeature, btnTopCashBackThisWeek,
            btnTopCashBackOffers;
    int numOfCount = 2;
    AutoCompleteTextView searchedittext;
    SharedPreferences preferences;
    SessionManager manager;
    List<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_cashback_offers);
        /*getActionBar().setTitle("Top CashBack List");*/
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Top Cashback Offer");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        getActionBar().setTitle("Coupons Top Cashback");

        cashbackgrid = (GridView) findViewById(R.id.cashbackgrid);

        parser = new JSONParser();


        manager = new SessionManager(TopCashbackOffers.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
		
		/*searchedittext=(AutoCompleteTextView)findViewById(R.id.searchstore);
		
		Set<String> set = preferences.getStringSet("key", null);
		names=new ArrayList<String>(set);
		
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(TopCashbackOffers.this, android.R.layout.simple_list_item_1, names);
		searchedittext.setAdapter(adapter);
		
		searchbtn=(ImageButton)findViewById(R.id.searchbtn);
		searchbtn.setOnClickListener(new OnClickListener() {

			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(searchedittext.getText().toString().equals("") || searchedittext.getText().toString().equals(null)){
					Toast.makeText(getApplicationContext(), "Please provide search term", 5000).show();
				}
				else{
				Intent intent=new Intent(TopCashbackOffers.this,CouponList.class);
				intent.putExtra("storeName",searchedittext.getText().toString());
				searchedittext.setText("");
				startActivity(intent);	
				}
			}
		});
		
		
		searchedittext.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
	//			Toast.makeText(getApplicationContext(), searchedittext.getText().toString(), 5000).show();
			
				Intent intent=new Intent(TopCashbackOffers.this,CouponList.class);
				intent.putExtra("storeName",searchedittext.getText().toString());
				searchedittext.setText("");
				startActivity(intent);
			}
		});
		*/

        btnTopCashBackAll = (Button) findViewById(R.id.btnTopCashBackAll);
        btnTopCashBackAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//				new LoadTopCashBack("All").execute();
//				List<NameValuePair> param = new ArrayList<NameValuePair>();
//				param.add(new BasicNameValuePair("type", type));
                String url = urlLoadTopCashbackProducts + "?type=All";
                volleyJsonObjectRequest(url);
            }
        });

        btnTopCashBackFeature = (Button) findViewById(R.id.btnTopCashBackFeature);
        btnTopCashBackFeature.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				new LoadTopCashBack("Featured").execute();
                String url = urlLoadTopCashbackProducts + "?type=Featured";
                volleyJsonObjectRequest(url);
            }
        });

        btnTopCashBackThisWeek = (Button) findViewById(R.id.btnTopCashBackThisWeek);
        btnTopCashBackThisWeek.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				new LoadTopCashBack("StoreOfTheWeek").execute();
                String url = urlLoadTopCashbackProducts + "?type=StoreOfTheWeek";
                volleyJsonObjectRequest(url);

            }
        });

        btnTopCashBackOffers = (Button) findViewById(R.id.btnTopCashBackOffers);
        btnTopCashBackOffers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				new LoadTopCashBack("Offers").execute();
                String url = urlLoadTopCashbackProducts + "?type=Offers";
                volleyJsonObjectRequest(url);
            }
        });

//		new LoadTopCashBack("All").execute();
        String url = urlLoadTopCashbackProducts + "?type=All";
        volleyJsonObjectRequest(url);


    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    productList = new ArrayList<ProductData>();
                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject
                                    .getJSONArray("top_cashback_stores");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object2 = array.getJSONObject(i);
                                ProductData data = new ProductData();
                                // data.setProId(object1.getString(""));
                                data.setProName(object2.getString("name"));
                                data.setProCashbackType(object2.getString("cashback_type"));
                                data.setProType(object2.getString("type"));
                                data.setProCashbackAmount(object2
                                        .getString("amount_percentage"));
                                data.setProImagePath(object2.getString("img_logo"));
                                data.setProCoupons(object2.getString("coupons"));
                                productList.add(data);
                            }
                            topCashbackAdapter = new TopCashbackAdapter(TopCashbackOffers.this, productList);
                            cashbackgrid.setAdapter(topCashbackAdapter);
                            //fillAllLayout();

                        } else {
                            Toast.makeText(TopCashbackOffers.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(TopCashbackOffers.this,
                                "Something goes wrong..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadTopCashBack extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        String type;

        public LoadTopCashBack(String type) {
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(TopCashbackOffers.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("type", type));
            object = parser.makeHttpRequest(TopCashbackOffers.this, urlLoadTopCashbackProducts, "GET",
                    param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            productList = new ArrayList<ProductData>();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        JSONArray array = object
                                .getJSONArray("top_cashback_stores");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object2 = array.getJSONObject(i);
                            ProductData data = new ProductData();
                            // data.setProId(object1.getString(""));
                            data.setProName(object2.getString("name"));
                            data.setProCashbackType(object2.getString("cashback_type"));
                            data.setProType(object2.getString("type"));
                            data.setProCashbackAmount(object2
                                    .getString("amount_percentage"));
                            data.setProImagePath(object2.getString("img_logo"));
                            data.setProCoupons(object2.getString("coupons"));
                            productList.add(data);
                        }
                        topCashbackAdapter = new TopCashbackAdapter(TopCashbackOffers.this, productList);
                        cashbackgrid.setAdapter(topCashbackAdapter);
                        //fillAllLayout();

                    } else {
                        Toast.makeText(TopCashbackOffers.this,
                                "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(TopCashbackOffers.this,
                        "Something goes wrong..", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }

    }
}
