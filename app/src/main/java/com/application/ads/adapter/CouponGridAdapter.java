package com.application.ads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.CodeGenerationPage;
import com.application.ads.LoginActivity;
import com.application.ads.R;
import com.application.ads.data.StoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CouponGridAdapter extends BaseAdapter {

    List<StoreData> storesList;
    Context context;
    LayoutInflater inflater;
    SessionManager manager;
    JSONParser parser;
    SharedPreferences preferences;

    private String urlAddtoClickHistory = DBConnection.BASEURL
            + "load_click_history.php";

    public CouponGridAdapter(Context context, List<StoreData> storesList) {
        this.context = context;
        this.storesList = storesList;
        manager = new SessionManager(context);
        preferences = context.getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        parser = new JSONParser();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return storesList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return storesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final int m = position;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.new_coupon_list_detail, null);
        TextView txtNewCouponStoreName = (TextView) view
                .findViewById(R.id.txtNewCouponStoreName);
        TextView txtNewCouponStoreOffer = (TextView) view
                .findViewById(R.id.txtNewCouponStoreOffer);
        TextView txtNewCouponCouponOffer = (TextView) view
                .findViewById(R.id.txtNewCouponCouponOffer);
        ImageView imgNewCouponCouponImage = (ImageView) view
                .findViewById(R.id.imgNewCouponCouponImage);
        TextView btnNewCouponShowCode = (Button) view
                .findViewById(R.id.btnNewCouponShowCode);

        imgNewCouponCouponImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manager.isLoggedIn()) {
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    intent.putExtra("name", storesList.get(m)
                            .getCounponData().getOfferName());
                    intent.putExtra("code", storesList.get(m)
                            .getCounponData().getCode());
                    intent.putExtra("offer_page", storesList.get(m)
                            .getCounponData().getOfferPage());
                    intent.putExtra("logo", storesList.get(m)
                            .getImgLogo());
                    intent.putExtra("description", storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"));
                    intent.putExtra("store_id", storesList.get(
                            m).getStoreId());
                    intent.putExtra("store_name", storesList.get(m).getName());
                    context.startActivity(intent);
                } else {
                    new ExecuteurlAddtoClickHistory(storesList.get(m)
                            .getCounponData().getOfferName(), storesList.get(m)
                            .getCounponData().getCode(), storesList.get(m)
                            .getCounponData().getOfferPage(), storesList.get(m)
                            .getImgLogo(), storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"), storesList.get(
                            m).getStoreId(), storesList.get(m).getName())
                            .execute();
/*

						List<NameValuePair> param = new ArrayList<NameValuePair>();
						// voucher_name=buytest&store_name=Flipkart
						param.add(new BasicNameValuePair("user_id", preferences.getString(
								SessionManager.KEY_USERID, "")));
						param.add(new BasicNameValuePair("affiliate_id", storeid));
						param.add(new BasicNameValuePair("store_name", storename));
						param.add(new BasicNameValuePair("voucher_name", description));
						JSONObject object = parser.makeHttpRequest(urlAddtoClickHistory,
								"GET", param);
*/


						/*String storeid= storesList.get(m).getStoreId();
                        String storename= storesList.get(m).getName();
						String description=  storesList.get(m).getCounponData().getTitle().replace("&#37;", " %");*/

					/*	String url=urlAddtoClickHistory;

						volleyJsonObjectRequest(url,storesList.get(m)
								.getCounponData().getOfferName(), storesList.get(m)
								.getCounponData().getCode(), storesList.get(m)
								.getCounponData().getOfferPage(), storesList.get(m)
								.getImgLogo(), storesList.get(m).getCounponData()
								.getTitle().replace("&#37;", " %"), storesList.get(
								m).getStoreId(), storesList.get(m).getName());*/


                }
            }
        });

        txtNewCouponCouponOffer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manager.isLoggedIn()) {
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    intent.putExtra("name", storesList.get(m)
                            .getCounponData().getOfferName());
                    intent.putExtra("code", storesList.get(m)
                            .getCounponData().getCode());
                    intent.putExtra("offer_page", storesList.get(m)
                            .getCounponData().getOfferPage());
                    intent.putExtra("logo", storesList.get(m)
                            .getImgLogo());
                    intent.putExtra("description", storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"));
                    intent.putExtra("store_id", storesList.get(
                            m).getStoreId());
                    intent.putExtra("store_name", storesList.get(m).getName());
                    context.startActivity(intent);
                } else {
                    new ExecuteurlAddtoClickHistory(storesList.get(m)
                            .getCounponData().getOfferName(), storesList.get(m)
                            .getCounponData().getCode(), storesList.get(m)
                            .getCounponData().getOfferPage(), storesList.get(m)
                            .getImgLogo(), storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"), storesList.get(
                            m).getStoreId(), storesList.get(m).getName())
                            .execute();

				/*	String url=urlAddtoClickHistory;

					volleyJsonObjectRequest(url,storesList.get(m)
							.getCounponData().getOfferName(), storesList.get(m)
							.getCounponData().getCode(), storesList.get(m)
							.getCounponData().getOfferPage(), storesList.get(m)
							.getImgLogo(), storesList.get(m).getCounponData()
							.getTitle().replace("&#37;", " %"), storesList.get(
							m).getStoreId(), storesList.get(m).getName());
*/
                }
            }
        });

        btnNewCouponShowCode.setText("Show Code");
        btnNewCouponShowCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manager.isLoggedIn()) {
                    Intent intent = new Intent(context,
                            LoginActivity.class);
                    intent.putExtra("name", storesList.get(m)
                            .getCounponData().getOfferName());
                    intent.putExtra("code", storesList.get(m)
                            .getCounponData().getCode());
                    intent.putExtra("offer_page", storesList.get(m)
                            .getCounponData().getOfferPage());
                    intent.putExtra("logo", storesList.get(m)
                            .getImgLogo());
                    intent.putExtra("description", storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"));
                    intent.putExtra("store_id", storesList.get(
                            m).getStoreId());
                    intent.putExtra("store_name", storesList.get(m).getName());
                    context.startActivity(intent);
                } else {
                    new ExecuteurlAddtoClickHistory(storesList.get(m)
                            .getCounponData().getOfferName(), storesList.get(m)
                            .getCounponData().getCode(), storesList.get(m)
                            .getCounponData().getOfferPage(), storesList.get(m)
                            .getImgLogo(), storesList.get(m).getCounponData()
                            .getTitle().replace("&#37;", " %"), storesList.get(
                            m).getStoreId(), storesList.get(m).getName())
                            .execute();

					/*String url=urlAddtoClickHistory;

					volleyJsonObjectRequest(url,storesList.get(m)
							.getCounponData().getOfferName(), storesList.get(m)
							.getCounponData().getCode(), storesList.get(m)
							.getCounponData().getOfferPage(), storesList.get(m)
							.getImgLogo(), storesList.get(m).getCounponData()
							.getTitle().replace("&#37;", " %"), storesList.get(
							m).getStoreId(), storesList.get(m).getName());
*/
                }
            }
        });

        txtNewCouponStoreName.setText(storesList.get(m).getName());
        if (storesList.get(m).getType().trim().equals("Percentage")) {
            txtNewCouponStoreOffer.setText("+ Earn upto "
                    + storesList.get(m).getAmountPercentage() + "% Cashback");
        } else if (storesList.get(m).getType().trim().equals("cate")) {
            txtNewCouponStoreOffer.setText(storesList.get(m).getCounponData().getOfferName());
        } else {
            txtNewCouponStoreOffer.setText("+ Earn upto Rs."
                    + storesList.get(m).getAmountPercentage() + " Cashback");
        }
        txtNewCouponCouponOffer.setText(storesList.get(m).getCounponData()
                .getTitle());

        if (storesList.get(m).getType().equalsIgnoreCase("cate")) {
            Picasso.with(context).load(DBConnection.AFFLIATE_IMGURL + storesList.get(m).getCounponData().getImgName()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgNewCouponCouponImage);

        } else {
            Picasso.with(context).load(storesList.get(m).getImgLogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgNewCouponCouponImage);

        }
        return view;
    }


    public void volleyJsonObjectRequest(String url, final String name, final String code, final String offerpage,
                                        final String logo, final String description, final String storeid,
                                        final String storename) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(context);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            Intent intent = new Intent(context,
                                    CodeGenerationPage.class);
                            intent.putExtra("name", name);
                            intent.putExtra("code", code);
                            intent.putExtra("offer_page", offerpage);
                            intent.putExtra("logo", logo);
                            intent.putExtra("description", description);
                            intent.putExtra("store_id", storeid);
                            intent.putExtra("store_name", storename);
                            context.startActivity(intent);
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


    class ExecuteurlAddtoClickHistory extends AsyncTask<String, String, String> {

        String name, code, offerpage, logo, description, storeid, storename;
        ProgressDialog dialog;

        ExecuteurlAddtoClickHistory(String name, String code, String offerpage,
                                    String logo, String description, String storeid,
                                    String storename) {
            this.name = name;
            this.code = code;
            this.offerpage = offerpage;
            this.logo = logo;
            this.description = description;
            this.storeid = storeid;
            this.storename = storename;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(context, "", "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            // voucher_name=buytest&store_name=Flipkart
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            param.add(new BasicNameValuePair("affiliate_id", storeid));
            param.add(new BasicNameValuePair("store_name", storename));
            param.add(new BasicNameValuePair("voucher_name", description));
            JSONObject object = parser.makeHttpRequest(context, urlAddtoClickHistory,
                    "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Intent intent = new Intent(context,
                                CodeGenerationPage.class);
                        intent.putExtra("name", name);
                        intent.putExtra("code", code);
                        intent.putExtra("offer_page", offerpage);
                        intent.putExtra("logo", logo);
                        intent.putExtra("description", description);
                        intent.putExtra("store_id", storeid);
                        intent.putExtra("store_name", storename);
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    Log.e("Exc", e.toString());
                }
            }
        }

    }
}
