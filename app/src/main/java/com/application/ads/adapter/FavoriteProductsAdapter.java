package com.application.ads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.ProductOverView;
import com.application.ads.R;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.application.ads.MyWishListActivity.preferences;

public class FavoriteProductsAdapter extends BaseAdapter {

    Context context;
    List<ProductDetails> productDetailsList;
    TextView favprodname, favprodprice;
    ImageView favprodremove, favprodimg;
    Button favprodshop;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";
    private ProgressDialog dialog;


    public FavoriteProductsAdapter(Context context, List<ProductDetails> productDetailsList) {

        this.context = context;
        this.productDetailsList = productDetailsList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productDetailsList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return productDetailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int m = position;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_favproduct_grid, null);
        favprodname = (TextView) view.findViewById(R.id.favprodname);
        favprodprice = (TextView) view.findViewById(R.id.favprodprice);
        favprodremove = (ImageView) view.findViewById(R.id.favprodremove);
        favprodimg = (ImageView) view.findViewById(R.id.favprodimg);
        favprodshop = (Button) view.findViewById(R.id.favprodshop);


        favprodname.setText(productDetailsList.get(position).getProduct_name());
        favprodprice.setText("Rs. " + productDetailsList.get(position).getMrp());

        favprodshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productDetailsList.get(m).getParent_child_id());
                intent.putExtra("prod_name", productDetailsList.get(m).getProduct_name());
                intent.putExtra("prod_img", productDetailsList.get(m).getProduct_image());
                intent.putExtra("prod_price", productDetailsList.get(m).getMrp());
                intent.putExtra("prod_rating", 0.0);
                intent.putExtra("prod_id", productDetailsList.get(m).getProduct_id());
                intent.putExtra("pp_id", "");
                intent.putExtra("prod_desc", "");
                context.startActivity(intent);
            }
        });

        favprodremove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("", "Remove clicked");
               // new AddToWishList("1", productDetailsList.get(m), context).execute();
                removeWishList(m);

            }
        });


        Picasso.with(context).load(DBConnection.PRODUCT_IMGURL + productDetailsList.get(position).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(favprodimg);


        return view;
    }


    private void removeWishList(final int position){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Removing..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue= Volley.newRequestQueue(context);
        String url=urlAddOrRemoveProductsFromWishList+"?user_id="+preferences.getString(SessionManager.KEY_USERID, "0")+"&product_id="+productDetailsList.get(position).getProduct_id()+"&wish_list_status=1";

        StringRequest request=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {
                                Toast.makeText(context, "Products removed from Wish List..", Toast.LENGTH_LONG).show();
                            productDetailsList.remove(position);
                            notifyDataSetChanged();

                        } else {
                            Toast.makeText(context,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }


}