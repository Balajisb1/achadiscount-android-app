package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.TicketData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.DatePickerFragment;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddCashbackTicket extends Activity {


    private final int RESULT_LOAD_IMG = 1;

    Spinner spnrAddCashbackTicketRetailer;
    JSONParser parser;
    EditText edtAddCashbackTicketTransactionId, edtAddCashbackTicketTotalAmountPaid,
            edtAddCashbackTicketCouponCodeUsed, edtAddCashbackTicketDetailsOfTransaction, edtAddCashbackTicket;
    Button btnSignInSignIn;
    SessionManager manager;
    TextView missingcashbackterms;
    CheckBox cashbacktermscheckbox;
    SharedPreferences preferences;
    ArrayList<TicketData> tickets;
    ImageView imgRegisterCalander;
    boolean mFirst = true;
    AutoCompleteTextView atoSearchStoreName;
    Button btnSearchGo;
    OnDateSetListener ondate = new OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            if (mFirst) {
                mFirst = false;
                String month = "";
                String day = "";

                if (String.valueOf(monthOfYear + 1).trim().length() > 1) {
                    month = String.valueOf(monthOfYear + 1);
                } else {
                    month = "0" + String.valueOf(monthOfYear + 1);
                }

                if (String.valueOf(dayOfMonth).trim().length() > 1) {
                    day = String.valueOf(dayOfMonth);
                } else {
                    day = "0" + String.valueOf(dayOfMonth);
                }

                if (String.valueOf(month).trim().length() == 1) {
                    month = "0" + month;
                }

                if (String.valueOf(day).trim().length() == 1) {
                    day = "0" + day;
                }

                edtAddCashbackTicket.setText(day + "-" + month + "-" + String.valueOf(year));
                new LoadCashBackDetails().execute();
    /*			String[] dateArr=edtAddCashbackTicket.getText().toString().trim().split("-");

		*//*List<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));

		param.add(new BasicNameValuePair("added_date",dateArr[2]+'-'+dateArr[1]+'-'+dateArr[0]));
		Log.e("param is :", param.toString());
		object = parser.makeHttpRequest(urlLoadMissingCashBackTicket, "GET", param);*//*

				String url=urlLoadMissingCashBackTicket+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0")+"&added_date="+dateArr[2]+'-'+dateArr[1]+'-'+dateArr[0];
				volleyJsonObjectRequest1(url);*/

            }
        }
    };
    private String urlAddMissingCashBackTicket = DBConnection.BASEURL + "add_cashback_ticket.php";
    private String urlLoadMissingCashBackTicket = DBConnection.BASEURL + "get_stores_for_ticket.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_cashback_ticket);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Cashback Ticket");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        tickets = new ArrayList<TicketData>();
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        missingcashbackterms = (TextView) findViewById(R.id.missingcashbackterms);
        manager = new SessionManager(AddCashbackTicket.this);
        cashbacktermscheckbox = (CheckBox) findViewById(R.id.cashbacktermscheckbox);

        imgRegisterCalander = (ImageView) findViewById(R.id.imgRegisterCalander);
        imgRegisterCalander.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        edtAddCashbackTicket = (EditText) findViewById(R.id.edtAddCashbackTicket);
        edtAddCashbackTicketTransactionId = (EditText) findViewById(R.id.edtAddCashbackTicketTransactionId);
        edtAddCashbackTicketTotalAmountPaid = (EditText) findViewById(R.id.edtAddCashbackTicketTotalAmountPaid);
        edtAddCashbackTicketCouponCodeUsed = (EditText) findViewById(R.id.edtAddCashbackTicketCouponCodeUsed);
        edtAddCashbackTicketDetailsOfTransaction = (EditText) findViewById(R.id.edtAddCashbackTicketDetailsOfTransaction);
        spnrAddCashbackTicketRetailer = (Spinner) findViewById(R.id.spnrAddCashbackTicketRetailer);
    /*	btnAddAttachments=(Button)findViewById(R.id.btnAddAttachments);
        btnAddAttachments.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loadImagefromGallery();
			}
		});*/

        missingcashbackterms.setText(Html.fromHtml("<p>&#8226; Retailers do not accept missing cashbacks older than 10 days</p>"
                + "<p>&#8226; Missing Cashback Tickets may take 30-45 days to get resolved and show in your Cashback Account</p>"
                + "<p>&#8226; Incorrect Information may lead to Cancellation of Cashback. Please do not raise multiple tickets for the same transaction. This may also lead to the Retailer declining the enquiry</p>"
                + "<p>&#8226; Cashback added from a Missing ticket may take more than 90 days to Confirm.</p>"
                + "<p>&#8226; Your Cashback may be updated at the time of confirmation</p>"
                + "<p>&#8226; While we shall try our best to resolve and get your missing cashback approved the retailer's decision is final</p>"));

        Calendar calender = Calendar.getInstance();

        String day = String.valueOf(calender.get(Calendar.DAY_OF_MONTH));
        if (day.trim().length() == 1) {
            day = "0" + day;
        }
        String month = String.valueOf(calender.get(Calendar.MONTH) + 1);
        if (month.trim().length() == 1) {
            month = "0" + month;
        }
        edtAddCashbackTicket.setText(day.trim() + "-" + month.trim() + "-" + calender.get(Calendar.YEAR));


        btnSignInSignIn = (Button) findViewById(R.id.btnSignInSignIn);
        btnSignInSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tickets.size() > 0) {
                    if (validateComponents()) {
                        new AddMissingCashBackTicket().execute();

/*

						 String retailerName=spnrAddCashbackTicketRetailer.getSelectedItem().toString();
						 String transactionAmount=edtAddCashbackTicketTotalAmountPaid.getText().toString();
						 String tramsactionRefId=edtAddCashbackTicketTransactionId.getText().toString();
						 String couponCode=edtAddCashbackTicketCouponCodeUsed.getText().toString();
						 String cbDetails=edtAddCashbackTicketDetailsOfTransaction.getText().toString();
						 String clickId=tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getClickId();
						 String clickDate=edtAddCashbackTicket.getText().toString();
						 String storeName=spnrAddCashbackTicketRetailer.getSelectedItem().toString();
						 String transDate=tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getAddedDate();
						 String statusUpdateDate=tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getAddedDate();


			*/
/*			List<NameValuePair> param = new ArrayList<NameValuePair>();
						param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
						param.add(new BasicNameValuePair("retailer_name",retailerName));
						param.add(new BasicNameValuePair("transation_amount",transactionAmount));
						param.add(new BasicNameValuePair("transaction_ref_id",tramsactionRefId));
						param.add(new BasicNameValuePair("coupon_code",couponCode));
						param.add(new BasicNameValuePair("cashback_details",cbDetails));
						param.add(new BasicNameValuePair("attachment",""));
						param.add(new BasicNameValuePair("click_id",clickId));
						param.add(new BasicNameValuePair("click_date",clickDate));
						param.add(new BasicNameValuePair("store_name",storeName));
						param.add(new BasicNameValuePair("status","3"));
						param.add(new BasicNameValuePair("trans_date",transDate));
						param.add(new BasicNameValuePair("cancel_msg",preferences.getString(SessionManager.KEY_USERID,"0")));
						param.add(new BasicNameValuePair("status_update_date",statusUpdateDate));
						param.add(new BasicNameValuePair("ordervalue",preferences.getString(SessionManager.KEY_USERID,"0")));
						object = parser.makeHttpRequest(urlAddMissingCashBackTicket, "GET", param);*//*


						String url=urlAddMissingCashBackTicket+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0")+"&retailer_name="+retailerName
								+"&transation_amount="+transactionAmount+"&transaction_ref_id="+tramsactionRefId+"&coupon_code="+couponCode+"&cashback_details=cbDetails" +
								"&attachment="+"&click_id="+clickId	+"&click_date="+clickDate+"&store_name="+storeName+"&status=3"+"&trans_date="+transDate
								+"&cancel_msg="+preferences.getString(SessionManager.KEY_USERID,"0")
								+"&status_update_date="+statusUpdateDate
								+"&ordervalue="+preferences.getString(SessionManager.KEY_USERID,"0");

						volleyJsonObjectRequest(url);
*/
                    }
                } else {
                    Toast.makeText(AddCashbackTicket.this, "Please select coupon..", Toast.LENGTH_LONG).show();
                }

            }
        });

        new LoadCashBackDetails().execute();
/*
		String[] dateArr=edtAddCashbackTicket.getText().toString().trim().split("-");

		*/
/*List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));

		param.add(new BasicNameValuePair("added_date",dateArr[2]+'-'+dateArr[1]+'-'+dateArr[0]));
		Log.e("param is :", param.toString());
		object = parser.makeHttpRequest(urlLoadMissingCashBackTicket, "GET", param);*//*


		String url=urlLoadMissingCashBackTicket+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0")+"&added_date="+dateArr[2]+'-'+dateArr[1]+'-'+dateArr[0];
		volleyJsonObjectRequest1(url);

*/


    }

    public void volleyJsonObjectRequest1(String url) {


        tickets = new ArrayList<TicketData>();
        TicketData data1 = new TicketData();
        data1.setStoreName("-Select-");
        tickets.add(data1);
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("stores");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                TicketData data = new TicketData();
                                data.setStoreId(object.getString("store_id"));
                                data.setStoreName(object.getString("store_name"));
                                data.setAddedDate(object.getString("date_added"));
                                data.setClickId(object.getString("click_id"));
                                data.setVoucherName(object.getString("voucher_name"));
                                tickets.add(data);
                            }
                            placeSpinner();
                        } else {
                            placeSpinner();
                            Toast.makeText(AddCashbackTicket.this,
                                    "No data found..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AddCashbackTicket.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void volleyJsonObjectRequest(String url) {
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (response.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            clearComponents();
                            Toast.makeText(AddCashbackTicket.this,
                                    "Ticket has been generated successfully..", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(AddCashbackTicket.this,
                                    "Loading Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AddCashbackTicket.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error is", error.toString());
            }
        });
        queue.add(request);

       /* CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            clearComponents();
                            Toast.makeText(AddCashbackTicket.this,
                                    "Ticket has been generated successfully..", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(AddCashbackTicket.this,
                                    "Loading Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AddCashbackTicket.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);
*/
    }

    private boolean validateComponents() {

        if (spnrAddCashbackTicketRetailer.getSelectedItemPosition() == 0) {
            Toast.makeText(AddCashbackTicket.this, "Please Select store..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAddCashbackTicketTransactionId.getText().toString().trim().length() <= 0) {
            Toast.makeText(AddCashbackTicket.this, "Please give value for transaction id..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAddCashbackTicketTotalAmountPaid.getText().toString().trim().length() <= 0) {
            Toast.makeText(AddCashbackTicket.this, "Please give value for amount..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAddCashbackTicketCouponCodeUsed.getText().toString().trim().length() <= 0) {
            Toast.makeText(AddCashbackTicket.this, "Please give value for coupon code..", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtAddCashbackTicketDetailsOfTransaction.getText().toString().trim().length() <= 0) {
            Toast.makeText(AddCashbackTicket.this, "Please give value for Details..", Toast.LENGTH_LONG).show();
            return false;
        } else if (!cashbacktermscheckbox.isChecked()) {
            Toast.makeText(AddCashbackTicket.this, "Please Accept Terms and Conditions..", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();

        String day = String.valueOf(calender.get(Calendar.DAY_OF_MONTH));
        if (day.trim().length() == 1) {
            day = "0" + day;
        }
        String month = String.valueOf(calender.get(Calendar.MONTH));
        if (month.trim().length() == 1) {
            month = "0" + month;
        }

        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", Integer.parseInt(month.trim()));
        args.putInt("day", Integer.parseInt(day.trim()));
        date.setArguments(args);
        mFirst = true;
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    private void placeSpinner() {
        List<String> list = new ArrayList<String>();
        for (TicketData data : tickets) {
            list.add(data.getStoreName());
        }
        spnrAddCashbackTicketRetailer.setAdapter(new ArrayAdapter<String>(AddCashbackTicket.this, android.R.layout.simple_list_item_activated_1, list));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void clearComponents() {
        edtAddCashbackTicketCouponCodeUsed.setText(null);
        edtAddCashbackTicketDetailsOfTransaction.setText(null);
        edtAddCashbackTicketTotalAmountPaid.setText(null);
        edtAddCashbackTicketTransactionId.setText(null);
    }
	
	
	/*public void loadImagefromGallery() {
		Intent galleryIntent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
					&& null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				imgPath = cursor.getString(columnIndex);
				cursor.close();
				new UploadFileToServer().execute();
			} else {
				Toast.makeText(this, "You haven't picked Image",
						Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
					.show();
		}
	}
	
	
	
	
	
	
	
	private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

		long totalSize = 0;
		
		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog=new ProgressDialog(AddCashbackTicket.this);
			dialog.setMessage("loading..");
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			return uploadFile();
		}

		@SuppressWarnings("deprecation")
		private String uploadFile() {
			String responseString = null;

			HttpClient httpclient = new JSONParser().getNewHttpClient();
			HttpPost httppost = new HttpPost(
					DBConnection.BASEURL+"image_upload.php");
			

			try {
				AndMultiPartEntity entity = new AndMultiPartEntity();
				File sourceFile = new File(imgPath);
				entity.addPart("uploaded_file", new FileBody(sourceFile));
				totalSize = entity.getContentLength();
				httppost.setEntity(entity);

				// Making server call
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity r_entity = response.getEntity();

				int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == 200) {
					// Server response
					responseString = EntityUtils.toString(r_entity);
				} else {
					responseString = "Error occurred! Http Status Code: "
							+ statusCode;
				}
			} catch (ClientProtocolException e) {
				responseString = e.toString();
			} catch (IOException e) {
				responseString = e.toString();
			}

			return responseString;

		}

		@Override
		protected void onPostExecute(String result) {
			JSONObject object;
			try {
				Log.e("Result is=========",result);
				object = new JSONObject(result);
				if(Integer.parseInt(object.getString("success").trim())==1){
					imageName=object.getString("url");
					Toast.makeText(AddCashbackTicket.this,"Image uploaded successfully..",Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(AddCashbackTicket.this,"Image upload failed..",Toast.LENGTH_LONG).show();
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			dialog.dismiss();
			super.onPostExecute(result);
		}

	}

	
	*/

    @Override
    protected void onResume() {
        super.onResume();


    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class AddMissingCashBackTicket extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        private String retailerName = spnrAddCashbackTicketRetailer.getSelectedItem().toString();
        private String transactionAmount = edtAddCashbackTicketTotalAmountPaid.getText().toString();
        private String tramsactionRefId = edtAddCashbackTicketTransactionId.getText().toString();
        private String couponCode = edtAddCashbackTicketCouponCodeUsed.getText().toString();
        private String cbDetails = edtAddCashbackTicketDetailsOfTransaction.getText().toString();
        private String clickId = tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getClickId();
        private String clickDate = edtAddCashbackTicket.getText().toString();
        private String storeName = spnrAddCashbackTicketRetailer.getSelectedItem().toString();
        private String transDate = tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getAddedDate();
        private String statusUpdateDate = tickets.get(spnrAddCashbackTicketRetailer.getSelectedItemPosition()).getAddedDate();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddCashbackTicket.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("retailer_name", retailerName));
            param.add(new BasicNameValuePair("transation_amount", transactionAmount));
            param.add(new BasicNameValuePair("transaction_ref_id", tramsactionRefId));
            param.add(new BasicNameValuePair("coupon_code", couponCode));
            param.add(new BasicNameValuePair("cashback_details", cbDetails));
            param.add(new BasicNameValuePair("attachment", ""));
            param.add(new BasicNameValuePair("click_id", clickId));
            param.add(new BasicNameValuePair("click_date", clickDate));
            param.add(new BasicNameValuePair("store_name", storeName));
            param.add(new BasicNameValuePair("status", "3"));
            param.add(new BasicNameValuePair("trans_date", transDate));
            param.add(new BasicNameValuePair("cancel_msg", preferences.getString(SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("status_update_date", statusUpdateDate));
            param.add(new BasicNameValuePair("ordervalue", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(AddCashbackTicket.this, urlAddMissingCashBackTicket, "GET", param);

            Log.e("param", param.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        clearComponents();
                        Toast.makeText(AddCashbackTicket.this,
                                "Ticket has been generated successfully..", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(AddCashbackTicket.this,
                                "Loading Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(AddCashbackTicket.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    class LoadCashBackDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        String[] dateArr = edtAddCashbackTicket.getText().toString().trim().split("-");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddCashbackTicket.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("added_date", dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0]));
            param.add(new BasicNameValuePair("type", "Coupon"));
            Log.e("param is :", param.toString());
            object = parser.makeHttpRequest(AddCashbackTicket.this, urlLoadMissingCashBackTicket, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            tickets = new ArrayList<TicketData>();
            TicketData data1 = new TicketData();
            data1.setStoreName("-Select-");
            tickets.add(data1);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        JSONArray array = object.getJSONArray("stores");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TicketData data = new TicketData();
                            data.setStoreId(object.getString("store_id"));
                            data.setStoreName(object.getString("store_name"));
                            data.setAddedDate(object.getString("date_added"));
                            data.setClickId(object.getString("click_id"));
                            data.setVoucherName(object.getString("voucher_name"));
                            tickets.add(data);
                        }
                        placeSpinner();
                    } else {
                        placeSpinner();
                        Toast.makeText(AddCashbackTicket.this,
                                "No data found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(AddCashbackTicket.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }


    }
}