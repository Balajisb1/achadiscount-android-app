package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.SalableCouponGridAdapter;
import com.application.ads.data.CouponData;
import com.application.ads.data.ProductData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.ExpandableHeightGridView;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.analytics.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SalableCoupons extends Activity {

    final String urlLoadTopCoupons = DBConnection.BASEURL + "load_salable_coupon.php";

    JSONObject object;
    JSONParser parser;
    ImageButton searchbtn;
    Button btnTopStores, btnTopCashBackOffers, btnSalableCoupon, btnAllCoupons;

    List<ProductData> productList;
    Tracker t;
    SalableCouponGridAdapter adapter;
    ExpandableHeightGridView coupongrid;
    List<CouponData> storesList;
    int rowWiseCount = 2;

    Button btnTopCouponAll, btnTopCouponA, btnTopCouponB, btnTopCouponC, btnTopCouponD,
            btnTopCouponE, btnTopCouponF, btnTopCouponG, btnTopCouponH,
            btnTopCouponI, btnTopCouponJ, btnTopCouponK, btnTopCouponL,
            btnTopCouponM, btnTopCouponN, btnTopCouponO, btnTopCouponP,
            btnTopCouponQ, btnTopCouponR, btnTopCouponS, btnTopCouponT,
            btnTopCouponU, btnTopCouponV, btnTopCouponW, btnTopCouponX,
            btnTopCouponY, btnTopCouponZ;
    int numOfCount = 2;


    AutoCompleteTextView searchedittext;
    SharedPreferences preferences;
    SessionManager manager;
    List<String> names;
    TextView toolbartitle;
    TextView nocouponstext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_grid);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Salable Coupons/All");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Salable Coupons");
        nocouponstext = (TextView) findViewById(R.id.nocouponstext);
        coupongrid = (ExpandableHeightGridView) findViewById(R.id.coupongrid);

        storesList = new ArrayList<CouponData>();
        btnTopStores = (Button) findViewById(R.id.btnTopStores);
        btnTopCashBackOffers = (Button) findViewById(R.id.btnTopCashBackOffers);
        btnSalableCoupon = (Button) findViewById(R.id.btnSalableCoupon);
        btnAllCoupons = (Button) findViewById(R.id.btnAllCoupons);

        btnSalableCoupon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalableCoupons.this, SalableCoupons.class);
                startActivity(intent);

            }
        });
        btnTopCashBackOffers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalableCoupons.this, TopCashbackOffers.class);
                startActivity(intent);

            }
        });
        btnTopStores.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalableCoupons.this, TopStoresList.class);
                startActivity(intent);
            }
        });

        btnAllCoupons.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalableCoupons.this,
                        CouponNavigation.class);
                startActivity(intent);

            }
        });

        parser = new JSONParser();


        storesList = new ArrayList<CouponData>();

        manager = new SessionManager(SalableCoupons.this);
        preferences = SalableCoupons.this.getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);


        btnTopCouponAll = (Button) findViewById(R.id.btnTopCouponAll);
        btnTopCouponAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadAllCouponList("").execute();
//						param.add(new BasicNameValuePair("store_name",startLetter));

/*
                        String url=urlLoadTopCoupons+"?store_name=";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/All");
            }
        });

        btnTopCouponA = (Button) findViewById(R.id.btnTopCouponA);
        btnTopCouponA.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("A").execute();
/*
                        String url=urlLoadTopCoupons+"?store_name=A";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/A");
            }
        });
        btnTopCouponB = (Button) findViewById(R.id.btnTopCouponB);
        btnTopCouponB.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("B").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=B";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/B");
            }
        });

        btnTopCouponC = (Button) findViewById(R.id.btnTopCouponC);
        btnTopCouponC.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("C").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=C";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/C");
            }
        });
        btnTopCouponD = (Button) findViewById(R.id.btnTopCouponD);
        btnTopCouponD.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("D").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=D";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/D");
            }
        });
        btnTopCouponE = (Button) findViewById(R.id.btnTopCouponE);
        btnTopCouponE.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("E").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=E";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/E");
            }
        });
        btnTopCouponF = (Button) findViewById(R.id.btnTopCouponF);
        btnTopCouponF.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("F").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=F";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/F");
            }
        });
        btnTopCouponG = (Button) findViewById(R.id.btnTopCouponG);
        btnTopCouponG.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("G").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=G";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/G");
            }
        });
        btnTopCouponH = (Button) findViewById(R.id.btnTopCouponH);
        btnTopCouponH.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("H").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=H";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/H");
            }
        });
        btnTopCouponI = (Button) findViewById(R.id.btnTopCouponI);
        btnTopCouponI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("I").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=I";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/I");
            }
        });
        btnTopCouponJ = (Button) findViewById(R.id.btnTopCouponJ);
        btnTopCouponJ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("J").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=J";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/J");
            }
        });
        btnTopCouponK = (Button) findViewById(R.id.btnTopCouponK);
        btnTopCouponK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("K").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=K";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/K");
            }
        });
        btnTopCouponL = (Button) findViewById(R.id.btnTopCouponL);
        btnTopCouponL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("L").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=L";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/L");
            }
        });
        btnTopCouponM = (Button) findViewById(R.id.btnTopCouponM);
        btnTopCouponM.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("M").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=M";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/M");
            }
        });
        btnTopCouponN = (Button) findViewById(R.id.btnTopCouponN);
        btnTopCouponN.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("N").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=N";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/N");
            }
        });
        btnTopCouponO = (Button) findViewById(R.id.btnTopCouponO);
        btnTopCouponO.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("O").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=O";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/O");
            }
        });
        btnTopCouponP = (Button) findViewById(R.id.btnTopCouponP);
        btnTopCouponP.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("P").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=P";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/P");
            }
        });
        btnTopCouponQ = (Button) findViewById(R.id.btnTopCouponQ);
        btnTopCouponQ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Q").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=Q";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/Q");
            }
        });
        btnTopCouponR = (Button) findViewById(R.id.btnTopCouponR);
        btnTopCouponR.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("R").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=R";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/R");
            }
        });
        btnTopCouponS = (Button) findViewById(R.id.btnTopCouponS);
        btnTopCouponS.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("S").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=S";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/S");
            }
        });
        btnTopCouponT = (Button) findViewById(R.id.btnTopCouponT);
        btnTopCouponT.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("T").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=T";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/T");
            }
        });
        btnTopCouponU = (Button) findViewById(R.id.btnTopCouponU);
        btnTopCouponU.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("U").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=U";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/U");
            }
        });
        btnTopCouponV = (Button) findViewById(R.id.btnTopCouponV);
        btnTopCouponV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("V").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=v";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/V");
            }
        });
        btnTopCouponW = (Button) findViewById(R.id.btnTopCouponW);
        btnTopCouponW.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("W").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=W";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/W");
            }
        });
        btnTopCouponX = (Button) findViewById(R.id.btnTopCouponX);
        btnTopCouponX.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("X").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=X";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/X");
            }
        });
        btnTopCouponY = (Button) findViewById(R.id.btnTopCouponY);
        btnTopCouponY.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Y").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=Y";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/Y");
            }
        });
        btnTopCouponZ = (Button) findViewById(R.id.btnTopCouponZ);
        btnTopCouponZ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Z").execute();
/*
						String url=urlLoadTopCoupons+"?store_name=Z";
						volleyJsonObjectRequest(url);
*/
                toolbartitle.setText("Salable Coupons/Z");
            }
        });

        new LoadAllCouponList("").execute();
	/*	String url=urlLoadTopCoupons+"?store_name=";
		volleyJsonObjectRequest(url);
	*/
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    storesList = new ArrayList<CouponData>();

                    List<CouponData> storedetails = new ArrayList<CouponData>();
                    storedetails.clear();
                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            storesList.clear();
                            JSONArray array = jsonObject.getJSONArray("salablecoupons");
                            for (int m = 0; m < array.length(); m++) {
                                JSONObject object3 = array.getJSONObject(m);
                                CouponData data = new CouponData();
                                data.setPrice(object3.getString("amount"));
                                data.setImgName(object3.getString("coupon_image"));
                                data.setDescription(object3
                                        .getString("description"));
                                data.setCouponId(object3.getString("coupon_id"));
                                data.setStoreid(object3.getString("store_location"));
                                data.setTitle(object3.getString("title"));
                                data.setStoredesc(object3.getString("store_description"));

                                JSONArray array1 = object3.getJSONArray("storedetails");
                                for (int j = 0; j < array1.length(); j++) {
                                    JSONObject object4 = array1.getJSONObject(j);
                                    CouponData data1 = new CouponData();
                                    data1.setStorename(object4.getString("store_name"));
                                    data1.setStorelogo(object4.getString("store_logo"));
                                    storedetails.add(data1);

                                }
                                storesList.add(data);
                                storesList.get(m).setStorelist(storedetails);
                            }
                            Log.e("sal", storesList.size() + "");
                            if (storesList.size() == 0) {
                                nocouponstext.setVisibility(View.VISIBLE);
                            } else {
                                nocouponstext.setVisibility(View.GONE);
                            }
                        } else {
                            Toast.makeText(SalableCoupons.this, "No Data Found..",
                                    Toast.LENGTH_LONG).show();
                        }
                        adapter = new SalableCouponGridAdapter(SalableCoupons.this, storesList);
                        coupongrid.setAdapter(adapter);
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadAllCouponList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        String startLetter;

        public LoadAllCouponList(String startLetter) {
            this.startLetter = startLetter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(SalableCoupons.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("store_name", startLetter));
            object = parser.makeHttpRequest(SalableCoupons.this, urlLoadTopCoupons, "GET", param);
            Log.e("parser", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            storesList = new ArrayList<CouponData>();

            List<CouponData> storedetails = new ArrayList<CouponData>();
            storedetails.clear();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        storesList.clear();
                        JSONArray array = object.getJSONArray("salablecoupons");
                        for (int m = 0; m < array.length(); m++) {
                            JSONObject object3 = array.getJSONObject(m);
                            CouponData data = new CouponData();
                            data.setPrice(object3.getString("amount"));
                            data.setImgName(object3.getString("coupon_image"));
                            data.setDescription(object3
                                    .getString("description"));
                            data.setCouponId(object3.getString("coupon_id"));
                            data.setStoreid(object3.getString("store_location"));
                            data.setTitle(object3.getString("title"));
                            data.setStoredesc(object3.getString("store_description"));

                            JSONArray array1 = object3.getJSONArray("storedetails");
                            for (int j = 0; j < array1.length(); j++) {
                                JSONObject object4 = array1.getJSONObject(j);
                                CouponData data1 = new CouponData();
                                data1.setStorename(object4.getString("store_name"));
                                data1.setStorelogo(object4.getString("store_logo"));
                                storedetails.add(data1);

                            }
                            storesList.add(data);
                            storesList.get(m).setStorelist(storedetails);
                        }
                        Log.e("sal", storesList.size() + "");
                        if (storesList.size() == 0) {
                            nocouponstext.setVisibility(View.VISIBLE);
                        } else {
                            nocouponstext.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(SalableCoupons.this, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                    adapter = new SalableCouponGridAdapter(SalableCoupons.this, storesList);
                    coupongrid.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.e("Error", e + "");
                }
            }
            dialog.dismiss();
        }
    }
}
