package com.application.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.OfflineFilterAdapter;
import com.application.ads.adapter.OfflineStoreAdapter;
import com.application.ads.adapter.OfflineStoreListviewAdapter;
import com.application.ads.adapter.PopupAdapter;
import com.application.ads.data.MyApplication;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OfflineHome extends Fragment implements
        ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    public static String urlLoadMainCategories = "https://achadiscount.in/offline/admin/index.php/json/getofferlist";
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    SessionManager manager;
    SharedPreferences preferences;
    ListView offlinestorelist;
    GridView offlinestoregrid;
    JSONParser parser;
    View markerView;
    List<OfflineStoreData> categorydata;
    OfflineStoreAdapter adapter;
    OfflineStoreListviewAdapter listviewAdapter;
    LinearLayout linearGridList, linearMap, linearmylocation;
    TextView gridlisttext, mapgridtext, mylocationtext;
    RelativeLayout maprelative;
    double distorderby[];
    boolean gpsStatus = false;
    //Double mylatitude, mylongitude;
    double latitude, longitude;
    LatLng latLng;
    LinearLayout storedetailslinear, linearOFflineFilter;
    String brand = "";
    String category = "";
    boolean status = false;
    MyApplication myApplication;
    String brandid, brandimage, branddesc;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Marker lastOpenned = null;
    private LocationRequest mLocationRequest;
    /* For google map */
    private GoogleMap googleMap;
    private Marker marker;

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels,
                displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build();
        StrictMode.setThreadPolicy(policy);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);
        View view = inflater.inflate(R.layout.activity_offline_stores, container, false);
        myApplication = MyApplication.getInstance();


        offlinestorelist = (ListView) view.findViewById(R.id.offlinestorelist);
        offlinestoregrid = (GridView) view.findViewById(R.id.offlinestoregrid);
        linearGridList = (LinearLayout) view.findViewById(R.id.linearGridList);
        linearMap = (LinearLayout) view.findViewById(R.id.linearMap);
        linearmylocation = (LinearLayout) view.findViewById(R.id.linearmylocation);
        linearOFflineFilter = (LinearLayout) view.findViewById(R.id.linearOFflineFilter);
        maprelative = (RelativeLayout) view.findViewById(R.id.maprelative);
        gridlisttext = (TextView) view.findViewById(R.id.gridlisttext);
        mapgridtext = (TextView) view.findViewById(R.id.mapgridtext);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        categorydata = new ArrayList<OfflineStoreData>();
        categorydata.clear();

        if (googleMap == null) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {

                googleMap = ((MapFragment) getChildFragmentManager().findFragmentById(
                        R.id.map)).getMap();
            } else {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.map)).getMap();
            }
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }


            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setMapToolbarEnabled(true);
        }

        linearOFflineFilter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity(), OfflineFilterActivity.class);
                gpsStatus = true;
                startActivityForResult(intent, 1);
            }
        });
        linearmylocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OfflineMap.class);
                startActivity(intent);
            }
        });





/*
        googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                // Check if there is an open info window
                if (lastOpenned != null) {
                    // Close the info window
                    lastOpenned.hideInfoWindow();

                    // Is the marker the same marker that was already open
                    if (lastOpenned.equals(marker)) {
                        // Nullify the lastOpenned object
                        lastOpenned = null;
                        // Return so that the info window isn't openned again
                        return true;
                    }
                }

                // Open the info window for the marker
                marker.showInfoWindow();
                // Re-assign the last openned such that we can close it later
                lastOpenned = marker;

                // Event was handled by our code do not launch default behaviour.
                return true;
            }
        });
        */


        googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle().toString().equalsIgnoreCase("me")) {
                    marker.hideInfoWindow();
                    return true;
                } else {
                    marker.showInfoWindow();
                }


                return false;

            }
        });


        offlinestorelist.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
              /*  start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;*/
                int topRowVerticalPosition = (offlinestorelist == null || offlinestorelist.getChildCount() == 0) ? 0 : offlinestorelist.getChildAt(0).getTop();
                /*
                 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */


                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub

                // Log.e("totalItemCount", totalcount+"");


            }

        });

        offlinestoregrid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
              /*  start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;*/
                int topRowVerticalPosition = (offlinestoregrid == null || offlinestoregrid.getChildCount() == 0) ? 0 : offlinestoregrid.getChildAt(0).getTop();
                /*
				 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */

                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                // Log.e("totalItemCount", totalcount+"");


            }

        });


        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                new ExecuteLoadMainCategories("", "").execute();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
		/*googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker marker) {

				String markertitle=marker.getTitle();

			      for(int i=0;i<categorydata.size();i++){

			    	  if(markertitle.equalsIgnoreCase(categorydata.get(i).getBrandname())){
			    		  Intent intent=new Intent(getActivity(),OfflineOfferDetails.class);
							intent.putExtra("id",categorydata.get(i).getBrand());
							intent.putExtra("desc",categorydata.get(i).getDescription());
							startActivity(intent);
			    	  }
			      }


			}
		});*/

        googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {


                String markertitle = marker.getTitle();

                for (int i = 0; i < categorydata.size(); i++) {

                    if (markertitle.equalsIgnoreCase(categorydata.get(i).getBrandname())) {
                        Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                        intent.putExtra("id", categorydata.get(i).getBrand());
                        intent.putExtra("desc", categorydata.get(i).getDescription());
                        startActivity(intent);
                    }
                }
            }
        });


        storedetailslinear = (LinearLayout) view.findViewById(R.id.storedetailslinear);
        parser = new JSONParser();
        manager = new SessionManager(getActivity());
        preferences = getActivity().getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);

        linearGridList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                    gridlisttext.setText("List");
                    if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {
                        mapgridtext.setText("Map");
                    }
                    offlinestoregrid.setVisibility(View.VISIBLE);
                    offlinestorelist.setVisibility(View.GONE);
                    maprelative.setVisibility(View.GONE);
                } else {
                    gridlisttext.setText("Grid");
                    if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {
                        mapgridtext.setText("Map");
                    }
                    offlinestoregrid.setVisibility(View.GONE);
                    offlinestorelist.setVisibility(View.VISIBLE);
                    maprelative.setVisibility(View.GONE);
                }

            }
        });


        new ExecuteLoadMainCategories(brand, category).execute();
//
//        List<NameValuePair> param = new ArrayList<NameValuePair>();
//        //?category=&brand=&lat=9.9252007&long=78.11977539999998
//        param.add(new BasicNameValuePair("category", category));
//        param.add(new BasicNameValuePair("brand", brand));
//        param.add(new BasicNameValuePair("lat", myApplication.getMyLatitude()));
//        param.add(new BasicNameValuePair("long", myApplication.getMyLongitude()));

//        String url=urlLoadMainCategories+"?category="+category+"&brand="+brand+"&lat="+ myApplication.getMyLatitude()+"&long="+ myApplication.getMyLongitude();
//        volleyJsonObjectRequest(url);


        linearMap.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mapgridtext.getText().toString().equalsIgnoreCase("grid")) {

                    mapgridtext.setText("Map");
                    if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                        gridlisttext.setText("List");
                    }

                    offlinestoregrid.setVisibility(View.VISIBLE);
                    maprelative.setVisibility(View.GONE);
                    offlinestorelist.setVisibility(View.GONE);

                } else {
                    mapgridtext.setText("Grid");

                    if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {
                        gridlisttext.setText("List");
                    }
                    //gridlisttext.setText("Grid");
                    offlinestoregrid.setVisibility(View.GONE);
                    maprelative.setVisibility(View.VISIBLE);
                    offlinestorelist.setVisibility(View.GONE);
                    checkGPS();

                    //googleMap.set
                }


            }
        });

        offlinestoregrid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                intent.putExtra("id", categorydata.get(position).getBrand());
                intent.putExtra("desc", categorydata.get(position).getDescription());
                startActivity(intent);
            }
        });

        offlinestorelist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                intent.putExtra("id", categorydata.get(position).getBrand());
                intent.putExtra("desc", categorydata.get(position).getDescription());
                startActivity(intent);

            }
        });

        return view;
        //initilizeMap();
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    categorydata = new ArrayList<OfflineStoreData>();
                    categorydata.clear();
                    List<String> categoryList = new ArrayList<String>();
                    categoryList.clear();

                    JSONArray jsonArray = jsonObject.getJSONArray("queryresult");
                    if (jsonArray.length() > 0) {
                        categorydata = new ArrayList<OfflineStoreData>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);
                            OfflineStoreData data = new OfflineStoreData();
                            data.setId(object1.getString("id"));
                            data.setImage(object1.getString("image"));
                            data.setBrand(object1.getString("brand"));
                            data.setOffer(object1.getString("offer"));
                            data.setDescription(object1.getString("description"));
                            data.setBrandname(object1.getString("brandname"));
                            data.setMaplat(object1.getDouble("maplat"));
                            data.setMaplong(object1.getDouble("maplong"));
                            data.setDist(object1.getDouble("dist"));
                            data.setCategory(object1.getString("category"));

                            List<String> categoryDBList = Arrays.asList(object1.getString("category").split(","));
                            if (category == "") {
                                categorydata.add(data);
                            } else {
                                categoryList = Arrays.asList(category.split(","));
                                for (int ij = 0; ij < categoryList.size(); ij++) {

                                    if (categoryDBList.contains(categoryList.get(ij))) {
                                        status = true;
                                    } else {
                                        status = false;
                                    }
                                }
                                if (status) {
                                    categorydata.add(data);
                                }

                            }

                        }
                        if (categorydata.size() == 0) {
                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                    }
                    initilizeMap(categorydata);
                    adapter = new OfflineStoreAdapter(getActivity(), categorydata);
                    listviewAdapter = new OfflineStoreListviewAdapter(getActivity(), categorydata);
                    offlinestorelist.setAdapter(listviewAdapter);
                    offlinestoregrid.setAdapter(adapter);

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void makeMap() {

        initilizeMap(categorydata);


    }

    public boolean checkGPS() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (myApplication.getMyLatitude() == null) {
                mGoogleApiClient.connect();
            } else {

                makeMap();
            }
            return true;
        } else {
            showGPSDisabledAlertToUser();
            return false;
        }
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("GPS Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                gpsStatus = true;
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        gpsStatus = false;

                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            if (OfflineFilterAdapter.offlinefilterids.size() > 0) {
                String brands = "";
                String categorys = "";
                for (int i = 0; i < OfflineFilterAdapter.offlinefilterids.size(); i++) {
                    if (OfflineFilterAdapter.offlinefilterids.get(i).matches("[0-9]+")) {
                        brands += OfflineFilterAdapter.offlinefilterids.get(i) + ",";
                        //brandslist.add(OfflineFilterAdapter.offlinefilterids.get(i));
                    } else {
                        categorys += OfflineFilterAdapter.offlinefilterids.get(i) + ",";
                        //	categorylist.add(OfflineFilterAdapter.offlinefilterids.get(i));
                    }
                }
                if (brands.length() > 0) {
                    brands = brands.substring(0, brands.length() - 1);
                }
                if (categorys.length() > 0) {
                    categorys = categorys.substring(0, categorys.length() - 1);
                }
                new ExecuteLoadMainCategories(brands, categorys).execute();


//                String url=urlLoadMainCategories+"?category="+category+"&brand="+brand+"&lat="+ myApplication.getMyLatitude()+"&long="+ myApplication.getMyLongitude();
//                volleyJsonObjectRequest(url);

                //Toast.makeText(getActivity(), OfflineFilterAdapter.offlinefilterids+"", 5000).show();
            } else {
                new ExecuteLoadMainCategories("", "").execute();
            }
        }

    }


    private void initilizeMap(List<OfflineStoreData> categorydata) {

        googleMap.clear();
//		Toast.makeText(getActivity(), gpsStatus+"", 5000).show();
        latitude = Double.parseDouble(myApplication.getMyLatitude());
        longitude = Double.parseDouble(myApplication.getMyLongitude());
        MarkerOptions marker1 = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("ME");
        marker1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_RED));
        googleMap.addMarker(marker1);

        googleMap.addCircle(new CircleOptions().fillColor(Color.parseColor("#20ff0000")).radius(10000).center(new LatLng(latitude, longitude)).strokeColor(Color.TRANSPARENT));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        placeAllStores(categorydata);

    }

    public void placeAllStores(final List<OfflineStoreData> categorydata) {
		
/*		for(int i=0;i<categorydata.size();i++){
			distorderby[i]=categorydata.get(i).getDist();
		}
		List<OfflineStoreData> templist=new ArrayList<OfflineStoreData>();
*/
        //Log.e("dist", categorydata.size()+"");
        storedetailslinear.removeAllViews();

        if (categorydata.size() > 0) {
            for (int i = 0; i < categorydata.size(); i++) {

                Log.e("dist", categorydata.get(i).getDist() + "");
                final int m = i;
                //if(categorydata.get(i).getDist()<=10){

                markerView = setView(categorydata.get(i).getBrand(), categorydata.get(i).getImage(), categorydata.get(i).getDescription());

                brandid = categorydata.get(i).getBrand();
                brandimage = categorydata.get(i).getImage();
                branddesc = categorydata.get(i).getDescription();
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(categorydata.get(i).getMaplat(),
                                categorydata.get(i).getMaplong())).title(categorydata.get(i).getBrandname());
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                googleMap.setInfoWindowAdapter(new PopupAdapter(getActivity(), categorydata));
                googleMap.addMarker(marker);
                View view = getActivity().getLayoutInflater().inflate(R.layout.activity_offline_categories, null);
                ImageView categoryimg = (ImageView) view.findViewById(R.id.categoryimg);
                TextView categoryname = (TextView) view.findViewById(R.id.categoryname);
                Picasso.with(getActivity()).load(DBConnection.OFFLINE_EXTRAIMG + categorydata.get(i).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);
                categoryname.setText(categorydata.get(i).getBrandname());


                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                        intent.putExtra("id", categorydata.get(m).getBrand());
                        intent.putExtra("desc", categorydata.get(m).getDescription());
                        startActivity(intent);
                    }
                });
                storedetailslinear.addView(view);
            }
        }
			/*googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker arg0) {
					Intent intent=new Intent(getActivity(),OfflineOfferDetails.class);
					intent.putExtra("id",categorydata.get(m).getBrand() );
					intent.putExtra("desc", categorydata.get(m).getDescription());
					startActivity(intent);
					
				}
			});*/
        //}
    }


    public View setView(final String brandid, String brandimage, final String branddesc) {
        View popup = null;
        if (popup == null) {
            popup = getActivity().getLayoutInflater().inflate(R.layout.activity_offline_offer_homepage, null);
        }

        ImageView tv = (ImageView) popup.findViewById(R.id.offerimghome);
        Picasso.with(getActivity()).load(DBConnection.OFFLINE_EXTRAIMG + brandimage).placeholder(R.drawable.adslogo).resize(144, 0).into(tv);


        popup.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), OfflineOfferDetails.class);
                intent.putExtra("id", brandid);
                intent.putExtra("desc", branddesc);
                startActivity(intent);
            }
        });
//		    tv.setText(marker.getSnippet());

        return (popup);
    }

    /*   @Override
       public void onStart() {
           super.onStart();
           mGoogleApiClient.connect();
       }
*/
    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.e("LastLocation", mLastLocation + "");
	       /* if(myApplication.getMyLatitude() ==  null){
	        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	        }*/
        if (mLastLocation != null) {
            handleNewLocation(mLastLocation);
            gpsStatus = false;

/*	        	latitude=mLastLocation.getLatitude();
	        	longitude=mLastLocation.getLongitude();
	        	myApplication.setMyLatitude(latitude+"");
	        	myApplication.setMyLongitude(longitude+"");
	            Toast.makeText(getActivity(), "Lat "+latitude+", Longitude"+longitude, Toast.LENGTH_LONG).show();
	           	latitude=Double.parseDouble(myApplication.getMyLatitude());
	        	longitude=Double.parseDouble(myApplication.getMyLongitude());

	        Log.e("latitute", latitude+"");
	        Log.e("longitude", longitude+"");

*/	            /*mLatitudeText.setText(String.format("%s: %f", mLatitudeLabel,
	                    mLastLocation.getLatitude()));
	            mLongitudeText.setText(String.format("%s: %f", mLongitudeLabel,
	                    mLastLocation.getLongitude()));*/
        } else {
	            /*Toast.makeText(getActivity(), "No location Detected", Toast.LENGTH_LONG).show();*/
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    public void handleNewLocation(Location location) {
        myApplication.setMyLatitude(location.getLatitude() + "");
        myApplication.setMyLongitude(location.getLongitude() + "");
        //Toast.makeText(getActivity(), "Lat "+location.getLatitude()+", Longitude"+location.getLongitude(), Toast.LENGTH_LONG).show();
        latitude = Double.parseDouble(myApplication.getMyLatitude());
        longitude = Double.parseDouble(myApplication.getMyLongitude());
        new ExecuteLoadMainCategories(brand, category).execute();

//        String url=urlLoadMainCategories+"?category="+category+"&brand="+brand+"&lat="+ myApplication.getMyLatitude()+"&long="+ myApplication.getMyLongitude();
//        volleyJsonObjectRequest(url);

        //makeMap();
        Log.e("latitute", latitude + "");
        Log.e("longitude", longitude + "");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("THIS", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i("THIS", "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
	    	/*offlinestoregrid.setVisibility(View.VISIBLE);
			offlinestorelist.setVisibility(View.GONE);
			maprelative.setVisibility(View.GONE);*/
        Log.e("loctn", "" + gpsStatus);
        if (!gpsStatus) {
            checkGPS();
            new ExecuteLoadMainCategories(brand, category).execute();

//            String url=urlLoadMainCategories+"?category="+category+"&brand="+brand+"&lat="+ myApplication.getMyLatitude()+"&long="+ myApplication.getMyLongitude();
//            volleyJsonObjectRequest(url);
        } else {
//            Toast.makeText(getActivity(), "Please wait until get your location..Once get Map refreshed automatically", Toast.LENGTH_LONG).show();

            checkGPS();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }



    public class ExecuteLoadMainCategories extends AsyncTask<String, String, String> {

        String category, brand;
        ProgressDialog dialog;

        ExecuteLoadMainCategories(String brand, String category) {
            this.category = category;
            this.brand = brand;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(getActivity(), "", "Loading..", true, false);
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            //?category=&brand=&lat=9.9252007&long=78.11977539999998
            param.add(new BasicNameValuePair("category", category));
            param.add(new BasicNameValuePair("brand", brand));
            param.add(new BasicNameValuePair("lat", myApplication.getMyLatitude()));
            param.add(new BasicNameValuePair("long", myApplication.getMyLongitude()));
            //JSONObject jsonObject=parser.getJSONFromUrl(urlLoadMainCategories);

            Log.e("params", param.toString());
            JSONObject object = parser.makeHttpRequest(getActivity(), urlLoadMainCategories, "GET", param);
            Log.e("object", object.toString());
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();
            categorydata = new ArrayList<>();
            categorydata.clear();
            List<String> categoryList = new ArrayList<String>();
            categoryList.clear();
            try {
							/*	JSONArray array=new JSONArray(result);
								for (int i = 0; i < array.length(); i++) {
								        JSONObject object1 = array.getJSONObject(i);
									StoreData data=new StoreData();
									data.setStoreId(object1.getString("id"));
									data.setName(object1.getString("name"));
									data.setImgLogo(object1.getString("image"));
									categorydata.add(data);
								}
*/
                JSONObject jsonObject = new JSONObject(result);
                List<String> brandDBList = new ArrayList<>();
                ;

                JSONArray jsonArray = jsonObject.getJSONArray("queryresult");
                Log.i("JSONARRAY", "" + jsonArray.length());
                if (jsonArray.length() > 0) {
                    categorydata = new ArrayList<OfflineStoreData>();
                    categorydata.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object1 = jsonArray.getJSONObject(i);
                        OfflineStoreData data = new OfflineStoreData();
                        data.setId(object1.getString("id"));
                        data.setImage(object1.getString("image"));
                        data.setBrand(object1.getString("brand"));
                        data.setOffer(object1.getString("offer"));
                        data.setDescription(object1.getString("description"));
                        data.setBrandname(object1.getString("brandname"));
                        data.setMaplat(object1.getDouble("maplat"));
                        data.setMaplong(object1.getDouble("maplong"));
                        data.setDist(object1.getDouble("dist"));
                        data.setCategory(object1.getString("category"));
                        List<String> categoryDBList = Arrays.asList(object1.getString("category").split(","));
                        if (brand != "") {

                            brandDBList = Arrays.asList(brand.split(","));
                        }
                        if (category == "") {
                            categorydata.add(data);
                        } else {


                            Log.e("categoryDBList",categoryDBList.toString());
                            categoryList = Arrays.asList(category.split(","));
                            Log.e("categoryList",categoryList.toString());
                            for (int ij = 0; ij < categoryList.size(); ij++) {

                                for(String value:categoryDBList){
                                    if (value.equalsIgnoreCase(categoryList.get(ij))) {
                                        categorydata.add(data);
                                        break;
                                    }
                                }
/*
                                if (categoryDBList.contains(categoryList.get(ij))) {
                                    status = true;
                                    break;


                                } else {
                                    status = false;
                                    if (brandDBList.contains(object1.getString("brand"))) {
                                        status = true;
                                        break;
                                    }

                                }
*/

                            }
                            /*Log.i("STATUS", "" + status);
                            if (status) {
                                categorydata.add(data);
                            }*/

                        }
//                        categorydata.add(data);

                    }
                    Log.i("CATEGORYDATA", "" + categorydata.size());
                    if (categorydata.size() == 0) {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                    }
                    initilizeMap(categorydata);
                    adapter = new OfflineStoreAdapter(getActivity(), categorydata);
                    listviewAdapter = new OfflineStoreListviewAdapter(getActivity(), categorydata);
                    offlinestorelist.setAdapter(listviewAdapter);
                    offlinestoregrid.setAdapter(adapter);

                } else {
                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                    categorydata.clear();
                    initilizeMap(categorydata);
                    adapter = new OfflineStoreAdapter(getActivity(), categorydata);
                    listviewAdapter = new OfflineStoreListviewAdapter(getActivity(), categorydata);
                    offlinestorelist.setAdapter(listviewAdapter);
                    offlinestoregrid.setAdapter(adapter);
                }


            } catch (Exception e) {
                // TODO: handle exception
                Log.e("offline exception", e.toString());
            }
        }
    }

}
