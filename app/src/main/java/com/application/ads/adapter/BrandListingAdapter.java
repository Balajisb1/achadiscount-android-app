package com.application.ads.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.BrandListActivity;
import com.application.ads.R;
import com.application.ads.data.MobileBrandsData;
import com.application.ads.extra.DBConnection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BrandListingAdapter extends BaseAdapter {

    List<MobileBrandsData> mobilebranddata;
    Context c;
    LayoutInflater inflater;


    public BrandListingAdapter(Context c, List<MobileBrandsData> mobilebranddata) {
        // TODO Auto-generated constructor stub
        this.mobilebranddata = mobilebranddata;
        this.c = c;

        Log.e("SIze ", mobilebranddata.size() + "");
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mobilebranddata.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mobilebranddata.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.activity_popularmobilephones, null, true);

        TextView popularimagename = (TextView) v.findViewById(R.id.popularimgname);
        ImageView popularimage = (ImageView) v.findViewById(R.id.popularimg);
        Button viewproduct = (Button) v.findViewById(R.id.viewproduct);
        popularimagename.setText(mobilebranddata.get(position).getMobile_name());
        Picasso.with(c).load(DBConnection.BRAND_IMGURL + mobilebranddata.get(position).getMobile_image()).placeholder(R.drawable.adslogo).into(popularimage);


        viewproduct.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(c, BrandListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("brand_id", mobilebranddata.get(pos).getMobile_id());
                intent.putExtra("brand_name", mobilebranddata.get(pos).getMobile_name());
                intent.putExtra("parent_id", mobilebranddata.get(pos).getParent_id());
                intent.putExtra("parent_child_id", mobilebranddata.get(pos).getParent_child_id());

                c.startActivity(intent);
                //Toast.makeText(getActivity(), mobilebranddata.get(m).getMobile_id().toString(), 5000).show();
            }
        });
        return v;
    }

}
