package com.application.ads.data;

public class FAQData {

    private String faqques;
    private String faqans;

    public String getFaqans() {
        return faqans;
    }

    public void setFaqans(String faqans) {
        this.faqans = faqans;
    }

    public String getFaqques() {
        return faqques;
    }

    public void setFaqques(String faqques) {
        this.faqques = faqques;
    }

}
