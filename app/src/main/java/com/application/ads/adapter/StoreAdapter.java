package com.application.ads.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.StoreData;
import com.application.ads.extra.DBConnection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StoreAdapter extends BaseAdapter {

    List<StoreData> categorydata;
    Context c;
    LayoutInflater inflater;

    public StoreAdapter(Context c, List<StoreData> categorydata) {
        // TODO Auto-generated constructor stub
        this.categorydata = categorydata;
        this.c = c;
        Log.e("SIze ", categorydata.size() + "");
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categorydata.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return categorydata.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.activity_offline_categories, null);
        TextView categoryname = (TextView) view.findViewById(R.id.categoryname);
        ImageView categoryimg = (ImageView) view.findViewById(R.id.categoryimg);

        categoryname.setText(categorydata.get(position).getName());
        Picasso.with(c).load(DBConnection.OFFLINE_IMGURL + categorydata.get(position).getImgLogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);

        return view;
    }

}
