package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.FilterAdapter;
import com.application.ads.adapter.ProductDetailsAdapter;
import com.application.ads.adapter.ProductGridAdapter;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import info.androidramp.gearload.Loading;

public class BrandListActivity extends Activity {


    public static String urlSearchProducts = DBConnection.BASEURL
            + "search_products.php";
    static ProductGridAdapter productgriddetailsadapter;
    private final int interval = 3000; // 3 Second
    ListView productlisting;
    GridView productgrid;
    TextView prod_parent_name, prodcount;
    JSONParser parser;
    List<ProductDetails> productdetailslist, productsdetails;
    LinearLayout linearFilter, linearGridList;
    ProductDetailsAdapter productdetailsadapter;
    int start = 0;
    TextView gridlisttext;
    int count = 10;
    SessionManager manager;
    int totalcount;
    boolean loadingMore = false;
    String filterids = "";
    int limitposition = 16;
    Button compareprod;
    SharedPreferences preferences;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AutoCompleteTextView searchtext;
    ImageView searchbtn;
    int status;
    List<String> produnctname;
    ArrayAdapter<String> prodadapter;
    Loading loading;
    private String urlLoadAllProducts = DBConnection.BASEURL + "load_brand_wise_products.php";
    private boolean prodstatus = false;
    private LinearLayout sortby;
    private Dialog dialog;
    private RadioButton priceLowToHigh, priceHighToLow;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Brands");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        sortby = (LinearLayout) findViewById(R.id.sortby);
        productlisting = (ListView) findViewById(R.id.productlisting);
        productgrid = (GridView) findViewById(R.id.productgrid);
        gridlisttext = (TextView) findViewById(R.id.gridlisttext);
        prodcount = (TextView) findViewById(R.id.prodcount);
        linearFilter = (LinearLayout) findViewById(R.id.linearFilter);
        linearGridList = (LinearLayout) findViewById(R.id.linearGridList);
        prod_parent_name = (TextView) findViewById(R.id.prod_parent_name);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        prod_parent_name.setText(getIntent().getStringExtra("brand_name"));

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.activity_dialog_linear);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


        priceLowToHigh = (RadioButton) dialog.findViewById(R.id.priceLowToHigh);
        priceHighToLow = (RadioButton) dialog.findViewById(R.id.priceHighToLow);


        sortby.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        productdetailslist = new ArrayList<ProductDetails>();
        manager = new SessionManager(BrandListActivity.this);

        productdetailsadapter = new ProductDetailsAdapter(BrandListActivity.this, productdetailslist);
        productlisting.setAdapter(productdetailsadapter);

        productgriddetailsadapter = new ProductGridAdapter(BrandListActivity.this, productdetailslist);
        productgrid.setAdapter(productgriddetailsadapter);

        priceLowToHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 2;
                productdetailslist.clear();
                dialog.dismiss();

        /*        new ExecuteLoadProductDetails(getIntent().getStringExtra(
                        "parent_child_id"), 0, limitposition, filterids,  "asc")
                        .execute();*/

                String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                if (userid != "" || userid != null) {
                    userid = preferences.getString(SessionManager.KEY_USERID, "0");
                } else {
                    userid = "";
                }

                String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                        + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=asc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                volleyJsonObjectRequest(url);

            }
        });

        priceHighToLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 3;
                dialog.dismiss();
                productdetailslist.clear();
             /*   new ExecuteLoadProductDetails(getIntent().getStringExtra(
                        "parent_child_id"), 0, limitposition, filterids,  "desc")
                        .execute();*/

                String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                if (userid != "" || userid != null) {
                    userid = preferences.getString(SessionManager.KEY_USERID, "0");
                } else {
                    userid = "";
                }

                String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                        + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=desc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                volleyJsonObjectRequest(url);

            }
        });


        searchtext = (AutoCompleteTextView) findViewById(R.id.searchtext);

        prodadapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                produnctname);
        searchtext.setAdapter(prodadapter);

        searchtext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("Text", s.toString());
                new ExecuteSearchProducts(s.toString()).execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchbtn = (ImageView) findViewById(R.id.searchbtn);
        searchbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (searchtext.getText().toString().equals("")
                        || searchtext.getText().toString().equals(null)) {
                    Toast.makeText(BrandListActivity.this, "Please provide search term",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String name = searchtext.getText().toString();
                    // Log.e("name", name);

                    for (int i = 0; i < productsdetails.size(); i++) {
                        // Log.e("prod name",
                        // productsdetails.get(i).getProduct_name());
                        if (name.equalsIgnoreCase(productsdetails.get(i)
                                .getProduct_name())) {
                            // Toast.makeText(getActivity(),
                            // productsdetails.get(i).getProduct_id(),
                            // Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(BrandListActivity.this,
                                    ProductOverView.class);
                            intent.putExtra("prod_id", productsdetails.get(i)
                                    .getProduct_id());
                            searchtext.setText("");
                            startActivity(intent);
                            status = 1;
                        } else {
                            status = 0;
                            // Toast.makeText(getActivity(),
                            // "No data found..Try different keyword",
                            // Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (status == 0) {
                        Toast.makeText(BrandListActivity.this,
                                "No data found..Try different keyword",
                                Toast.LENGTH_SHORT).show();
                        searchtext.setText("");
                    }
                }
            }
        });

        searchtext.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                // Toast.makeText(getApplicationContext(),
                // searchedittext.getText().toString(), Toast.LENGTH_SHORT).show();
                /*
				 * Intent intent = new Intent(getActivity(), CouponList.class);
				 * intent.putExtra("storeName", searchtext.getText()
				 * .toString()); searchtext.setText(""); startActivity(intent);
				 */

                String name = searchtext.getText().toString();
                // Log.e("name", name);
                for (int i = 0; i < productsdetails.size(); i++) {
                    // Log.e("prod name",
                    // productsdetails.get(i).getProduct_name());
                    if (name.equalsIgnoreCase(productsdetails.get(i)
                            .getProduct_name())) {
						/*
						 * Toast.makeText(getActivity(),
						 * productsdetails.get(i).getProduct_id(), Toast.LENGTH_SHORT)
						 * .show();
						 */
                        Intent intent = new Intent(BrandListActivity.this,
                                ProductOverView.class);

                        intent.putExtra("prod_id", productsdetails.get(i)
                                .getProduct_id());
                        searchtext.setText("");
                        startActivity(intent);
                    }
                }
            }
        });


        linearGridList.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (gridlisttext.getText().toString().equalsIgnoreCase("grid")) {

                    gridlisttext.setText("List");
                    productgrid.setVisibility(View.VISIBLE);
                    productlisting.setVisibility(View.GONE);
                } else {
                    gridlisttext.setText("Grid");
                    productgrid.setVisibility(View.GONE);
                    productlisting.setVisibility(View.VISIBLE);
                }

            }
        });


        linearFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BrandListActivity.this, FilterActivity.class);
                intent.putExtra("category_id", getIntent().getStringExtra("parent_child_id"));
                intent.putExtra("status", "0");
                startActivityForResult(intent, 1);

            }
        });

        //	productdetaillistinglinear=(LinearLayout)findViewById(R.id.productdetaillistinglinear);

        productgrid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //	Toast.makeText(getApplicationContext(), productdetailslist.get(position).getProduct_id(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(BrandListActivity.this, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(position).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(position).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(position).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(position).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(position).getRating());
                intent.putExtra("prod_id", productdetailslist.get(position).getProduct_id());
                intent.putExtra("prod_desc", productdetailslist.get(position).getKey_feature().toString().trim().split(",")[0]);
                startActivity(intent);
            }
        });

        productgrid.setOnScrollListener(new OnScrollListener() {


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;
                int topRowVerticalPosition = (productgrid == null || productgrid.getChildCount() == 0) ? 0 : productgrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
/*				Log.e("firstVisibleItem", firstVisibleItem+"");
				Log.e("visibleItemCount", visibleItemCount+"");
				Log.e("totalItemCount", totalItemCount+"");
*/
            }


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                //Log.e("totalItemCount", totalcount+"");

                if ((start + count) == totalcount && scrollState == SCROLL_STATE_IDLE) {
                    //Toast.makeText(getApplicationContext(), "End of file Reached", Toast.LENGTH_SHORT).show();
                    if (filterids == "") {

                        if (status == 2) {
                            /* new ExecuteLoadProductDetails(getIntent().getStringExtra(
                                     "parent_child_id"), (totalcount++),
                                     limitposition, filterids,  "asc").execute();*/

                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

							/* List<NameValuePair> param=new ArrayList<NameValuePair>();
							 param.add(new BasicNameValuePair("brand_id",parentchildid));
							 param.add(new BasicNameValuePair("user_id",userid));
							 param.add(new BasicNameValuePair("start",start+""));
							 param.add(new BasicNameValuePair("end",end+""));
							 param.add(new BasicNameValuePair("filter_ids",filterids));
							 param.add(new BasicNameValuePair("sort",sortOrder));
							 param.add(new BasicNameValuePair("parent_child_id", getIntent().getStringExtra("parent_child_id")));
							 Log.e("Params", param.toString());
							 JSONObject object=parser.makeHttpRequest(urlLoadAllProducts,"GET",param)*/

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=asc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);


                        } else if (status == 3) {
//                             new ExecuteLoadProductDetails(getIntent().getStringExtra("parent_child_id"), (totalcount++),limitposition, filterids,  "desc").execute();

                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=desc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);
                        } else {
//                             new ExecuteLoadProductDetails(getIntent().getStringExtra("brand_id"), (totalcount++), limitposition, filterids,"").execute();

                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);
                        }
                    }
                }
            }

        });


        productlisting.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //	Toast.makeText(getApplicationContext(), productdetailslist.get(position).getProduct_id(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(BrandListActivity.this, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(position).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(position).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(position).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(position).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(position).getRating());
                intent.putExtra("prod_id", productdetailslist.get(position).getProduct_id());
                intent.putExtra("prod_desc", productdetailslist.get(position).getKey_feature().toString().trim().split(",")[0]);
                startActivity(intent);
            }
        });

        productlisting.setOnScrollListener(new OnScrollListener() {


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;
                int topRowVerticalPosition = (productgrid == null || productgrid.getChildCount() == 0) ? 0 : productgrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
/*				Log.e("firstVisibleItem", firstVisibleItem+"");
				Log.e("visibleItemCount", visibleItemCount+"");
				Log.e("totalItemCount", totalItemCount+"");
*/
            }


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                //Log.e("totalItemCount", totalcount+"");

                if ((start + count) == totalcount && scrollState == SCROLL_STATE_IDLE) {
                    //Toast.makeText(getApplicationContext(), "End of file Reached", Toast.LENGTH_SHORT).show();
                    if (prodstatus) {

                        if (status == 2) {
                            /* new ExecuteLoadProductDetails(getIntent().getStringExtra(
                                     "parent_child_id"), (totalcount++),
                                     limitposition, filterids,  "asc").execute();
*/
                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=asc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);
                        } else if (status == 3) {
                             /*new ExecuteLoadProductDetails(getIntent().getStringExtra(
                                     "parent_child_id"), (totalcount++),
                                     limitposition, filterids,  "desc").execute();
*/
                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=desc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);
                        } else {
//                             new ExecuteLoadProductDetails(getIntent().getStringExtra("brand_id"), (totalcount++), limitposition, filterids,"").execute();

                            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            if (userid != "" || userid != null) {
                                userid = preferences.getString(SessionManager.KEY_USERID, "0");
                            } else {
                                userid = "";
                            }

                            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                            volleyJsonObjectRequest(url);
                        }
                    }
                }
            }

        });

        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                productdetailslist.clear();
               /* new ExecuteLoadProductDetails(getIntent().getStringExtra(
                        "parent_child_id"), 0, limitposition, filterids, "")
                        .execute();*/

                String userid = preferences.getString(SessionManager.KEY_USERID, "0");
                if (userid != "" || userid != null) {
                    userid = preferences.getString(SessionManager.KEY_USERID, "0");
                } else {
                    userid = "";
                }


                if(status==2) {
                    String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                            + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=asc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                    volleyJsonObjectRequest(url);
                }else if(status==3) {
                    String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                            + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=desc&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                    volleyJsonObjectRequest(url);
                }else{
                    String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                            + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
                    volleyJsonObjectRequest(url);
                }



                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

//        new ExecuteLoadProductDetails(getIntent().getStringExtra("brand_id"),0,limitposition,filterids,"").execute();
        String userid = preferences.getString(SessionManager.KEY_USERID, "0");
        if (userid != "" || userid != null) {
            userid = preferences.getString(SessionManager.KEY_USERID, "0");
        } else {
            userid = "";
        }

        String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
        volleyJsonObjectRequest(url);

        loading = (Loading) findViewById(R.id.loading);

        loading.Start();

        Runnable runnable = new Runnable() {
            public void run() {

//				pd.dismiss();
                loading.Cancel();

            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (FilterAdapter.filterids != null) {
            FilterAdapter.filterids.clear();
            filterids = "";
        }
    }


    public void volleyJsonObjectRequest(String url) {

        if (filterids != "") {
            //productdetailslist.clear();
            productdetailsadapter.notifyDataSetChanged();
            productgriddetailsadapter.notifyDataSetChanged();
        }

        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONArray subcatarray = jsonObject.getJSONArray("products");
                            for (int j = 0; j < subcatarray.length(); j++) {
                                JSONObject subcatobj = subcatarray.getJSONObject(j);
                                ProductDetails productdetails = new ProductDetails();
                                productdetails.setProduct_price(subcatobj.getString("product_price"));
                                productdetails.setProduct_name(subcatobj.getString("product_name"));
                                productdetails.setCashbackprice(subcatobj.getString("cashback_price"));
                                productdetails.setProduct_id(subcatobj.getString("product_id"));
                                productdetails.setProduct_image(subcatobj.getString("product_image"));
                                productdetails.setParent_child_id(subcatobj.getString("parent_child_id"));
                                productdetails.setProduct_parent(subcatobj.getString("parent_child_id"));
                                productdetails.setRating(subcatobj.getString("rating"));
                                productdetails.setKey_feature(subcatobj.getString("key_feature"));
                                productdetails.setProWishListStatus(subcatobj.getString("wishlist_status"));
                                productdetailslist.add(productdetails);
                                //Log.e("Product parent id", productdetails.getProduct_parent()+"");
                            }
                            productdetailsadapter.notifyDataSetChanged();
                            productgriddetailsadapter.notifyDataSetChanged();
                        } else {
                            prodstatus = true;
                            Toast.makeText(BrandListActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        prodstatus = false;
        if (resultCode == 1) {
            for (int i = 0; i < FilterAdapter.filterids.size(); i++) {
                if (i == 0) {
                    this.filterids = FilterAdapter.filterids.get(i);
                } else {
                    this.filterids += "," + FilterAdapter.filterids.get(i);
                }
            }
            filterids = data.getStringExtra("filter_ids");
            productdetailslist.clear();
//			new ExecuteLoadProductDetails(getIntent().getStringExtra("brand_id"),0,limitposition,filterids,"").execute();

            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
            if (userid != "" || userid != null) {
                userid = preferences.getString(SessionManager.KEY_USERID, "0");
            } else {
                userid = "";
            }

            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
            volleyJsonObjectRequest(url);
        } else {
//			new ExecuteLoadProductDetails(getIntent().getStringExtra("brand_id"),0,limitposition,filterids,"").execute();

            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
            if (userid != "" || userid != null) {
                userid = preferences.getString(SessionManager.KEY_USERID, "0");
            } else {
                userid = "";
            }

            String url = urlLoadAllProducts + "?brand_id=" + getIntent().getStringExtra("brand_id") + "&user_id=" + userid
                    + "&start=" + start + "" + "&end=" + limitposition + "&filter_ids=" + filterids + "&sort=&parent_child_id=" + getIntent().getStringExtra("parent_child_id");
            volleyJsonObjectRequest(url);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    public class ExecuteLoadProductDetails extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String parentchildid;
        int start, end;
        private String filterids;
        private String sortOrder;

        public ExecuteLoadProductDetails(String parentchildid, int start, int end, String filterids, String sortOrder) {
            this.parentchildid = parentchildid;
            this.start = start;
            this.filterids = filterids;
            this.end = end;
            this.sortOrder = sortOrder;
            Log.e("parentChildId", parentchildid);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(BrandListActivity.this, "", "Loading..", true, false);

        }


        @Override
        protected String doInBackground(String... params) {

            String userid = preferences.getString(SessionManager.KEY_USERID, "0");
            if (userid != "" || userid != null) {
                userid = preferences.getString(SessionManager.KEY_USERID, "0");
            } else {
                userid = "";
            }
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("brand_id", parentchildid));
            param.add(new BasicNameValuePair("user_id", userid));
            param.add(new BasicNameValuePair("start", start + ""));
            param.add(new BasicNameValuePair("end", end + ""));
            param.add(new BasicNameValuePair("filter_ids", filterids));
            param.add(new BasicNameValuePair("sort", sortOrder));
            param.add(new BasicNameValuePair("parent_child_id", getIntent().getStringExtra("parent_child_id")));
            Log.e("Params", param.toString());
            JSONObject object = parser.makeHttpRequest(BrandListActivity.this, urlLoadAllProducts, "GET", param);
            //	Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            dialog.dismiss();
            String count = "";
            if (filterids != "") {
                //productdetailslist.clear();
                productdetailsadapter.notifyDataSetChanged();
                productgriddetailsadapter.notifyDataSetChanged();
            }
            //		Toast.makeText(BrandListActivity.this, result.length()+"", Toast.LENGTH_SHORT).show();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray subcatarray = object.getJSONArray("products");
                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setProduct_price(subcatobj.getString("product_price"));
                            productdetails.setProduct_name(subcatobj.getString("product_name"));
                            productdetails.setCashbackprice(subcatobj.getString("cashback_price"));
                            productdetails.setProduct_id(subcatobj.getString("product_id"));
                            productdetails.setProduct_image(subcatobj.getString("product_image"));
                            productdetails.setParent_child_id(subcatobj.getString("parent_child_id"));
                            productdetails.setProduct_parent(subcatobj.getString("parent_child_id"));
                            productdetails.setRating(subcatobj.getString("rating"));
                            productdetails.setKey_feature(subcatobj.getString("key_feature"));
                            productdetails.setProWishListStatus(subcatobj.getString("wishlist_status"));
                            productdetailslist.add(productdetails);
                            //Log.e("Product parent id", productdetails.getProduct_parent()+"");
                        }
                        productdetailsadapter.notifyDataSetChanged();
                        productgriddetailsadapter.notifyDataSetChanged();
                    } else {
                        prodstatus = true;
                        Toast.makeText(BrandListActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }
            }
        }
    }

    class ExecuteSearchProducts extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String start_letter;


        public ExecuteSearchProducts(String start_letter) {
            this.start_letter = start_letter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(BrandListActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("start_letter", start_letter));
            JSONObject object = parser.makeHttpRequest(BrandListActivity.this, urlSearchProducts,
                    "GET", param);

            Log.e("Object", urlSearchProducts + param.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //		dialog.dismiss();

            List<SubCategoryData> subcatlist = new ArrayList<SubCategoryData>();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        productsdetails = new ArrayList<ProductDetails>();

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productsdetails.add(productdetails);
                        }

                        produnctname = new ArrayList<String>();
                        for (int i = 0; i < productsdetails.size(); i++) {

                            produnctname.add(productsdetails.get(i)
                                    .getProduct_name());
                        }
                        manager.setStoreDetails(produnctname);

                        prodadapter = new ArrayAdapter<String>(
                                BrandListActivity.this,
                                android.R.layout.simple_list_item_1,
                                produnctname);
                        searchtext.setAdapter(prodadapter);


                    } else {
                        Toast.makeText(BrandListActivity.this, "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    }


}
