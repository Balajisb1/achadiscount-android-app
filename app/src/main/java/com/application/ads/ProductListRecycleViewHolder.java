package com.application.ads;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Created by devel-91 on 18/10/16.
 */

public class ProductListRecycleViewHolder extends RecyclerView.ViewHolder {

    public ImageView prodimg, prodcompare, imgProductListingDetailFavourites;
    public TextView prodprice, prodname;
    public RatingBar prodrating;

    public ProductListRecycleViewHolder(View itemView) {
        super(itemView);
        prodimg = (ImageView) itemView.findViewById(R.id.prodimg);
        prodprice = (TextView) itemView.findViewById(R.id.prodprice);

        prodname = (TextView) itemView.findViewById(R.id.prodname);
        prodcompare = (ImageView) itemView
                .findViewById(R.id.prodcomparegrid);
        imgProductListingDetailFavourites = (ImageView) itemView
                .findViewById(R.id.imgProductListingDetailFavourites);
        prodrating = (RatingBar) itemView
                .findViewById(R.id.prodrating);
    }
}
