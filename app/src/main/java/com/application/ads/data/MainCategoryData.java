package com.application.ads.data;

import java.util.ArrayList;

public class MainCategoryData {

    private String catId;
    private String catName;
    private String catImage;
    private String productsCount;
    private ArrayList<SubCategoryData> subCategories;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }

    public String getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(String productsCount) {
        this.productsCount = productsCount;
    }

    public ArrayList<SubCategoryData> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(ArrayList<SubCategoryData> subCategories) {
        this.subCategories = subCategories;
    }


}