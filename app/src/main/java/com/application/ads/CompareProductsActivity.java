package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CompareProducts;
import com.application.ads.data.FilterData;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CompareProductsActivity extends Activity {


    final String urlLoadCompareProducts = DBConnection.BASEURL + "compare_products_new.php";

    JSONObject object;
    JSONParser parser;

    ArrayList<FilterData> filterList;

    List<String> storeids;
    List<ProductDetails> producsSpecList, productsPriceList;
    //ProductDetails productList1,productList2;
    ProductDetails producsSpec, productsPrice;

    Button btnFilterFilter;

    LinearLayout linearProductCompareSpecification, linearProductPriceCompare;
    TextView txtProductComparission1, txtProductComparission2, compprodname;
    ImageView imgProductComparission1, imgProductComparission2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_comparission);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Compare Products");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setTitle("");

        parser = new JSONParser();


        txtProductComparission1 = (TextView) findViewById(R.id.txtProductComparission1);
        txtProductComparission2 = (TextView) findViewById(R.id.txtProductComparission2);
        compprodname = (TextView) findViewById(R.id.compprodname);
        imgProductComparission1 = (ImageView) findViewById(R.id.imgProductComparission1);
        imgProductComparission2 = (ImageView) findViewById(R.id.imgProductComparission2);

        filterList = new ArrayList<FilterData>();


        linearProductCompareSpecification = (LinearLayout) findViewById(R.id.linearProductCompareSpecification);
        linearProductPriceCompare = (LinearLayout) findViewById(R.id.linearProductPriceCompare);

//		new LoadProductCompare().execute();
/*
        List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("category_id", CompareProducts.getProducts().get(0).getProduct_parent()));
		param.add(new BasicNameValuePair("product_id1", ""+CompareProducts.getProducts().get(0).getProduct_id()));
		param.add(new BasicNameValuePair("product_id2", ""+CompareProducts.getProducts().get(1).getProduct_id()));
		Log.e("compare param is",param.toString());
		object = parser.makeHttpRequest(urlLoadCompareProducts, "GET",param);*/

        String url = urlLoadCompareProducts + "?category_id=" + CompareProducts.getProducts().get(0).getProduct_parent() + "&product_id1=" + CompareProducts.getProducts().get(0).getProduct_id()
                + "&product_id2=" + CompareProducts.getProducts().get(1).getProduct_id();
        volleyJsonObjectRequest(url);

    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        filterList = new ArrayList<FilterData>();

        producsSpecList = new ArrayList<ProductDetails>();
        productsPriceList = new ArrayList<ProductDetails>();
        final List<String> spectitle = new ArrayList<String>();
        storeids = new ArrayList<String>();
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject object1 = new JSONObject(jsonString);
                    if (jsonString.toString().trim().length() > 0) {
                        JSONArray array = object1.getJSONArray("spectitle");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            spectitle.add(object.getString("name"));
                        }
                        spectitle.add("price");
                        JSONObject array1 = object1.getJSONObject("spec");
                        for (int i = 0; i < spectitle.size(); i++) {
                            producsSpec = new ProductDetails();
                            producsSpec.setProductSpecName(spectitle.get(i).toString());
                            if (spectitle.get(i).toString().equalsIgnoreCase("price")) {

                                JSONArray array2 = array1.getJSONArray("storeids");
                                for (int j = 0; j < array2.length(); j++) {
                                    JSONObject object = array2.getJSONObject(j);
                                    storeids.add(object.getString("storeid"));
                                }
                                //	Toast.makeText(CompareProductsActivity.this, "Store ids size"+storeids.size(), Toast.LENGTH_SHORT).show();


                                if (storeids.size() > 0) {
                                    for (int j = 0; j < storeids.size(); j++) {
                                        JSONObject jsonObject = array1.getJSONObject(spectitle.get(i));
                                        JSONArray array3 = jsonObject.getJSONArray(storeids.get(j));

                                        for (int k = 0; k < array3.length(); k++) {
                                            JSONObject object5 = array3.getJSONObject(k);
                                            productsPrice = new ProductDetails();
                                            productsPrice.setProductAffiiateLogo(object5.getString("affiliate_logo"));
                                            productsPrice.setProductId(object5.getString("product_id"));
                                            productsPrice.setProductStoreId(object5.getString("store_id"));
                                            productsPrice.setProductAffiliatePrice(object5.getString("product_price"));
                                            productsPrice.setProductPPId(object5.getString("pp_id"));
                                            productsPriceList.add(productsPrice);
                                        }
                                    }
                                }
                                producsSpec.setProductSpecValue(array1.getJSONObject(spectitle.get(i)).toString());
                            } else {
                                producsSpec.setProductSpecValue(array1.getJSONArray(spectitle.get(i)).toString());
                            }
                            producsSpecList.add(producsSpec);
                        }
                        fillSpecs(producsSpecList);
                    } else {
                        Toast.makeText(CompareProductsActivity.this,
                                "Something goes wrong..", Toast.LENGTH_LONG).show();
                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void fillSpecs(List<ProductDetails> producsSpecList) {

        List<String> addedStores = new ArrayList<String>();

        TextView txtProCompareSpecTitle, txtProCompareSpecProductName1, txtProCompareSpecProductName2;
        TextView txtProCompareSpecProductValue1, txtProCompareSpecProductValue2;

        ImageView imgProCompareLogo;
        TextView txtProCompareSpecProduct1Price, txtProCompareSpecProduct2Price;
        Button btnProCompareSpecProduct1Price, btnProCompareSpecProduct2Price;

        compprodname.setText(CompareProducts.getProducts().get(0).getProduct_name() + " (vs)" + CompareProducts.getProducts().get(1).getProduct_name());

        Picasso.with(CompareProductsActivity.this).load(DBConnection.PRODUCT_IMGURL + CompareProducts.getProducts().get(0).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgProductComparission1);
        Picasso.with(CompareProductsActivity.this).load(DBConnection.PRODUCT_IMGURL + CompareProducts.getProducts().get(1).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgProductComparission2);


        linearProductCompareSpecification.removeAllViews();
        for (int i = 0; i < producsSpecList.size(); i++) {


            if (producsSpecList.get(i).getProductSpecName().equalsIgnoreCase("price")) {

                //		Toast.makeText(CompareProductsActivity.this, "Storeids size: "+storeids.size(), Toast.LENGTH_SHORT).show();
                for (int j = 0; j < productsPriceList.size(); j++) {

                    View view = getLayoutInflater().inflate(R.layout.activity_price_compare_details, null);
                    imgProCompareLogo = (ImageView) view.findViewById(R.id.imgProCompareLogo);
                    txtProCompareSpecProduct1Price = (TextView) view.findViewById(R.id.txtProCompareSpecProduct1Price);
                    txtProCompareSpecProduct2Price = (TextView) view.findViewById(R.id.txtProCompareSpecProduct2Price);
                    txtProCompareSpecProductName1 = (TextView) view.findViewById(R.id.txtProCompareSpecProductName1);
                    txtProCompareSpecProductName2 = (TextView) view.findViewById(R.id.txtProCompareSpecProductName2);
                    btnProCompareSpecProduct1Price = (Button) view.findViewById(R.id.btnProCompareSpecProduct1Price);
                    btnProCompareSpecProduct2Price = (Button) view.findViewById(R.id.btnProCompareSpecProduct2Price);
                    txtProCompareSpecProductName1.setText(CompareProducts.getProducts().get(0).getProduct_name());
                    txtProCompareSpecProductName2.setText(CompareProducts.getProducts().get(1).getProduct_name());

                    if (productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(0).getProduct_id())) {
                        txtProCompareSpecProduct1Price.setText("Rs. " + productsPriceList.get(j).getProductAffiliatePrice());

                        Picasso.with(CompareProductsActivity.this).load(DBConnection.AFFLIATE_IMGURL + productsPriceList.get(j).getProductAffiiateLogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgProCompareLogo);

                        final int pos = j;
                        btnProCompareSpecProduct1Price.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(
                                        CompareProductsActivity.this,
                                        ProductGotoStore.class);
                                intent.putExtra("product_id",
                                        productsPriceList.get(pos).getProductId());
                                intent.putExtra("store_id", productsPriceList.get(pos).getProductStoreId());
                                intent.putExtra("pp_id", productsPriceList.get(pos).getProductPPId());
                                startActivity(intent);
                            }
                        });
                    } else {
                        txtProCompareSpecProduct1Price.setText("-");
                    }

                    boolean isAdded = true;

                    for (int m = 0; m < productsPriceList.size(); m++) {

                        if (productsPriceList.get(m).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(1).getProduct_id())) {
                            if (productsPriceList.get(m).getProductStoreId().equalsIgnoreCase(productsPriceList.get(j).getProductStoreId())) {
                                txtProCompareSpecProduct2Price.setText("Rs. " + productsPriceList.get(m).getProductAffiliatePrice());
                                Picasso.with(CompareProductsActivity.this).load(DBConnection.AFFLIATE_IMGURL + productsPriceList.get(m).getProductAffiiateLogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgProCompareLogo);
                                isAdded = false;
                                final int posm = m;
                                btnProCompareSpecProduct2Price.setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(
                                                CompareProductsActivity.this,
                                                ProductGotoStore.class);
                                        intent.putExtra("product_id",
                                                productsPriceList.get(posm).getProductId());
                                        intent.putExtra("store_id", productsPriceList.get(posm).getProductStoreId());
                                        intent.putExtra("pp_id", productsPriceList.get(posm).getProductPPId());
                                        startActivity(intent);
                                    }
                                });
                            }
                        }
                    }

                    if (isAdded) {
                        txtProCompareSpecProduct2Price.setText("-");
                    }
								/*	}

				else if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(1).getProduct_id())){

					for(int k=0;k<productsPriceList.size();k++){

						if(productsPriceList.get(k).getProductStoreId().equalsIgnoreCase(storeids.get(j))){
							txtProCompareSpecProduct2Price.setText("Rs. "+productsPriceList.get(k).getProductAffiliatePrice());
							loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(k).getProductAffiiateLogo(),imgProCompareLogo);
						}else{
							txtProCompareSpecProduct1Price.setText("Rs. "+productsPriceList.get(k).getProductAffiliatePrice());
							loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(k).getProductAffiiateLogo(),imgProCompareLogo);
						}

					}
				}*/
                    if (!addedStores.contains(productsPriceList.get(j).getProductStoreId())) {
                        linearProductCompareSpecification.addView(view);
                    }

                    addedStores.add(productsPriceList.get(j).getProductStoreId());

				/*	if(productsPriceList.get(j).getProductStoreId().equalsIgnoreCase(storeids.get(j))){
						txtProCompareSpecProduct1Price.setText("Rs. "+productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}else{
						txtProCompareSpecProduct1Price.setText(productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
				}
				else if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(1).getProduct_id())){
					if(productsPriceList.get(j).getProductStoreId().equalsIgnoreCase(storeids.get(j))){
						txtProCompareSpecProduct2Price.setText("Rs. "+productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}else{
						txtProCompareSpecProduct2Price.setText(productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
				}*/

				/*if(storeids.get(j).equalsIgnoreCase(productsPriceList.get(j).getProductStoreId())){
					if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(0).getProduct_id())){
						txtProCompareSpecProduct1Price.setText("Rs. "+productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
					else if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(1).getProduct_id())){
						txtProCompareSpecProduct2Price.setText(productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
				}
				else{
					if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(0).getProduct_id())){
						txtProCompareSpecProduct1Price.setText("Rs. "+productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
					else if(productsPriceList.get(j).getProductId().equalsIgnoreCase(CompareProducts.getProducts().get(1).getProduct_id())){
						txtProCompareSpecProduct2Price.setText(productsPriceList.get(j).getProductAffiliatePrice());
						loader.DisplayImage(DBConnection.AFFLIATE_IMGURL+productsPriceList.get(j).getProductAffiiateLogo(),imgProCompareLogo);
					}
				//	linearProductCompareSpecification.addView(view);
				}*/

                }
            } else {

                View view = getLayoutInflater().inflate(R.layout.activity_product_compare_specification, null);
                txtProCompareSpecTitle = (TextView) view.findViewById(R.id.txtProCompareSpecTitle);
                txtProCompareSpecProductName1 = (TextView) view.findViewById(R.id.txtProCompareSpecProductName1);
                txtProCompareSpecProductName2 = (TextView) view.findViewById(R.id.txtProCompareSpecProductName2);
                txtProCompareSpecProductValue1 = (TextView) view.findViewById(R.id.txtProCompareSpecProductValue1);
                txtProCompareSpecProductValue2 = (TextView) view.findViewById(R.id.txtProCompareSpecProductValue2);
                txtProCompareSpecTitle.setText(producsSpecList.get(i).getProductSpecName());

                String result = producsSpecList.get(i).getProductSpecValue().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");

                if (result.contains(",")) {
                    txtProCompareSpecProductValue1.setText(result.split(",")[0]);
                    txtProCompareSpecProductValue2.setText(result.split(",")[1]);
                } else {
                    txtProCompareSpecProductValue1.setText(result);
                    txtProCompareSpecProductValue2.setText("--");
                }
                txtProCompareSpecProductName1.setText(CompareProducts.getProducts().get(0).getProduct_name());
                txtProCompareSpecProductName2.setText(CompareProducts.getProducts().get(1).getProduct_name());
                linearProductCompareSpecification.addView(view);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                CompareProducts.getProducts().clear();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CompareProducts.getProducts().clear();
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadProductCompare extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CompareProductsActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("category_id", CompareProducts.getProducts().get(0).getProduct_parent()));
            param.add(new BasicNameValuePair("product_id1", "" + CompareProducts.getProducts().get(0).getProduct_id()));
            param.add(new BasicNameValuePair("product_id2", "" + CompareProducts.getProducts().get(1).getProduct_id()));
            Log.e("compare param is", param.toString());
            object = parser.makeHttpRequest(getApplicationContext(), urlLoadCompareProducts, "GET", param);
            //	Log.e("obj", object.toString());
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            filterList = new ArrayList<FilterData>();

            producsSpecList = new ArrayList<ProductDetails>();
            productsPriceList = new ArrayList<ProductDetails>();
            List<String> spectitle = new ArrayList<String>();
            storeids = new ArrayList<String>();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object1 = new JSONObject(result);
                    JSONArray array = object1.getJSONArray("spectitle");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        spectitle.add(object.getString("name"));
                    }
                    spectitle.add("price");
                    JSONObject array1 = object1.getJSONObject("spec");
                    for (int i = 0; i < spectitle.size(); i++) {
                        producsSpec = new ProductDetails();
                        producsSpec.setProductSpecName(spectitle.get(i).toString());
                        if (spectitle.get(i).toString().equalsIgnoreCase("price")) {

                            JSONArray array2 = array1.getJSONArray("storeids");
                            for (int j = 0; j < array2.length(); j++) {
                                JSONObject object = array2.getJSONObject(j);
                                storeids.add(object.getString("storeid"));
                            }
                            //	Toast.makeText(CompareProductsActivity.this, "Store ids size"+storeids.size(), Toast.LENGTH_SHORT).show();


                            if (storeids.size() > 0) {
                                for (int j = 0; j < storeids.size(); j++) {
                                    JSONObject jsonObject = array1.getJSONObject(spectitle.get(i));
                                    JSONArray array3 = jsonObject.getJSONArray(storeids.get(j));

                                    for (int k = 0; k < array3.length(); k++) {
                                        JSONObject object5 = array3.getJSONObject(k);
                                        productsPrice = new ProductDetails();
                                        productsPrice.setProductAffiiateLogo(object5.getString("affiliate_logo"));
                                        productsPrice.setProductId(object5.getString("product_id"));
                                        productsPrice.setProductStoreId(object5.getString("store_id"));
                                        productsPrice.setProductAffiliatePrice(object5.getString("product_price"));
                                        productsPrice.setProductPPId(object5.getString("pp_id"));
                                        productsPriceList.add(productsPrice);
                                    }
                                }
                            }
                            producsSpec.setProductSpecValue(array1.getJSONObject(spectitle.get(i)).toString());
                        } else {
                            producsSpec.setProductSpecValue(array1.getJSONArray(spectitle.get(i)).toString());
                        }
                        producsSpecList.add(producsSpec);
                    }
                    fillSpecs(producsSpecList);
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("product size ", producsSpecList.size() + "");
                //Toast.makeText(CompareProductsActivity.this, producsSpecList.size()+"", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(CompareProductsActivity.this,
                        "Something goes wrong..", Toast.LENGTH_LONG).show();
            }

        }

    }
}
