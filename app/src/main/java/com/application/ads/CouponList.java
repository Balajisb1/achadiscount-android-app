package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CategoryData;
import com.application.ads.data.CouponData;
import com.application.ads.data.StoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CouponList extends Activity {

    private final String urlLoadCouponList = DBConnection.BASEURL + "load_store_wise_coupon_list.php";
    private final String urlAddToFav = DBConnection.BASEURL + "add_to_favourite.php";
    private final String urlRemoveFromFav = DBConnection.BASEURL + "remove_from_fav.php";
    LinearLayout linearCouponList, noofferslayout;
    TextView txtStoreOverAllTitle, txtStoreDetails, txtStoreDetailsTitle;
    ImageView imgStoreLogo;
    SessionManager manager;
    String terms_of_servicetxt;
    JSONParser parser;
    StoreData storeData;
    SharedPreferences preferences;
    Button button1, button2, button3;
    List<CategoryData> categoryDatas;
    List<StoreData> storeDatas;
    TextView txtAddReview, txtViewReviews;
    WebView txtStoreDetailsDescriptionWeb;
    TextView txtViewMore;
    private String urlAddtoClickHistory = DBConnection.BASEURL + "load_click_history.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupons_list);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Coupon Discounts");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        categoryDatas = new ArrayList<CategoryData>();
        storeDatas = new ArrayList<StoreData>();
        manager = new SessionManager(CouponList.this);

        imgStoreLogo = (ImageView) findViewById(R.id.imgStoreLogo);
        txtStoreDetailsTitle = (TextView) findViewById(R.id.txtStoreDetailsTitle);
        txtStoreOverAllTitle = (TextView) findViewById(R.id.txtStoreOverAllTitle);
        // txtStoreDetailsDescription = (TextView)
        // findViewById(R.id.txtStoreDetailsDescription);
        linearCouponList = (LinearLayout) findViewById(R.id.linearCouponList);
        noofferslayout = (LinearLayout) findViewById(R.id.noofferslayout);
        txtStoreDetailsDescriptionWeb = (WebView) findViewById(R.id.txtStoreDetailsDescriptionWeb);

        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                filterCoupons(1);
            }
        });
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                filterCoupons(2);
            }
        });
        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                filterCoupons(3);
            }
        });

        txtAddReview = (TextView) findViewById(R.id.txtAddReview);
        txtViewReviews = (TextView) findViewById(R.id.txtViewReviews);

        txtViewMore = (TextView) findViewById(R.id.txtViewMore);
        txtViewMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                txtViewMore.setVisibility(View.GONE);
                /*
                 * WebView.LayoutParams params=new
				 * WebView.LayoutParams(LayoutParams
				 * .MATCH_PARENT,LayoutParams.WRAP_CONTENT, 0, 0);
				 * txtStoreDetailsDescriptionWeb.setLayoutParams(params);
				 */

                ViewGroup.LayoutParams vc = txtStoreDetailsDescriptionWeb
                        .getLayoutParams();
                vc.height = LayoutParams.WRAP_CONTENT;
                vc.width = LayoutParams.MATCH_PARENT;
                txtStoreDetailsDescriptionWeb.setLayoutParams(vc);

                // txtStoreDetailsDescription.setMaxLines(10000);
                // txtStoreDetailsDescription.setTextColor(Color.parseColor("#29BAB0"));
            }
        });

    }

    public void fillStoreDetails() {

        txtViewReviews.setText("Reviews(" + storeData.getReviewsCount() + ")");
        txtStoreOverAllTitle.setText("MOST POPULAR "
                + storeData.getName().toUpperCase() + " OFFERS WITH CASHBACK");
        String storeTitle = storeData.getName().toUpperCase();

        if (Integer.parseInt(storeData.getCoupons().trim()) > 0) {
            storeTitle = storeTitle + " COUPONS - " + storeData.getCoupons()
                    + " OFFERS ";
        }

        if (storeData.getType().trim().equals("Flat")) {
            storeTitle = storeTitle + "TO EARN UPTO RS."
                    + storeData.getAmountPercentage() + " EXTRA CASHBACK";
        } else {
            storeTitle = storeTitle + "TO EARN UPTO "
                    + storeData.getAmountPercentage() + "% EXTRA CASHBACK";
        }

        txtStoreDetailsTitle.setText(storeTitle);
        String desc = String
                .valueOf(Html
                        .fromHtml("<![CDATA[<body style=\"text-align:justify;text-size:12px;color:#222222; \">"
                                + storeData.getDesc() + "</body>]]>"));
        txtStoreDetailsDescriptionWeb.loadData(desc, "text/html", "utf-8");
		/*
		 * txtStoreDetailsDescription.setText(Html.fromHtml(
		 * "<font style='text-align:justify;'>" + storeData.getDesc() +
		 * "</font>"));
		 */
        Picasso.with(CouponList.this).load(storeData.getImgLogo()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgStoreLogo);

        Log.e("Stores size is", "is===================="
                + storeData.getDatas().size());
        if (storeData.getDatas().size() > 0) {

            for (int i = 0; i < storeData.getDatas().size(); i++) {
                fillCouponsLayout(storeData.getDatas().get(i));
            }
        } else {

            noofferslayout.setVisibility(View.VISIBLE);
            // Toast.makeText(CouponList.this, "No Offers Available",
            // Toast.LENGTH_SHORT).show();
        }
    }

    public void fillCouponsLayout(final CouponData data) {
        View view = getLayoutInflater().inflate(R.layout.coupon_detail_page,
                null);

        TextView txtCouponTitle = (TextView) view
                .findViewById(R.id.txtCouponTitle);
		/*
		 * final TextView txtCouponDescription = (TextView) view
		 * .findViewById(R.id.txtCouponDescription);
		 */

        TextView txtCouponRemainingDays = (TextView) view
                .findViewById(R.id.txtCouponRemainingDays);

        TextView txtCouponDetalsHowToGetThisOffer = (TextView) view
                .findViewById(R.id.txtCouponDetalsHowToGetThisOffer);
        txtCouponDetalsHowToGetThisOffer
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        showHowToGetThisOffer();
                    }
                });

        TextView txtCouponDetalsTermsAndCondition = (TextView) view
                .findViewById(R.id.txtCouponDetalsTermsAndCondition);
        txtCouponDetalsTermsAndCondition
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        showTermsAndCondition();
                    }
                });



        Button btnCouponActivateOffer = (Button) view
                .findViewById(R.id.btnCouponActivateOffer);
        btnCouponActivateOffer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

				/*
				 * if(!manager.checkLogin()){ Intent intent=new
				 * Intent(CouponList.this,LoginActivity.class);
				 * intent.putExtra("title",data.getTitle());
				 * intent.putExtra("name",data.getOfferName());
				 * intent.putExtra("code",data.getCode());
				 * intent.putExtra("type",data.getType());
				 * intent.putExtra("offer_page",data.getOfferPage());
				 * intent.putExtra("logo",storeData.getImgLogo());
				 * startActivity(intent); } else{
				 */

                if (!manager.isLoggedIn()) {
                    Intent intent = new Intent(CouponList.this,
                            LoginActivity.class);
                    intent.putExtra("title", data.getTitle());
                    intent.putExtra("name", data.getOfferName());
                    intent.putExtra("code", data.getCode());
                    intent.putExtra("type", data.getType());
                    intent.putExtra("offer_page", data.getOfferPage());
                    intent.putExtra("logo", storeData.getImgLogo());
                    startActivity(intent);
                } else {
                    new ExecuteurlAddtoClickHistory(data.getOfferName(), data
                            .getCode(), data.getOfferPage(), storeData
                            .getImgLogo(), data.getTitle().replace("&#37;",
                            " %"), storeData.getStoreId(), storeData.getName())
                            .execute();
/*
					List<NameValuePair> param = new ArrayList<NameValuePair>();
					// voucher_name=buytest&store_name=Flipkart
					param.add(new BasicNameValuePair("user_id", preferences.getString(
							SessionManager.KEY_USERID, "")));
					param.add(new BasicNameValuePair("affiliate_id", storeid));
					param.add(new BasicNameValuePair("store_name", storename));
					param.add(new BasicNameValuePair("voucher_name", description));
					JSONObject object = parser.makeHttpRequest(urlAddtoClickHistory,
							"GET", param);*/

           /*         String storeid = storeData.getStoreId();
                    String storename = storeData.getName();
                    String description = data.getTitle().replace("&#37;", " %");
                    String url = urlAddtoClickHistory+"?user_id=" + preferences.getString(SessionManager.KEY_USERID, "") + "&affiliate_id=" + storeid
                            + "&store_name=" + storename + "&voucher_name=" + description;
                    Log.e("url",url);
                    volleyJsonObjectRequest1(url, data.getOfferName(), data
                            .getCode(), data.getOfferPage(), storeData
                            .getImgLogo(), data.getTitle().replace("&#37;",
                            " %"), storeData.getStoreId(), storeData.getName());*/

                }
                // }
            }
        });


        // final ImageView
        // btnCouponAddToFavouriteList=(ImageView)view.findViewById(R.id.btnCouponAddToFavouriteList);
		/*
		 * btnCouponAddToFavouriteList.setOnClickListener(new OnClickListener()
		 * {
		 *
		 * @Override public void onClick(View v) {
		 * if(Integer.parseInt(data.getFavCount().trim())>0){ new
		 * RemoveFromFavouriteList
		 * (storeData.getStoreId(),data.getCouponId(),btnCouponAddToFavouriteList
		 * ).execute(); }else{ new
		 * AddToFavouriteList(storeData.getStoreId(),data
		 * .getCouponId(),btnCouponAddToFavouriteList).execute(); }
		 *
		 * } });
		 *
		 * if(Integer.parseInt(data.getFavCount().trim())>0){
		 * btnCouponAddToFavouriteList
		 * .setImageResource(R.drawable.filled_heart); }else{
		 * btnCouponAddToFavouriteList.setImageResource(R.drawable.heart); }
		 */

        txtCouponTitle.setText(data.getTitle()+" from AchaDiscount");

		/*
		 * ViewGroup.LayoutParams vc=txtCouponDescriptionWeb.getLayoutParams();
		 * vc.height=LayoutParams.WRAP_CONTENT;
		 * vc.width=LayoutParams.MATCH_PARENT;
		 *
		 * txtCouponDescriptionWeb.setLayoutParams(vc);
		 */

		/*
		 * txtCouponDescription.setText(Html.fromHtml(data.getDescription().replace
		 * ("&#37;", " %")));
		 */
		Log.e("exp day",data.getRemainingDays()+"");

        if (data.getRemainingDays().trim().length() < 0
                || data.getRemainingDays().trim().startsWith("0")) {
            txtCouponRemainingDays.setText("Expires in 2 days");
        } else {
            txtCouponRemainingDays.setText("Exp. " + data.getRemainingDays());
        }

       /* ImageView imgCouponImage = (ImageView) view
                .findViewById(R.id.imgCouponImage);*/
//        Picasso.with(CouponList.this).load(data.getImgName()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgCouponImage);


        ImageView imgCouponShare = (ImageView) view
                .findViewById(R.id.imgCouponShare);
        imgCouponShare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                shareMessage(data.getOfferPage());
            }
        });
        linearCouponList.addView(view);
    }

    public void volleyJsonObjectRequest1(String url, final String name, final String code, final String offerpage,
                                         final String logo, final String description, final String storeid,
                                         final String storename) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            Intent intent = new Intent(CouponList.this,
                                    CodeGenerationPage.class);
                            intent.putExtra("name", name);
                            intent.putExtra("code", code);
                            intent.putExtra("offer_page", offerpage);
                            intent.putExtra("logo", logo);
                            intent.putExtra("description", description);
                            intent.putExtra("store_id", storeid);
                            intent.putExtra("store_name", storename);
                            startActivity(intent);
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showTermsAndCondition() {
        final AlertDialog builder = new Builder(CouponList.this).create();
        View view = getLayoutInflater().inflate(R.layout.term_and_condition,
                null);
        // TextView
        // txtTermsAndCondition=(TextView)view.findViewById(R.id.txtTermsAndCondition);
        WebView txtTermsAndCondition = (WebView) view
                .findViewById(R.id.txtTermsAndCondition);
        Button btnTermAndConditionNext = (Button) view
                .findViewById(R.id.btnTermAndConditionNext);
        btnTermAndConditionNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });

        String termsAndCondition = "<p><b>&#8226;"
                + storeData.getName()
                + " does NOT accept any Missing Cashback Claims. Hence, we do not get paid for any missing transactions and are unable to pay you Cashback</b><br>"
                + "<b>&#8226; Please add products in your "
                + storeData.getName()
                + " cart only AFTER you click out of Achadiscount &amp; not before. Else your Cashback will not track</b><br>"
                + "<b>&#8226;Your cashback will track 4 days after your shipment has been dispatched for delivery</b><br>"
                + "<b>&#8226; Cashback is not payable if you return or exchange, all or any part of your order. In all these cases Cashback for the full order will be Cancelled</b><br>"
                + "&#8226; Missing Cashback tickets for "
                + storeData.getName()
                + " will be marked as Resolved as we are not allowed to forward them to "
                + storeData.getName()
                + "<br>"
                + "&#8226; Offer mentioned in this coupon is valid for a limited time only while stocks last<br>"
                + "&#8226; To earn Cashback, remember to login to Ullasita.com, click out to the ecommerce site through us &amp; then place your order<br>"
                + "&#8226; Complete your purchase in the same session after clicking from All Discount Sale<br>"
                + "&#8226; Cashback may not be paid on purchases made using store credits/gift vouchers<br>"
                + "&#8226; Do not visit any other price comparison, coupon or deal site in between clicking-out from All Discount Sale &amp; ordering on retailer's site<br>"
                + "&#8226; Only use Coupons available on Ullasita to ensure validity &amp; Cashback tracking. Restrictions may apply in some cases<br>"
                + "&#8226; Please ensure you follow T&amp;Cs and best practices listed at the end of this page to ensure Cashback tracks</p>";

        if (storeData.getTermsConditions().trim().length() > 0) {
            // Log.e("terms", storeData.getTermsConditions());
            txtTermsAndCondition.loadDataWithBaseURL(null,
                    storeData.getTermsConditions(), "text/html", "utf-8", null);
            // txtTermsAndCondition.setText(Html.fromHtml(storeData.getTermsConditions()));
        } else {
            // txtTermsAndCondition.setText(Html.fromHtml(termsAndCondition));
            txtTermsAndCondition.loadDataWithBaseURL(null, termsAndCondition,
                    "text/html", "utf-8", null);
        }
        builder.setTitle("Terms & Conditions");
        builder.setView(view);
        builder.show();
    }

    public void showHowToGetThisOffer() {
        AlertDialog.Builder builder = new Builder(CouponList.this);
        View view = getLayoutInflater().inflate(R.layout.how_to_get, null);
        TextView txtHowToGetOffer = (TextView) view
                .findViewById(R.id.txtHowToGetOffer);
		/*
		 * Button btnTermAndConditionNext=(Button)view.findViewById(R.id.
		 * btnTermAndConditionNext);
		 * btnTermAndConditionNext.setOnClickListener(new OnClickListener() {
		 *
		 * @Override public void onClick(View v) {
		 *
		 * } });
		 */
        String howToGet = " <strong>Step 1:</strong> Join achadiscount.in Free &amp; login<br><br>"
                + "<strong>Step 2:</strong> Click on the offer that you want. This will take you to "
                + storeData.getName()
                + "'s website. "
                + "<br><br>"
                + "<strong>Step 3:</strong> Shop normally at "
                + storeData.getName()
                + ". Pay as you normally do, including by Cash-on-delivery<br><br>"
                + "<strong>Step 4:</strong> Your Cashback will track automatically &amp; be added to your Ullasita account within 48 hours ";

        if (storeData.getHowToGetThisOffer().trim().length() > 0) {
            txtHowToGetOffer.setText(Html.fromHtml(storeData
                    .getHowToGetThisOffer()));
        } else {
            txtHowToGetOffer.setText(Html.fromHtml(howToGet));
        }
        builder.setTitle("How to get Offer");
        builder.setView(view);
        builder.show();
    }

    // type 1=All 2=Coupon 3=Deals
    public void filterCoupons(int type) {
        linearCouponList.removeAllViews();
        boolean isValid = false;

        for (int i = 0; i < storeData.getDatas().size(); i++) {
            CouponData data = storeData.getDatas().get(i);
            if (type == 2) {
                if (data.getType().trim().equals("Coupon")) {
                    fillCouponsLayout(data);
                    isValid = true;
                }
            } else if (type == 3) {
                if (data.getType().trim().equals("Promotion")) {
                    fillCouponsLayout(data);
                    isValid = true;
                }
            } else {
                fillCouponsLayout(data);
                isValid = true;
            }
        }

        if (isValid == false) {
            Toast.makeText(CouponList.this, "No offers available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        linearCouponList.removeAllViews();
        new LoadAllCouponList().execute();

	/*	List<NameValuePair> param = new ArrayList<NameValuePair>();
			*//*
			 * String startletter="";
			 * if(getIntent().getStringExtra("storeName").
			 * toString().equalsIgnoreCase("big basket")){
			 * startletter="big-basket"; } else{
			 * startletter=getIntent().getStringExtra("storeName").toString(); }
			 *//*
		param.add(new BasicNameValuePair("start_letter", getIntent()
				.getStringExtra("storeName")));
		param.add(new BasicNameValuePair("user_id", preferences.getString(
				SessionManager.KEY_USERID, "")));
		Log.e("Param is ", urlLoadCouponList + param.toString());
		object = parser.makeHttpRequest(urlLoadCouponList, "GET", param);*/


/*
        String url = urlLoadCouponList + "?start_letter=" + getIntent().getStringExtra("storeName").replace(" ", "%20") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");

        volleyJsonObjectRequest(url);
*/

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {


                        if (jsonObject.getInt("success") == 1) {
                            JSONObject object1 = jsonObject
                                    .getJSONObject("store_details");
                            storeData = new StoreData();
                            storeData.setStoreId(object1.getString("id"));
                            storeData.setName(object1.getString("name"));
                            storeData.setType(object1.getString("type"));
                            storeData.setAmountPercentage(object1
                                    .getString("amount_percentage"));
                            storeData.setCoupons(object1.getString("coupons"));
                            storeData.setReviewsCount(object1
                                    .getString("review_count"));
                            storeData.setImgLogo(object1.getString("img_logo"));
                            storeData.setTermsConditions(object1
                                    .getString("terms_and_conditions"));
                            storeData.setHowToGetThisOffer(object1
                                    .getString("how_to_get_this_offer"));
                            storeData.setDesc(object1.getString("desc"));

                            JSONArray array = jsonObject.getJSONArray("coupons");
                            ArrayList<CouponData> couponList = new ArrayList<CouponData>();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object2 = array.getJSONObject(i)
                                        .getJSONObject("coupon_detail");
                                CouponData data = new CouponData();
                                data.setOfferName(object2.getString("offer_name"));
                                data.setCouponId(object2.getString("coupon_id"));
                                data.setFavCount(object2.getString("fav_count"));
                                data.setTitle(object2.getString("title"));
                                data.setDescription(object2
                                        .getString("description"));
                                data.setOfferPage(object2.getString("offer_page"));
                                data.setCode(object2.getString("code"));
                                data.setType(object2.getString("type"));
                                data.setExclusive(object2.getString("exclusive"));
                                data.setFeatured(object2.getString("featured"));
                                data.setImgName(object2.getString("coupon_image"));
                                data.setRemainingDays(object2
                                        .getString("remaining_days"));
                                couponList.add(data);
                            }

                            storeData.setDatas(couponList);
                            fillStoreDetails();
                            // filterRecomndedCategories();

                            txtAddReview.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (manager.isLoggedIn()) {
                                        Intent intent = new Intent(CouponList.this,
                                                AddReviews.class);
                                        Log.e("Store id=",
                                                storeData.getStoreId());
                                        intent.putExtra("storeId",
                                                storeData.getStoreId());
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(CouponList.this,
                                                LoginActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            });

                            txtViewReviews
                                    .setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(
                                                    CouponList.this,
                                                    LoadReviews.class);
                                            Log.e("Store id=",
                                                    storeData.getStoreId());
                                            intent.putExtra("storeId",
                                                    storeData.getStoreId());
                                            intent.putExtra("storeName",
                                                    storeData.getName());
                                            intent.putExtra("storeCoupons",
                                                    storeData.getCoupons());
                                            if (storeData.getType().trim()
                                                    .equals("Percentage")) {
                                                intent.putExtra(
                                                        "cashBack",
                                                        storeData
                                                                .getAmountPercentage()
                                                                + "%");
                                            } else {
                                                intent.putExtra(
                                                        "cashBack",
                                                        "Rs."
                                                                + storeData
                                                                .getAmountPercentage());
                                            }

                                            startActivity(intent);
                                        }
                                    });
                        } else {
                            Toast.makeText(CouponList.this, "No Data Found..",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

	/*
	 * public void filterRecomndedCategories(){
	 * linearCashbackCategories.removeAllViews(); for(int
	 * i=0;i<categoryDatas.size();i++){ final CategoryData
	 * data=categoryDatas.get(i); TextView textView=new
	 * TextView(CouponList.this); textView.setTextSize(14);
	 * LinearLayout.LayoutParams params=new
	 * LinearLayout.LayoutParams(LayoutParams
	 * .MATCH_PARENT,LayoutParams.WRAP_CONTENT); params.gravity=Gravity.CENTER;
	 * textView.setLayoutParams(params); textView.setText(data.getCatname());
	 * textView.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { Intent intent=new
	 * Intent(CouponList.this,CategoryWiseStores.class);
	 * intent.putExtra("catId",data.getCatid());
	 * intent.putExtra("subCatName",data.getCatname()); startActivity(intent); }
	 * }); linearCashbackCategories.addView(textView); } }
	 */

    public void shareMessage(String msg) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(sharingIntent, "Ullasita"));
    }

    class LoadAllCouponList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CouponList.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
			/*
			 * String startletter="";
			 * if(getIntent().getStringExtra("storeName").
			 * toString().equalsIgnoreCase("big basket")){
			 * startletter="big-basket"; } else{
			 * startletter=getIntent().getStringExtra("storeName").toString(); }
			 */
            param.add(new BasicNameValuePair("start_letter", getIntent()
                    .getStringExtra("storeName")));
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            Log.e("Param is ", urlLoadCouponList + param.toString());
            object = parser.makeHttpRequest(CouponList.this, urlLoadCouponList, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            storeDatas = new ArrayList<StoreData>();
            categoryDatas = new ArrayList<CategoryData>();
            linearCouponList.removeAllViews();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONObject object1 = object
                                .getJSONObject("store_details");
                        storeData = new StoreData();
                        storeData.setStoreId(object1.getString("id"));
                        storeData.setName(object1.getString("name"));
                        storeData.setType(object1.getString("type"));
                        storeData.setAmountPercentage(object1
                                .getString("amount_percentage"));
                        storeData.setCoupons(object1.getString("coupons"));
                        storeData.setReviewsCount(object1
                                .getString("review_count"));
                        storeData.setImgLogo(object1.getString("img_logo"));
                        storeData.setTermsConditions(object1
                                .getString("terms_and_conditions"));
                        storeData.setHowToGetThisOffer(object1
                                .getString("how_to_get_this_offer"));
                        storeData.setDesc(object1.getString("desc"));

                        JSONArray array = object.getJSONArray("coupons");
                        ArrayList<CouponData> couponList = new ArrayList<CouponData>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object2 = array.getJSONObject(i)
                                    .getJSONObject("coupon_detail");
                            CouponData data = new CouponData();
                            data.setOfferName(object2.getString("offer_name"));
                            data.setCouponId(object2.getString("coupon_id"));
                            data.setFavCount(object2.getString("fav_count"));
                            data.setTitle(object2.getString("title"));
                            data.setDescription(object2
                                    .getString("description"));
                            data.setOfferPage(object2.getString("offer_page"));
                            data.setCode(object2.getString("code"));
                            data.setType(object2.getString("type"));
                            data.setExclusive(object2.getString("exclusive"));
                            data.setFeatured(object2.getString("featured"));
                            data.setImgName(object2.getString("coupon_image"));
                            data.setRemainingDays(object2
                                    .getString("remaining_days"));
                            couponList.add(data);
                        }

                        storeData.setDatas(couponList);
                        fillStoreDetails();
                        // filterRecomndedCategories();

                        txtAddReview.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (manager.isLoggedIn()) {
                                    Intent intent = new Intent(CouponList.this,
                                            AddReviews.class);
                                    Log.e("Store id=",
                                            storeData.getStoreId());
                                    intent.putExtra("storeId",
                                            storeData.getStoreId());
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(CouponList.this,
                                            LoginActivity.class);
                                    startActivity(intent);
                                }
                            }
                        });

                        txtViewReviews
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(
                                                CouponList.this,
                                                LoadReviews.class);
                                        Log.e("Store id=",
                                                storeData.getStoreId());
                                        intent.putExtra("storeId",
                                                storeData.getStoreId());
                                        intent.putExtra("storeName",
                                                storeData.getName());
                                        intent.putExtra("storeCoupons",
                                                storeData.getCoupons());
                                        if (storeData.getType().trim()
                                                .equals("Percentage")) {
                                            intent.putExtra(
                                                    "cashBack",
                                                    storeData
                                                            .getAmountPercentage()
                                                            + "%");
                                        } else {
                                            intent.putExtra(
                                                    "cashBack",
                                                    "Rs."
                                                            + storeData
                                                            .getAmountPercentage());
                                        }

                                        startActivity(intent);
                                    }
                                });
                    } else {
                        Toast.makeText(CouponList.this, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }

    }

    class ExecuteurlAddtoClickHistory extends AsyncTask<String, String, String> {

        String name, code, offerpage, logo, description, storeid, storename;
        ProgressDialog dialog;

        ExecuteurlAddtoClickHistory(String name, String code, String offerpage,
                                    String logo, String description, String storeid,
                                    String storename) {
            this.name = name;
            this.code = code;
            this.offerpage = offerpage;
            this.logo = logo;
            this.description = description;
            this.storeid = storeid;
            this.storename = storename;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(CouponList.this, "", "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            // voucher_name=buytest&store_name=Flipkart
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            param.add(new BasicNameValuePair("affiliate_id", storeid));
            param.add(new BasicNameValuePair("store_name", storename));
            param.add(new BasicNameValuePair("voucher_name", description));
            JSONObject object = parser.makeHttpRequest(CouponList.this, urlAddtoClickHistory,
                    "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Intent intent = new Intent(CouponList.this,
                                CodeGenerationPage.class);
                        intent.putExtra("name", name);
                        intent.putExtra("code", code);
                        intent.putExtra("offer_page", offerpage);
                        intent.putExtra("logo", logo);
                        intent.putExtra("description", description);
                        intent.putExtra("store_id", storeid);
                        intent.putExtra("store_name", storename);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    Log.e("Exc", e.toString());
                }
            }
        }

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class AddToFavouriteList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        ImageView button;
        private String store_id, coupon_id;

        public AddToFavouriteList(String store_id, String coupon_id,
                                  ImageView button) {
            this.store_id = store_id;
            this.coupon_id = coupon_id;
            this.button = button;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CouponList.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            param.add(new BasicNameValuePair("coupon_id", coupon_id));
            param.add(new BasicNameValuePair("store_id", store_id));
            Log.e("Fav params =======", param.toString());
            Log.e("Fav url ==============", urlAddToFav);
            object = parser.makeHttpRequest(CouponList.this, urlAddToFav, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            storeDatas = new ArrayList<StoreData>();
            categoryDatas = new ArrayList<CategoryData>();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Toast.makeText(CouponList.this,
                                "Successfully added in your wishlist..",
                                Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(CouponList.this, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
            new LoadAllCouponList().execute();

/*
            String url = urlLoadCouponList + "?start_letter=" + getIntent().getStringExtra("storeName") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");

            volleyJsonObjectRequest(url);
*/

        }

    }

    class RemoveFromFavouriteList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        ImageView button;
        private String store_id, coupon_id;

        public RemoveFromFavouriteList(String store_id, String coupon_id,
                                       ImageView button) {
            this.store_id = store_id;
            this.coupon_id = coupon_id;
            this.button = button;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CouponList.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            param.add(new BasicNameValuePair("coupon_id", coupon_id));
            param.add(new BasicNameValuePair("store_id", store_id));
            Log.e("Fav params =====", param.toString());
            Log.e("Fav url ==============", urlRemoveFromFav);
            object = parser.makeHttpRequest(CouponList.this, urlRemoveFromFav, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            storeDatas = new ArrayList<StoreData>();
            categoryDatas = new ArrayList<CategoryData>();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Toast.makeText(CouponList.this,
                                "Successfully removed from your wishlist..",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(CouponList.this, "No Data Found..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
            new LoadAllCouponList().execute();

/*
            String url = urlLoadCouponList + "?start_letter=" + getIntent().getStringExtra("storeName") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "");

            volleyJsonObjectRequest(url);
*/

        }
    }
}