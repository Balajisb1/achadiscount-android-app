package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.MyApplication;
import com.application.ads.extra.PlaceApi;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OfflineMap extends Activity implements ConnectionCallbacks,
        OnConnectionFailedListener {

    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    LinearLayout storedetailslinear;
    SharedPreferences preferences;
    SessionManager manager;
    LatLng latLng;
    ImageView locbtn, mylocation;
    Double latitude, longitude;
    double destlatitude, destlongitude;
    AutoCompleteTextView location;
    MarkerOptions markerOptions;
    MyApplication myApplication;
    double autoComplteSelectedLattitute;
    boolean gpsStatus = false;
    double autoComplteSelectedLaongitute;
    private GoogleMap googleMap;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        createLocationRequest();
        buildGoogleApiClient();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_map);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads().detectDiskWrites().detectNetwork()
                .penaltyDeath().build());
        myApplication = MyApplication.getInstance();
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("My Location");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        storedetailslinear = (LinearLayout) findViewById(R.id.storedetailslinear);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        manager = new SessionManager(OfflineMap.this);

        location = (AutoCompleteTextView) findViewById(R.id.location);
        locbtn = (ImageView) findViewById(R.id.locbtn);
        mylocation = (ImageView) findViewById(R.id.mylocation);

        location.setAdapter(new AutoCompleteAdapter(OfflineMap.this,
                R.layout.auto_complete_listitem));
        location.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                googleMap.clear();
                String loc = (String) parent.getItemAtPosition(position);
                new GetLocationInfo(loc).execute();
            }
        });

        locbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String loc = location.getText().toString();

                if (location != null && !location.equals("")) {
                    new GetLocationInfo(loc).execute();
                } else {
                    Toast.makeText(OfflineMap.this, "Please enter location",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        mylocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                createLocationRequest();
                buildGoogleApiClient();
                mGoogleApiClient.connect();


            }
        });

        checkGPS();

    }

    public void makeMap() {

        initilizeMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
    }

    public boolean checkGPS() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.e("GPSSTATUS", gpsStatus + "");
            if (myApplication.getMyLatitude() == null) {
                createLocationRequest();
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            } else {
                makeMap();
            }
            return true;
        } else {
            showGPSDisabledAlertToUser();
            return false;
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage(
                        "GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("GPS Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                gpsStatus = true;
                                startActivity(callGPSSettingIntent);

                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        checkGPS();
                        gpsStatus = false;
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {

                Toast.makeText(OfflineMap.this, "Sorry! unable to create maps",
                        Toast.LENGTH_SHORT).show();
            }
            googleMap.clear();
            /*
             * destlatitude=Double.parseDouble(getIntent().getStringExtra("latitude"
			 * )); destlongitude=Double.parseDouble(getIntent().getStringExtra(
			 * "longtitude")); String title=getIntent().getStringExtra("title");
			 * googleMap.addMarker(new MarkerOptions().position(new
			 * LatLng(destlatitude,
			 * destlongitude)).icon(BitmapDescriptorFactory.
			 * defaultMarker(BitmapDescriptorFactory.HUE_RED)).title(title));
			 * CameraPosition cameraPosition = new CameraPosition.Builder()
			 * .target(new LatLng(destlatitude,
			 * destlongitude)).zoom(15).build();
			 * googleMap.animateCamera(CameraUpdateFactory
			 * .newCameraPosition(cameraPosition));
			 */

            if (myApplication.getMyLatitude() == null
                    || myApplication.getMyLongitude() == null) {
                myApplication = MyApplication.getInstance();
                createLocationRequest();
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            } else {
                latitude = Double.parseDouble(myApplication.getMyLatitude());
                longitude = Double.parseDouble(myApplication.getMyLongitude());
            }
            MarkerOptions marker1 = new MarkerOptions().position(new LatLng(latitude, longitude)).title("ME");
            marker1.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            googleMap.addMarker(marker1);
            CameraPosition cameraPosition1 = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition1));
        }
        LatLng origin = new LatLng(latitude, longitude);
        LatLng dest = new LatLng(destlatitude, destlongitude);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        // DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        // downloadTask.execute(url);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Eror in downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(OfflineMap.this,
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(OfflineMap.this,
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;
            case R.id.action_coupons:
                Intent intent1 = new Intent(OfflineMap.this, CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(OfflineMap.this,
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well
        // suited for
        // applications that do not require a fine-grained location and that do
        // not need location
        // updates. Gets the best and most recent location currently available,
        // which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        Log.e("LastLocation", mLastLocation + "");
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            myApplication.setMyLatitude(latitude + "");
            myApplication.setMyLongitude(longitude + "");
			/*Toast.makeText(this, "Lat " + latitude + ", Longitude" + longitude,
					Toast.LENGTH_LONG).show();*/
            latitude = Double.parseDouble(myApplication.getMyLatitude());
            longitude = Double.parseDouble(myApplication.getMyLongitude());
            Log.e("latitute", latitude + "");
            Log.e("longitude", longitude + "");
            gpsStatus = true;
            makeMap();
            googleMap.clear();
            MarkerOptions marker1 = new MarkerOptions().position(
                    new LatLng(Double.parseDouble(myApplication
                            .getMyLatitude()), Double
                            .parseDouble(myApplication.getMyLongitude())))
                    .title("ME");
            marker1.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            googleMap.addMarker(marker1);
            CameraPosition cameraPosition1 = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(myApplication
                            .getMyLatitude()), Double
                            .parseDouble(myApplication.getMyLongitude())))
                    .zoom(15).build();

            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition1));

			/*
			 * mLatitudeText.setText(String.format("%s: %f", mLatitudeLabel,
			 * mLastLocation.getLatitude()));
			 * mLongitudeText.setText(String.format("%s: %f", mLongitudeLabel,
			 * mLastLocation.getLongitude()));
			 */
        } else {
			/*Toast.makeText(this, "No location Detected", Toast.LENGTH_SHORT)
					.show();*/
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes
        // might be returned in
        // onConnectionFailed.
        Log.i("THIS", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    public void putMap() {
        googleMap.clear();
        MarkerOptions marker1 = new MarkerOptions().position(
                new LatLng(Double.parseDouble(myApplication.getMyLatitude()),
                        Double.parseDouble(myApplication.getMyLongitude())))
                .title("ME");
        marker1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        googleMap.addMarker(marker1);
        CameraPosition cameraPosition1 = new CameraPosition.Builder()
                .target(new LatLng(Double.parseDouble(myApplication
                        .getMyLatitude()), Double.parseDouble(myApplication
                        .getMyLongitude()))).zoom(15).build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition1));
    }

	/*
	 * @Override protected void onStart() { super.onStart();
	 * mGoogleApiClient.connect(); }
	 * 
	 * @Override protected void onStop() { super.onStop(); if
	 * (mGoogleApiClient.isConnected()) { mGoogleApiClient.disconnect(); } }
	 */

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We
        // call connect() to
        // attempt to re-establish the connection.
        Log.i("THIS", "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (!gpsStatus) {
            //	mGoogleApiClient.connect();
        } else {
            checkGPS();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                // DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                // routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);

            }

            // Drawing polyline in the Google Map for the i-th route
            googleMap.addPolyline(lineOptions);
        }
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            // parserTask.execute(result);

        }
    }

    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineMap.this, "",
                    "Searching Locations..");
        }

        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;
            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            dialog.dismiss();
            if (addresses == null || addresses.size() == 0) {
                Toast.makeText(getBaseContext(), "No Location found",
                        Toast.LENGTH_SHORT).show();
            }

            // Clears all the existing markers on the map
            googleMap.clear();

            // Adding Markers on Google Map for each matching address
            for (int i = 0; i < addresses.size(); i++) {

                Address address = (Address) addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(),
                        address.getLongitude());

                String addressText = String.format(
                        "%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address
                                .getAddressLine(0) : "", address
                                .getCountryName());
                // manager.initializelocation(address.getLatitude()+"",
                // address.getLongitude()+"");

                myApplication.setMyLatitude(String.valueOf(address
                        .getLatitude()));
                myApplication.setMyLongitude(String.valueOf(address
                        .getLongitude()));
                googleMap.clear();
                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(addressText);
                markerOptions.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                googleMap.addMarker(markerOptions);
                // if(i==0)
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        }
    }

    class AutoCompleteAdapter extends ArrayAdapter<Object> implements
            Filterable {

        Context mContext;
        int mResource;
        PlaceApi api = new PlaceApi();
        private ArrayList<String> resultList;

        public AutoCompleteAdapter(Context context, int resource) {
            super(context, resource);
            this.mContext = context;
            this.mResource = resource;
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public Object getItem(int position) {
            return resultList.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View add;
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // if (position != (resultList.size() - 1)){
            add = inflater.inflate(R.layout.auto_complete_listitem, null);
            // }

            if (position != (resultList.size() - 1)) {

                TextView auTextView = (TextView) add
                        .findViewById(R.id.auto_complete_single_text);
                // TextView autocompleteTextView = (TextView) view
                // .findViewById(R.id.auto_complete_single_text);
                // autocompleteTextView.setText(resultList.get(position));
                auTextView.setText(resultList.get(position));
            }

            return add;
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence arg0) {
                    // TODO Auto-generated method stub

                    FilterResults filterResults = new FilterResults();

                    if (arg0 != null) {
                        // Retrieve the autocomplete results.
                        resultList = api.autocomplete(arg0.toString());

                        // Assign the data to the FilterResults

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();

                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence arg0,
                                              FilterResults arg1) {
                    // TODO Auto-generated method stub

                    if (arg1 != null && arg1.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }

                }

            };
            return filter;

        }

    }

    class GetLocationInfo extends AsyncTask<String, String, String> {

        String address;

        GetLocationInfo(String address) {
            this.address = address;
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder stringBuilder = new StringBuilder();
            try {

                address = address.replaceAll(" ", "%20");

                HttpPost httppost = new HttpPost(
                        "http://maps.google.com/maps/api/geocode/json?address="
                                + address + "&sensor=false");
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                stringBuilder = new StringBuilder();

                response = client.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = null;
            }

            if (jsonObject == null) {
                return "";
            } else {
                return jsonObject.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {

                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(result);
                    autoComplteSelectedLaongitute = ((JSONArray) jsonObject
                            .get("results")).getJSONObject(0)
                            .getJSONObject("geometry")
                            .getJSONObject("location").getDouble("lng");

                    autoComplteSelectedLattitute = ((JSONArray) jsonObject
                            .get("results")).getJSONObject(0)
                            .getJSONObject("geometry")
                            .getJSONObject("location").getDouble("lat");

                    myApplication.setMyLatitude(String
                            .valueOf(autoComplteSelectedLattitute));
                    myApplication.setMyLongitude(String
                            .valueOf(autoComplteSelectedLaongitute));
                    latLng = new LatLng(autoComplteSelectedLattitute,
                            autoComplteSelectedLaongitute);
                    markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.title(address.replace("%20", " "));
                    markerOptions.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                    googleMap.addMarker(markerOptions);
                    // if(i==0)
                    googleMap.animateCamera(CameraUpdateFactory
                            .newLatLng(latLng));

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng).zoom(15).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                } catch (JSONException e) {
                    Log.e("latt long===", e.getMessage());
                }

                Log.e("latt long====",
                        autoComplteSelectedLattitute + "="
                                + autoComplteSelectedLaongitute);

            } else {
                Toast.makeText(OfflineMap.this, "No data found",
                        Toast.LENGTH_LONG).show();
            }

        }

    }


}
