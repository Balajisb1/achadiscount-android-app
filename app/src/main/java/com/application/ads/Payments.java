package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.EarningsData;
import com.application.ads.data.PaymentData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Payments extends Activity {


    JSONParser parser;
    List<PaymentData> list;
    List<PaymentData> paymentDatas;
    TableLayout tblLayout;
    EarningsData earningsData;
    Spinner spnrUserPaymentsCount;
    EditText edtUserPaymentFilter;
    int rowCount = 2;
    int currentPageNo = 1;
    LinearLayout linearPageNo;
    TextView txtPrevious;
    TextView noRecordsTxt;
    TextView txtNext, txtShowingResult;
    SharedPreferences preferences;
    TextView txtCashBackPendingEarnings, txtCashBackEarnings, txtPaidEarnings,
            txtAvailableForPaymentEarnings, txtWithdrawEarnings, txtTotalEarnings;
    Button btnWithDraw;
    private String urlLoadPayments = DBConnection.BASEURL + "mypayment.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_payment);
        /*getActionBar().setTitle("My Payments");*/
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("My Payments");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        list = new ArrayList<PaymentData>();
        tblLayout = (TableLayout) findViewById(R.id.tblLayout);
        parser = new JSONParser();

        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);
        noRecordsTxt = (TextView) findViewById(R.id.noRecordsTxt);

        linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

        txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo > 1) {
                    currentPageNo -= 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        txtNext = (TextView) findViewById(R.id.txtNext);
        txtNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo < (list.size() / rowCount)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                } else if (currentPageNo == (list.size() / rowCount) && (list.size() % rowCount > 0)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        edtUserPaymentFilter = (EditText) findViewById(R.id.edtUserPaymentFilter);
        edtUserPaymentFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterOptionByEditText(edtUserPaymentFilter.getText().toString().trim());
            }
        });

        spnrUserPaymentsCount = (Spinner) findViewById(R.id.spnrUserPaymentsCount);
        spnrUserPaymentsCount.setAdapter(new ArrayAdapter<String>(Payments.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrUserPaymentsCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (paymentDatas != null) {
                    rowCount = Integer.parseInt(spnrUserPaymentsCount.getSelectedItem().toString());
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        txtCashBackPendingEarnings = (TextView) findViewById(R.id.txtCashBackPendingEarnings);
        txtCashBackEarnings = (TextView) findViewById(R.id.txtCashBackEarnings);
        txtPaidEarnings = (TextView) findViewById(R.id.txtPaidEarnings);
        txtAvailableForPaymentEarnings = (TextView) findViewById(R.id.txtAvailableForPaymentEarnings);
        txtWithdrawEarnings = (TextView) findViewById(R.id.txtWithdrawEarnings);
        txtTotalEarnings = (TextView) findViewById(R.id.txtTotalEarnings);

        btnWithDraw = (Button) findViewById(R.id.btnWithDraw);

        btnWithDraw.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//				Toast.makeText(Payments.this,earningsData.getWithdrawMinBal(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Payments.this, WithdrawActivity.class);
                intent.putExtra("availableBalance", earningsData.getAvailablePayment().toString());
                intent.putExtra("withdrawminbal", earningsData.getWithdrawMinBal());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        new LoadPayments().execute();

//		List<NameValuePair> param=new ArrayList<NameValuePair>();
//		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));

/*		String url=urlLoadPayments+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
		volleyJsonObjectRequest(url);*/
        super.onResume();
    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    paymentDatas = new ArrayList<PaymentData>();

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("mypayments");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                PaymentData data = new PaymentData();
                                data.setDateRequest(object1.getString("date_added"));
                                data.setDateAccept(object1.getString("closing_date"));
                                data.setRequestAmount(object1.getString("requested_amount"));
                                data.setStatus(object1.getString("status"));
                                paymentDatas.add(data);
                            }

                            list = new ArrayList<PaymentData>();
                            list = paymentDatas;
                            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                            createPageNo();

                        } else {
                            Toast.makeText(Payments.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }


                        earningsData = new EarningsData();
                        earningsData.setAvailablePayment(jsonObject.getString("available_balance"));
                        earningsData.setConfirmedCashBack(jsonObject.getString("ref_amount"));
                        earningsData.setPaidEarnings(jsonObject.getString("paid_amount"));
                        earningsData.setPendingCashBack(jsonObject.getString("pending_cashback"));
                        earningsData.setTotalEarnings(jsonObject.getString("total_earnings"));
                        earningsData.setWithdrawApproval(jsonObject.getString("waiting_bal"));
                        earningsData.setWithdrawMinBal(jsonObject.getString("withdraw_min"));
                        Log.e("withdrawmin", earningsData.getWithdrawMinBal());
                        placeEarningList();
                    } else {
                        Toast.makeText(Payments.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void loadTableRows(int start, int end) {
        tblLayout.removeAllViews();


        if(list!=null){
            if(list.size()>0){
                noRecordsTxt.setVisibility(View.GONE);

        if (list.size() >= end) {
            for (int i = start; i < end; i++) {
                View view = getLayoutInflater().inflate(R.layout.user_payment_table_row, null);
                TableRow tblrowUserPayment = (TableRow) view.findViewById(R.id.tblrowUserPayment);
                TextView txtUserPaymentDateRequest = (TextView) view.findViewById(R.id.txtUserPaymentDateRequest);
                TextView txtUserPaymentRequestAmount = (TextView) view.findViewById(R.id.txtUserPaymentRequestAmount);
                Button btnUserPaymentStatus = (Button) view.findViewById(R.id.btnUserPaymentStatus);
                TextView txtUserPaymentConfirmationDate = (TextView) view.findViewById(R.id.txtUserPaymentConfirmationDate);

                txtUserPaymentDateRequest.setText(list.get(i).getDateRequest());
                txtUserPaymentRequestAmount.setText("RS." + list.get(i).getRequestAmount());
                if (list.get(i).getStatus().equals("Processing")) {
                    btnUserPaymentStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                } else {
                    btnUserPaymentStatus.setBackgroundColor(Color.parseColor("#449d44"));
                }
                btnUserPaymentStatus.setText(list.get(i).getStatus());
                if (list.get(i).getDateAccept().toString().startsWith("0")) {
                    txtUserPaymentConfirmationDate.setText("-");
                } else {
                    txtUserPaymentConfirmationDate.setText(list.get(i).getDateAccept());
                }
                tblLayout.addView(view);
            }
            String starting = String.valueOf(start + 1);
            txtShowingResult.setText("Showing " + starting + " to " + end + " entries");
        } else {
            for (int i = start; i < list.size(); i++) {
                View view = getLayoutInflater().inflate(R.layout.user_payment_table_row, null);
				/*TableRow tblrowUserPayment=(TableRow)view.findViewById(R.id.tblrowUserPayment);*/
                TextView txtUserPaymentDateRequest = (TextView) view.findViewById(R.id.txtUserPaymentDateRequest);
                TextView txtUserPaymentRequestAmount = (TextView) view.findViewById(R.id.txtUserPaymentRequestAmount);
                Button btnUserPaymentStatus = (Button) view.findViewById(R.id.btnUserPaymentStatus);
                TextView txtUserPaymentConfirmationDate = (TextView) view.findViewById(R.id.txtUserPaymentConfirmationDate);
                txtUserPaymentDateRequest.setText(list.get(i).getDateRequest());
                txtUserPaymentRequestAmount.setText("RS." + list.get(i).getRequestAmount());
                if (list.get(i).getStatus().equals("Processing")) {
                    btnUserPaymentStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                } else {
                    btnUserPaymentStatus.setBackgroundColor(Color.parseColor("#449d44"));
                }
                btnUserPaymentStatus.setText(list.get(i).getStatus());
                if (list.get(i).getDateAccept().toString().startsWith("0")) {
                    txtUserPaymentConfirmationDate.setText("-");
                } else {
                    txtUserPaymentConfirmationDate.setText(list.get(i).getDateAccept());
                }
                tblLayout.addView(view);
            }
            String starting = String.valueOf(start + 1);
            txtShowingResult.setText("Showing " + starting + " to " + list.size() + " entries");
        }

            }else{
                noRecordsTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void filterOption(int start, int end) {
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void filterOptionByEditText(String text) {
        list = new ArrayList<PaymentData>();
        for (int i = 0; i < paymentDatas.size(); i++) {
            if (paymentDatas.get(i).getDateRequest().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    paymentDatas.get(i).getRequestAmount().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    paymentDatas.get(i).getStatus().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                list.add(paymentDatas.get(i));
            }
        }
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void createPageNo() {
        linearPageNo.removeAllViews();

        int numOfPage = list.size() / rowCount;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        params.setMargins(5, 5, 5, 5);

        for (int i = 0; i < numOfPage; i++) {
            final int j = i;
            TextView textView = new TextView(Payments.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(i + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j + 1;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearPageNo.addView(textView);
        }

        if ((list.size() % rowCount) > 0) {
            final int j = numOfPage + 1;
            TextView textView = new TextView(Payments.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(numOfPage + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearPageNo.addView(textView);
        }
    }

    public void placeEarningList() {
        if (earningsData != null) {
            if (earningsData.getPendingCashBack() != null || earningsData.getPendingCashBack().equals("null")) {
                txtCashBackPendingEarnings.setText("Rs." + earningsData.getPendingCashBack() + ".00");
            } else {
                txtCashBackPendingEarnings.setText("Rs.0.00");
            }

            if (earningsData.getPaidEarnings() != null || earningsData.getPaidEarnings().equals("null")) {
                txtPaidEarnings.setText("Rs." + earningsData.getPaidEarnings() + ".00");
            } else {
                txtPaidEarnings.setText("Rs.0.00");
            }

            if (earningsData.getTotalEarnings() != null || earningsData.getTotalEarnings().equals("null")) {
                txtTotalEarnings.setText("Rs." + earningsData.getTotalEarnings() + ".00");
            } else {
                txtTotalEarnings.setText("Rs.0.00");
            }

            if (earningsData.getWithdrawApproval() != null || earningsData.getWithdrawApproval().equals("null")) {
                txtWithdrawEarnings.setText("Rs." + earningsData.getWithdrawApproval() + ".00");
            } else {
                txtWithdrawEarnings.setText("Rs.0.00");
            }

            if (earningsData.getAvailablePayment() != null || earningsData.getAvailablePayment().equals("null")) {
                txtAvailableForPaymentEarnings.setText("Rs." + earningsData.getAvailablePayment() + ".00");
            } else {
                txtAvailableForPaymentEarnings.setText("Rs.0.00");
            }


            if (earningsData.getConfirmedCashBack() != null || earningsData.getConfirmedCashBack().equals("null")) {
                txtCashBackEarnings.setText("Rs." + earningsData.getConfirmedCashBack() + ".00");
            } else {
                txtCashBackEarnings.setText("Rs.0.00");
            }


        } else {
            txtCashBackPendingEarnings.setText("Rs.0.00");
            txtPaidEarnings.setText("Rs.0.00");
            txtTotalEarnings.setText("Rs.0.00");
            txtWithdrawEarnings.setText("Rs.0.00");
            txtAvailableForPaymentEarnings.setText("Rs.0.00");
            txtCashBackEarnings.setText("Rs.0.00");

        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadPayments extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Payments.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(Payments.this, urlLoadPayments, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            paymentDatas = new ArrayList<PaymentData>();
            if (result.toString().trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        noRecordsTxt.setVisibility(View.GONE);
                        JSONArray array = object.getJSONArray("mypayments");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            PaymentData data = new PaymentData();
                            data.setDateRequest(object1.getString("date_added"));
                            data.setDateAccept(object1.getString("closing_date"));
                            data.setRequestAmount(object1.getString("requested_amount"));
                            data.setStatus(object1.getString("status"));
                            paymentDatas.add(data);
                        }

                        list = new ArrayList<PaymentData>();
                        list = paymentDatas;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                        createPageNo();

                    } else {
                        noRecordsTxt.setVisibility(View.VISIBLE);
                        Toast.makeText(Payments.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }


                    earningsData = new EarningsData();
                    earningsData.setAvailablePayment(object.getString("available_balance"));
                    earningsData.setConfirmedCashBack(object.getString("ref_amount"));
                    earningsData.setPaidEarnings(object.getString("paid_amount"));
                    earningsData.setPendingCashBack(object.getString("pending_cashback"));
                    earningsData.setTotalEarnings(object.getString("total_earnings"));
                    earningsData.setWithdrawApproval(object.getString("waiting_bal"));
                    earningsData.setWithdrawMinBal(object.getString("withdraw_min"));
                    Log.e("withdrawmin", earningsData.getWithdrawMinBal());
                    placeEarningList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(Payments.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
            }
        }
    }

}
