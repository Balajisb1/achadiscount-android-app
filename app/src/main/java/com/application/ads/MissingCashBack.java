package com.application.ads;


import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.MissingCashbackData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MissingCashBack extends Activity {


    JSONParser parser;
    List<MissingCashbackData> missingCashBackDatas;
    TableLayout tblLayout;
    Spinner spnrUserMissingCount;
    List<MissingCashbackData> list;
    EditText edtUserMissingFilter;
    int rowCount = 2;
    int currentPageNo = 1;
    LinearLayout linearPageNo;
    TextView txtPrevious;
    TextView noRecordsTxt;
    TextView txtNext, txtShowingResult;
    SharedPreferences preferences;
    Button btnAddTicket;
    AutoCompleteTextView atoSearchStoreName;
    Button btnSearchGo;
    private String urlLoadMissingCashBack = DBConnection.BASEURL + "missing_cashback_list.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_missing_cashback);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Missing Cashback");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        /*getActionBar().setTitle("Missing Cashback");*/
        getActionBar().setTitle("");
        tblLayout = (TableLayout) findViewById(R.id.tblLayout);
        parser = new JSONParser();

        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        noRecordsTxt = (TextView) findViewById(R.id.noRecordsTxt);
        txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);

        linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

        txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    if (currentPageNo > 1) {
                        currentPageNo -= 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtNext = (TextView) findViewById(R.id.txtNext);
        txtNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    if (currentPageNo < (list.size() / rowCount)) {
                        currentPageNo += 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    } else if (currentPageNo == (list.size() / rowCount) && (list.size() % rowCount > 0)) {
                        currentPageNo += 1;
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        edtUserMissingFilter = (EditText) findViewById(R.id.edtUserMissingFilter);
        edtUserMissingFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterOptionByEditText(edtUserMissingFilter.getText().toString().trim());
            }
        });

        spnrUserMissingCount = (Spinner) findViewById(R.id.spnrUserMissingCount);
        spnrUserMissingCount.setAdapter(new ArrayAdapter<String>(MissingCashBack.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrUserMissingCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {
                    if (missingCashBackDatas != null) {
                        rowCount = Integer.parseInt(spnrUserMissingCount.getSelectedItem().toString());
                        filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        btnAddTicket = (Button) findViewById(R.id.btnAddTicket);
        btnAddTicket.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MissingCashBack.this, AddCashbackTicket.class));
            }
        });


    }


    @Override
    protected void onResume() {
        new LoadPayments().execute();

	/*	List<NameValuePair> param=new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));*/
/*

		String url=urlLoadMissingCashBack+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"0");
		volleyJsonObjectRequest(url);
*/
        super.onResume();
    }


    public void volleyJsonObjectRequest(String url) {
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    missingCashBackDatas = new ArrayList<MissingCashbackData>();
                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("missing_cashback");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                MissingCashbackData data = new MissingCashbackData();
                                data.setRetailerName(object1.getString("retailer_name"));
                                data.setStatus(object1.getString("status"));
                                data.setTransactionAmount(object1.getString("transaction_amount"));
                                data.setTransactionDate(object1.getString("transaction_date"));
                                data.setTransactionId(object1.getString("transaction_id"));
                                data.setQueryDate(object1.getString("query_date"));
                                missingCashBackDatas.add(data);
                            }
                            list = new ArrayList<MissingCashbackData>();
                            list = missingCashBackDatas;
                            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                            createPageNo();
                        } else {
                            Toast.makeText(MissingCashBack.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(MissingCashBack.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void loadTableRows(int start, int end) {
        try {
            tblLayout.removeAllViews();

            if (list != null) {
                if (list.size() > 0) {
                    noRecordsTxt.setVisibility(View.GONE);

                    if (list.size() >= end) {
                        for (int i = start; i < end; i++) {
                            View view = getLayoutInflater().inflate(R.layout.user_missing_cashback_table_row, null);
                            TextView txtUserMissingCashbackRetailerName = (TextView) view.findViewById(R.id.txtUserMissingCashbackRetailerName);
                            TextView txtUserMissingCashbackTransactionAmount = (TextView) view.findViewById(R.id.txtUserMissingCashbackTransactionAmount);
                            TextView txtUserMissingCashbackDateOfTransaction = (TextView) view.findViewById(R.id.txtUserMissingCashbackDateOfTransaction);
                            TextView txtUserMissingCashbackDateOfQuery = (TextView) view.findViewById(R.id.txtUserMissingCashbackDateOfQuery);
                            TextView txtUserMissingCashbackTransactionId = (TextView) view.findViewById(R.id.txtUserMissingCashbackTransactionId);
                            Button btnUserMissingCashbackStatus = (Button) view.findViewById(R.id.btnUserMissingCashbackStatus);

                            if (list.get(i).getStatus().equals("3")) {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                                btnUserMissingCashbackStatus.setText("Created");
                            } else if (list.get(i).getStatus().equals("1")) {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                                btnUserMissingCashbackStatus.setText("Pending");
                            } else if (list.get(i).getStatus().equals("0")) {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                                btnUserMissingCashbackStatus.setText("Completed");
                            } else {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#449d44"));
                                btnUserMissingCashbackStatus.setText("Pending");
                            }
                            txtUserMissingCashbackDateOfQuery.setText(list.get(i).getQueryDate());
                            txtUserMissingCashbackTransactionId.setText(list.get(i).getTransactionId());
                            txtUserMissingCashbackRetailerName.setText(list.get(i).getRetailerName());
                            txtUserMissingCashbackTransactionAmount.setText("Rs. " + list.get(i).getTransactionAmount());
                            txtUserMissingCashbackDateOfTransaction.setText(list.get(i).getTransactionDate());
                            tblLayout.addView(view);
                        }

                        String starting = String.valueOf(start + 1);
                        txtShowingResult.setText("Showing " + starting + " to " + end + " entries");

                    } else {
                        for (int i = start; i < list.size(); i++) {
                            View view = getLayoutInflater().inflate(R.layout.user_missing_cashback_table_row, null);
                            TextView txtUserMissingCashbackRetailerName = (TextView) view.findViewById(R.id.txtUserMissingCashbackRetailerName);
                            TextView txtUserMissingCashbackTransactionAmount = (TextView) view.findViewById(R.id.txtUserMissingCashbackTransactionAmount);
                            TextView txtUserMissingCashbackDateOfTransaction = (TextView) view.findViewById(R.id.txtUserMissingCashbackDateOfTransaction);
                            TextView txtUserMissingCashbackDateOfQuery = (TextView) view.findViewById(R.id.txtUserMissingCashbackDateOfQuery);
                            TextView txtUserMissingCashbackTransactionId = (TextView) view.findViewById(R.id.txtUserMissingCashbackTransactionId);
                            Button btnUserMissingCashbackStatus = (Button) view.findViewById(R.id.btnUserMissingCashbackStatus);

                            if (list.get(i).getStatus().equals("0")) {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                                btnUserMissingCashbackStatus.setText("Completed");
                            } else {
                                btnUserMissingCashbackStatus.setBackgroundColor(Color.parseColor("#449d44"));
                                btnUserMissingCashbackStatus.setText("Created");
                            }
                            txtUserMissingCashbackDateOfQuery.setText(list.get(i).getQueryDate());
                            txtUserMissingCashbackTransactionId.setText(list.get(i).getTransactionId());
                            txtUserMissingCashbackRetailerName.setText(list.get(i).getRetailerName());
                            txtUserMissingCashbackTransactionAmount.setText("Rs. " + list.get(i).getTransactionAmount());
                            txtUserMissingCashbackDateOfTransaction.setText(list.get(i).getTransactionDate());
                            tblLayout.addView(view);
                        }
                        String starting = String.valueOf(start + 1);
                        txtShowingResult.setText("Showing " + starting + " to " + list.size() + " entries");
                    }
                } else {
                    noRecordsTxt.setVisibility(View.VISIBLE);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void filterOption(int start, int end) {
        try {
            createPageNo();
            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void filterOptionByEditText(String text) {
        try {
            list = new ArrayList<MissingCashbackData>();
            for (int i = 0; i < missingCashBackDatas.size(); i++) {
                if (missingCashBackDatas.get(i).getRetailerName().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                        missingCashBackDatas.get(i).getStatus().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                        missingCashBackDatas.get(i).getTransactionDate().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                        missingCashBackDatas.get(i).getTransactionAmount().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                    list.add(missingCashBackDatas.get(i));
                }
            }
            createPageNo();
            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createPageNo() {
        try {
            linearPageNo.removeAllViews();

            int numOfPage = list.size() / rowCount;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            params.setMargins(5, 5, 5, 5);

            for (int i = 0; i < numOfPage; i++) {
                final int j = i;
                TextView textView = new TextView(MissingCashBack.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(i + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j + 1;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }

            if ((list.size() % rowCount) > 0) {
                final int j = numOfPage + 1;
                TextView textView = new TextView(MissingCashBack.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(numOfPage + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadPayments extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MissingCashBack.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(MissingCashBack.this, urlLoadMissingCashBack, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            missingCashBackDatas = new ArrayList<MissingCashbackData>();
            if (result.toString().trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        noRecordsTxt.setVisibility(View.GONE);
                        JSONArray array = object.getJSONArray("missing_cashback");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            MissingCashbackData data = new MissingCashbackData();
                            data.setRetailerName(object1.getString("retailer_name"));
                            data.setStatus(object1.getString("status"));
                            data.setTransactionAmount(object1.getString("transaction_amount"));
                            data.setTransactionDate(object1.getString("transaction_date"));
                            data.setTransactionId(object1.getString("transaction_id"));
                            data.setQueryDate(object1.getString("query_date"));
                            missingCashBackDatas.add(data);
                        }
                        list = new ArrayList<MissingCashbackData>();
                        list = missingCashBackDatas;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                        createPageNo();
                    } else {
                        noRecordsTxt.setVisibility(View.VISIBLE);
                        Toast.makeText(MissingCashBack.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("err", e.toString());
                }
            } else {
                Toast.makeText(MissingCashBack.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
            }
        }
    }


}
