package com.application.ads.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.LoginActivity;
import com.application.ads.R;
import com.application.ads.SalableCouponDetails;
import com.application.ads.data.CouponData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SalableCouponGridAdapter extends BaseAdapter {

    List<CouponData> couponlist;
    Context context;
    LayoutInflater inflater;
    SessionManager manager;

    public SalableCouponGridAdapter(Context context, List<CouponData> couponlist) {
        this.context = context;
        this.couponlist = couponlist;
        manager = new SessionManager(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return couponlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return couponlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.top_cashback_detail, null);
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.about_store);
        dialog.setTitle("About Store");
        TextView aboutstore = (TextView) dialog.findViewById(R.id.aboutstore);
        ImageView aboutstoreclose = (ImageView) dialog.findViewById(R.id.aboutstoreclose);

        aboutstoreclose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        final int m = position;
        aboutstore.setText(couponlist.get(position)
                .getStoredesc());
        Button btnTopCashBackViewStore = (Button) view
                .findViewById(R.id.btnTopCashBackViewStore);
        Button btnTopCashBackAboutStore = (Button) view
                .findViewById(R.id.btnTopCashBackAboutStore);

        btnTopCashBackAboutStore.setVisibility(View.VISIBLE);


        btnTopCashBackAboutStore.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
        btnTopCashBackViewStore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!manager.isLoggedIn()) {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.putExtra("store_name", couponlist.get(m).getStorelist().get(m)
                            .getStorename());
                    intent.putExtra("store_logo", couponlist.get(m).getStorelist().get(m)
                            .getStorelogo());
                    intent.putExtra("store_desc", couponlist.get(m)
                            .getStoredesc());
                    intent.putExtra("coup_title", couponlist.get(m).getTitle());
                    intent.putExtra("coup_desc", couponlist.get(m)
                            .getDescription());
                    intent.putExtra("coup_type", "salable");
                    intent.putExtra("coup_amt", couponlist.get(m).getPrice());
                    context.startActivity(intent);


                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context,
                            SalableCouponDetails.class);
                    intent.putExtra("store_name", couponlist.get(m).getStorelist().get(m)
                            .getStorename());
                    intent.putExtra("store_logo", couponlist.get(m).getStorelist().get(m)
                            .getStorelogo());
                    intent.putExtra("store_desc", couponlist.get(m)
                            .getStoredesc());
                    intent.putExtra("coup_title", couponlist.get(m).getTitle());
                    intent.putExtra("coup_desc", couponlist.get(m)
                            .getDescription());
                    intent.putExtra("coup_amt", couponlist.get(m).getPrice());
                    context.startActivity(intent);
                }

				/*
                 * Intent intent=new Intent(context,CouponList.class);
				 * intent.putExtra
				 * ("storeName",couponlist.get(position).getCouponId());
				 * context.startActivity(intent);
				 */
            }
        });
        btnTopCashBackViewStore.setText("Buy Now");
        TextView txtCashbackTypeLogo = (TextView) view
                .findViewById(R.id.txtCashbackTypeLogo);

        ImageView imgTopCashBackLogo = (ImageView) view
                .findViewById(R.id.imgTopCashBackLogo);
        TextView txtTopCashBackName = (TextView) view
                .findViewById(R.id.txtTopCashBackName);
        TextView txtTopCashBackPrice = (TextView) view
                .findViewById(R.id.txtTopCashBackPrice);
        TextView txtTopCashBackDescription = (TextView) view
                .findViewById(R.id.txtTopCashBackDescription);

        txtTopCashBackName.setText(couponlist.get(m).getStorelist().get(m)
                .getStorename());
        txtTopCashBackPrice
                .setText("Rs. " + couponlist.get(position).getPrice());
        txtTopCashBackDescription.setText(couponlist.get(position)
                .getDescription());
        Picasso.with(context).load(DBConnection.OFFLINE_CATEGORY_IMGURL
                + couponlist.get(position).getImgName()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgTopCashBackLogo);

        txtCashbackTypeLogo.setText("SALABLE");
        txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#ff0000"));
        return view;
    }

}
