package com.application.ads.data;

/**
 * Created by devel-73 on 6/7/17.
 */

public class CouponsData {

    private String couponId;
    private String offerName;
    private String offerType;
    private String title;
    private String affiliateDesc;
    private String affiliateId;
    private String affiliateImg;


    public String getAffiliateImg() {
        return affiliateImg;
    }

    public void setAffiliateImg(String affiliateImg) {
        this.affiliateImg = affiliateImg;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAffiliateDesc() {
        return affiliateDesc;
    }

    public void setAffiliateDesc(String affiliateDesc) {
        this.affiliateDesc = affiliateDesc;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }
}
