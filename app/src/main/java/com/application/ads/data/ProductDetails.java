package com.application.ads.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ProductDetails implements Parcelable {

    public static final Parcelable.Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {

        @Override
        public ProductDetails[] newArray(int size) {
            return new ProductDetails[size];
        }

        @Override
        public ProductDetails createFromParcel(Parcel source) {
            return new ProductDetails(source);
        }
    };
    private String pp_id;
    private String product_id;
    private String product_price;
    private String product_image;
    private String affliate_name;
    private String affliate_logo;
    private String mrp;
    private String parent_child_id;
    private String cashbackprice;
    private String rating;
    private String proWishListStatus;
    private String key_feature;
    private String product_name;
    private String store_id;
    private String prod_desc;
    private String product_url;
    private String brand_name;
    private String brand_image;
    private String tot_stores;
    private String product_parent;
    private String productCompAff;
    private String productCompPrice;
    private String productCompId;
    private String wegive;
    private String weget;
    private String reward_point;
    private List<Stores> storelist;
    private boolean selected;
    private String productSpecName;
    private String productSpecValue;
    private String productStoreId;
    private String productId;
    private String productAffiiateLogo;
    private String productAffiliatePrice;
    private String productPPId;
    private List<String> category_id;
    private List<String> category_per;
    private ArrayList<SpecificationData> specificationList;
    private ArrayList<StoresData> stores;
    private ArrayList<ReviewsData> reviews;
    private String productDailyPrice;
    private String productDailyDate;

    public ProductDetails(Parcel source) {
        setPp_id(source.readString());
        setProduct_price(source.readString());
        setRating(source.readString());
        setProduct_name(source.readString());
        setProduct_image(source.readString());
        setProduct_id(source.readString());
        setMrp(source.readString());
        setParent_child_id(source.readString());
        setKey_feature(source.readString());

    }

    public ProductDetails() {
        // TODO Auto-generated constructor stub
    }

    public String getProductDailyPrice() {
        return productDailyPrice;
    }

    public void setProductDailyPrice(String productDailyPrice) {
        this.productDailyPrice = productDailyPrice;
    }

    public String getProductDailyDate() {
        return productDailyDate;
    }

    public void setProductDailyDate(String productDailyDate) {
        this.productDailyDate = productDailyDate;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getProd_desc() {
        return prod_desc;
    }

    public void setProd_desc(String prod_desc) {
        this.prod_desc = prod_desc;
    }

    public String getProduct_url() {
        return product_url;
    }

    public void setProduct_url(String product_url) {
        this.product_url = product_url;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_image() {
        return brand_image;
    }

    public void setBrand_image(String brand_image) {
        this.brand_image = brand_image;
    }

    public String getTot_stores() {
        return tot_stores;
    }

    public void setTot_stores(String tot_stores) {
        this.tot_stores = tot_stores;
    }

    public String getAffliate_name() {
        return affliate_name;
    }

    public void setAffliate_name(String affliate_name) {
        this.affliate_name = affliate_name;
    }

    public String getReward_point() {
        return reward_point;
    }

    public void setReward_point(String reward_point) {
        this.reward_point = reward_point;
    }

    public String getAffliate_logo() {
        return affliate_logo;
    }

    public void setAffliate_logo(String affliate_logo) {
        this.affliate_logo = affliate_logo;
    }

    public String getPp_id() {
        return pp_id;
    }

    public void setPp_id(String pp_id) {
        this.pp_id = pp_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getKey_feature() {
        return key_feature;
    }

    public void setKey_feature(String key_feature) {
        this.key_feature = key_feature;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public List<Stores> getStorelist() {
        return storelist;
    }

    public void setStorelist(List<Stores> storelist) {
        this.storelist = storelist;
    }

    public List<String> getCategory_per() {
        return category_per;
    }

    public void setCategory_per(List<String> category_per) {
        this.category_per = category_per;
    }

    public List<String> getCategory_id() {
        return category_id;
    }

    public void setCategory_id(List<String> category_id) {
        this.category_id = category_id;
    }

    public String getParent_child_id() {
        return parent_child_id;
    }

    public void setParent_child_id(String parent_child_id) {
        this.parent_child_id = parent_child_id;
    }

    public String getProduct_parent() {
        return product_parent;
    }

    public void setProduct_parent(String product_parent) {
        this.product_parent = product_parent;
    }

    public ArrayList<SpecificationData> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(ArrayList<SpecificationData> specificationList) {
        this.specificationList = specificationList;
    }

    public ArrayList<StoresData> getStores() {
        return stores;
    }

    public void setStores(ArrayList<StoresData> stores) {
        this.stores = stores;
    }

    public ArrayList<ReviewsData> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<ReviewsData> reviews) {
        this.reviews = reviews;
    }

    public String getCashbackprice() {
        return cashbackprice;
    }

    public void setCashbackprice(String cashbackprice) {
        this.cashbackprice = cashbackprice;
    }

    public String getProWishListStatus() {
        return proWishListStatus;
    }

    public void setProWishListStatus(String proWishListStatus) {
        this.proWishListStatus = proWishListStatus;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getWeget() {
        return weget;
    }

    public void setWeget(String weget) {
        this.weget = weget;
    }

    public String getWegive() {
        return wegive;
    }

    public void setWegive(String wegive) {
        this.wegive = wegive;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getPp_id());
        dest.writeString(getProduct_price());
        dest.writeString(getRating());
        dest.writeString(getProduct_name());
        dest.writeString(getProduct_image());
        dest.writeString(getProduct_id());
        dest.writeString(getMrp());
        dest.writeString(getParent_child_id());
        dest.writeString(getKey_feature());
    }

    public String getProductCompAff() {
        return productCompAff;
    }

    public void setProductCompAff(String productCompAff) {
        this.productCompAff = productCompAff;
    }

    public String getProductCompPrice() {
        return productCompPrice;
    }

    public void setProductCompPrice(String productCompPrice) {
        this.productCompPrice = productCompPrice;
    }

    public String getProductCompId() {
        return productCompId;
    }

    public void setProductCompId(String productCompId) {
        this.productCompId = productCompId;
    }

    public String getProductSpecValue() {
        return productSpecValue;
    }

    public void setProductSpecValue(String productSpecValue) {
        this.productSpecValue = productSpecValue;
    }

    public String getProductSpecName() {
        return productSpecName;
    }

    public void setProductSpecName(String productSpecName) {
        this.productSpecName = productSpecName;
    }

    public String getProductAffiliatePrice() {
        return productAffiliatePrice;
    }

    public void setProductAffiliatePrice(String productAffiliatePrice) {
        this.productAffiliatePrice = productAffiliatePrice;
    }

    public String getProductAffiiateLogo() {
        return productAffiiateLogo;
    }

    public void setProductAffiiateLogo(String productAffiiateLogo) {
        this.productAffiiateLogo = productAffiiateLogo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductStoreId() {
        return productStoreId;
    }

    public void setProductStoreId(String productStoreId) {
        this.productStoreId = productStoreId;
    }

    public String getProductPPId() {
        return productPPId;
    }

    public void setProductPPId(String productPPId) {
        this.productPPId = productPPId;
    }


}
