package com.application.ads.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCategoryAdapter extends BaseAdapter {

    List<SubCategoryData> subcategorydata;
    Context context;
    LayoutInflater inflater;

    public SubCategoryAdapter(Context context,
                              List<SubCategoryData> subcategorydata) {
        this.subcategorydata = subcategorydata;
        this.context = context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return subcategorydata.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return subcategorydata.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_categories, null, false);

        TextView categoryname = (TextView) view.findViewById(R.id.categoryname);
        ImageView categoryimg = (ImageView) view.findViewById(R.id.categoryimg);

        categoryname.setText(subcategorydata.get(position).getSubcat_name());

        Picasso.with(context).load(DBConnection.CATEGORY_IMGURL + subcategorydata.get(position).getSubcat_img()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);

        return view;
    }
}
