package com.application.ads.data;

public class StoresData {

    private String storeId;
    private String affiliateName;
    private String affiliateLogo;
    private String productUrl;
    private String productPrice;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAffiliateName() {
        return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
        this.affiliateName = affiliateName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getAffiliateLogo() {
        return affiliateLogo;
    }

    public void setAffiliateLogo(String affiliateLogo) {
        this.affiliateLogo = affiliateLogo;
    }

}