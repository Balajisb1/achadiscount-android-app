package com.application.ads;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import info.androidramp.gearload.Loading;

public class OfflineHomeNavigation extends FragmentActivity {

    private static final int FILTER_ID = 0;
    private final int interval = 3000; // 3 Second
    List<String> couponlist;
    SharedPreferences preferences;
    SessionManager manager;
    int totalCount = 0;
    JSONParser parser;
    View view_Group;
    Loading loading;
    boolean drawerstatus = false;
    private DrawerLayout mDrawerLayout;
    private Fragment fragment = null;
    private ListView expListView;
    private CouponListAdapter listAdapter;
    private int lastExpandedPosition = -1;
    private String urlLoadMainCategories = DBConnection.BASEURL + "load_coupon_categories.php";
    private Handler handler = new Handler();
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    // Catch the events related to the drawer to arrange views according to this
    // action if necessary...
    private DrawerListener mDrawerListener = new DrawerListener() {

        @Override
        public void onDrawerStateChanged(int status) {

        }

        @Override
        public void onDrawerSlide(View view, float slideArg) {

        }

        @Override
        public void onDrawerOpened(View view) {
            getActionBar().setTitle(mDrawerTitle);
            // calling onPrepareOptionsMenu() to hide action bar icons
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            getActionBar().setTitle("");
            // calling onPrepareOptionsMenu() to show action bar icons
            invalidateOptionsMenu();
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        parser = new JSONParser();
        getActionBar().setTitle("Offline Discounts Sales");
        setContentView(R.layout.activity_coupon_navigation);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        ImageView homedrawer = (ImageView) toolbarview.findViewById(R.id.homedrawer);
        homedrawer.setVisibility(View.VISIBLE);
        homedrawer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerstatus) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    drawerstatus = true;
                } else {
                    drawerstatus = false;
                    mDrawerLayout.closeDrawers();
                }

            }
        });

        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Discount Sales");
        getActionBar().setCustomView(toolbarview);

        mTitle = mDrawerTitle = getTitle();
        fragment = new OfflineHome();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        setUpDrawer();
        manager = new SessionManager(OfflineHomeNavigation.this);
        getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
        mDrawerLayout.closeDrawer(expListView);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.nav_drawer, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for
                R.string.app_name // nav drawer close - description for
        ) {
            @Override
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("Offline Discounts Sales");
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Offline Discounts Sales");
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        makeActionOverflowMenuShown();


        loading = (Loading) findViewById(R.id.loading);

        loading.Start();

        Runnable runnable = new Runnable() {
            public void run() {

//
                loading.Cancel();

            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

				/*for(int i=0;i<OfflineFilterAdapter.offlinefilterids.size();i++){
					if(i==0){
						this.filterids=OfflineFilterAdapter.offlinefilterids.get(i);
					}
					else{
						this.filterids+=","+OfflineFilterAdapter.offlinefilterids.get(i);
					}
				}

				//new ExecuteLoadProductDetails(getIntent().getStringExtra("parent_child_id"),0,limitposition,data.getStringExtra("filter_ids")).execute();
			}*/


    }

    private void makeActionOverflowMenuShown() {
        // devices with hardware menu button (e.g. Samsung ) don't show action
        // overflow menu
        try {
            final ViewConfiguration config = ViewConfiguration.get(this);
            final Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (final Exception e) {
            Log.e("", e.getLocalizedMessage());
        }
    }

    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			/*
			 * mDrawerLayout.setScrimColor(getResources().getColor(
			 * android.R.color.transparent));
			 */
        mDrawerLayout.setDrawerListener(mDrawerListener);
        expListView = (ListView) findViewById(R.id.coupon_slidermenu);
        //prepareListData();
        couponlist = new ArrayList<String>();
        couponlist.add("Active Deals");
        couponlist.add("Upcoming");
        couponlist.add("Expired");
        couponlist.add("Brands");
        listAdapter = new CouponListAdapter(OfflineHomeNavigation.this, couponlist);
        expListView.setAdapter(listAdapter);

        // expandable list view click listener

        expListView.setOnItemClickListener(new SlideMenuClickListener());
			/*
			expListView.setOnChildClickListener(new OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					// setbackground color for list that is selected in child group


					Toast.makeText(CouponNavigation.this,categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_name() , 5000).show();

					getFragmentManager().beginTransaction()
							.replace(R.id.frame_container, fragment).commit();
					expListView.setItemChecked(childPosition, true);
					mDrawerLayout.closeDrawer(expListView);
					Intent intent=new Intent(CouponNavigation.this, ProductDetailsActivity.class);
					intent.putExtra("prod_parent_name", categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_name());
					intent.putExtra("parent_child_id", categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_id());
				//	Toast.makeText(SubCategoryActivity.this, subcategorydata.get(position).getSubcat_id(), 5000).show();
					startActivity(intent);
					return false;
				}
			});
			*/


			/*expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			    @Override
			    public void onGroupExpand(int groupPosition) {
			            if (lastExpandedPosition != -1
			                    && groupPosition != lastExpandedPosition) {
			            	expListView.collapseGroup(lastExpandedPosition);
			            }
			            lastExpandedPosition = groupPosition;
			    }
			});*/
    }

    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        mDrawerLayout.closeDrawer(expListView);
        switch (position) {

            case 1:
                Intent intent = new Intent(OfflineHomeNavigation.this, OfflineUpcomingOffer.class);
                startActivity(intent);
                break;
            case 2:
                Intent intent1 = new Intent(OfflineHomeNavigation.this, OfflineExpiredOffer.class);
                startActivity(intent1);
                break;
            case 3:

                Intent intent2 = new Intent(OfflineHomeNavigation.this, OfflineBrandsHome.class);
                startActivity(intent2);
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerLayout.closeDrawer(expListView);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), ProductDetailsActivity.class);
                intent2.putExtra("parent_child_id", preferences.getString(SessionManager.KEY_COMP_ID, "20003"));
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                if (mDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
		/*private void prepareListData() {
			new  LoadAllCaegory().execute();
		}
		
	class LoadAllCaegory extends AsyncTask<String,String,String>{
			
			ProgressDialog dialog;
			JSONObject object;
			
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog=new ProgressDialog(OfflineHomeNavigation.this);
				dialog.setMessage("Loading..");
				dialog.show();
			}

			@Override
			protected String doInBackground(String... params) {
				List<NameValuePair> param=new ArrayList<NameValuePair>();
				object=parser.makeHttpRequest(urlLoadMainCategories	,"GET",param);
				if(object!=null){
					return object.toString();
				}else{
					return "";	
				}
			}
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				dialog.dismiss();
				
				couponlist = new ArrayList<CouponData>();
				
				if(result.trim().length()>0){
					try {
						object=new JSONObject(result);
						if(object.getInt("success")==1){
							JSONArray array=object.getJSONArray("category");
							for(int i=0;i<array.length();i++){
								JSONObject object1=array.getJSONObject(i);
								CouponData data=new CouponData();
								data.setCategoryName(object1.getString("cate_name"));
								data.setCategoryId(object1.getString("cate_id"));
								couponlist.add(data);
							}
							listAdapter = new CouponListAdapter(OfflineHomeNavigation.this, couponlist);
							expListView.setAdapter(listAdapter);
						
						}else{
							Toast.makeText(OfflineHomeNavigation.this,"No Data Found..",Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				
				
			}*/

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    public class CouponListAdapter extends BaseAdapter {

        // child data in format of header title, child title
        LayoutInflater inflater;
        private Context _context;
        private List<String> couponlist; // header titles

        public CouponListAdapter(Context context,
                                 List<String> couponlist) {
            this._context = context;
            this.couponlist = couponlist;

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return couponlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return couponlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.activity_coupon_category, null);
            TextView couponcategorytitle = (TextView) view.findViewById(R.id.couponcategorytitle);
            couponcategorytitle.setText(Html.fromHtml(couponlist.get(position)));

            return view;
        }
    }
}
