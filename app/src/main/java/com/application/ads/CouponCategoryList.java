package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.application.ads.adapter.CouponsAdapter;
import com.application.ads.data.CouponsData;
import com.application.ads.data.ProductData;
import com.application.ads.data.StoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.ExpandableHeightGridView;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.analytics.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CouponCategoryList extends Activity {


    //    final String urlLoadTopCoupons = DBConnection.BASEURL + "load_category_wise_coupons.php";
    final String urlLoadTopCoupons = DBConnection.BASEURL + "coupons";

    JSONObject object;
    JSONParser parser;
    ImageButton searchbtn;
    Button btnTopStores, btnTopCashBackOffers, btnSalableCoupon, btnAllCoupons;

    List<ProductData> productList;
    Tracker t;
    //CouponGridAdapter adapter;
    private CouponsAdapter adapter;
    ExpandableHeightGridView coupongrid;
    List<StoreData> storesList;
    int rowWiseCount = 2;

    Button btnTopCouponAll, btnTopCouponA, btnTopCouponB, btnTopCouponC,
            btnTopCouponD, btnTopCouponE, btnTopCouponF, btnTopCouponG,
            btnTopCouponH, btnTopCouponI, btnTopCouponJ, btnTopCouponK,
            btnTopCouponL, btnTopCouponM, btnTopCouponN, btnTopCouponO,
            btnTopCouponP, btnTopCouponQ, btnTopCouponR, btnTopCouponS,
            btnTopCouponT, btnTopCouponU, btnTopCouponV, btnTopCouponW,
            btnTopCouponX, btnTopCouponY, btnTopCouponZ;
    int numOfCount = 2;

    AutoCompleteTextView searchedittext;
    SharedPreferences preferences;
    SessionManager manager;
    List<String> names;
    TextView toolbartitle;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView nocouponstext;
    private List<CouponsData> couponsDatas;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_grid);
        nocouponstext = (TextView) findViewById(R.id.nocouponstext);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Coupons");
        mContext=CouponCategoryList.this;
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        coupongrid = (ExpandableHeightGridView) findViewById(R.id.coupongrid);
        storesList = new ArrayList<StoreData>();
        couponsDatas = new ArrayList<>();
        adapter = new CouponsAdapter(this, couponsDatas);
        coupongrid.setAdapter(adapter);
        btnTopStores = (Button) findViewById(R.id.btnTopStores);
        btnTopCashBackOffers = (Button) findViewById(R.id.btnTopCashBackOffers);

        btnSalableCoupon = (Button) findViewById(R.id.btnSalableCoupon);
        btnAllCoupons = (Button) findViewById(R.id.btnAllCoupons);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        btnSalableCoupon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponCategoryList.this,
                        SalableCoupons.class);
                startActivity(intent);

            }
        });
        btnTopCashBackOffers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponCategoryList.this,
                        TopCashbackOffers.class);
                startActivity(intent);

            }
        });
        btnAllCoupons.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponCategoryList.this,
                        CouponNavigation.class);
                startActivity(intent);

            }
        });
        btnTopStores.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponCategoryList.this,
                        TopStoresList.class);
                startActivity(intent);
            }
        });

        parser = new JSONParser();


        storesList = new ArrayList<StoreData>();

        manager = new SessionManager(CouponCategoryList.this);
        preferences = CouponCategoryList.this.getSharedPreferences(
                SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        btnTopCouponAll = (Button) findViewById(R.id.btnTopCouponAll);
       /* btnTopCouponAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadAllCouponList("").execute();
            *//*	List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("cate_id", getIntent()
						.getStringExtra("cate_id")));
				param.add(new BasicNameValuePair("store_name", startLetter));
				Log.e("parm", param.toString());
				object = parser.makeHttpRequest(urlLoadTopCoupons, "GET", param);*//*

			*//*	String url=urlLoadTopCoupons+"?startLetter=";
                volleyJsonObjectRequest(url);
*//*

                toolbartitle.setText("Coupons/All");
            }
        });*/

        btnTopCouponA = (Button) findViewById(R.id.btnTopCouponA);
       /* btnTopCouponA.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("A").execute();
				*//*String url=urlLoadTopCoupons+"?startLetter=A";
				volleyJsonObjectRequest(url);
				*//*
                toolbartitle.setText("Coupons/A");
            }
        });*/
        btnTopCouponB = (Button) findViewById(R.id.btnTopCouponB);
        /*btnTopCouponB.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("B").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=B";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/B");
            }
        });
*/
        btnTopCouponC = (Button) findViewById(R.id.btnTopCouponC);
  /*      btnTopCouponC.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("C").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=C";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/C");
            }
        });
  */      btnTopCouponD = (Button) findViewById(R.id.btnTopCouponD);
      /*  btnTopCouponD.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("D").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=D";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/D");
            }
        });*/
        btnTopCouponE = (Button) findViewById(R.id.btnTopCouponE);
        /*btnTopCouponE.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("E").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=E";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/E");
            }
        });*/
        btnTopCouponF = (Button) findViewById(R.id.btnTopCouponF);
      /*  btnTopCouponF.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("F").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=F";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/F");
            }
        });*/
        btnTopCouponG = (Button) findViewById(R.id.btnTopCouponG);
      /*  btnTopCouponG.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("G").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=G";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/G");
            }
        });*/
        btnTopCouponH = (Button) findViewById(R.id.btnTopCouponH);
      /*  btnTopCouponH.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("H").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=H";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/H");
            }
        });*/
        btnTopCouponI = (Button) findViewById(R.id.btnTopCouponI);
        /*btnTopCouponI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("I").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=I";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/I");
            }
        });*/
        btnTopCouponJ = (Button) findViewById(R.id.btnTopCouponJ);
      /*  btnTopCouponJ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("J").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=J";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/J");
            }
        });*/
        btnTopCouponK = (Button) findViewById(R.id.btnTopCouponK);
       /* btnTopCouponK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("K").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=K";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/K");
            }
        });*/
        btnTopCouponL = (Button) findViewById(R.id.btnTopCouponL);
      /*  btnTopCouponL.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("L").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=L";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/L");
            }
        });*/
        btnTopCouponM = (Button) findViewById(R.id.btnTopCouponM);
      /*  btnTopCouponM.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("M").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=M";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/M");
            }
        });*/
        btnTopCouponN = (Button) findViewById(R.id.btnTopCouponN);
       /* btnTopCouponN.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("N").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=N";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/N");
            }
        });*/
        btnTopCouponO = (Button) findViewById(R.id.btnTopCouponO);
      /*  btnTopCouponO.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("O").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=O";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/O");
            }
        });*/
        btnTopCouponP = (Button) findViewById(R.id.btnTopCouponP);
       /* btnTopCouponP.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("P").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=P";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/P");
            }
        });*/
        btnTopCouponQ = (Button) findViewById(R.id.btnTopCouponQ);
       /* btnTopCouponQ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Q").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=Q";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/Q");
            }
        });*/
        btnTopCouponR = (Button) findViewById(R.id.btnTopCouponR);
      /*  btnTopCouponR.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("R").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=R";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/R");
            }
        });*/
        btnTopCouponS = (Button) findViewById(R.id.btnTopCouponS);
        /*btnTopCouponS.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("S").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=S";
				volleyJsonObjectRequest(url);
			*//*
                toolbartitle.setText("Coupons/S");
            }

        });*/
        btnTopCouponT = (Button) findViewById(R.id.btnTopCouponT);
        /*btnTopCouponT.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("T").execute();
			*//*	String url=urlLoadTopCoupons+"?startLetter=T";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/T");
            }
        });*/
        btnTopCouponU = (Button) findViewById(R.id.btnTopCouponU);
        /*btnTopCouponU.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("U").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=U";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/U");
            }
        });*/
        btnTopCouponV = (Button) findViewById(R.id.btnTopCouponV);
        /*btnTopCouponV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("V").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=V";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/V");
            }
        });*/
        btnTopCouponW = (Button) findViewById(R.id.btnTopCouponW);
        /*btnTopCouponW.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("W").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=W";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/W");
            }
        });*/
        btnTopCouponX = (Button) findViewById(R.id.btnTopCouponX);
        /*btnTopCouponX.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("X").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=X";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/X");
            }
        });*/
        btnTopCouponY = (Button) findViewById(R.id.btnTopCouponY);
        /*btnTopCouponY.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Y").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=Y";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/Y");
            }
        });*/
        btnTopCouponZ = (Button) findViewById(R.id.btnTopCouponZ);
        /*btnTopCouponZ.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadAllCouponList("Z").execute();
		*//*		String url=urlLoadTopCoupons+"?startLetter=Z";
				volleyJsonObjectRequest(url);
		*//*
                toolbartitle.setText("Coupons/Z");
            }
        });*/


        btnTopCouponAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter=new CouponsAdapter(mContext,couponsDatas);
                coupongrid.setAdapter(adapter);
            }
        });
        btnTopCouponA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("A")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("B")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("C")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("D")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("E")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("F")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("G")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("H")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("I")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("J")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("K")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("L")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("M")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("N")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("O")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("P")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Q")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("R")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("S")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("T")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("U")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("W")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("X")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        btnTopCouponY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Y")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });
        btnTopCouponZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CouponsData> tempList=new ArrayList<>();
                for(CouponsData data:couponsDatas){
                    if(data.getOfferName().trim().startsWith("Z")){
                        tempList.add(data);
                    }
                }
                if(tempList.size()>0) {
                    nocouponstext.setVisibility(View.GONE);
                    adapter = new CouponsAdapter(mContext, tempList);
                    coupongrid.setAdapter(adapter);
                    coupongrid.setVisibility(View.VISIBLE);
                }else{
                    nocouponstext.setVisibility(View.VISIBLE);
                    coupongrid.setVisibility(View.GONE);
                }
            }
        });

        coupongrid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
              /*  start = firstVisibleItem;
                count = visibleItemCount;
                totalcount = totalItemCount;*/
                int topRowVerticalPosition = (coupongrid == null || coupongrid.getChildCount() == 0) ? 0 : coupongrid.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                /*
				 * Log.e("firstVisibleItem", firstVisibleItem+"");
				 * Log.e("visibleItemCount", visibleItemCount+"");
				 * Log.e("totalItemCount", totalItemCount+"");
				 */
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                // Log.e("totalItemCount", totalcount+"");
            }

        });
        mSwipeRefreshLayout.setEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                new LoadAllCouponList("").execute();
/*
				String url=urlLoadTopCoupons+"?startLetter=";
				volleyJsonObjectRequest(url);
*/
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        new LoadAllCouponList("").execute();
    }

    /*public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    storesList = new ArrayList<StoreData>();
                    storesList.clear();

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {

                            JSONArray array = jsonObject.getJSONArray("coupon_list");

                            for (int m = 0; m < array.length(); m++) {
                                StoreData storeData = new StoreData();
                                JSONObject object3 = array.getJSONObject(m);
                                CouponData data = new CouponData();
                                data.setOfferName(object3.getString("offer_name"));

                                data.setTitle(object3.getString("title"));
                                data.setDescription(object3
                                        .getString("description"));
                                data.setOfferPage(object3.getString("offer_page"));
                                data.setCode(object3.getString("code"));
                                data.setExclusive(object3.getString("exclusive"));
                                data.setFeatured(object3.getString("featured"));
                                data.setImgName(object3.getString("affiliate_logo"));
                                data.setRemainingDays(object3
                                        .getString("remaining_days"));
                                storeData.setCounponData(data);
                                storeData.setType("cate");
                                storesList.add(storeData);

                                Log.e("sal", storesList.size() + "");
                                if (storesList.size() == 0) {
                                    nocouponstext.setVisibility(View.VISIBLE);
                                } else {
                                    nocouponstext.setVisibility(View.GONE);
                                }
                            }

                        } else {
                            Toast.makeText(CouponCategoryList.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                        adapter = new CouponGridAdapter(
                                CouponCategoryList.this, storesList);
                        coupongrid.setAdapter(adapter);
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadAllCouponList extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        String startLetter;

        public LoadAllCouponList(String startLetter) {
            this.startLetter = startLetter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CouponCategoryList.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("cate_url", getIntent()
                    .getStringExtra("cate_url")));
            Log.e("parm", param.toString());
            object = parser.makeHttpRequest(CouponCategoryList.this, urlLoadTopCoupons, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            couponsDatas.clear();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONObject object = new JSONObject(result);
                        JSONArray array = object.getJSONArray("coupons");
                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                JSONObject object2 = object1.getJSONObject("coupons");
                                JSONObject object3 = object1.getJSONObject("store_details");
                                CouponsData data = new CouponsData();
                                data.setCouponId(object2.getString("coupon_id"));
                                data.setTitle(object2.getString("title"));
                                data.setOfferName(object2.getString("offer_name"));
                                data.setOfferType(object2.getString("type"));
                                data.setAffiliateId(object3.getString("affiliate_id"));
                                data.setAffiliateDesc(object3.getString("affiliate_desc"));
                                data.setAffiliateImg(object3.getString("affiliate_logo"));
                                couponsDatas.add(data);
                            }
                        } else {
                            nocouponstext.setVisibility(View.VISIBLE);
                        }
                        adapter.notifyDataSetChanged();


                    } else {
                        nocouponstext.setVisibility(View.VISIBLE);
                    }
                   } catch (JSONException e) {
                    Log.e("err", e + "");
                }
            }
            dialog.dismiss();
        }
    }

}
