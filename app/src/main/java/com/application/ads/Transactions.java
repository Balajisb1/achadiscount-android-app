package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.AvailableEarningsData;
import com.application.ads.data.TransactionData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Transactions extends Activity {

    JSONParser parser;
    SessionManager manager;
    List<TransactionData> transactionDatas;
    TableLayout tblLayout;
    Spinner spnrUserTransactionCount;
    List<TransactionData> list;
    EditText edtUserTransactionFilter;
    int rowCount = 2;
    int currentPageNo = 1;
    LinearLayout linearPageNo;
    TextView txtPrevious;
    TextView noRecordsTxt;
    TextView txtNext, txtShowingResult;
    SharedPreferences preferences;
    AvailableEarningsData availableEarningsData;
    private String urlLoadTransaction = DBConnection.BASEURL + "transactionslist.php";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_transaction);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("My Transactions");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        /*getActionBar().setTitle("My Transactions");*/
        getActionBar().setTitle("");
        tblLayout = (TableLayout) findViewById(R.id.tblLayout);
        parser = new JSONParser();
        manager = new SessionManager(Transactions.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);
        noRecordsTxt = (TextView) findViewById(R.id.noRecordsTxt);

        linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

        txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo > 1) {
                    currentPageNo -= 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        txtNext = (TextView) findViewById(R.id.txtNext);
        txtNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo < (list.size() / rowCount)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                } else if (currentPageNo == (list.size() / rowCount) && (list.size() % rowCount > 0)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        edtUserTransactionFilter = (EditText) findViewById(R.id.edtUserTransactionFilter);
        edtUserTransactionFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterOptionByEditText(edtUserTransactionFilter.getText().toString().trim());
            }
        });

        spnrUserTransactionCount = (Spinner) findViewById(R.id.spnrUserTransactionCount);
        spnrUserTransactionCount.setAdapter(new ArrayAdapter<String>(Transactions.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrUserTransactionCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (transactionDatas != null) {
                    rowCount = Integer.parseInt(spnrUserTransactionCount.getSelectedItem().toString());
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
		
		
	/*	new LoadTransaction().execute();
		List<NameValuePair> param=new ArrayList<NameValuePair>();
		Log.e("User Id is==",preferences.getString(SessionManager.KEY_USERID,"0"));
		param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));*/

        String url = urlLoadTransaction + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0");

        volleyJsonObjectRequest(url);

    }


    public void volleyJsonObjectRequest(String url) {

        dialog = new ProgressDialog(Transactions.this);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    dialog.dismiss();
                    transactionDatas = new ArrayList<TransactionData>();
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
						/*availableEarningsData=new AvailableEarningsData();
						availableEarningsData.setAvailableEarnings(object.getString("available_balance"));
						availableEarningsData.setCashBackEarnings(object.getString("cashback_bal"));
						availableEarningsData.setPaidEarnings(object.getString("paid_amount"));
						availableEarningsData.setRefferalEarnings(object.getString("ref_amount"));
						availableEarningsData.setTotalEarnings(object.getString("total_earnings"));
						availableEarningsData.setWithdrawEarnings(object.getString("waiting_bal"));*/
                            if(jsonObject.has("transaction")) {
                                noRecordsTxt.setVisibility(View.GONE);
                                JSONArray array = jsonObject.getJSONArray("transaction");
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object1 = array.getJSONObject(i);
                                        TransactionData data = new TransactionData();
                                        data.setTransactionReason(object1.getString("transation_reason"));
                                        data.setTransactionAmount(object1.getString("transation_amount"));
                                        data.setTransactionType(object1.getString("type"));
                                        data.setTransactionDate(object1.getString("transation_date"));
                                        data.setTransactionStatus(object1.getString("transation_status"));
                                        transactionDatas.add(data);
                                    }
                                }
                            }else{
                                noRecordsTxt.setVisibility(View.VISIBLE);
                            }

						/*showEarningsList();*/
                            list = new ArrayList<TransactionData>();
                            list = transactionDatas;

                            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                            createPageNo();
                        } else {
                            dialog.dismiss();
                            noRecordsTxt.setVisibility(View.VISIBLE);
                            Toast.makeText(Transactions.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        dialog.dismiss();
                        Toast.makeText(Transactions.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void loadTableRows(int start, int end) {
        tblLayout.removeAllViews();
        if (list != null) {

            if(list.size()>0) {
                noRecordsTxt.setVisibility(View.GONE);
                if (list.size() >= end) {
                    for (int i = start; i < end; i++) {
                        View view = getLayoutInflater().inflate(R.layout.user_transaction_table_row, null);
                        TextView txtUserTransactionReason = (TextView) view.findViewById(R.id.txtUserTransactionReason);
                        TextView txtUserTransactionAmount = (TextView) view.findViewById(R.id.txtUserTransactionAmount);
                        TextView txtUserTransactionType = (TextView) view.findViewById(R.id.txtUserTransactionType);
                        TextView txtUserTransactionDate = (TextView) view.findViewById(R.id.txtUserTransactionDate);
                        Button btnUserTransactionStatus = (Button) view.findViewById(R.id.btnUserTransactionStatus);
                        txtUserTransactionReason.setText(list.get(i).getTransactionReason());
                        txtUserTransactionAmount.setText("Rs. " + list.get(i).getTransactionAmount());
                        txtUserTransactionType.setText(list.get(i).getTransactionType());
                        txtUserTransactionDate.setText(list.get(i).getTransactionDate());
                        if (list.get(i).getTransactionStatus().trim().toLowerCase().equals("paid")) {
                            btnUserTransactionStatus.setText("Success");
                        } else {
                            btnUserTransactionStatus.setText(list.get(i).getTransactionStatus());
                        }

                        tblLayout.addView(view);
                    }
                    String starting = String.valueOf(start + 1);
                    txtShowingResult.setText("Showing " + starting + " to " + end + " entries");
                } else {
                    for (int i = start; i < list.size(); i++) {
                        View view = getLayoutInflater().inflate(R.layout.user_transaction_table_row, null);
                        TextView txtUserTransactionReason = (TextView) view.findViewById(R.id.txtUserTransactionReason);
                        TextView txtUserTransactionAmount = (TextView) view.findViewById(R.id.txtUserTransactionAmount);
                        TextView txtUserTransactionType = (TextView) view.findViewById(R.id.txtUserTransactionType);
                        TextView txtUserTransactionDate = (TextView) view.findViewById(R.id.txtUserTransactionDate);
                        Button btnUserTransactionStatus = (Button) view.findViewById(R.id.btnUserTransactionStatus);
                        txtUserTransactionReason.setText(list.get(i).getTransactionReason());
                        txtUserTransactionAmount.setText("Rs. " + list.get(i).getTransactionAmount());
                        txtUserTransactionType.setText(list.get(i).getTransactionType());
                        txtUserTransactionDate.setText(list.get(i).getTransactionDate());
                        if (list.get(i).getTransactionStatus().trim().toLowerCase().equals("paid")) {
                            btnUserTransactionStatus.setText("Success");
                        } else {
                            btnUserTransactionStatus.setText(list.get(i).getTransactionStatus());
                        }
                        tblLayout.addView(view);
                    }
                    String starting = String.valueOf(start + 1);
                    txtShowingResult.setText("Showing " + starting + " to " + list.size() + " entries");
                }
            }
            else{
                noRecordsTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem=menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void filterOption(int start, int end) {
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void filterOptionByEditText(String text) {
        list = new ArrayList<TransactionData>();
        for (int i = 0; i < transactionDatas.size(); i++) {
            if (transactionDatas.get(i).getTransactionAmount().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    transactionDatas.get(i).getTransactionDate().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    transactionDatas.get(i).getTransactionStatus().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    transactionDatas.get(i).getTransactionType().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    transactionDatas.get(i).getTransactionReason().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                list.add(transactionDatas.get(i));
            }
        }
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void createPageNo() {
        linearPageNo.removeAllViews();

        if (list != null) {
            int numOfPage = list.size() / rowCount;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            params.setMargins(5, 5, 5, 5);

            for (int i = 0; i < numOfPage; i++) {
                final int j = i;
                TextView textView = new TextView(Transactions.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(i + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j + 1;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }

            if ((list.size() % rowCount) > 0) {
                final int j = numOfPage + 1;
                TextView textView = new TextView(Transactions.this);
                textView.setTextColor(Color.parseColor("#29BAB0"));
                textView.setText(String.valueOf(numOfPage + 1));
                textView.setLayoutParams(params);
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPageNo = j;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                    }
                });
                linearPageNo.addView(textView);
            }
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadTransaction extends AsyncTask<String, String, String> {

        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Transactions.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            Log.e("User Id is==", preferences.getString(SessionManager.KEY_USERID, "0"));
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            object = parser.makeHttpRequest(Transactions.this, urlLoadTransaction, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.toString().trim().length() > 0) {
                try {
                    transactionDatas = new ArrayList<TransactionData>();
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
						/*availableEarningsData=new AvailableEarningsData();
						availableEarningsData.setAvailableEarnings(object.getString("available_balance"));
						availableEarningsData.setCashBackEarnings(object.getString("cashback_bal"));
						availableEarningsData.setPaidEarnings(object.getString("paid_amount"));
						availableEarningsData.setRefferalEarnings(object.getString("ref_amount"));
						availableEarningsData.setTotalEarnings(object.getString("total_earnings"));
						availableEarningsData.setWithdrawEarnings(object.getString("waiting_bal"));*/

                        JSONArray array = object.getJSONArray("transaction");
                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                TransactionData data = new TransactionData();
                                data.setTransactionReason(object1.getString("transation_reason"));
                                data.setTransactionAmount(object1.getString("transation_amount"));
                                data.setTransactionType(object1.getString("type"));
                                data.setTransactionDate(object1.getString("transation_date"));
                                data.setTransactionStatus(object1.getString("transation_status"));
                                transactionDatas.add(data);
                            }
                        }

						/*showEarningsList();*/
                        list = new ArrayList<TransactionData>();
                        list = transactionDatas;
                        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                        createPageNo();
                    } else {
                        Toast.makeText(Transactions.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(Transactions.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
            }
        }
    }
	
	
	/*public void showEarningsList(){
		TextView txtTotalEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtCashBackEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtPaidEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtAvailableForPaymentEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtWithdrawEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtRefferalEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		TextView txtAvailableEarnings=(TextView)findViewById(R.id.txtTotalEarnings);
		
		if(availableEarningsData!=null){
			txtTotalEarnings.setText("Rs."+availableEarningsData.getTotalEarnings());
			txtCashBackEarnings.setText("Rs."+availableEarningsData.getCashBackEarnings());
			txtPaidEarnings.setText("Rs."+availableEarningsData.getPaidEarnings());
			txtAvailableForPaymentEarnings.setText("Rs."+availableEarningsData.getAvailableEarnings());
			txtWithdrawEarnings.setText("Rs."+availableEarningsData.getWithdrawEarnings());
			txtRefferalEarnings.setText("Rs."+availableEarningsData.getRefferalEarnings());
			txtAvailableEarnings.setText("Rs."+availableEarningsData.getAvailableEarnings());
		}
			
	}*/


}