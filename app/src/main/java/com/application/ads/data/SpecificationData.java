package com.application.ads.data;

import java.util.List;

public class SpecificationData {

    private String parentId;
    private String parentName;
    private String optionId;
    private String optionName;
    private String value;
    private List<SpecificationData> specificationDataslist;
    private boolean selected;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<SpecificationData> getSpecificationDataslist() {
        return specificationDataslist;
    }

    public void setSpecificationDataslist(List<SpecificationData> specificationDataslist) {
        this.specificationDataslist = specificationDataslist;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}