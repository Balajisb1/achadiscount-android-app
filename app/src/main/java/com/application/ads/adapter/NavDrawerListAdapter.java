package com.application.ads.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.SubCategoryActivity;
import com.application.ads.data.NavDrawerItem;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {


    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;


    public NavDrawerListAdapter(Context context) {
        context = this.context;
        navDrawerItems = new ArrayList<NavDrawerItem>();
    }

    public NavDrawerListAdapter(Context context,
                                ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position).getHeading();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (navDrawerItems.get(position).getSubHeading() == null) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drawer_list_header, null);
            }
            TextView txtTitle = (TextView) convertView.findViewById(R.id.slideheader);
            txtTitle.setTextSize(13);
            txtTitle.setTextColor(Color.parseColor("#ffffff"));
            txtTitle.setText(navDrawerItems.get(position).getHeading());
            return convertView;
        } else {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drawer_list_item, null);
            }
            convertView.setBackgroundColor(Color.parseColor("#ffffff"));
            TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
            txtTitle.setText(navDrawerItems.get(position).getSubHeading());
            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    intent.putExtra("sub_cat_id", String.valueOf(navDrawerItems.get(position).getSubHeadingId()).trim());
                    //		intent.putExtra("sub_cat_name",navDrawerItems.get(position).getSubHeading());
                    //		intent.putExtra("sub_cat_img",navDrawerItems.get(position).getSubHeadImg());
                    context.startActivity(intent);
                }
            });
            return convertView;
        }


    }
}
