package com.application.ads.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfflineBrandAdapter extends BaseAdapter {

    List<OfflineStoreData> offlineStoreDatas;
    Context context;

    public OfflineBrandAdapter(List<OfflineStoreData> offlineStoreDatas, Context context) {
        this.offlineStoreDatas = offlineStoreDatas;
        this.context = context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return offlineStoreDatas.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return offlineStoreDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_offline_store_list_details, null);
        ImageView categoryimg = (ImageView) view.findViewById(R.id.categoryimg);
        TextView categoryname = (TextView) view.findViewById(R.id.categoryname);

        categoryname.setText(offlineStoreDatas.get(position).getBrandname());
        Picasso.with(context).load(DBConnection.OFFLINE_EXTRAIMG + offlineStoreDatas.get(position).getImage()).placeholder(R.drawable.adslogo).resize(144, 0).into(categoryimg);

//		Picasso.with(context).load(DBConnection.OFFLINE_EXTRAIMG+offlineStoreDatas.get(position).getImage()).placeholder(R.drawable.adslogo)
//				.resize(350,350).into(categoryimg);

        return view;
    }

}
