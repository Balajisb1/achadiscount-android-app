package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddReviews extends Activity {

    EditText edtAddReview;
    RadioGroup rdoGroupAddReview;
    SessionManager manager;
    JSONParser parser;
    SharedPreferences preferences;
    String rating = "5";
    Button btnAddReviewSubmit;
    private String urlAddReviews = DBConnection.BASEURL + "add_review.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_review);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Add Reviews");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        edtAddReview = (EditText) findViewById(R.id.edtAddReview);
        rdoGroupAddReview = (RadioGroup) findViewById(R.id.rdoGroupAddReview);
        rdoGroupAddReview.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio0:
                        rating = "5";
                        break;
                    case R.id.radio1:
                        rating = "4";
                        break;
                    case R.id.radio2:
                        rating = "3";
                        break;
                    case R.id.radio3:
                        rating = "2";
                        break;
                    case R.id.radio4:
                        rating = "1";
                        break;
                    default:
                        rating = "5";
                        break;
                }
            }
        });
        btnAddReviewSubmit = (Button) findViewById(R.id.btnAddReviewSubmit);
        btnAddReviewSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtAddReview.getText().toString().trim().length() > 0) {
//					new ExecuteAddReviews().execute();


				/*	List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("store_id",getIntent().getStringExtra("storeId")));
					param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
					param.add(new BasicNameValuePair("comments",comments));
					param.add(new BasicNameValuePair("rating", rating));
					param.add(new BasicNameValuePair("status","0"));
					Log.e("param", param.toString());
					object = parser.makeHttpRequest(urlAddReviews, "GET", param);
					*/
                    String comments = edtAddReview.getText().toString().trim();
                    String url = urlAddReviews + "?store_id=" + getIntent().getStringExtra("storeId") + "&user_id=" + preferences.getString(SessionManager.KEY_USERID, "0")
                            + "&comments=" + comments + "&rating=" + rating + "&status=0";
                    volleyJsonObjectRequest(url);
                } else {
                    Toast.makeText(AddReviews.this, "Please give any comments..", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(AddReviews.this,
                                    "Review Added Successfully..", Toast.LENGTH_LONG)
                                    .show();
                            finish();
                        } else {
                            Toast.makeText(AddReviews.this,
                                    "Review Adding Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                        Toast.makeText(AddReviews.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteAddReviews extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        private String comments = edtAddReview.getText().toString().trim();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddReviews.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("store_id", getIntent().getStringExtra("storeId")));
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("comments", comments));
            param.add(new BasicNameValuePair("rating", rating));
            param.add(new BasicNameValuePair("status", "0"));
            Log.e("param", param.toString());
            object = parser.makeHttpRequest(AddReviews.this, urlAddReviews, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        Toast.makeText(AddReviews.this,
                                "Review Added Successfully..", Toast.LENGTH_LONG)
                                .show();
                        finish();
                    } else {
                        Toast.makeText(AddReviews.this,
                                "Review Adding Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(AddReviews.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }
}
