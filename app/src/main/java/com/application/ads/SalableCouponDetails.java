package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.application.ads.paymentgate.PayUMoneyActivity;
import com.squareup.picasso.Picasso;

public class SalableCouponDetails extends Activity {

    TextView storename, storedesc, couptitle, coupdesc, coupprice;
    ImageView storelogo;

    SessionManager manager;
    Button paynow;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salablecoupon);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Salable Coupons Details");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        manager = new SessionManager(SalableCouponDetails.this);
        paynow = (Button) findViewById(R.id.paynow);
        storename = (TextView) findViewById(R.id.storename);
        storedesc = (TextView) findViewById(R.id.storedesc);
        couptitle = (TextView) findViewById(R.id.couptitle);
        coupdesc = (TextView) findViewById(R.id.coupdesc);
        coupprice = (TextView) findViewById(R.id.coupprice);
        storelogo = (ImageView) findViewById(R.id.storelogo);

        storename.setText(getIntent().getStringExtra("store_name"));
        storedesc.setText(getIntent().getStringExtra("store_desc"));
        couptitle.setText(getIntent().getStringExtra("coup_title"));
        coupdesc.setText(getIntent().getStringExtra("coup_desc"));
        coupprice.setText("Rs. " + getIntent().getStringExtra("coup_amt"));

        Picasso.with(SalableCouponDetails.this).load(DBConnection.OFFLINE_IMGURL
                + getIntent().getStringExtra("store_logo")).placeholder(R.drawable.adslogo).resize(144, 0).into(storelogo);

        paynow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalableCouponDetails.this,
                        PayUMoneyActivity.class);
                intent.putExtra("amount", getIntent().getStringExtra("coup_amt"));
                intent.putExtra("title", getIntent().getStringExtra("coup_title"));
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
