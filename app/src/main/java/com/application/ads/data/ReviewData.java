package com.application.ads.data;

public class ReviewData {

    private String userId;
    private String userName;
    private String comments;
    private String rating;
    private String totalReviews;
    private String overallreview;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    public String getOverallreview() {
        return overallreview;
    }

    public void setOverallreview(String overallreview) {
        this.overallreview = overallreview;
    }

}