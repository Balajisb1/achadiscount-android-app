package com.application.ads.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ads.CouponList;
import com.application.ads.R;
import com.application.ads.data.ProductData;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TopCashbackAdapter extends BaseAdapter {

    List<ProductData> productList;
    Context c;
    LayoutInflater inflater;

    public TopCashbackAdapter(Context c, List<ProductData> productList) {
        // TODO Auto-generated constructor stub
        this.productList = productList;
        this.c = c;
        Log.e("SIze ", productList.size() + "");
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.top_cashback_detail,
                null);

        Button btnTopCashBackViewStore = (Button) view.findViewById(R.id.btnTopCashBackViewStore);
        btnTopCashBackViewStore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, CouponList.class);
                intent.putExtra("storeName", productList.get(position).getProName());
                c.startActivity(intent);
            }
        });

        TextView txtCashbackTypeLogo = (TextView) view
                .findViewById(R.id.txtCashbackTypeLogo);

        ImageView imgTopCashBackLogo = (ImageView) view
                .findViewById(R.id.imgTopCashBackLogo);

        TextView txtTopCashBackName = (TextView) view
                .findViewById(R.id.txtTopCashBackName);

        TextView txtTopCashBackDescription = (TextView) view
                .findViewById(R.id.txtTopCashBackDescription);

        txtTopCashBackName.setText(productList.get(position).getProName());

        String details;
        if (productList.get(position).getProType().equals("Flat")) {
            details = "Up to Rs." + productList.get(position).getProCashbackAmount() + " cashback ";
        } else {
            details = "Up to " + productList.get(position).getProCashbackAmount() + "% cashback ";
        }
        if (Integer.parseInt(productList.get(position).getProCoupons().trim()) > 0) {
            details = details + "& " + Integer.parseInt(productList.get(position).getProCoupons())
                    + " coupons";
        }
        details = details + " From All Discount Sale";
        txtTopCashBackDescription.setText(details);
        Picasso.with(c).load(productList.get(position).getProImagePath()).placeholder(R.drawable.adslogo).resize(144, 0).into(imgTopCashBackLogo);


        if (productList.get(position).getProCashbackType().equals("featured")) {
            txtCashbackTypeLogo.setText("FEATURE");
            txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#ED0C0C"));
        } else if (productList.get(position).getProCashbackType().equals("offers")) {
            txtCashbackTypeLogo.setText("OFFER");
            txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#125DDE"));
        } else {
            txtCashbackTypeLogo.setText("STORE OF THE WEEK");
            txtCashbackTypeLogo.setBackgroundColor(Color.parseColor("#a5d16c"));
        }

        return view;
    }

}
