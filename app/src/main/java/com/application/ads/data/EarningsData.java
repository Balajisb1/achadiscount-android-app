package com.application.ads.data;

public class EarningsData {

    private String pendingCashBack;
    private String confirmedCashBack;
    private String paidEarnings;
    private String availablePayment;
    private String withdrawApproval;
    private String totalEarnings;
    private String withdrawMinBal;

    public String getPendingCashBack() {
        return pendingCashBack;
    }

    public void setPendingCashBack(String pendingCashBack) {
        this.pendingCashBack = pendingCashBack;
    }

    public String getConfirmedCashBack() {
        return confirmedCashBack;
    }

    public void setConfirmedCashBack(String confirmedCashBack) {
        this.confirmedCashBack = confirmedCashBack;
    }

    public String getPaidEarnings() {
        return paidEarnings;
    }

    public void setPaidEarnings(String paidEarnings) {
        this.paidEarnings = paidEarnings;
    }

    public String getAvailablePayment() {
        return availablePayment;
    }

    public void setAvailablePayment(String availablePayment) {
        this.availablePayment = availablePayment;
    }

    public String getWithdrawApproval() {
        return withdrawApproval;
    }

    public void setWithdrawApproval(String withdrawApproval) {
        this.withdrawApproval = withdrawApproval;
    }

    public String getTotalEarnings() {
        return totalEarnings;
    }

    public void setTotalEarnings(String totalEarnings) {
        this.totalEarnings = totalEarnings;
    }

    public String getWithdrawMinBal() {
        return withdrawMinBal;
    }

    public void setWithdrawMinBal(String withdrawMinBal) {
        this.withdrawMinBal = withdrawMinBal;
    }

}
