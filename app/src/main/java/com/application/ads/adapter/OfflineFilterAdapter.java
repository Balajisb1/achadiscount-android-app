package com.application.ads.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.application.ads.R;
import com.application.ads.data.FilterData;

import java.util.ArrayList;
import java.util.List;

public class OfflineFilterAdapter extends BaseExpandableListAdapter {
    public static List<String> offlinefilterids;
    FilterData subcatdata;
    FilterData specdata;
    private List<String> _listDataHeader;
    private Activity _context;
    private List<FilterData> filterDatas;
    private List<FilterData> categoryDatas;
    private boolean isExpanded = false;

    public OfflineFilterAdapter(Activity context, List<String> listDataHeader,
                                List<FilterData> filterDatas, List<FilterData> categoryDatas) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this.filterDatas = filterDatas;
        if (offlinefilterids != null) {
            if (offlinefilterids.size() == 0) {
                offlinefilterids = new ArrayList<String>();
            }
        } else {
            offlinefilterids = new ArrayList<String>();
        }


        this.categoryDatas = categoryDatas;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        if (groupPosition == 1) {
            return filterDatas.get(childPosititon);
        } else {
            return categoryDatas.get(childPosititon);
        }

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


			/*if (convertView == null) {
                viewHolder = new ViewHolder();
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.activity_filter_child, null);

			viewHolder.txtListChild = (CheckBox)convertView.findViewById(R.id.filterchild);
			convertView.setTag(viewHolder);
            convertView.setTag(R.id.filterchild, viewHolder.txtListChild);

			}
			else {
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
			viewHolder.txtListChild.setText(subcatdata.getOptionName());
			viewHolder.txtListChild.setTag(this._listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition));
			*/
        //	Log.e("grouppos", groupPosition+"");
        if (groupPosition == 1) {
            subcatdata = (FilterData) filterDatas.get(childPosition);
        } else {
            subcatdata = (FilterData) categoryDatas.get(childPosition);
        }
        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = _context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.activity_filter_child, null);
            viewHolder = new ViewHolder();
            //viewHolder.text = (TextView) convertView.findViewById(R.id.label);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.filterchild);

            convertView.setTag(viewHolder);
            //     convertView.setTag(R.id.label, viewHolder.text);
            convertView.setTag(R.id.check, viewHolder.checkbox);

            /*viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    CheckBox cb = (CheckBox) buttonView;
                    specdata = (FilterData) cb.getTag();
                    if (groupPosition == 1) {
                        filterDatas.get(childPosition).setSelected(cb.isChecked());
                    } else {
                        categoryDatas.get(childPosition).setSelected(cb.isChecked());
                    }
                    if (cb.isChecked() == true) {
                        //Toast.makeText(_context, specdata.getBrandname(), 5000).show();
                        if (!offlinefilterids.contains(specdata.getBrandid())) {
                            offlinefilterids.add(specdata.getBrandid() + "");
                        }
                        // 		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                    } else {
                        if (offlinefilterids.contains(specdata.getBrandid())) {
                            offlinefilterids.remove(specdata.getBrandid() + "");
                        }
                        //		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                    }
                }
            });*/
        } else {
            viewHolder = (ViewHolder) convertView.getTag();


        }


        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                specdata = (FilterData) cb.getTag();
                if (groupPosition == 1) {
                    filterDatas.get(childPosition).setSelected(cb.isChecked());
                } else {
                    categoryDatas.get(childPosition).setSelected(cb.isChecked());
                }
                if (cb.isChecked() == true) {
                    //Toast.makeText(_context, specdata.getBrandname(), 5000).show();
                    if (!offlinefilterids.contains(specdata.getBrandid())) {
                        offlinefilterids.add(specdata.getBrandid() + "");
                    }
                    // 		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                } else {
                    if (offlinefilterids.contains(specdata.getBrandid())) {
                        offlinefilterids.remove(specdata.getBrandid() + "");
                    }
                    //		Toast.makeText(_context, _listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString()+cb.isChecked(),5000).show();
                }
                //Toast.makeText(_context, filterids.toString(), 5000).show();

            }
        });

/*
        viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
*/

        if (offlinefilterids != null) {
            for (String filterid : offlinefilterids) {
                // Log.e("filterid",filterid);
                //   Log.e("par text",subcatdata.getParentId().toString() + "_" +  _listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition).getOptionId());

                if (filterid.equalsIgnoreCase(subcatdata.getBrandid())) {
                    viewHolder.checkbox.setChecked(true);
                }
            }

            if (offlinefilterids.size() > 0) {

                if (offlinefilterids.contains(subcatdata.getBrandid().toString())) {
                    //subcatdata.setSelected(true);
                    if (groupPosition == 1) {
                        filterDatas.get(childPosition).setSelected(true);
                    } else {
                        categoryDatas.get(childPosition).setSelected(true);
                    }
                    //viewHolder.checkbox.setChecked(true);
                    Log.e("par text", subcatdata.getBrandid());
                }
            }
        }

        viewHolder.checkbox.setTag(subcatdata); // This line is important.
        viewHolder.checkbox.setText(subcatdata.getBrandname());
        viewHolder.checkbox.setChecked(subcatdata.isSelected());

		       /* if(filterids.size()>0){
                     viewHolder.checkbox.setChecked(_listDataHeader.get(groupPosition).getParentId().toString()+"_"+specdata.getOptionId().toString());
		        }

		        */

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (groupPosition == 1) {
            return (this.filterDatas.size());
        } else {
            return categoryDatas.size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        String specificationData = (String) _listDataHeader.get(groupPosition);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflator = _context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.activity_filter_parent, null);


            viewHolder.lblListHeader = (TextView) convertView
                    .findViewById(R.id.filterparent);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.lblListHeader.setTag(groupPosition);
        viewHolder.lblListHeader.setText(specificationData);
        TextView collapseparent = (TextView) convertView.findViewById(R.id.collapseparent);
        if (isExpanded) {
            isExpanded = true;
            collapseparent.setText("-");
        } else {
            collapseparent.setText("+");
        }
		    /*ViewHolder viewHolder = null;
	        if (convertView == null) {
	            LayoutInflater inflator = _context.getLayoutInflater();
	            convertView = inflator.inflate(R.layout.activity_filter_child, null);
	            viewHolder = new ViewHolder();
	            //viewHolder.text = (TextView) convertView.findViewById(R.id.label);
	            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.filterchild);
	            viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	                @Override
	                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	                	CheckBox cb=(CheckBox)buttonView;
	                	SpecificationData specdata=(SpecificationData)cb.getTag();
	                	specdata.setSelected(cb.isChecked());
	                	filterids.add(specdata.getOptionId().toString());
	                	Toast.makeText(_context, filterids.toString(), 5000).show();
	                }

	            });

	            convertView.setTag(viewHolder);
	       //     convertView.setTag(R.id.label, viewHolder.text);
	            convertView.setTag(R.id.check, viewHolder.checkbox);
	            } else {
	            viewHolder = (ViewHolder) convertView.getTag();
	        }
	        viewHolder.checkbox.setTag(_listDataHeader.get(groupPosition).getSpecificationDataslist().get(childPosition)); // This line is important.

	        viewHolder.checkbox.setText(subcatdata.getOptionName());
	        viewHolder.checkbox.setChecked(subcatdata.isSelected());
*/

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
        protected TextView lblListHeader;
        protected TextView collapseparent;
    }

}
