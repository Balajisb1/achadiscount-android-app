package com.application.ads;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.CategoryData;
import com.application.ads.data.MainCategoryData;
import com.application.ads.data.MyApplication;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NavigationActivity extends FragmentActivity implements
        ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    //GPSCurrentLocation gpsCurrentLocation;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int FILTER_ID = 0;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    List<SubCategoryData> subCategoryDatas;
    SharedPreferences preferences;
    //GPSTracker tracker;
    double latitude, longitude;
    SessionManager manager;
    boolean gpsStatus = false;
    ArrayList<MainCategoryData> mainCategories;
    int totalCount = 0;
    JSONParser parser;
    List<String> listDataHeader;
    List<CategoryData> categoryList;
    HashMap<String, List<SubCategoryData>> listDataChild;
    View view_Group;
    MyApplication myApplication;
    boolean drawerstatus = false;
    boolean doubleBackToExitPressedOnce = false;
    private DrawerLayout mDrawerLayout;
    private android.app.Fragment fragment = null;
    private ListView expListView;
    private ExpandableListAdapter listAdapter;
    private int lastExpandedPosition = -1;
    private LocationRequest mLocationRequest;
    private String urlLoadMainCategories = DBConnection.BASEURL
            + "load_main_categories.php";
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    // Catch the events related to the drawer to arrange views according to this
    // action if necessary...
    private DrawerListener mDrawerListener = new DrawerListener() {

        @Override
        public void onDrawerStateChanged(int status) {

        }

        @Override
        public void onDrawerSlide(View view, float slideArg) {

        }

        @Override
        public void onDrawerOpened(View view) {
            getActionBar().setTitle(mDrawerTitle);
            // calling onPrepareOptionsMenu() to hide action bar icons
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            getActionBar().setTitle("");
            // calling onPrepareOptionsMenu() to show action bar icons
            invalidateOptionsMenu();
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        createLocationRequest();
        buildGoogleApiClient();
        super.onCreate(arg0);
        getActionBar().setTitle("");
        parser = new JSONParser();
        setContentView(R.layout.activity_coupon_navigation);
        myApplication = MyApplication.getInstance();
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View view = getLayoutInflater().inflate(R.layout.activity_hometoolbar, null);
        getActionBar().setCustomView(view);
//        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); //show it
        ImageView homedrawer = (ImageView) view.findViewById(R.id.homedrawer);


        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);
        homedrawer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerstatus) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    drawerstatus = true;
                } else {
                    drawerstatus = false;
                    mDrawerLayout.closeDrawers();
                }

            }
        });

        //gpsCurrentLocation=new GPSCurrentLocation(NavigationActivity.this);


        // getActionBar().setLogo(R.drawable.adslogo);
        //getActionBar().setHomeButtonEnabled(true);
        /*
         * getActionBar().setDisplayHomeAsUpEnabled(true);
		 * getActionBar().setHomeButtonEnabled(true);
		 */
        mTitle = mDrawerTitle = getTitle();
        fragment = new HomeFragment();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        setUpDrawer();
        manager = new SessionManager(NavigationActivity.this);
        getFragmentManager().beginTransaction()
                .replace(R.id.frame_container, fragment).commit();
        mDrawerLayout.closeDrawer(expListView);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.nav_drawer, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for
                R.string.app_name // nav drawer close - description for
        ) {
            @Override
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("");
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("");
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        makeActionOverflowMenuShown();


    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    private void makeActionOverflowMenuShown() {
        // devices with hardware menu button (e.g. Samsung ) don't show action
        // overflow menu
        try {
            final ViewConfiguration config = ViewConfiguration.get(this);
            final Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (final Exception e) {
            Log.e("", e.getLocalizedMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        invalidateOptionsMenu();
        if (gpsStatus) {
            mGoogleApiClient.connect();
        }
    }

    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*
         * mDrawerLayout.setScrimColor(getResources().getColor(
		 * android.R.color.transparent));
		 */
        mDrawerLayout.setDrawerListener(mDrawerListener);
        expListView = (ListView) findViewById(R.id.coupon_slidermenu);
        prepareListData();

        expListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                mDrawerLayout.closeDrawers();
                if (position == 0) {
                    Intent intent = new Intent(NavigationActivity.this,
                            OfflineHomeNavigation.class);
                    startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(NavigationActivity.this,
                            OnlineHomeNavigation.class);
                    startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(NavigationActivity.this,
                            CouponNavigation.class);
                    startActivity(intent);
                } else if (position == 3) {
                    if (manager.checkLogin()) {
                        Intent intent5 = new Intent(NavigationActivity.this,
                                AccountTitle.class);
                        startActivity(intent5);
                    }
                } else if (position == 4) {
                    Intent intent5 = new Intent(NavigationActivity.this,
                            AboutUs.class);
                    startActivity(intent5);
                } else if (position == 5) {
                    Intent intent5 = new Intent(NavigationActivity.this,
                            FAQ.class);
                    startActivity(intent5);
                } else if (position == 6) {
                    Intent intent5 = new Intent(NavigationActivity.this,
                            TermsandConditions.class);
                    startActivity(intent5);
                } else if (position == 7) {
                    Intent intent5 = new Intent(NavigationActivity.this,
                            PrivacyPolicy.class);
                    startActivity(intent5);
                } else if (position == 8) {
                    if (manager.checkLogin()) {
                        Intent intent5 = new Intent(NavigationActivity.this,
                                AddReferalActivity.class);
                        startActivity(intent5);
                    }
                }
            }
        });

        // expandable list view click listener

		/*
         * expListView.setOnChildClickListener(new OnChildClickListener() {
		 *
		 * @Override public boolean onChildClick(ExpandableListView parent, View
		 * v, int groupPosition, int childPosition, long id) { // setbackground
		 * color for list that is selected in child group
		 *
		 *
		 * //
		 * Toast.makeText(NavigationActivity.this,categoryList.get(groupPosition
		 * ).getSubcatlist().get(childPosition).getSubcat_name() , 5000).show();
		 *
		 * getFragmentManager().beginTransaction()
		 * .replace(R.id.frame_container, fragment).commit();
		 * expListView.setItemChecked(childPosition, true);
		 * mDrawerLayout.closeDrawer(expListView); Intent intent=new
		 * Intent(NavigationActivity.this, ProductDetailsActivity.class);
		 * intent.putExtra("prod_parent_name",
		 * categoryList.get(groupPosition).getSubcatlist
		 * ().get(childPosition).getSubcat_name());
		 * intent.putExtra("parent_child_id",
		 * categoryList.get(groupPosition).getSubcatlist
		 * ().get(childPosition).getSubcat_id()); //
		 * Toast.makeText(SubCategoryActivity.this,
		 * subcategorydata.get(position).getSubcat_id(), 5000).show();
		 * startActivity(intent); return false; } });
		 *
		 *
		 *
		 * expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
		 *
		 * @Override public void onGroupExpand(int groupPosition) { if
		 * (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition)
		 * { expListView.collapseGroup(lastExpandedPosition); }
		 * lastExpandedPosition = groupPosition; } });
		 */

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent2 = new Intent(NavigationActivity.this,
                        NavigationActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.action_compare:
                Intent intent = new Intent(NavigationActivity.this,
                        ProductDetailsActivity.class);
                intent.putExtra("parent_child_id",
                        preferences.getString(SessionManager.KEY_COMP_ID, "20003"));
                startActivity(intent);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(NavigationActivity.this,
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.action_coupons:
                Intent intent1 = new Intent(NavigationActivity.this,
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;

            case R.id.myaccount:
                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(NavigationActivity.this,
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                if (mDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();

        listDataHeader.add("Offline Discount sale Around You");
        listDataHeader.add("Online/Offline Price Comparision with Cashback");
        listDataHeader.add("Coupon Discounts");
        listDataHeader.add("Your Account");
        listDataHeader.add("About Us");
        listDataHeader.add("FAQ");
        listDataHeader.add("Terms and Conditions");
        listDataHeader.add("Privacy Policy");
        listDataHeader.add("Refer and Earn");
        NavListAdapter adapter = new NavListAdapter(NavigationActivity.this,
                listDataHeader);
        expListView.setAdapter(adapter);

        // listDataChild = new HashMap<String, List<String>>();

        // new LoadAllCaegory().execute();
		/*
		 * String[] array = getResources()
		 * .getStringArray(R.array.nav_drawer_items); listDataHeader =
		 * Arrays.asList(array);
		 *
		 * // Adding child data // dash board List<String> dashboard = new
		 * ArrayList<String>(); String[] dash =
		 * getResources().getStringArray(R.array.dash_board); dashboard =
		 * Arrays.asList(dash);
		 *
		 * // before you file List<String> l1 = new ArrayList<String>();
		 * String[] before = getResources()
		 * .getStringArray(R.array.before_you_file); l1 = Arrays.asList(before);
		 *
		 * // profile List<String> l2 = new ArrayList<String>(); String[] myproe
		 * = getResources().getStringArray(R.array.my_profile); l2 =
		 * Arrays.asList(myproe);
		 *
		 * // income slip List<String> l3 = new ArrayList<String>(); String[]
		 * inco = getResources().getStringArray(R.array.income_slip); l3 =
		 * Arrays.asList(inco);
		 *
		 * // federal deduction List<String> l4 = new ArrayList<String>();
		 * String[] fed =
		 * getResources().getStringArray(R.array.federal_deduction); l4 =
		 * Arrays.asList(fed);
		 *
		 * // provincial credits List<String> l5 = new ArrayList<String>();
		 * String[] provi = getResources().getStringArray(
		 * R.array.provincial_credit); l5 = Arrays.asList(provi);
		 *
		 * // expenses List<String> l6 = new ArrayList<String>(); String[] expen
		 * = getResources().getStringArray(R.array.expenses); l6 =
		 * Arrays.asList(expen);
		 *
		 * // review List<String> l7 = new ArrayList<String>(); String[] revieww
		 * = getResources().getStringArray(R.array.review); l7 =
		 * Arrays.asList(revieww);
		 *
		 * // cra profile List<String> l8 = new ArrayList<String>();
		 *
		 * // submit List<String> l9 = new ArrayList<String>();
		 *
		 * // cloud drive List<String> l10 = new ArrayList<String>();
		 */

		/*
		 * // assigning values to menu and submenu
		 * listDataChild.put(listDataHeader.get(0), dashboard); // Header, Child
		 * // data listDataChild.put(listDataHeader.get(1), l1);
		 * listDataChild.put(listDataHeader.get(2), l2);
		 * listDataChild.put(listDataHeader.get(3), l3);
		 * listDataChild.put(listDataHeader.get(4), l4);
		 * listDataChild.put(listDataHeader.get(5), l5);
		 * listDataChild.put(listDataHeader.get(6), l6);
		 * listDataChild.put(listDataHeader.get(7), l7);
		 * listDataChild.put(listDataHeader.get(8), l8);
		 * listDataChild.put(listDataHeader.get(9), l9);
		 * listDataChild.put(listDataHeader.get(10), l10);
		 */

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            // super.onBackPressed();
            moveTaskToBack(true);
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(NavigationActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void checkPermission() {
       /* int result = ContextCompat.checkSelfPermission(NavigationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;

        } else {

            return false;

        }*/

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 100);

        }
    }

    public boolean checkGPS() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gpsStatus = false;
            return true;
        } else {
            showGPSDisabledAlertToUser();
            return false;
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("GPS Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                                gpsStatus = true;
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        gpsStatus = false;

                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(NavigationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

            Toast.makeText(NavigationActivity.this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(NavigationActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(NavigationActivity.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();
                    mGoogleApiClient.connect();
                } else {

                    Toast.makeText(NavigationActivity.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                }
                break;
        }

    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */

    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.GET_ACCOUNTS}, 100);

        }


        if (checkGPS()) {


            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                latitude = mLastLocation.getLatitude();

                longitude = mLastLocation.getLongitude();

                handleNewLocation(mLastLocation);

	            /*mLatitudeText.setText(String.format("%s: %f", mLatitudeLabel,
	                    mLastLocation.getLatitude()));

	            mLongitudeText.setText(String.format("%s: %f", mLongitudeLabel,
	                    mLastLocation.getLongitude()));*/
            } else {
                //        Toast.makeText(this, "No location Detected", Toast.LENGTH_LONG).show();
                mGoogleApiClient.connect();
            }

        }
    }

    public void handleNewLocation(Location location) {
        myApplication.setMyLatitude(location.getLatitude() + "");
        myApplication.setMyLongitude(location.getLongitude() + "");
        //          Toast.makeText(this, "Lat "+latitude+", Longitude"+longitude, Toast.LENGTH_LONG).show();
        latitude = Double.parseDouble(myApplication.getMyLatitude());
        longitude = Double.parseDouble(myApplication.getMyLongitude());


        Log.e("latitute", latitude + "");
        Log.e("longitude", longitude + "");

        if (myApplication.getMyLongitude() != null && myApplication.getMyLongitude() != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("THIS", "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i("THIS", "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    class LoadAllCaegory extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(NavigationActivity.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            object = parser
                    .makeHttpRequest(NavigationActivity.this, urlLoadMainCategories, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            listDataChild = new HashMap<String, List<SubCategoryData>>();

            List<String> childlist;

            categoryList = new ArrayList<CategoryData>();

            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        categoryList = new ArrayList<CategoryData>();

                        JSONArray array = object
                                .getJSONArray("main_categories");
                        for (int i = 0; i < array.length(); i++) {
                            subCategoryDatas = new ArrayList<SubCategoryData>();
                            JSONObject object1 = array.getJSONObject(i);
                            CategoryData data = new CategoryData();
                            data.setCatid(object1.getString("cate_id"));
                            data.setCatname(object1.getString("cate_name"));
                            data.setCatimg(object1.getString("cate_img"));
                            JSONArray array1 = object1
                                    .getJSONArray("sub_categories");
                            listDataHeader.add(data.getCatname());
                            childlist = new ArrayList<String>();
                            for (int j = 0; j < array1.length(); j++) {

                                JSONObject object2 = array1.getJSONObject(j);
                                SubCategoryData data1 = new SubCategoryData();
                                data1.setSubcat_id(object2.getString("cate_id"));
                                data1.setSubcat_name(object2
                                        .getString("cate_name"));
                                data1.setSubcat_img(object2
                                        .getString("cate_img"));
                                childlist.add(data1.getSubcat_name());
                                subCategoryDatas.add(data1);
                            }
                            listDataChild.put(listDataHeader.get(i),
                                    subCategoryDatas);

                            data.setSubcatlist(subCategoryDatas);
                            categoryList.add(data);
                        }

                        listAdapter = new ExpandableListAdapter(
                                NavigationActivity.this, categoryList,
                                listDataChild);
                        // setting list adapter
                        // expListView.setAdapter(listAdapter);

                    } else {
                        Toast.makeText(NavigationActivity.this,
                                "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public class NavListAdapter extends BaseAdapter {

        // child data in format of header title, child title
        LayoutInflater inflater;
        private Context _context;
        private List<String> couponlist; // header titles

        public NavListAdapter(Context context, List<String> couponlist) {
            this._context = context;
            this.couponlist = couponlist;

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return couponlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return couponlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.activity_coupon_category,
                    null);
            TextView couponcategorytitle = (TextView) view
                    .findViewById(R.id.couponcategorytitle);
            couponcategorytitle
                    .setText(Html.fromHtml(couponlist.get(position)));

            return view;
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<CategoryData> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<SubCategoryData>> _listDataChild;

        public ExpandableListAdapter(Context context,
                                     List<CategoryData> listDataHeader,
                                     HashMap<String, List<SubCategoryData>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(
                    this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final SubCategoryData subcatdata = (SubCategoryData) _listDataHeader
                    .get(groupPosition).getSubcatlist().get(childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            ImageView imgListItem = (ImageView) convertView
                    .findViewById(R.id.imgListItem);
            Picasso.with(NavigationActivity.this).load(DBConnection.CATEGORY_IMGURL + subcatdata.getSubcat_img()).placeholder(R.drawable.placeholder).resize(144, 0).into(imgListItem);

            txtListChild.setText(subcatdata.getSubcat_name());
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return (this._listDataHeader.get(groupPosition).getSubcatlist())
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            CategoryData categorydata = (CategoryData) _listDataHeader
                    .get(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }
            // changing the +/- on expanded list view

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            // lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(categorydata.getCatname());

            ImageView imgListItem = (ImageView) convertView
                    .findViewById(R.id.imgListItem);
            Picasso.with(NavigationActivity.this).load(DBConnection.CATEGORY_IMGURL + categorydata.getCatimg()).placeholder(R.drawable.placeholder).resize(144, 0).into(imgListItem);


            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

}
