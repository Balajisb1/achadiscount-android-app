package com.application.ads;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CategoryData;
import com.application.ads.data.MainCategoryData;
import com.application.ads.data.MyApplication;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.androidramp.gearload.Loading;

public class OnlineHomeNavigation extends FragmentActivity {

    private static final int FILTER_ID = 0;
    private final int interval = 3000; // 2 Second
    List<SubCategoryData> subCategoryDatas;
    SharedPreferences preferences;
    List<ProductDetails> productsdetails;
    SessionManager manager;
    Bundle bundle;
    MyApplication application;
    Loading loading;
    ArrayList<MainCategoryData> mainCategories;
    int totalCount = 0;
    JSONParser parser;
    List<String> listDataHeader;
    List<CategoryData> categoryList;
    HashMap<String, List<SubCategoryData>> listDataChild;
    View view_Group;
    int newpos;
    int position;
    boolean drawerstatus = false;
    ProgressDialog pd;
    Handler actHandler;
    private DrawerLayout mDrawerLayout;
    private android.app.Fragment fragment = null;
    // private ExpandableListView expListView;
    private ListView expListView;
    private CategoryListAdapter listAdapter;
    private int lastExpandedPosition = -1;
    private String urlLoadMainCategories = DBConnection.BASEURL + "load_main_categories.php";
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private Handler handler = new Handler();
    // Catch the events related to the drawer to arrange views according to this
    // action if necessary...
    private DrawerListener mDrawerListener = new DrawerListener() {

        @Override
        public void onDrawerStateChanged(int status) {

        }

        @Override
        public void onDrawerSlide(View view, float slideArg) {

        }

        @Override
        public void onDrawerOpened(View view) {
            getActionBar().setTitle(mDrawerTitle);
            // calling onPrepareOptionsMenu() to hide action bar icons
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            getActionBar().setTitle("");
            // calling onPrepareOptionsMenu() to show action bar icons
            invalidateOptionsMenu();
        }
    };

    public OnlineHomeNavigation() {
    }


    public OnlineHomeNavigation(int position) {
        this.newpos = position;

        // displayView(position);
    }

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        ImageView homedrawer = (ImageView) toolbarview
                .findViewById(R.id.homedrawer);
        homedrawer.setVisibility(View.VISIBLE);
        homedrawer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerstatus) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    drawerstatus = true;
                } else {
                    drawerstatus = false;
                    mDrawerLayout.closeDrawers();
                }
            }
        });
        bundle = new Bundle();

        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Online/Offline Price Comparision");
        getActionBar().setCustomView(toolbarview);

        getActionBar()
                .setTitle("Online/Offline Price Comparison With Cashback");
        parser = new JSONParser();
        setContentView(R.layout.activity_coupon_navigation);
        mTitle = mDrawerTitle = getTitle();
        position = newpos;
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
//		pd=ProgressDialog.show(this,"","Please wait...");
//		pd.show();


        loading = (Loading) findViewById(R.id.loading);

        loading.Start();


        Runnable runnable = new Runnable() {
            public void run() {

//				pd.dismiss();
                loading.Cancel();
                setUpDrawer();
                mDrawerLayout.closeDrawer(expListView);
                mDrawerLayout.setDrawerListener(mDrawerToggle);

            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);

        manager = new SessionManager(OnlineHomeNavigation.this);
        application = MyApplication.getInstance();


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.nav_drawer, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for
                R.string.app_name // nav drawer close - description for
        ) {
            @Override
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(
                        "Online/Offline Price Comparison With Cashback");
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(
                        "Online/Offline Price Comparison With Cashback");
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };

        makeActionOverflowMenuShown();



/*		pd=ProgressDialog.show(this,"","Please wait...");
        //move this HERE!!
		actHandler=new Handler(){
			public void handleMessage(android.os.Message msg)
			{
				super.handleMessage(msg);
				pd.dismiss();
			}
		};*/


    }

    private void makeActionOverflowMenuShown() {
        // devices with hardware menu button (e.g. Samsung ) don't show action
        // overflow menu
        try {
            final ViewConfiguration config = ViewConfiguration.get(this);
            final Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (final Exception e) {
            Log.e("", e.getLocalizedMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		/*
		 * mDrawerLayout.setScrimColor(getResources().getColor(
		 * android.R.color.transparent));
		 */
        mDrawerLayout.setDrawerListener(mDrawerListener);
        expListView = (ListView) findViewById(R.id.coupon_slidermenu);
        prepareListData();

        // expandable list view click listener

        expListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                displayView(position);

            }
        });

    }

    public void displayView(int position) {

        if (position == categoryList.size() - 1) {
            if (manager.checkLogin()) {
                Intent intent = new Intent(OnlineHomeNavigation.this, AddReferalActivity.class);
                startActivity(intent);
            }
        } else {
            bundle.putString("parentid", categoryList.get(position).getCatid());
            bundle.putString("parenttitle", categoryList.get(position).getCatname());
            fragment = new SubCategoryActivity();
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        }
        mDrawerLayout.closeDrawer(expListView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent2 = new Intent(OnlineHomeNavigation.this,
                        NavigationActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.action_compare:
                Intent intent = new Intent(OnlineHomeNavigation.this,
                        ProductDetailsActivity.class);
                intent.putExtra("parent_child_id",
                        preferences.getString(SessionManager.KEY_COMP_ID, "20003"));
                startActivity(intent);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(OnlineHomeNavigation.this,
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.action_coupons:
                Intent intent1 = new Intent(OnlineHomeNavigation.this,
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;

            case R.id.myaccount:
                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(OnlineHomeNavigation.this,
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                if (mDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        // listDataChild = new HashMap<String, List<String>>();

//		new LoadAllCaegory().execute();


        String url = urlLoadMainCategories + "";
        volleyJsonObjectRequest(url);


    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);


                    listDataChild = new HashMap<String, List<SubCategoryData>>();

                    List<String> childlist;

                    categoryList = new ArrayList<CategoryData>();
                    productsdetails = new ArrayList<ProductDetails>();

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            categoryList = new ArrayList<CategoryData>();


                            JSONArray array = jsonObject
                                    .getJSONArray("main_categories");
                            for (int i = 0; i < array.length(); i++) {
                                subCategoryDatas = new ArrayList<SubCategoryData>();
                                JSONObject object1 = array.getJSONObject(i);
                                CategoryData data = new CategoryData();
                                data.setCatid(object1.getString("cate_id"));
                                data.setCatname(object1.getString("cate_name"));
                                data.setCatimg(object1.getString("cate_img"));
                                JSONArray array1 = object1
                                        .getJSONArray("sub_categories");
                                listDataHeader.add(data.getCatname());
                                childlist = new ArrayList<String>();
                                for (int j = 0; j < array1.length(); j++) {

                                    JSONObject object2 = array1.getJSONObject(j);
                                    SubCategoryData data1 = new SubCategoryData();
                                    data1.setSubcat_id(object2.getString("cate_id"));
                                    data1.setSubcat_name(object2
                                            .getString("cate_name"));
                                    data1.setSubcat_img(object2
                                            .getString("cate_img"));
                                    childlist.add(data1.getSubcat_name());
                                    subCategoryDatas.add(data1);
                                }
                                listDataChild.put(listDataHeader.get(i),
                                        subCategoryDatas);

                                data.setSubcatlist(subCategoryDatas);
                                categoryList.add(data);
                            }

                            CategoryData categoryData = new CategoryData();
                            categoryData.setCatname("Refer and Earn");
                            categoryList.add(categoryData);

                            listAdapter = new CategoryListAdapter(OnlineHomeNavigation.this, categoryList);
                            // setting list adapter
                            expListView.setAdapter(listAdapter);

                            Log.e("pos", application.getTempcatid() + "");
                            if (application.getTempcatid() != null) {
                                displayView(Integer.parseInt(application
                                        .getTempcatid()));
                            } else {
                                displayView(0);
                            }
                        } else {
                            Toast.makeText(OnlineHomeNavigation.this,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class LoadAllCaegory extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(OnlineHomeNavigation.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            object = parser.makeHttpRequest(OnlineHomeNavigation.this, urlLoadMainCategories, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            listDataChild = new HashMap<String, List<SubCategoryData>>();

            List<String> childlist;

            categoryList = new ArrayList<CategoryData>();
            productsdetails = new ArrayList<ProductDetails>();

            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        categoryList = new ArrayList<CategoryData>();


                        JSONArray array = object
                                .getJSONArray("main_categories");
                        for (int i = 0; i < array.length(); i++) {
                            subCategoryDatas = new ArrayList<SubCategoryData>();
                            JSONObject object1 = array.getJSONObject(i);
                            CategoryData data = new CategoryData();
                            data.setCatid(object1.getString("cate_id"));
                            data.setCatname(object1.getString("cate_name"));
                            data.setCatimg(object1.getString("cate_img"));
                            JSONArray array1 = object1
                                    .getJSONArray("sub_categories");
                            listDataHeader.add(data.getCatname());
                            childlist = new ArrayList<String>();
                            for (int j = 0; j < array1.length(); j++) {

                                JSONObject object2 = array1.getJSONObject(j);
                                SubCategoryData data1 = new SubCategoryData();
                                data1.setSubcat_id(object2.getString("cate_id"));
                                data1.setSubcat_name(object2
                                        .getString("cate_name"));
                                data1.setSubcat_img(object2
                                        .getString("cate_img"));
                                childlist.add(data1.getSubcat_name());
                                subCategoryDatas.add(data1);
                            }
                            listDataChild.put(listDataHeader.get(i),
                                    subCategoryDatas);

                            data.setSubcatlist(subCategoryDatas);
                            categoryList.add(data);
                        }

                        CategoryData categoryData = new CategoryData();
                        categoryData.setCatname("Refer and Earn");
                        categoryList.add(categoryData);

                        listAdapter = new CategoryListAdapter(
                                OnlineHomeNavigation.this, categoryList);
                        // setting list adapter
                        expListView.setAdapter(listAdapter);

                        Log.e("pos", application.getTempcatid() + "");
                        if (application.getTempcatid() != null) {
                            displayView(Integer.parseInt(application
                                    .getTempcatid()));
                        } else {
                            displayView(0);
                        }
                    } else {
                        Toast.makeText(OnlineHomeNavigation.this,
                                "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public class CategoryListAdapter extends BaseAdapter {

        LayoutInflater inflater;
        private Context _context;
        // child data in format of header title, child title
        private List<CategoryData> couponlist; // header titles

        public CategoryListAdapter(Context context,
                                   List<CategoryData> couponlist) {
            this._context = context;
            this.couponlist = couponlist;

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return couponlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return couponlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_group, null);
            TextView couponcategorytitle = (TextView) view
                    .findViewById(R.id.lblListHeader);
            ImageView cateimg = (ImageView) view.findViewById(R.id.imgListItem);
            couponcategorytitle.setText(Html.fromHtml(couponlist.get(position)
                    .getCatname()));
            Picasso.with(OnlineHomeNavigation.this).load(DBConnection.CATEGORY_IMGURL
                    + categoryList.get(position).getCatimg()).placeholder(R.drawable.adslogo).resize(144, 0).into(cateimg);

            return view;
        }
    }

}
