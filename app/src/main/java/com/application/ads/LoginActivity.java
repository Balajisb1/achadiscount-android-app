package com.application.ads;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.MyApplication;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.application.ads.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LoginActivity extends Activity implements OnConnectionFailedListener, ConnectionCallbacks, ResultCallback<LoadPeopleResult> {

    public static final int PICK_MEDIA_REQUEST_CODE = 8;
    public static final int SHARE_MEDIA_REQUEST_CODE = 9;
    public static final int SIGN_IN_REQUEST_CODE = 10;
    public static final int ERROR_DIALOG_REQUEST_CODE = 11;
    private static final int SIGN_IN_CODE = 0;
    private static final int PROFILE_PIC_SIZE = 120;
    private static final String urlSignUp = DBConnection.BASEURL + "social_signup.php";
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static String urlLogin = DBConnection.BASEURL + "login.php";
    public boolean mSignInClicked;
    public boolean mIntentInProgress;
    GoogleApiClient google_api_client;
    GoogleApiAvailability google_api_availability;
    SignInButton signIn_btn;
    String username;
    ProgressDialog progress_dialog;
    TextView forgotpassword, logincashbackortext, logincashbacktext;
    EditText txtUsername, txtPassword;
    Button fblogin, gpluslogin, signin, register;
    CheckBox rememberme;
    Button logincashbackbtn;
    SessionManager manager;
    MyApplication myApplication;
    JSONParser parser;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private ConnectionResult connection_result;
    private boolean is_intent_inprogress;
    private boolean is_signInBtn_clicked;
    private int request_code;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Profile profile = Profile.getCurrentProfile();
            //nextActivity(profile);
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException e) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy old = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(old)
                .permitDiskWrites()
                .build());
        StrictMode.setThreadPolicy(old);

        FacebookSdk.sdkInitialize(LoginActivity.this);
        buidNewGoogleApiClient();
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progress_dialog = new ProgressDialog(this);
        progress_dialog.setMessage("Signing in....");

        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //  nextActivity(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        signIn_btn = (SignInButton) findViewById(R.id.sign_in_button);


        signIn_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                progress_dialog.show();
                gPlusSignIn();

              /*  if (checkPermission()) {
                    gPlusSignIn();
                } else {
                    requestPermission();
`
                }*/
            }
        });

        parser = new JSONParser();


        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                //   nextActivity(profile);
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    String email = object.getString("email");
                                    String name = object.getString("name");
                                    Log.e("email", email);
                                    new SignUpExecute(name, email, "1").execute();
//                                    String url=urlSignUp+"?first_name="+name+"&email="+email;
//                                    getjsonValues(url,email);


                                    //  Toast.makeText(LoginActivity.this, "Email is..."+email, Toast.LENGTH_SHORT).show();
                                } catch (Exception exception) {
                                    Log.e("Exc", exception.toString());
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();


                //Toast.makeText(getApplicationContext(), "Logging in...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        };
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        loginButton.registerCallback(callbackManager, callback);


        myApplication = MyApplication.getInstance();
        txtUsername = (EditText) findViewById(R.id.txtusername);
        txtPassword = (EditText) findViewById(R.id.txtpassword);
        forgotpassword = (TextView) findViewById(R.id.forgotpassword);
        signin = (Button) findViewById(R.id.signin);
        register = (Button) findViewById(R.id.register);
        rememberme = (CheckBox) findViewById(R.id.rememberme);
        logincashbackbtn = (Button) findViewById(R.id.logincashbackbtn);
        manager = new SessionManager(LoginActivity.this);
        logincashbackortext = (TextView) findViewById(R.id.logincashbackortext);
        logincashbacktext = (TextView) findViewById(R.id.logincashbacktext);


        forgotpassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });


        register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        signin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validateComponents()) {
                    new ExecuteLoginTask().execute();
                }


            }
        });

        if (getIntent().getStringExtra("code") != null || getIntent().getStringExtra("coup_type") != null || getIntent().getStringExtra("coupon_id") != null || getIntent().getStringExtra("product_id") != null) {
            logincashbackbtn.setVisibility(View.VISIBLE);
            logincashbackortext.setVisibility(View.VISIBLE);
            logincashbacktext.setVisibility(View.VISIBLE);
        } else {
            logincashbackbtn.setVisibility(View.GONE);
            logincashbackortext.setVisibility(View.GONE);
            logincashbacktext.setVisibility(View.GONE);
        }

        logincashbackbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("code") != null) {
                    Intent intent = new Intent(LoginActivity.this, CodeGenerationPage.class);
                    intent.putExtra("title", getIntent().getStringExtra("title"));
                    intent.putExtra("name", getIntent().getStringExtra("name"));
                    intent.putExtra("code", getIntent().getStringExtra("code"));
                    intent.putExtra("type", getIntent().getStringExtra("type"));
                    intent.putExtra("offer_page", getIntent().getStringExtra("offer_page"));
                    intent.putExtra("logo", getIntent().getStringExtra("logo"));
                    startActivity(intent);

                } else if (getIntent().getStringExtra("coup_type") != null) {
                    Intent intent = new Intent(LoginActivity.this,
                            SalableCouponDetails.class);
                    intent.putExtra("store_name", getIntent().getStringExtra("store_name"));
                    intent.putExtra("store_logo", getIntent().getStringExtra("store_logo"));
                    intent.putExtra("store_desc", getIntent().getStringExtra("store_desc"));
                    intent.putExtra("coup_title", getIntent().getStringExtra("coup_title"));
                    intent.putExtra("coup_desc", getIntent().getStringExtra("coup_desc"));
                    intent.putExtra("coup_amt", getIntent().getStringExtra("coup_amt"));
                    startActivity(intent);
                } else if (getIntent().getStringExtra("coupon_id") != null) {
                    Intent intent = new Intent(LoginActivity.this, CouponGoToStore.class);
                    intent.putExtra("coupon_id", "" + getIntent().getStringExtra("coupon_id"));
                    intent.putExtra("store_id", "" + getIntent().getStringExtra("store_id"));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(LoginActivity.this, ProductGotoStore.class);
                    intent.putExtra("product_id", getIntent().getStringExtra("product_id"));
                    intent.putExtra("store_id", getIntent().getStringExtra("store_id"));
                    intent.putExtra("pp_id", getIntent().getStringExtra("pp_id"));
                    startActivity(intent);
                }
                finish();
            }
        });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;

        } else {

            return false;

        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, android.Manifest.permission.GET_ACCOUNTS)) {

            Toast.makeText(LoginActivity.this, "Needs to access your Accounts", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.GET_ACCOUNTS}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(LoginActivity.this, "Permission Granted for accessing accounts", Toast.LENGTH_LONG).show();
                    gPlusSignIn();
                } else {
                    Toast.makeText(LoginActivity.this, "Permission Denied, we cannot access account.", Toast.LENGTH_LONG).show();

                }
                break;
        }

    }

    public boolean validateComponents() {
        if (txtUsername.getText().toString().equalsIgnoreCase("")
                || txtUsername.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(LoginActivity.this, "Email is Mandatory",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtPassword.getText().toString().equalsIgnoreCase("")
                || txtPassword.getText().toString().equalsIgnoreCase(null)) {
            Toast.makeText(LoginActivity.this, "Password is mandatory",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void buidNewGoogleApiClient() {

        google_api_client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();
    }
    
	    
	   /* private void nextActivity(Profile profile){
            if(profile != null){
	            Intent main = new Intent(LoginActivity.this, DemoActivity.class);
	            main.putExtra("name", profile.getFirstName());
	            main.putExtra("surname", profile.getLastName());
	            main.putExtra("imageUrl", profile.getProfilePictureUri(200,200).toString());
	            startActivity(main);
	        }
	    }*/

    protected void onStart() {
        super.onStart();
        google_api_client.connect();
        if (FacebookSdk.isInitialized()) {
            LoginManager.getInstance().logOut();
        }
    }

    /*  @Override
      protected void onResume() {
          super.onResume();
          //Facebook login
          Profile profile = Profile.getCurrentProfile();
          //nextActivity(profile);
      }
  */
    /*@Override
    protected void onPause() {

        super.onPause();
    }
*/
    protected void onStop() {
        super.onStop();
        //Facebook login
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
        if (google_api_client.isConnected()) {
            google_api_client.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            google_api_availability.getErrorDialog(this, result.getErrorCode(), request_code).show();
            return;
        }

        if (!is_intent_inprogress) {

            connection_result = result;

            if (is_signInBtn_clicked) {

                resolveSignInError();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        Log.e("Data is", data + "");
        if (requestCode == SIGN_IN_CODE) {
            request_code = requestCode;
            if (resultCode != RESULT_OK) {
                is_signInBtn_clicked = false;
                //    progress_dialog.dismiss();

            }

            is_intent_inprogress = false;

            if (!google_api_client.isConnecting()) {
                google_api_client.connect();
            }
        }

    }


    @Override
    public void onConnected(Bundle arg0) {

        Log.e("On Connected", arg0 + "");
        // Get user's information and set it into the layout
        if (is_signInBtn_clicked) {
            is_signInBtn_clicked = false;
            getProfileInfo();
        } else {
            gPlusSignOut();
        }
        // Update the UI after signin

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Log.e("On Suspended", arg0 + "");
        google_api_client.connect();
    }


    private void gPlusSignIn() {
        if (!google_api_client.isConnecting()) {
            Log.d("user connected", "connected");
            is_signInBtn_clicked = true;
//            progress_dialog.show();
            resolveSignInError();

        }
    }

    private void gPlusRevokeAccess() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            Plus.AccountApi.revokeAccessAndDisconnect(google_api_client)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.d("MainActivity", "User access revoked!");
                            buidNewGoogleApiClient();
                            google_api_client.connect();
                        }

                    });
        }
    }


	    /*
          Method to resolve any signin errors
	     */

    private void resolveSignInError() {
        if (connection_result.hasResolution()) {
            try {
                is_intent_inprogress = true;
                connection_result.startResolutionForResult(this, SIGN_IN_CODE);
                Log.d("resolve error", "sign in error resolved");
            } catch (SendIntentException e) {
                is_intent_inprogress = false;
                google_api_client.connect();
            }
        }
    }

	    /*
          Sign-out from Google+ account
	     */

    private void gPlusSignOut() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            google_api_client.disconnect();
            google_api_client.connect();
        }
    }


    private void getProfileInfo() {
        try {
            Person singer = Plus.PeopleApi.getCurrentPerson(google_api_client);
            Log.e("SIng", singer + "");

            if (Plus.PeopleApi.getCurrentPerson(google_api_client) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(google_api_client);
                Log.e("SIng", currentPerson + "");
                //setPersonalInfo(currentPerson);

                Log.e("Name", currentPerson.getDisplayName() + "");
                Log.e("Email", Plus.AccountApi.getAccountName(google_api_client) + "");
                new SignUpExecute(currentPerson.getDisplayName(), Plus.AccountApi.getAccountName(google_api_client), "2").execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "No Personal info mention", Toast.LENGTH_LONG).show();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getjsonValues(String url, final String email) {
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.e("URl", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        if (response.trim().length() > 0) {

                            try {
                                JSONObject object = new JSONObject(response);
                                Log.e("response", response);

                                if (object.getInt("success") == 1) {
                                    manager.createLoginSession(object.getString("user_id"), email, "", "", object.getString("random_code"), object.getString("contact_no"), "1");

						/*
                        Intent intent = new Intent(LoginActivity.this,
								LoginActivity.class);
						startActivity(intent);*/
                                    finish();
                        /*
                         * Toast.makeText(getActivity(),
						 * "You have Registered Successfully..",
						 * Toast.LENGTH_LONG).show();
						 */
                                    // SignUpActivity.setCurrentPosition(0);
                                } else {
                                    Toast.makeText(
                                            LoginActivity.this,
                                            "Already the given Email has been registered..",
                                            Toast.LENGTH_LONG).show();
                                }


                            } catch (Exception e) {
                                // TODO: handle exception
                                System.out.println(e);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onResult(@NonNull LoadPeopleResult arg0) {
        // TODO Auto-generated method stub

    }

    private class SignUpExecute extends AsyncTask<String, String, String> {

        ProgressDialog pDialog;
        String username, email;
        String type;

        public SignUpExecute(String username, String email, String type) {
            this.username = username;
            this.email = email;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Loading..");
            pDialog.setCanceledOnTouchOutside(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("first_name", username.replace("%20", " ")));
            param.add(new BasicNameValuePair("email", email));
            Log.e("Parm is====", param.toString());
            JSONObject object = parser.makeHttpRequest(LoginActivity.this, urlSignUp, "GET", param);
            Log.e("object", "" + object);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    Log.e("resulttttttt", result);

                    if (object.getInt("success") == 1) {
                        manager.createLoginSession(object.getString("user_id"), email, "", "", object.getString("random_code"), object.getString("contact_no"), "2");
                        finish();

                    } else {
                        Toast.makeText(
                                LoginActivity.this,
                                "Already the given Email has been registered..",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("err", e.toString());
                    Toast.makeText(LoginActivity.this, "Somethings goes wrong..",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(LoginActivity.this,
                        "Please check Internet Connection OR Server..",
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    class ExecuteLoginTask extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        private String emailId = txtUsername.getText().toString();
        private String password = Utils.encryptToMD5(txtPassword.getText().toString());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(LoginActivity.this, "", "Loading..", true, false);

        }


        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("email_id", emailId));
            param.add(new BasicNameValuePair("password", password));
            Log.e("params are", param.toString());
            JSONObject object = parser.makeHttpRequest(LoginActivity.this, urlLogin, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        manager.createLoginSession(object.getString("user_id"), object.getString("email"), object.getString("password"), object.getString("user_name"), object.getString("random_code"), object.getString("contact_no"), "1");
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_SHORT).show();
                        txtUsername.setText("");
                        txtPassword.setText("");
                    }
                } catch (Exception e) {
                    Log.e("", e.toString());
                }
            }
        }

    }


}
