package com.application.ads.data;

public class ReviewsData {


    private String rating;
    private String quality;
    private String expectations;
    private String prosSummary;
    private String consSummary;
    private String zeroStar;
    private String oneStar;
    private String twoStar;
    private String threeStar;
    private String fourStar;
    private String fiveStar;
    private String overAllRating;
    private String totalReviews;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getExpectations() {
        return expectations;
    }

    public void setExpectations(String expectations) {
        this.expectations = expectations;
    }

    public String getProsSummary() {
        return prosSummary;
    }

    public void setProsSummary(String prosSummary) {
        this.prosSummary = prosSummary;
    }

    public String getConsSummary() {
        return consSummary;
    }

    public void setConsSummary(String consSummary) {
        this.consSummary = consSummary;
    }

    public String getOneStar() {
        return oneStar;
    }

    public void setOneStar(String oneStar) {
        this.oneStar = oneStar;
    }

    public String getTwoStar() {
        return twoStar;
    }

    public void setTwoStar(String twoStar) {
        this.twoStar = twoStar;
    }

    public String getThreeStar() {
        return threeStar;
    }

    public void setThreeStar(String threeStar) {
        this.threeStar = threeStar;
    }

    public String getFourStar() {
        return fourStar;
    }

    public void setFourStar(String fourStar) {
        this.fourStar = fourStar;
    }

    public String getFiveStar() {
        return fiveStar;
    }

    public void setFiveStar(String fiveStar) {
        this.fiveStar = fiveStar;
    }

    public String getOverAllRating() {
        return overAllRating;
    }

    public void setOverAllRating(String overAllRating) {
        this.overAllRating = overAllRating;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    public String getZeroStar() {
        return zeroStar;
    }

    public void setZeroStar(String zeroStar) {
        this.zeroStar = zeroStar;
    }

}
