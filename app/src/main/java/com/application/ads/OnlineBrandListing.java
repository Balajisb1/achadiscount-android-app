package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.BrandListingAdapter;
import com.application.ads.adapter.StoreAdapter;
import com.application.ads.data.MobileBrandsData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class OnlineBrandListing extends Activity {

    GridView brandlistgrid;
    JSONParser parser;

    StoreAdapter adapter;
    List<MobileBrandsData> mobilebranddata;
    BrandListingAdapter brandadapter;
    SharedPreferences preferences;
    SessionManager manager;
    private String urlLoadAllBrands = DBConnection.BASEURL + "load_all_brands.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brandlist);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Brands");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mobilebranddata = new ArrayList<MobileBrandsData>();
        brandlistgrid = (GridView) findViewById(R.id.brandlistgrid);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        manager = new SessionManager(OnlineBrandListing.this);
        parser = new JSONParser();


		/*brandlistgrid.setOnItemClickListener(new OnItemClickListener() {
            @Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			
			}
		});*/

//	new ExecuteLoadPopularBrands().execute();

//		List<NameValuePair> param = new ArrayList<NameValuePair>();
//		param.add(new BasicNameValuePair("start_letter", ""));

        String url = urlLoadAllBrands + "?start_letter=";
        volleyJsonObjectRequest(url);

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    mobilebranddata = new ArrayList<MobileBrandsData>();
                    List<String> cataffdata = new ArrayList<String>();
                    // Toast.makeText(HomeActivity.this, result.length()+"",
                    // Toast.LENGTH_SHORT).show();

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("brands");
                            // Toast.makeText(HomeActivity.this, array.length()+"",
                            // Toast.LENGTH_SHORT).show();

                            for (int i = 0; i < array.length(); i++) {
                                MobileBrandsData data = new MobileBrandsData();
                                JSONObject object1 = array.getJSONObject(i);
                                data.setMobile_id(object1.getString("id"));
                                data.setMobile_name(object1.getString("name"));
                                data.setMobile_image(object1.getString("image"));
                                data.setParent_id(object1.getString("parent_id"));
                                data.setParent_child_id(object1.getString("parent_child_id"));

                                mobilebranddata.add(data);
                            }

                            brandadapter = new BrandListingAdapter(
                                    getApplicationContext(), mobilebranddata);
                            brandlistgrid.setAdapter(brandadapter);

                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "No data found", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteLoadPopularBrands extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(OnlineBrandListing.this, "",
                    "Loading..");

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("start_letter", ""));
            JSONObject object = parser.makeHttpRequest(OnlineBrandListing.this, urlLoadAllBrands, "GET",
                    param);
            Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();

            mobilebranddata = new ArrayList<MobileBrandsData>();
            List<String> cataffdata = new ArrayList<String>();
            // Toast.makeText(HomeActivity.this, result.length()+"",
            // Toast.LENGTH_SHORT).show();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray array = object.getJSONArray("brands");
                        // Toast.makeText(HomeActivity.this, array.length()+"",
                        // Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < array.length(); i++) {
                            MobileBrandsData data = new MobileBrandsData();
                            JSONObject object1 = array.getJSONObject(i);
                            data.setMobile_id(object1.getString("id"));
                            data.setMobile_name(object1.getString("name"));
                            data.setMobile_image(object1.getString("image"));
                            data.setParent_id(object1.getString("parent_id"));
                            data.setParent_child_id(object1.getString("parent_child_id"));

                            mobilebranddata.add(data);
                        }

                        brandadapter = new BrandListingAdapter(
                                getApplicationContext(), mobilebranddata);
                        brandlistgrid.setAdapter(brandadapter);

                    } else {
                        Toast.makeText(getApplicationContext(),
                                "No data found", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }
            }
        }
    }
}
