package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.application.ads.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class WithdrawActivity extends Activity {

    public String urlExecuteWithdraw = DBConnection.BASEURL + "withdraw.php";
    EditText edtWithDraw, edtWithDrawPassword;
    Button btnWithdrawWithdraw;
    TextView txtWithDrawAvailableBalance, txtWithDrawWithdrawMinBalance;
    SessionManager manager;
    JSONParser parser;
    SharedPreferences preferences;
    LinearLayout withdrawPwdLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdraw_amount);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        withdrawPwdLinear = (LinearLayout) findViewById(R.id.withdrawPwdLinear);
        toolbartitle.setText("Withdraw");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        manager = new SessionManager(WithdrawActivity.this);
        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);

        //	Log.e("withdraw min", getIntent().getStringExtra("withdrawminbal"));

        if (preferences.getString(SessionManager.KEY_PASSWORD, "").equalsIgnoreCase("")) {
            withdrawPwdLinear.setVisibility(View.GONE);
        } else {
            withdrawPwdLinear.setVisibility(View.VISIBLE);
        }

        edtWithDraw = (EditText) findViewById(R.id.edtWithDraw);
        edtWithDrawPassword = (EditText) findViewById(R.id.edtWithDrawPassword);
        txtWithDrawAvailableBalance = (TextView) findViewById(R.id.txtWithDrawAvailableBalance);
        txtWithDrawWithdrawMinBalance = (TextView) findViewById(R.id.txtWithDrawWithdrawMinBalance);
        txtWithDrawAvailableBalance.setText("AVAILABLE BALANCE : Rs."
                + getIntent().getStringExtra("availableBalance"));
        txtWithDrawWithdrawMinBalance.setText("Withdraw Minimum Balance : Rs."
                + getIntent().getStringExtra("withdrawminbal"));
        btnWithdrawWithdraw = (Button) findViewById(R.id.btnWithdrawWithdraw);
        btnWithdrawWithdraw.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateComponents()) {
                    new ExecuteWithdraw().execute();
/*
                    List<NameValuePair> param = new ArrayList<NameValuePair>();
					param.add(new BasicNameValuePair("user_id", preferences.getString(
							SessionManager.KEY_USERID, "0")));
					param.add(new BasicNameValuePair("amount", amount));
					Log.e("Params is ", urlExecuteWithdraw + "?" + param.toString());*/
/*

					String amount=edtWithDraw.getText().toString();
					String url=urlExecuteWithdraw+"?user_id="+preferences.getString(SessionManager.KEY_USERID, "0")+"&amount="+amount;
					volleyJsonObjectRequest(url);
*/

                }
            }
        });
    }


    public boolean validateComponents() {

        if (TextUtils.isEmpty(edtWithDraw.getText().toString())) {
            Toast.makeText(WithdrawActivity.this,
                    "Please Enter the amount", Toast.LENGTH_LONG)
                    .show();
            return false;
        } else if (!preferences.getString(SessionManager.KEY_PASSWORD, "").equalsIgnoreCase("")) {
            if (edtWithDrawPassword.getText().toString().equals("")
                    || edtWithDrawPassword.getText().toString()
                    .equals(null)) {
                Toast.makeText(WithdrawActivity.this,
                        "Please Enter the Password", Toast.LENGTH_LONG)
                        .show();
                return false;
            } else if (Integer.parseInt(getIntent().getStringExtra(
                    "availableBalance").trim()) < Integer
                    .parseInt(edtWithDraw.getText().toString().trim())) {
                Toast.makeText(WithdrawActivity.this,
                        "Insufficiant balance..", Toast.LENGTH_LONG).show();
                return false;
            } else if (Integer.parseInt(edtWithDraw.getText().toString()) < Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim())) {
                Toast.makeText(WithdrawActivity.this,
                        "Your minimum amount to withdraw is .." + Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim()),
                        Toast.LENGTH_LONG).show();
                return false;
            } else if (Integer.parseInt(edtWithDraw.getText().toString()
                    .trim()) < Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim())) {
                Toast.makeText(WithdrawActivity.this,
                        "Your minimum amount to withdraw is .." + Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim()),
                        Toast.LENGTH_LONG).show();
                return false;
            } else if (!Utils.encryptToMD5(edtWithDrawPassword
                    .getText()
                    .toString())
                    .equals(preferences.getString(SessionManager.KEY_PASSWORD, ""))) {
                Toast.makeText(WithdrawActivity.this,
                        "Please provide valid password", Toast.LENGTH_LONG).show();
                return false;
            } else {
                return true;
            }
        } else if (Integer.parseInt(getIntent().getStringExtra(
                "availableBalance").trim()) < Integer
                .parseInt(edtWithDraw.getText().toString().trim())) {
            Toast.makeText(WithdrawActivity.this,
                    "Insufficiant balance..", Toast.LENGTH_LONG).show();
            return false;
        } else if (Integer.parseInt(edtWithDraw.getText().toString()) < Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim())) {
            Toast.makeText(WithdrawActivity.this,
                    "Your minimum amount to withdraw is .." + Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim()),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (Integer.parseInt(edtWithDraw.getText().toString()
                .trim()) < Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim())) {
            Toast.makeText(WithdrawActivity.this,
                    "Your minimum amount to withdraw is .." + Integer.parseInt(getIntent().getStringExtra("withdrawminbal").trim()),
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (!Utils.encryptToMD5(edtWithDrawPassword
                .getText()
                .toString())
                .equals(preferences.getString(SessionManager.KEY_PASSWORD, ""))) {
            Toast.makeText(WithdrawActivity.this,
                    "Please provide valid password", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }


    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(WithdrawActivity.this, "Your withdraw request in progress..", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(WithdrawActivity.this, "Withdraw has been failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(WithdrawActivity.this, "Please check your Internet connection OR server..", Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteWithdraw extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;
        private String amount = edtWithDraw.getText().toString();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(WithdrawActivity.this);
            dialog.setMessage("Loading..");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(
                    SessionManager.KEY_USERID, "0")));
            param.add(new BasicNameValuePair("amount", amount));
            Log.e("Params is ", urlExecuteWithdraw + "?" + param.toString());
            object = parser.makeHttpRequest(WithdrawActivity.this, urlExecuteWithdraw, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result.toString().trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Toast.makeText(WithdrawActivity.this,
                                "Your withdraw request in progress..",
                                Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(WithdrawActivity.this,
                                "Withdraw has been failed..", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(WithdrawActivity.this,
                        "Please check your Internet connection OR server..",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

}
