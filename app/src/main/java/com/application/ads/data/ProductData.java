package com.application.ads.data;

public class ProductData {

    private String proId;
    private String proName;
    private String proCoupons;
    private String proType;
    private String proCashbackType;
    private String proCashbackAmount;
    private String proImagePath;
    private String prodColor;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProCoupons() {
        return proCoupons;
    }

    public void setProCoupons(String proCoupons) {
        this.proCoupons = proCoupons;
    }

    public String getProCashbackType() {
        return proCashbackType;
    }

    public void setProCashbackType(String proCashbackType) {
        this.proCashbackType = proCashbackType;
    }

    public String getProCashbackAmount() {
        return proCashbackAmount;
    }

    public void setProCashbackAmount(String proCashbackAmount) {
        this.proCashbackAmount = proCashbackAmount;
    }

    public String getProImagePath() {
        return proImagePath;
    }

    public void setProImagePath(String proImagePath) {
        this.proImagePath = proImagePath;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getProdColor() {
        return prodColor;
    }

    public void setProdColor(String prodColor) {
        this.prodColor = prodColor;
    }

}
