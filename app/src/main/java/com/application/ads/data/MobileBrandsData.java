package com.application.ads.data;

import java.util.List;

public class MobileBrandsData {

    private String mobile_id;
    private String mobile_name;
    private String mobile_image;
    private String comp_aff_logo;
    private List<String> compafflogolist;
    private String parent_id;
    private String parent_child_id;

    private String bannerid;
    private String bannerimage;

    public String getMobile_id() {
        return mobile_id;
    }

    public void setMobile_id(String mobile_id) {
        this.mobile_id = mobile_id;
    }

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }

    public String getMobile_image() {
        return mobile_image;
    }

    public void setMobile_image(String mobile_image) {
        this.mobile_image = mobile_image;
    }

    public String getComp_aff_logo() {
        return comp_aff_logo;
    }

    public void setComp_aff_logo(String comp_aff_logo) {
        this.comp_aff_logo = comp_aff_logo;
    }

    public List<String> getCompafflogolist() {
        return compafflogolist;
    }

    public void setCompafflogolist(List<String> compafflogolist) {
        this.compafflogolist = compafflogolist;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getParent_child_id() {
        return parent_child_id;
    }

    public void setParent_child_id(String parent_child_id) {
        this.parent_child_id = parent_child_id;
    }

    public String getBannerimage() {
        return bannerimage;
    }

    public void setBannerimage(String bannerimage) {
        this.bannerimage = bannerimage;
    }

    public String getBannerid() {
        return bannerid;
    }

    public void setBannerid(String bannerid) {
        this.bannerid = bannerid;
    }


}

