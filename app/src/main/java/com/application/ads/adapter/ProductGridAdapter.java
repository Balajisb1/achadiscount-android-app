package com.application.ads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.LoginActivity;
import com.application.ads.ProductOverView;
import com.application.ads.R;
import com.application.ads.data.CompareProducts;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.application.ads.data.CompareProducts.productdetails;


public class ProductGridAdapter extends BaseAdapter {

    private SharedPreferences preferences;
    List<ProductDetails> productdetailslist;
    Context context;
    LayoutInflater inflater;
    SessionManager manager;
    private ProgressDialog dialog;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";

    public ProductGridAdapter(Context context,
                              List<ProductDetails> productdetailslist) {
        this.context = context;
        this.productdetailslist = productdetailslist;
        manager = new SessionManager(context);
        preferences = context.getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productdetailslist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return productdetailslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final int m = position;
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(
                    R.layout.activity_product_grid_details, null);
            viewHolder = new ViewHolder();
            viewHolder.prodimg = (ImageView) convertView
                    .findViewById(R.id.prodimg);
            viewHolder.prodprice = (TextView) convertView
                    .findViewById(R.id.prodprice);

            viewHolder.prodname = (TextView) convertView
                    .findViewById(R.id.prodname);
            viewHolder.prodcompare = (ImageView) convertView
                    .findViewById(R.id.prodcomparegrid);
            viewHolder.imgProductListingDetailFavourites = (ImageView) convertView
                    .findViewById(R.id.imgProductListingDetailFavourites);
            viewHolder.prodrating = (RatingBar) convertView
                    .findViewById(R.id.prodrating);
            /*LayerDrawable stars = (LayerDrawable) viewHolder.prodrating
                    .getProgressDrawable();
			stars.getDrawable(2).setColorFilter(Color.YELLOW,
					PorterDuff.Mode.SRC_ATOP);*/


            viewHolder.prodcompare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();  // Here we get the position that we have set for the checkbox using setTag.

                    if (!CompareProducts.isAlreadyInCompare(productdetailslist
                            .get(position))) {
                        if (CompareProducts.getProducts().size() >= 2) {
                            Toast.makeText(context, "Maximum compare 2 products",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            if (CompareProducts.addToCompare(productdetailslist
                                    .get(position))) {
                                viewHolder.prodcompare
                                        .setImageResource(R.drawable.added_compare);
                                /*Toast.makeText(context,
                                        "Successfully added to compare list",
										Toast.LENGTH_LONG).show();*/
                                productdetailslist.get(position).setSelected(true);
                            }
                        }
                    } else {
                        if (CompareProducts.removeFromCompare(productdetailslist
                                .get(position))) {
                            viewHolder.prodcompare
                                    .setImageResource(R.drawable.prod_compare);
                            /*Toast.makeText(context,
                                    "Successfully removed from compare list",
									Toast.LENGTH_LONG).show();*/
                            productdetailslist.get(position).setSelected(false);
                        }
                    }
/*
					Toast.makeText(context,
							CompareProducts.productdetails.size() + "", Toast.LENGTH_SHORT)
							.show();*/
                }
            });

            viewHolder.imgProductListingDetailFavourites.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    if (manager.checkLogin()) {
                        // Remove from wish list
                        if (Integer.parseInt(productdetailslist.get(pos)
                                .getProWishListStatus().trim()) > 0) {

                            Toast.makeText(context, "It is already added in wishlist", Toast.LENGTH_SHORT).show();
//                            new AddToWishList("1", productdetailslist.get(pos), viewHolder.imgProductListingDetailFavourites, context).execute(); // Remove from wish
                            // list
                        } else {
                            addToWishList(pos, 0, viewHolder);
                        }
                    } else {
                        context.startActivity(new Intent(context,
                                LoginActivity.class));
                    }

                }
            });

            if(productdetailslist.get(position).getProWishListStatus().equalsIgnoreCase("0")){
                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
            }else{
                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
            }


            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.prodcompare.setTag(position);
        viewHolder.imgProductListingDetailFavourites.setTag(position);

        if (productdetailslist.size() > 0) {

            if (productdetailslist.get(position) != null) {
                if (productdetailslist.get(position).isSelected()) {
                    for (int i = 0; i < productdetails.size(); i++) {
                        if (productdetails.get(i).getProduct_id().equalsIgnoreCase(productdetailslist.get(position).getProduct_id())) {
                            viewHolder.prodcompare
                                    .setImageResource(R.drawable.added_compare);
                        } else {
                            viewHolder.prodcompare
                                    .setImageResource(R.drawable.prod_compare);
                        }
                    }
                } else {
                    viewHolder.prodcompare
                            .setImageResource(R.drawable.prod_compare);
                }
            }

            viewHolder.prodrating.setRating(Float.parseFloat(productdetailslist.get(position).getRating()));
            viewHolder.prodprice.setText("Rs." + productdetailslist.get(position).getProduct_price());
            viewHolder.prodname.setText(productdetailslist.get(position).getProduct_name());
		/*viewHolder.prodminprice.setText("Rs."
				+ productdetailslist.get(position).getMrp());*/
            if (productdetailslist.get(position).getProWishListStatus().equals(null)
                    || productdetailslist.get(position).getProWishListStatus()
                    .equals("")) {
                Log.e("", "Null");
            } else {
                if (Integer.parseInt(productdetailslist.get(position)
                        .getProWishListStatus().trim()) > 0) {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.filled_heart);
                } else {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.heart);
                }
            }
        }

        viewHolder.prodimg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });


        viewHolder.prodname.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });

        if (productdetailslist.size() > 0) {
//            Log.i("Product Image", DBConnection.PRODUCT_IMGURL + productdetailslist.get(position).getProduct_image());

            Picasso.with(context).load(DBConnection.PRODUCT_IMGURL
                    + productdetailslist.get(m).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(viewHolder.prodimg);

/*

            Glide.with(context).load(DBConnection.PRODUCT_IMGURL
                    + productdetailslist.get(m).getProduct_image())
                    .placeholder(R.drawable.adslogo)
                    .thumbnail(0.5f)
                    .override(144,144)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.prodimg);
*/
        }


        //convertView.setTag(R.id.prodcompare, viewHolder.prodcompare);

        return convertView;
    }

    class ViewHolder {
        protected ImageView prodimg;
        protected TextView prodprice, prodname;
        protected ImageView prodcompare, imgProductListingDetailFavourites;
        protected RatingBar prodrating;
    }


    private void addToWishList(final int pos, final int i, final ViewHolder viewHolder) {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = urlAddOrRemoveProductsFromWishList + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0") + "&product_id=" + productdetailslist.get(pos).getProduct_id() + "&wish_list_status=" + i;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {

                            if (i == 0) {
                                Toast.makeText(context, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
                                productdetailslist.get(pos).setProWishListStatus("1");
                            } else if (i == 1) {
                                Toast.makeText(context, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
                            }


                        } else {
                            Toast.makeText(context,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }

}
