package com.application.ads;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ForgotPassword extends Activity {
    public static String urlForgot = DBConnection.BASEURL + "forgot_password.php";
    EditText forgotpasswordtxt;
    Button resetpwd, cancel;
    JSONParser parser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        forgotpasswordtxt = (EditText) findViewById(R.id.forgotpasswordtxt);
        resetpwd = (Button) findViewById(R.id.resetpwd);
        cancel = (Button) findViewById(R.id.cancel);
        parser = new JSONParser();
        resetpwd.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (validateComponents()) {
                    new ExecuteRegisterTask().execute();

					/*List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("email_id",emailId ));
					*/
//					String emailId=forgotpasswordtxt.getText().toString();
//
//					String url=urlForgot+"?email_id="+emailId;
//					volleyJsonObjectRequest(url);

                }
            }
        });

        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            Toast.makeText(ForgotPassword.this,
                                    "Password sent to your mail Successfully", Toast.LENGTH_SHORT).show();

                            forgotpasswordtxt.setText("");

                        } else {
                            Toast.makeText(ForgotPassword.this, "Invalid Email Id..Check your mailid",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }
//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public boolean validateComponents() {

        if (TextUtils.isEmpty(forgotpasswordtxt.getText().toString())) {
            Toast.makeText(ForgotPassword.this, "Email is mandatory..", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteRegisterTask extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        private String emailId = forgotpasswordtxt
                .getText().toString();

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(ForgotPassword.this, "", "Loading...",
                    true, false);
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("email_id", emailId));
            Log.e("params are", param.toString());
            JSONObject object = parser.makeHttpRequest(ForgotPassword.this, urlForgot, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        Toast.makeText(ForgotPassword.this,
                                "Password sent to your mail Successfully", Toast.LENGTH_SHORT).show();
                        forgotpasswordtxt.setText("");
                    } else {
                        Toast.makeText(ForgotPassword.this, "Invalid Email Id..Check your mailid",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("on Post", e + "");
                }
            }
        }
    }
}
