package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.FilterAdapter;
import com.application.ads.data.SpecificationData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterActivity extends Activity {

    ExpandableListView explistview;
    FilterAdapter expadapter;
    List<SpecificationData> listDataHeader;
    HashMap<String, List<SpecificationData>> listChildData;
    List<SpecificationData> specificationDatas;
    Button submitfilter;
    TextView filterback;
    JSONParser parser;
    String filterids;
    SharedPreferences preferences;
    SessionManager manager;
    RadioGroup priceradiogroup1, priceradiogroup2;
    RadioButton twobelow, twotofive, fivettoten, tentoeighteen,
            eighteentotwofive, twofivetothreefive, threefiveabove;
    private String urlFilter = DBConnection.BASEURL + "load_filter_options.php";
    private ProgressDialog dialog;

/*	MultiSlider multiSlider2 ;
       TextView min2;
	    TextView max2;
	   MultiSlider multiSlider;*/


    // EditText mintext,maxtext;
    // TextView minmaxtext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Online/Offline Filters");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Online/Offline Filters");
/*		
		max2 = (TextView)findViewById(R.id.maxValue2);
		min2 = (TextView)findViewById(R.id.minValue2);
		multiSlider2 = (MultiSlider)findViewById(R.id.range_slider2);
		multiSlider=new MultiSlider(FilterActivity.this);
		multiSlider.setmScaleMin(5000);
		multiSlider.setmScaleMax(6000);
		
		
		 min2.setText(String.valueOf(multiSlider2.getThumb(0).getValue()));
	        max2.setText(String.valueOf(multiSlider2.getThumb(1).getValue()));
	        
	        
	        multiSlider2.setOnThumbValueChangeListener(new MultiSlider.SimpleOnThumbValueChangeListener() {
	            @Override
	            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
	                if (thumbIndex == 0) {
	                    min2.setText(String.valueOf(value));
	                } else {
	                    max2.setText(String.valueOf(value));
	                }
	            }
	        });*/
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        explistview = (ExpandableListView) findViewById(R.id.filterexp);
        filterback = (TextView) findViewById(R.id.filterback);
        parser = new JSONParser();
        manager = new SessionManager(FilterActivity.this);
        listDataHeader = new ArrayList<SpecificationData>();
        listChildData = new HashMap<String, List<SpecificationData>>();
        priceradiogroup1 = (RadioGroup) findViewById(R.id.priceradiogroup1);
        priceradiogroup2 = (RadioGroup) findViewById(R.id.priceradiogroup2);

        twobelow = (RadioButton) findViewById(R.id.twobelow);
        twotofive = (RadioButton) findViewById(R.id.twotofive);
        fivettoten = (RadioButton) findViewById(R.id.fivettoten);
        tentoeighteen = (RadioButton) findViewById(R.id.tentoeighteen);
        eighteentotwofive = (RadioButton) findViewById(R.id.eighteentotwofive);
        twofivetothreefive = (RadioButton) findViewById(R.id.twofivetothreefive);
        threefiveabove = (RadioButton) findViewById(R.id.threefiveabove);

		/*
		 * mintext=(EditText)findViewById(R.id.mintext);
		 * maxtext=(EditText)findViewById(R.id.maxtext);
		 * minmaxtext=(TextView)findViewById(R.id.minmaxtext);
		 * 
		 * 
		 * minmaxtext.setText("Enter value from Rs. "+getIntent().getStringExtra(
		 * "min_price")+" to Rs. "+getIntent().getStringExtra("max_price") );
		 */
        if (twobelow.isChecked() || twotofive.isChecked()
                || fivettoten.isChecked() || tentoeighteen.isChecked()) {
            priceradiogroup1.clearCheck();
        }
        if (priceradiogroup2.isSelected()) {
            priceradiogroup2.clearCheck();
        }
        filterback.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setResult(2);
                finish();
            }
        });

        submitfilter = (Button) findViewById(R.id.submitfilter);
        submitfilter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(FilterActivity.this,
                // FilterAdapter.filterids.toString(), 5000).show();

                if (FilterAdapter.filterids != null) {
                    for (int i = 0; i < FilterAdapter.filterids.size(); i++) {
                        if (i == 0) {
                            filterids = FilterAdapter.filterids.get(i);
                        } else {
                            filterids += "," + FilterAdapter.filterids.get(i);
                        }
                    }
				/*
				 * if(mintext.getText().toString().equals("") ||
				 * mintext.getText().toString().equals(null) ){
				 * Toast.makeText(FilterActivity.this,
				 * "Please provide minimum price", 5000).show(); } else
				 * if(maxtext.getText().toString().equals("") ||
				 * maxtext.getText().toString().equals(null) ){
				 * Toast.makeText(FilterActivity.this,
				 * "Please provide maximum price", 5000).show(); } else
				 * if(Integer.parseInt(mintext.getText().toString()) <
				 * Integer.parseInt(getIntent().getStringExtra("min_price"))){
				 * Toast.makeText(FilterActivity.this,
				 * "Please provide valid minimum price", 5000).show(); } else
				 * if(Integer.parseInt(maxtext.getText().toString()) >
				 * Integer.parseInt(getIntent().getStringExtra("max_price")) ||
				 * Integer.parseInt(maxtext.getText().toString()) <
				 * Integer.parseInt(getIntent().getStringExtra("min_price")) ||
				 * Integer.parseInt(maxtext.getText().toString()) <
				 * Integer.parseInt(mintext.getText().toString())){
				 * Toast.makeText(FilterActivity.this,
				 * "Please provide valid maximum price", 5000).show(); } else{
				 */
                    Intent intent = new Intent(FilterActivity.this,
                            ProductDetailsActivity.class);
                    intent.putExtra("filter_ids", filterids);
                    intent.putExtra("min_price", "");
                    intent.putExtra("max_price", "");
                    setResult(1, intent);
                    finish();
                    // }
                } else {
                    Intent intent = new Intent(FilterActivity.this,
                            ProductDetailsActivity.class);
                    intent.putExtra("filter_ids", "");
                    intent.putExtra("min_price", "");
                    intent.putExtra("max_price", "");
                    setResult(2, intent);
                    finish();
                }
            }

        });

//		new ExecuteFilterActivity().execute();

/*		List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("cate_id", getIntent()
				.getStringExtra("category_id")));
		param.add(new  BasicNameValuePair("status", getIntent().getStringExtra("status")));*/

        String url = urlFilter + "?cate_id=" + getIntent().getStringExtra("category_id") + "&status=" + getIntent().getStringExtra("status");

        volleyJsonObjectRequest(url);
    }

    public void volleyJsonObjectRequest(String url) {

        dialog = ProgressDialog.show(FilterActivity.this, "", "Loading...",
                true, false);
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(jsonString);

                    listDataHeader = new ArrayList<SpecificationData>();
                    listChildData = new HashMap<String, List<SpecificationData>>();
                    if (jsonString.toString().trim().length() > 0) {
                        if (object.getInt("success") == 1) {


                            specificationDatas = new ArrayList<SpecificationData>();
                            if (object.has("brands")) {
                                JSONObject jsonObject = object.getJSONObject("brands");
                                SpecificationData specificationData = new SpecificationData();
                                specificationData.setParentName("Brands");
                                JSONArray options = jsonObject
                                        .getJSONArray("filter_ids");
                                for (int j = 0; j < options.length(); j++) {
                                    JSONObject jsonOptionObject = options
                                            .getJSONObject(j);
                                    SpecificationData specdata = new SpecificationData();
                                    specdata.setOptionId(jsonOptionObject
                                            .getString("filter_id"));
                                    specdata.setParentId(jsonOptionObject
                                            .getString("filter_name"));
                                    specificationDatas.add(specdata);
                                }
                                specificationData
                                        .setSpecificationDataslist(specificationDatas);
                                listDataHeader.add(specificationData);
                            }

                            JSONArray array = object.getJSONArray("filter_ids");
                            if (array.length() > 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    specificationDatas = new ArrayList<SpecificationData>();
                                    JSONObject jsonObject1 = array.getJSONObject(i);
                                    SpecificationData specificationData1 = new SpecificationData();
                                    specificationData1.setParentName(jsonObject1
                                            .getString("cate_name"));
                                    specificationData1.setParentId(jsonObject1
                                            .getString("spec_id"));
                                    JSONArray options1 = jsonObject1
                                            .getJSONArray("filter_ids");
                                    for (int j = 0; j < options1.length(); j++) {

                                        JSONObject jsonOptionObject = options1
                                                .getJSONObject(j);
                                        SpecificationData specdata = new SpecificationData();
                                        specdata.setOptionId(jsonOptionObject
                                                .getString("filter_id").trim());
                                        specdata.setParentId(jsonOptionObject
                                                .getString("filter_name"));
                                        specificationDatas.add(specdata);
                                    }
                                    specificationData1
                                            .setSpecificationDataslist(specificationDatas);
                                    listDataHeader.add(specificationData1);
                                }
                            }
                            expadapter = new FilterAdapter(FilterActivity.this,
                                    listDataHeader, listChildData);
                            explistview.setAdapter(expadapter);
                        }
                    } else {

                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences.getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        // invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(),
                        NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(),
                        CompareProductListActivity.class);
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(),
                        CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(),
                        OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(),
                            AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        setResult(2);
        finish();
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    public class ExecuteFilterActivity extends
            AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(FilterActivity.this, "", "Loading...",
                    true, false);
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("cate_id", getIntent()
                    .getStringExtra("category_id")));
            param.add(new BasicNameValuePair("status", getIntent().getStringExtra("status")));
            Log.e("Params are", param.toString());
            JSONObject object = parser.makeHttpRequest(FilterActivity.this, urlFilter, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            listDataHeader = new ArrayList<SpecificationData>();
            listChildData = new HashMap<String, List<SpecificationData>>();

            try {

                if (result.trim().length() > 0) {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {


                        specificationDatas = new ArrayList<SpecificationData>();
                        if (object.has("brands")) {
                            JSONObject jsonObject = object.getJSONObject("brands");
                            SpecificationData specificationData = new SpecificationData();
                            specificationData.setParentName("Brands");
                            JSONArray options = jsonObject
                                    .getJSONArray("filter_ids");
                            for (int j = 0; j < options.length(); j++) {
                                JSONObject jsonOptionObject = options
                                        .getJSONObject(j);
                                SpecificationData specdata = new SpecificationData();
                                specdata.setOptionId(jsonOptionObject
                                        .getString("filter_id"));
                                specdata.setParentId(jsonOptionObject
                                        .getString("filter_name"));
                                specificationDatas.add(specdata);
                            }
                            specificationData
                                    .setSpecificationDataslist(specificationDatas);
                            listDataHeader.add(specificationData);
                        }

                        JSONArray array = object.getJSONArray("filter_ids");
                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                specificationDatas = new ArrayList<SpecificationData>();
                                JSONObject jsonObject1 = array.getJSONObject(i);
                                SpecificationData specificationData1 = new SpecificationData();
                                specificationData1.setParentName(jsonObject1
                                        .getString("cate_name"));
                                specificationData1.setParentId(jsonObject1
                                        .getString("spec_id"));
                                JSONArray options1 = jsonObject1
                                        .getJSONArray("filter_ids");
                                for (int j = 0; j < options1.length(); j++) {

                                    JSONObject jsonOptionObject = options1
                                            .getJSONObject(j);
                                    SpecificationData specdata = new SpecificationData();
                                    specdata.setOptionId(jsonOptionObject
                                            .getString("filter_id"));
                                    specdata.setParentId(jsonOptionObject
                                            .getString("filter_name"));
                                    specificationDatas.add(specdata);
                                }
                                specificationData1
                                        .setSpecificationDataslist(specificationDatas);
                                listDataHeader.add(specificationData1);
                            }
                        }
                        expadapter = new FilterAdapter(FilterActivity.this,
                                listDataHeader, listChildData);
                        explistview.setAdapter(expadapter);
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();

            }

        }

    }
}
