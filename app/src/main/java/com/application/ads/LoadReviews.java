package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.ReviewData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class LoadReviews extends Activity {

    SharedPreferences preferences;
    JSONParser parser;
    List<ReviewData> reviewList;
    TextView txtReviewStoreName, txtReviewDescription;
    LinearLayout linearAllReviews;
    private String urlLoadReviews = DBConnection.BASEURL + "load_all_reviews.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reviews);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText("All Reviews");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
        parser = new JSONParser();
        reviewList = new ArrayList<ReviewData>();
        txtReviewStoreName = (TextView) findViewById(R.id.txtReviewStoreName);
        txtReviewStoreName.setText(getIntent().getStringExtra("storeName"));
        txtReviewDescription = (TextView) findViewById(R.id.txtReviewDescription);
        txtReviewDescription.setText(getIntent().getStringExtra("storeCoupons") + " Offers to Earn Upto " +
                getIntent().getStringExtra("cashBack") + " Extra Cashback");
        linearAllReviews = (LinearLayout) findViewById(R.id.linearAllReviews);

        new ExecuteLoadAllReviews().execute();
/*
        List<NameValuePair> param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("store_id",getIntent().getStringExtra("storeId")));*/

	/*	String url=urlLoadReviews+"?store_id="+getIntent().getStringExtra("storeId");
		volleyJsonObjectRequest(url);*/


    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    reviewList = new ArrayList<ReviewData>();

                    if (jsonString.toString().trim().length() > 0) {
                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("reviews");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                ReviewData data = new ReviewData();
                                data.setUserId(object.getString("user_id"));
                                data.setComments(object.getString("comments"));
                                data.setRating(object.getString("rating"));
                                data.setUserName(object.getString("user_name"));
                                reviewList.add(data);
                            }
                            fillReviewsLayout(reviewList);
                        } else {
                            Toast.makeText(LoadReviews.this,
                                    "Review Adding Failed..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                        Toast.makeText(LoadReviews.this,
                                "Please check your Internet connection or Server..",
                                Toast.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private void fillReviewsLayout(List<ReviewData> reviewList) {
        linearAllReviews.removeAllViews();
        TextView txtReviewSNo, txtReviewUserName, txtReviewComments, txtReviewRating;
        for (int i = 0; i < reviewList.size(); i++) {
            View view = getLayoutInflater().inflate(R.layout.review_details, null);
            txtReviewSNo = (TextView) view.findViewById(R.id.txtReviewSNo);
            txtReviewUserName = (TextView) view.findViewById(R.id.txtReviewUserName);
            txtReviewComments = (TextView) view.findViewById(R.id.txtReviewComments);
            txtReviewRating = (TextView) view.findViewById(R.id.txtReviewRating);
            txtReviewSNo.setText((i + 1) + "");
            txtReviewUserName.setText(reviewList.get(i).getUserName());
            txtReviewComments.setText(reviewList.get(i).getComments());
            txtReviewRating.setText(reviewList.get(i).getRating());
            linearAllReviews.addView(view);
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteLoadAllReviews extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(LoadReviews.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("store_id", getIntent().getStringExtra("storeId")));
            object = parser.makeHttpRequest(LoadReviews.this, urlLoadReviews, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            reviewList = new ArrayList<ReviewData>();
            if (result.trim().length() > 0) {
                JSONObject object1;
                try {
                    object1 = new JSONObject(result);
                    if (object1.getInt("success") == 1) {
                        JSONArray array = object1.getJSONArray("reviews");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            ReviewData data = new ReviewData();
                            data.setUserId(object.getString("user_id"));
                            data.setComments(object.getString("comments"));
                            data.setRating(object.getString("rating"));
                            data.setUserName(object.getString("user_name"));
                            reviewList.add(data);
                        }
                        fillReviewsLayout(reviewList);
                    } else {
                        Toast.makeText(LoadReviews.this,
                                "Review Adding Failed..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(LoadReviews.this,
                        "Please check your Internet connection or Server..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }

    }
}
