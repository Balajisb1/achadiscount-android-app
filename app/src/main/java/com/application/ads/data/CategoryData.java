package com.application.ads.data;

import java.util.List;

public class CategoryData {

    private String catid;
    private String catname;
    private String catimg;
    private List<SubCategoryData> subcatlist;

    private String tempcatid;

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCatimg() {
        return catimg;
    }

    public void setCatimg(String catimg) {
        this.catimg = catimg;
    }

    public List<SubCategoryData> getSubcatlist() {
        return subcatlist;
    }

    public void setSubcatlist(List<SubCategoryData> subcatlist) {
        this.subcatlist = subcatlist;
    }

    public String getTempcatid() {
        return tempcatid;
    }

    public void setTempcatid(String tempcatid) {
        this.tempcatid = tempcatid;
    }

}
