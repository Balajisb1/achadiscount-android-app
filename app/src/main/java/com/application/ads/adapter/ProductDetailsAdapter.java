package com.application.ads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.LoginActivity;
import com.application.ads.ProductOverView;
import com.application.ads.R;
import com.application.ads.data.CompareProducts;
import com.application.ads.data.ProductDetails;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.application.ads.data.CompareProducts.productdetails;


public class ProductDetailsAdapter extends BaseAdapter {

    List<ProductDetails> productdetailslist;
    Context context;
    LayoutInflater layoutInflater;
    SessionManager manager;
    List<String> productdetailsfeat;
    private ProgressDialog dialog;
    String urlAddOrRemoveProductsFromWishList = DBConnection.BASEURL
            + "add_to_wishlist.php";
    private SharedPreferences preferences;

    public ProductDetailsAdapter(Context context, List<ProductDetails> productdetailslist) {
        this.context = context;
        this.productdetailslist = productdetailslist;
        manager = new SessionManager(context);
        preferences = context.getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productdetailslist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return productdetailslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int m = position;
        //	Log.e("Key is :", productdetailslist.get(position).getKey_feature().toString());
        final ViewHolder viewHolder;
        View view = null;
        if (view == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.activity_product_detail_details, null);
            viewHolder.speclinear = (LinearLayout) view.findViewById(R.id.speclinear);
            viewHolder.prodimg = (ImageView) view.findViewById(R.id.prodimg);
            viewHolder.prodprice = (TextView) view.findViewById(R.id.prodprice);
            viewHolder.prodname = (TextView) view.findViewById(R.id.prodname);
            viewHolder.prodcompare = (TextView) view.findViewById(R.id.prodcompare);
            viewHolder.prodrating = (RatingBar) view.findViewById(R.id.prodrating);
            viewHolder.imgProductListingDetailFavourites = (ImageView) view
                    .findViewById(R.id.imgProductListingDetailFavourites);
    /*	LayerDrawable stars = (LayerDrawable) viewHolder.prodrating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/


            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();

        }

        viewHolder.prodcompare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = (Integer) v.getTag();
                ProductDetails productdetails = productdetailslist.get(pos);
                /*if (!CompareProducts.isAlreadyInCompare(productdetailslist
                        .get(pos))) {
					if (CompareProducts.getProducts().size() >= 2) {
						Toast.makeText(context, "Maximum compare 2 products",
								5000).show();
					} else {
					if(CompareProducts.addToCompare(productdetails)){
						viewHolder.prodcompare.setText("Remove from Compare");
						productdetailslist.get(pos).setSelected(true);
					}
				else{
					if(CompareProducts.removeFromCompare(productdetails)){
						viewHolder.prodcompare.setText("Add to Compare");
						productdetailslist.get(pos).setSelected(false);
					}
				}
				}
				}*/


                if (!CompareProducts.isAlreadyInCompare(productdetailslist
                        .get(pos))) {
                    if (CompareProducts.getProducts().size() >= 2) {
                        Toast.makeText(context, "Maximum compare 2 products",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        if (CompareProducts.addToCompare(productdetailslist
                                .get(pos))) {
                            viewHolder.prodcompare.setText("Remove from Compare");
							/*Toast.makeText(context,
									"Successfully added to compare list",
									Toast.LENGTH_LONG).show();*/
                            productdetailslist.get(pos).setSelected(true);

                        }
                    }
                } else {
                    if (CompareProducts.removeFromCompare(productdetailslist
                            .get(pos))) {
                        viewHolder.prodcompare.setText("Add to Compare");
						/*Toast.makeText(context,
								"Successfully removed from compare list",
								Toast.LENGTH_LONG).show();*/
                        productdetailslist.get(pos).setSelected(false);
                    }
                }
            }
        });

        viewHolder.prodcompare.setTag(position);
        viewHolder.speclinear.setTag(position);

        View sepview = (View) view.findViewById(R.id.sepview);
        //int specpos=(Integer) viewHolder.speclinear.getTag();
        if (productdetailslist.size() > 0) {
            if (productdetailslist.get(position).getKey_feature() != "" || productdetailslist.get(position).getKey_feature() != null) {
                productdetailsfeat = new ArrayList<String>();
                productdetailsfeat = Arrays.asList(productdetailslist.get(position).getKey_feature().toString().split(","));

                if (productdetailslist.get(position).getKey_feature().toString().length() > 0) {
                    viewHolder.speclinear.removeAllViews();
                    for (int i = 0; i < productdetailsfeat.size(); i++) {
                        View specview = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_spec_title, null);
                        TextView spectitle = (TextView) specview.findViewById(R.id.spectitle);
                        spectitle.setText(productdetailsfeat.get(i));
                        viewHolder.speclinear.addView(specview);
                    }
                } else {
                    sepview.setVisibility(View.GONE);

                }
            }
        }
        if (productdetailslist.size() > 0) {

            if (productdetailslist.get(position).isSelected()) {
                for (int i = 0; i < productdetails.size(); i++) {
                    if (productdetails.get(i).getProduct_id().equalsIgnoreCase(productdetailslist.get(position).getProduct_id())) {

                        viewHolder.prodcompare.setText("Remove from Compare");
                    } else {
                        viewHolder.prodcompare.setText("Add to Compare");
                    }
                }
            } else {
                viewHolder.prodcompare.setText("Add to Compare");
            }

            viewHolder.prodrating.setRating(Float.parseFloat(productdetailslist.get(position).getRating()));
            viewHolder.prodprice.setText("Rs." + productdetailslist.get(position).getProduct_price());
            viewHolder.prodname.setText(productdetailslist.get(position).getProduct_name());
        }
        viewHolder.imgProductListingDetailFavourites.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (manager.checkLogin()) {
                    // Remove from wish list
                    if (Integer.parseInt(productdetailslist.get(m)
                            .getProWishListStatus().trim()) > 0) {

                        Toast.makeText(context, "It is already added in wishlist", Toast.LENGTH_SHORT).show();
//                        new AddToWishList("1", productdetailslist.get(m), viewHolder.imgProductListingDetailFavourites, context).execute(); // Remove from wish
                        // list
                    } else {

                        addToWishList(m,0,viewHolder);

                    }
                } else {
                    context.startActivity(new Intent(context,
                            LoginActivity.class));
                }

            }
        });

        viewHolder.prodimg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });


        viewHolder.prodname.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductOverView.class);
                intent.putExtra("parent_child_id", productdetailslist.get(m).getParent_child_id());
                intent.putExtra("prod_name", productdetailslist.get(m).getProduct_name());
                intent.putExtra("prod_img", productdetailslist.get(m).getProduct_image());
                intent.putExtra("prod_price", productdetailslist.get(m).getProduct_price());
                intent.putExtra("prod_rating", productdetailslist.get(m).getRating());
                intent.putExtra("prod_id", productdetailslist.get(m).getProduct_id());
                intent.putExtra("pp_id", productdetailslist.get(m).getPp_id());
                intent.putExtra("prod_desc", productdetailslist.get(m).getKey_feature().toString().trim());
                context.startActivity(intent);
            }
        });

        if (productdetailslist.size() > 0) {
            if (productdetailslist.get(position).getProWishListStatus().equals(null)
                    || productdetailslist.get(position).getProWishListStatus()
                    .equals("")) {
                Log.e("", "Null");
            } else {
                if (Integer.parseInt(productdetailslist.get(position)
                        .getProWishListStatus().trim()) > 0) {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.filled_heart);
                } else {
                    viewHolder.imgProductListingDetailFavourites
                            .setImageResource(R.drawable.heart);
                }

            }


            Picasso.with(context).load(DBConnection.PRODUCT_IMGURL + productdetailslist.get(position).getProduct_image()).placeholder(R.drawable.adslogo).resize(144, 0).into(viewHolder.prodimg);
        }
        //   notifyDataSetChanged();
        return view;
    }

    class ViewHolder {
        protected ImageView prodimg, imgProductListingDetailFavourites;
        protected TextView prodprice, prodname, prodcompare;
        protected RatingBar prodrating;
        protected LinearLayout speclinear;

    }

    private void addToWishList(final int m, final int i, final ViewHolder viewHolder) {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = urlAddOrRemoveProductsFromWishList + "?user_id=" + preferences.getString(SessionManager.KEY_USERID, "0") + "&product_id=" + productdetailslist.get(m).getProduct_id() + "&wish_list_status=" + i;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                dialog.dismiss();
                if (result.trim().length() > 0) {
                    try {
                        JSONObject object1 = new JSONObject(result);
                        if (object1.getInt("success") == 1) {

                            if (i == 0) {
                                Toast.makeText(context, "Products Added to Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.filled_heart);
                               // productdetailslist.clear();

                                productdetailslist.get(m).setProWishListStatus("1");
                            } else if (i == 1) {
                                Toast.makeText(context, "Products Removed from Wish List..", Toast.LENGTH_LONG).show();
                                viewHolder.imgProductListingDetailFavourites.setImageResource(R.drawable.heart);
                            }

                        } else {
                            Toast.makeText(context,
                                    "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context,
                            "Something goes wrong..", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }
}
