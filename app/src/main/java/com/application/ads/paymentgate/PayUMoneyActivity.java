package com.application.ads.paymentgate;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PayUMoneyActivity extends Activity {

    WebView webView;
    String merchant_key = "OwGF14";    // Merchant key
    String salt = "RjWAdXh0";        // Salt Key
    String action1 = "";
    String base_url = "https://test.payu.in";    // For Sandbox Mode
    //	String base_url = "https://secure.payu.in";  // For Live Mode
    String txnid = "TXN8367286482920";
    String amount = "";
    String productInfo = "";
    String firstName = "Vijay Kiruba";
    String emailId = "gkm.logistics@gmail.com";
    ProgressDialog progressDialog;
    Handler mHandler = new Handler();
    private String SUCCESS_URL = "https://achadiscount.in/cashback/success_withdraw/MTUxOQ";
    private String FAILED_URL = "https://achadiscount.in/";
    private String phone = "9876543210";
    private String serviceProvider = "payu_paisa";
    private String hash = "";

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        webView = new WebView(this);
        setContentView(webView);
        amount = getIntent().getStringExtra("amount");
        progressDialog = new ProgressDialog(PayUMoneyActivity.this);
        progressDialog.setMessage("Redirecting..");

        productInfo = getIntent().getStringExtra("coup_title");

        Random rand = new Random();
        String rndm = Integer.toString(rand.nextInt())
                + (System.currentTimeMillis() / 1000L);

        Log.e("txnid", txnid);
        txnid = hashCal("SHA-256", rndm).substring(0, 20);

        hash = hashCal("SHA-512", merchant_key + "|" + txnid + "|" + amount
                + "|" + productInfo + "|" + firstName + "|" + emailId
                + "|||||||||||" + salt);

        action1 = base_url.concat("/_payment");

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                progressDialog.show();
            }

         /*   @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
                // TODO Auto-generated method stub
                Toast.makeText(PayUMoneyActivity.this, "Oh no! " + description,
                        Toast.LENGTH_SHORT).show();
            }
*/
      /*      @Override
            public void onReceivedSslError(WebView view,
                    SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                Toast.makeText(PayUMoneyActivity.this, "SslError! " + error,
                        Toast.LENGTH_SHORT).show();
                handler.proceed();
            }*/

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("code=14")) {
                    view.loadUrl(url);
                }

                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                progressDialog.dismiss();
                if (url.equals(SUCCESS_URL)) {
                    finish();
                }
            }
        });


        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearHistory();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setLoadWithOverviewMode(false);
        webView.addJavascriptInterface(new PayUJavaScriptInterface(PayUMoneyActivity.this),
                "PayUMoney");
        Map<String, String> mapParams = new HashMap<>();
        mapParams.put("key", merchant_key);
        mapParams.put("hash", hash);
        mapParams.put("txnid", txnid);
        mapParams.put("service_provider", "payu_paisa");
        mapParams.put("amount", amount);
        mapParams.put("firstname", firstName);
        mapParams.put("email", emailId);
        mapParams.put("phone", phone);

        mapParams.put("productinfo", productInfo);
        mapParams.put("surl", SUCCESS_URL);
        mapParams.put("furl", FAILED_URL);
        mapParams.put("lastname", "SB");

        mapParams.put("address1", "");
        mapParams.put("address2", "");
        mapParams.put("city", "");
        mapParams.put("state", "");

        mapParams.put("country", "");
        mapParams.put("zipcode", "");
        mapParams.put("udf1", "");
        mapParams.put("udf2", "");

        mapParams.put("udf3", "");
        mapParams.put("udf4", "");
        mapParams.put("udf5", "");
        webview_ClientPost(webView, action1, mapParams.entrySet());

    }

    public void webview_ClientPost(WebView webView, String url,
                                   Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();

        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>",
                url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format(
                    "<input name='%s' type='hidden' value='%s' />",
                    item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("WEBViEW", "webview_ClientPost called");
        webView.loadData(sb.toString(), "text/html", "utf-8");
    }

    public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }

    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException nsae) {
        }
        return hexString.toString();

    }

    private class GetData extends AsyncTask<String, Void, JSONObject> {

        ProgressDialog progressDialog;
        String URL;

        public GetData(String URL) {
            this.URL = URL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(PayUMoneyActivity.this, "", "");
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            String response;

            try {
                Log.e("URL", URL);
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(URL);
                HttpResponse responce = httpclient.execute(httppost);
                HttpEntity httpEntity = responce.getEntity();

                response = EntityUtils.toString(httpEntity);
                Log.d("response is", response);

                return new JSONObject(response);

            } catch (Exception ex) {

                ex.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progressDialog.dismiss();

            if (result != null) {
                try {
                    JSONObject jobj = result.getJSONObject("result");

                    String status = jobj.getString("status");

                    if (status.equals("true")) {
                        JSONArray array = jobj.getJSONArray("data");

                        for (int x = 0; x < array.length(); x++) {
                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("name", array.getJSONObject(x).getString("name"));

                            map.put("date", array.getJSONObject(x).getString("date"));

                            map.put("description", array.getJSONObject(x).getString("description"));

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(PayUMoneyActivity.this, "Network Problem", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class PayUJavaScriptInterface {
        Context mContext;
        /**
         * Instantiate the interface and set the context
         */
        PayUJavaScriptInterface(Context c) {
            mContext = c;
        }

        public void success(long id, final String paymentId) {

            mHandler.post(new Runnable() {

                public void run() {
                    mHandler = null;
                    Toast.makeText(PayUMoneyActivity.this, "Success",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
