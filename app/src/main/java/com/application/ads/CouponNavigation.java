package com.application.ads;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CouponData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CouponNavigation extends FragmentActivity {

    private static final int FILTER_ID = 0;
    protected TextView toolbartitle;
    List<CouponData> couponlist;
    SharedPreferences preferences;
    SessionManager manager;
    int totalCount = 0;
    Bundle bundle;
    JSONParser parser;
    View view_Group;
    boolean drawerstatus = false;
    private DrawerLayout mDrawerLayout;
    private android.app.Fragment fragment = null;
    private ListView expListView;
    private CouponListAdapter listAdapter;
    private int lastExpandedPosition = -1;
    private String urlLoadMainCategories = DBConnection.BASEURL + "load_coupon_categories.php";
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    // Catch the events related to the drawer to arrange views according to this
    // action if necessary...
    private DrawerListener mDrawerListener = new DrawerListener() {

        @Override
        public void onDrawerStateChanged(int status) {

        }

        @Override
        public void onDrawerSlide(View view, float slideArg) {

        }

        @Override
        public void onDrawerOpened(View view) {
            getActionBar().setTitle(mDrawerTitle);
            // calling onPrepareOptionsMenu() to hide action bar icons
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            getActionBar().setTitle("Coupon Discounts");
            // calling onPrepareOptionsMenu() to show action bar icons
            invalidateOptionsMenu();
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        getActionBar().setTitle("Coupon Discounts");
        parser = new JSONParser();
        setContentView(R.layout.activity_coupon_navigation);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        getActionBar().setCustomView(toolbarview);
        ImageView homedrawer = (ImageView) toolbarview.findViewById(R.id.homedrawer);
        homedrawer.setVisibility(View.VISIBLE);
        bundle = new Bundle();
        homedrawer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerstatus) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    drawerstatus = true;
                } else {
                    drawerstatus = false;
                    mDrawerLayout.closeDrawers();
                }

            }
        });
        toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        toolbartitle.setText(TopCouponsFragment.toolbartitle);
        toolbartitle.setText("Coupons");
        getActionBar().setCustomView(toolbarview);
        mTitle = mDrawerTitle = getTitle();
        bundle.putString("toolbartitle", toolbartitle.getText().toString());
        //fragment = new TopCouponsFragment();
        fragment = new CouponsHome();
        fragment.setArguments(bundle);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);
        setUpDrawer();
        manager = new SessionManager(CouponNavigation.this);
        getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
        mDrawerLayout.closeDrawer(expListView);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.nav_drawer, // nav menu toggle icon
                R.string.app_name, // nav drawer open - description for
                R.string.app_name // nav drawer close - description for
        ) {
            @Override
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("Coupon Discounts");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Coupon Discounts");
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        makeActionOverflowMenuShown();

    }

    private void makeActionOverflowMenuShown() {
        // devices with hardware menu button (e.g. Samsung ) don't show action
        // overflow menu
        try {
            final ViewConfiguration config = ViewConfiguration.get(this);
            final Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (final Exception e) {
            Log.e("", e.getLocalizedMessage());
        }
    }

    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            /*
             * mDrawerLayout.setScrimColor(getResources().getColor(
			 * android.R.color.transparent));
			 */
        mDrawerLayout.setDrawerListener(mDrawerListener);
        expListView = (ListView) findViewById(R.id.coupon_slidermenu);
        prepareListData();

        // expandable list view click listener

        expListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                mDrawerLayout.closeDrawer(expListView);
                if (position == couponlist.size() - 1) {
                    if (manager.checkLogin()) {
                        Intent intent = new Intent(CouponNavigation.this, AddReferalActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(CouponNavigation.this, CouponCategoryList.class);
                    intent.putExtra("cate_url", couponlist.get(position).getCategoryURL());
                    startActivity(intent);
                }
            }
        });
            /*
            expListView.setOnChildClickListener(new OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					// setbackground color for list that is selected in child group


					Toast.makeText(CouponNavigation.this,categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_name() , 5000).show();

					getFragmentManager().beginTransaction()
							.replace(R.id.frame_container, fragment).commit();
					expListView.setItemChecked(childPosition, true);
					mDrawerLayout.closeDrawer(expListView);
					Intent intent=new Intent(CouponNavigation.this, ProductDetailsActivity.class);
					intent.putExtra("prod_parent_name", categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_name());
					intent.putExtra("parent_child_id", categoryList.get(groupPosition).getSubcatlist().get(childPosition).getSubcat_id());
				//	Toast.makeText(SubCategoryActivity.this, subcategorydata.get(position).getSubcat_id(), 5000).show();
					startActivity(intent);
					return false;
				}
			});
			*/


			/*expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			    @Override
			    public void onGroupExpand(int groupPosition) {
			            if (lastExpandedPosition != -1
			                    && groupPosition != lastExpandedPosition) {
			            	expListView.collapseGroup(lastExpandedPosition);
			            }
			            lastExpandedPosition = groupPosition;
			    }
			});*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        String userid = preferences
                .getString(SessionManager.KEY_USERID, "");
        if (userid == null || userid == "") {
            MenuItem menuItem = menu.findItem(R.id.logout);
            menuItem.setVisible(false);
        }
        //invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_home:
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.action_compare:
                Intent intent2 = new Intent(getApplicationContext(), ProductDetailsActivity.class);
                intent2.putExtra("parent_child_id", preferences.getString(SessionManager.KEY_COMP_ID, "20003"));
                startActivity(intent2);
                break;

            case R.id.action_coupons:
                Intent intent1 = new Intent(getApplicationContext(), CouponNavigation.class);
                startActivity(intent1);
                break;
            case R.id.action_offline:
                Intent intent3 = new Intent(getApplicationContext(), OfflineHomeNavigation.class);
                startActivity(intent3);
                break;
            case R.id.logout:
                manager.logoutUser();
                invalidateOptionsMenu();
                break;
            case R.id.myaccount:

                if (manager.checkLogin()) {
                    Intent intent5 = new Intent(getApplicationContext(), AccountTitle.class);
                    startActivity(intent5);
                }
                break;

            default:
                if (mDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareListData() {
//        new LoadAllCaegory().execute();

        String url = urlLoadMainCategories;
        volleyJsonObjectRequest(url);


    }


    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    couponlist = new ArrayList<CouponData>();

                    if (response.toString().trim().length() > 0) {

                        if (jsonObject.getInt("success") == 1) {
                            JSONArray array = jsonObject.getJSONArray("category");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                CouponData data = new CouponData();
                                data.setCategoryName(object1.getString("cate_name"));
                                data.setCategoryId(object1.getString("cate_id"));
                                data.setCategoryURL(object1.getString("cate_url"));
                                couponlist.add(data);
                            }

                            CouponData data = new CouponData();
                            data.setCategoryName("Refer and Earn");
                            couponlist.add(data);
                            listAdapter = new CouponListAdapter(CouponNavigation.this, couponlist);
                            expListView.setAdapter(listAdapter);

                        } else {
                            Toast.makeText(CouponNavigation.this, "No Data Found..", Toast.LENGTH_LONG).show();
                        }
                    } else {

                    }
//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


    class LoadAllCaegory extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CouponNavigation.this);
            dialog.setMessage("Loading..");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            object = parser.makeHttpRequest(CouponNavigation.this, urlLoadMainCategories, "GET", param);
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();

            couponlist = new ArrayList<CouponData>();

            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray array = object.getJSONArray("category");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            CouponData data = new CouponData();
                            data.setCategoryName(object1.getString("cate_name"));
                            data.setCategoryId(object1.getString("cate_id"));
                            data.setCategoryURL(object1.getString("cate_url"));
                            couponlist.add(data);
                        }

                        CouponData data = new CouponData();
                        data.setCategoryName("Refer and Earn");
                        couponlist.add(data);
                        listAdapter = new CouponListAdapter(CouponNavigation.this, couponlist);
                        expListView.setAdapter(listAdapter);

                    } else {
                        Toast.makeText(CouponNavigation.this, "No Data Found..", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    public class CouponListAdapter extends BaseAdapter {

        LayoutInflater inflater;
        private Context _context;
        // child data in format of header title, child title
        private List<CouponData> couponlist; // header titles

        public CouponListAdapter(Context context,
                                 List<CouponData> couponlist) {
            this._context = context;
            this.couponlist = couponlist;


        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return couponlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return couponlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.activity_coupon_category, null);
            TextView couponcategorytitle = (TextView) view.findViewById(R.id.couponcategorytitle);
            couponcategorytitle.setText(Html.fromHtml(couponlist.get(position).getCategoryName()));

            return view;
        }
    }
}
