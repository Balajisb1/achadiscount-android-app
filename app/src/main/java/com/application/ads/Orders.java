/*package com.application.ads;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ads.data.OrderData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

public class Orders extends Activity {

	JSONParser parser;
	private String urlLoadOrder = DBConnection.BASEURL + "orders.php";

	List<OrderData> orderDatas;
	TableLayout tblLayout;

	Spinner spnrUserOrdersCount;

	List<OrderData> list;

	EditText edtUserOrdersFilter;

	int rowCount = 2;
	int currentPageNo = 1;
SessionManager manager;
	LinearLayout linearPageNo;

	TextView txtPrevious;
	TextView txtNext, txtShowingResult;
	
	SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_orders);
		manager=new SessionManager(Orders.this);
		
		preferences=getSharedPreferences(SessionManager.PREF_NAME,SessionManager.PRIVATE_MODE);
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		View toolbarview=getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
		TextView toolbartitle=(TextView)toolbarview.findViewById(R.id.toolbartitle);
		toolbartitle.setText("Orders");
		getActionBar().setCustomView(toolbarview);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Orders");
		getActionBar().setTitle(""); 
		
		tblLayout = (TableLayout) findViewById(R.id.tblLayout);
		parser = new JSONParser();

		txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);

		linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

		txtPrevious = (TextView) findViewById(R.id.txtPrevious);
		txtPrevious.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					if (currentPageNo > 1) {
						currentPageNo -= 1;
						filterOption((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		txtNext = (TextView) findViewById(R.id.txtNext);
		txtNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					if (currentPageNo < (list.size() / rowCount)) {
						currentPageNo += 1;
						filterOption((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
					} else if (currentPageNo == (list.size() / rowCount)
							&& (list.size() % rowCount > 0)) {
						currentPageNo += 1;
						filterOption((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		edtUserOrdersFilter = (EditText) findViewById(R.id.edtUserOrdersFilter);
		edtUserOrdersFilter.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				filterOptionByEditText(edtUserOrdersFilter.getText().toString()
						.trim());
			}
		});

		spnrUserOrdersCount = (Spinner) findViewById(R.id.spnrUserOrdersCount);
		spnrUserOrdersCount.setAdapter(new ArrayAdapter<String>(Orders.this,
				android.R.layout.simple_spinner_item, new String[] { "2", "4",
						"10" }));
		spnrUserOrdersCount
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						
						try {
							if (orderDatas != null) {
								rowCount = Integer.parseInt(spnrUserOrdersCount
										.getSelectedItem().toString());
								filterOption((currentPageNo * rowCount) - rowCount,
										(currentPageNo * rowCount));
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

		new LoadOrderDetails().execute();
	}

	class LoadOrderDetails extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		JSONObject object;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(Orders.this);
			dialog.setMessage("Loading..");
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"0")));
			object = parser.makeHttpRequest(urlLoadOrder, "GET", param);
			if (object == null) {
				return "";
			} else {
				return object.toString();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();
			orderDatas = new ArrayList<OrderData>();
			if (result.toString().trim().length() > 0) {
				try {
					JSONObject object = new JSONObject(result);
					if (object.getInt("success") == 1) {
						JSONArray array = object.getJSONArray("orders");
						for (int i = 0; i < array.length(); i++) {
							JSONObject object1 = array.getJSONObject(i);
							OrderData data = new OrderData();
							data.setAmount(object1.getString("amount"));
							data.setDate(object1.getString("date"));
							data.setName(object1.getString("amount"));
							data.setQuantity(object1.getString("quantity"));
							data.setTotAmount(object1.getString("tot_amount"));
							orderDatas.add(data);
						}
						list = new ArrayList<OrderData>();
						list = orderDatas;
						loadTableRows((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
						createPageNo();
					} else {
						Toast.makeText(Orders.this, "No Data Found..",
								Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {
				Toast.makeText(Orders.this,
						"Please check your Internet connection OR server..",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private void loadTableRows(int start, int end) {
		tblLayout.removeAllViews();

		try {
			if (list.size() >= end) {
				for (int i = start; i < end; i++) {
					View view = getLayoutInflater().inflate(
							R.layout.user_orders_table_row, null);
					TextView txtOrderName = (TextView) view
							.findViewById(R.id.txtOrderName);
					TextView txtOrderQty = (TextView) view
							.findViewById(R.id.txtOrderQty);
					TextView txtOrderAmount = (TextView) view
							.findViewById(R.id.txtOrderAmount);
					TextView txtOrderTotalAmount = (TextView) view
							.findViewById(R.id.txtOrderTotalAmount);
					TextView txtOrderDate = (TextView) view
							.findViewById(R.id.txtOrderDate);
					txtOrderName.setText(list.get(i).getName());
					txtOrderQty.setText(list.get(i).getQuantity());
					txtOrderAmount.setText(list.get(i).getAmount());
					txtOrderTotalAmount.setText(list.get(i).getTotAmount());
					txtOrderDate.setText(list.get(i).getDate());
					tblLayout.addView(view);
				}
				String starting = String.valueOf(start + 1);
				txtShowingResult.setText("Showing " + starting + " to " + end
						+ " entries");
			} else {
				for (int i = start; i < list.size(); i++) {
					View view = getLayoutInflater().inflate(
							R.layout.user_orders_table_row, null);
					TextView txtOrderName = (TextView) view
							.findViewById(R.id.txtOrderName);
					TextView txtOrderQty = (TextView) view
							.findViewById(R.id.txtOrderQty);
					TextView txtOrderAmount = (TextView) view
							.findViewById(R.id.txtOrderAmount);
					TextView txtOrderTotalAmount = (TextView) view
							.findViewById(R.id.txtOrderTotalAmount);
					TextView txtOrderDate = (TextView) view
							.findViewById(R.id.txtOrderDate);
					txtOrderName.setText(list.get(i).getName());
					txtOrderQty.setText(list.get(i).getQuantity());
					txtOrderAmount.setText(list.get(i).getAmount());
					txtOrderTotalAmount.setText(list.get(i).getTotAmount());
					txtOrderDate.setText(list.get(i).getDate());
					tblLayout.addView(view);
				}
				String starting = String.valueOf(start + 1);
				txtShowingResult.setText("Showing " + starting + " to " + end
						+ " entries");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main,menu);
		String userid = preferences
				.getString(SessionManager.KEY_USERID, "");
		if (userid == null || userid == "") {
			MenuItem menuItem=menu.findItem(R.id.logout);
			menuItem.setVisible(false);
		}
		//invalidateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
			case R.id.action_home:
				Intent intent=new Intent(getApplicationContext(),NavigationActivity.class);
				startActivity(intent);
				finish();
				break;
			case R.id.action_compare:
				Intent intent2=new Intent(getApplicationContext(),CompareProductListActivity.class);
				startActivity(intent2);
				break;
			
			case R.id.action_coupons:
				Intent intent1=new Intent(getApplicationContext(),CouponNavigation.class);
				startActivity(intent1);
				break;
			case R.id.action_offline:
				Intent intent3=new Intent(getApplicationContext(),OfflineHomeNavigation.class);
				startActivity(intent3);
				break;
			case R.id.logout:
				manager.logoutUser();
				invalidateOptionsMenu();
				break;
			case R.id.myaccount:
				
				if(manager.checkLogin()){
				Intent intent5=new Intent(getApplicationContext(),AccountTitle.class);
				startActivity(intent5);
				}
				break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	
	
	
	
	
	
	
	public void filterOption(int start, int end) {
		try {
			createPageNo();
			loadTableRows((currentPageNo * rowCount) - rowCount,
					(currentPageNo * rowCount));
			
		} catch (Exception e) {
		}
	}


	
	
	
	
	
	
	
	public void filterOptionByEditText(String text) {
		try {
			list = new ArrayList<OrderData>();
			for (int i = 0; i < orderDatas.size(); i++) {
				if (orderDatas.get(i).getAmount().toLowerCase().trim()
						.contains(text.toLowerCase().trim())
						|| orderDatas.get(i).getDate().toLowerCase().trim()
								.contains(text.toLowerCase().trim())
						|| orderDatas.get(i).getName().toLowerCase().trim()
								.contains(text.toLowerCase().trim())
						|| orderDatas.get(i).getQuantity().toLowerCase().trim()
								.contains(text.toLowerCase().trim())
						|| orderDatas.get(i).getTotAmount().toLowerCase().trim()
								.contains(text.toLowerCase().trim())) {
					list.add(orderDatas.get(i));
				}
			}
			createPageNo();
			loadTableRows((currentPageNo * rowCount) - rowCount,
					(currentPageNo * rowCount));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	
	
	
	
	
	
	public void createPageNo() {
		linearPageNo.removeAllViews();
		try {
			int numOfPage = list.size() / rowCount;
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
			params.setMargins(5, 5, 5, 5);

			for (int i = 0; i < numOfPage; i++) {
				final int j = i;
				TextView textView = new TextView(Orders.this);
				textView.setTextColor(Color.parseColor("#29BAB0"));
				textView.setText(String.valueOf(i + 1));
				textView.setLayoutParams(params);
				textView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						currentPageNo = j + 1;
						loadTableRows((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
					}
				});
				linearPageNo.addView(textView);
			}

			if ((list.size() % rowCount) > 0) {
				final int j = numOfPage + 1;
				TextView textView = new TextView(Orders.this);
				textView.setTextColor(Color.parseColor("#29BAB0"));
				textView.setText(String.valueOf(numOfPage + 1));
				textView.setLayoutParams(params);
				textView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						currentPageNo = j;
						loadTableRows((currentPageNo * rowCount) - rowCount,
								(currentPageNo * rowCount));
					}
				});
				linearPageNo.addView(textView);
			}
		} catch (Exception e) {

		}
	}

}*/