package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.CashbackData;
import com.application.ads.data.EarningData;
import com.application.ads.data.EarningsData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyEarnings extends Activity {

    JSONParser parser;
    SharedPreferences preferences;
    SessionManager manager;
    TextView txtUserEarningCashbackEarning, txtUserEarningRefferalEarning,
            txtUserEarningAvailableEarning;
    TextView txtCashBackPendingEarnings, txtCashBackEarnings, txtPaidEarnings,
            txtAvailableForPaymentEarnings, txtWithdrawEarnings, txtTotalEarnings;
    TextView txtCashBackConfirmedReferralEarnings, txtCashBackPendingRefferalEarnings;
    EarningsData earningsData;
    Button btnRequestAmount;
    TableLayout tblLayout;
    TableLayout tblCBLayout;
    List<EarningData> list;
    List<EarningData> earningDatas;
    List<CashbackData> cblist;
    List<CashbackData> cashbackDatas;
    Spinner spnrUserPaymentsCount;
    Spinner spnrCBUserPaymentsCount;
    EditText edtUserPaymentFilter;
    EditText edtCBUserPaymentFilter;
    int rowCount = 2;
    int currentPageNo = 1;
    LinearLayout linearPageNo;
    LinearLayout linearCBPageNo;
    LinearLayout earningCashbackLinear;
    TextView txtPrevious;
    TextView noRecordsTxt;
    TextView txtCBPrevious, txtCBNext, txtCBShowingResult;
    TextView txtNext, txtShowingResult;
    private String urlLoadEarning = DBConnection.BASEURL + "earnings_list.php";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_earnings);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview.findViewById(R.id.toolbartitle);
        earningCashbackLinear = (LinearLayout) findViewById(R.id.earningCashbackLinear);


        toolbartitle.setText("My Earnings");
        getActionBar().setCustomView(toolbarview);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
        manager = new SessionManager(MyEarnings.this);
        list = new ArrayList<>();
        cblist = new ArrayList<>();

        parser = new JSONParser();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        txtCashBackPendingEarnings = (TextView) findViewById(R.id.txtCashBackPendingEarnings);
        txtCashBackEarnings = (TextView) findViewById(R.id.txtCashBackEarnings);
        txtPaidEarnings = (TextView) findViewById(R.id.txtPaidEarnings);
        txtAvailableForPaymentEarnings = (TextView) findViewById(R.id.txtAvailableForPaymentEarnings);
        txtWithdrawEarnings = (TextView) findViewById(R.id.txtWithdrawEarnings);
        txtTotalEarnings = (TextView) findViewById(R.id.txtTotalEarnings);
        noRecordsTxt = (TextView) findViewById(R.id.noRecordsTxt);

        txtCashBackConfirmedReferralEarnings = (TextView) findViewById(R.id.txtCashBackConfirmedReferralEarnings);
        txtCashBackPendingRefferalEarnings = (TextView) findViewById(R.id.txtCashBackPendingRefferalEarnings);

        tblLayout = (TableLayout) findViewById(R.id.tblLayout);


        txtShowingResult = (TextView) findViewById(R.id.txtShowingResult);

        linearPageNo = (LinearLayout) findViewById(R.id.linearPageNo);

        txtPrevious = (TextView) findViewById(R.id.txtPrevious);
        txtPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo > 1) {
                    currentPageNo -= 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        txtNext = (TextView) findViewById(R.id.txtNext);
        txtNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo < (list.size() / rowCount)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                } else if (currentPageNo == (list.size() / rowCount) && (list.size() % rowCount > 0)) {
                    currentPageNo += 1;
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        edtUserPaymentFilter = (EditText) findViewById(R.id.edtUserPaymentFilter);
        edtUserPaymentFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterOptionByEditText(edtUserPaymentFilter.getText().toString().trim());
            }
        });

        spnrUserPaymentsCount = (Spinner) findViewById(R.id.spnrUserPaymentsCount);
        spnrUserPaymentsCount.setAdapter(new ArrayAdapter<String>(MyEarnings.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrUserPaymentsCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (earningDatas != null) {
                    rowCount = Integer.parseInt(spnrUserPaymentsCount.getSelectedItem().toString());
                    filterOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

		
		/*cb
         *
		 */

        tblCBLayout = (TableLayout) findViewById(R.id.tblCBLayout);


        txtCBShowingResult = (TextView) findViewById(R.id.txtCBShowingResult);

        linearCBPageNo = (LinearLayout) findViewById(R.id.linearCBPageNo);

        txtCBPrevious = (TextView) findViewById(R.id.txtCBPrevious);
        txtCBPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo > 1) {
                    currentPageNo -= 1;
                    filterCBOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        txtCBNext = (TextView) findViewById(R.id.txtCBNext);
        txtCBNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentPageNo < (cblist.size() / rowCount)) {
                    currentPageNo += 1;
                    filterCBOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                } else if (currentPageNo == (cblist.size() / rowCount) && (cblist.size() % rowCount > 0)) {
                    currentPageNo += 1;
                    filterCBOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }
        });

        edtCBUserPaymentFilter = (EditText) findViewById(R.id.edtCBUserPaymentFilter);
        edtCBUserPaymentFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterCBOptionByEditText(edtCBUserPaymentFilter.getText().toString().trim());
            }
        });

        spnrCBUserPaymentsCount = (Spinner) findViewById(R.id.spnrCBUserPaymentsCount);
        spnrCBUserPaymentsCount.setAdapter(new ArrayAdapter<String>(MyEarnings.this, android.R.layout.simple_spinner_item, new String[]{"2", "4", "10"}));
        spnrCBUserPaymentsCount.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (cashbackDatas != null) {
                    rowCount = Integer.parseInt(spnrCBUserPaymentsCount.getSelectedItem().toString());
                    filterCBOption((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



		/*List<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("user_id",preferences.getString(SessionManager.KEY_USERID,"")));*/

/*
		String url=urlLoadEarning+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"");
		volleyJsonObjectRequest(url);
*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
			/*case R.id.action_home:
				Intent intent=new Intent(getApplicationContext(),NavigationActivity.class);
				startActivity(intent);
				finish();
				break;
			case R.id.action_compare:
				Intent intent2=new Intent(getApplicationContext(),CompareProductListActivity.class);
				startActivity(intent2);
				break;

			case R.id.action_coupons:
				Intent intent1=new Intent(getApplicationContext(),CouponNavigation.class);
				startActivity(intent1);
				break;
			case R.id.action_offline:
				Intent intent3=new Intent(getApplicationContext(),OfflineHomeNavigation.class);
				startActivity(intent3);
				break;
			case R.id.logout:
				manager.logoutUser();
				invalidateOptionsMenu();
				break;
			case R.id.myaccount:

				if(manager.checkLogin()){
				Intent intent5=new Intent(getApplicationContext(),AccountTitle.class);
				startActivity(intent5);
				}
				break;
*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main,menu);
		String userid = preferences
				.getString(SessionManager.KEY_USERID, "");
		if (userid == null || userid == "") {
			MenuItem menuItem=menu.findItem(R.id.logout);
			menuItem.setVisible(false);
		}
		//invalidateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}*/

    public void placeEarningList() {
        if (earningsData != null) {
            if (earningsData.getPendingCashBack() != null || earningsData.getPendingCashBack().equals("null")) {
                txtCashBackPendingEarnings.setText("Rs." + earningsData.getPendingCashBack() + ".00");
            } else {
                txtCashBackPendingEarnings.setText("Rs.0.00");
            }

            if (earningsData.getPaidEarnings() != null || earningsData.getPaidEarnings().equals("null")) {
                txtPaidEarnings.setText("Rs." + earningsData.getPaidEarnings() + ".00");
            } else {
                txtPaidEarnings.setText("Rs.0.00");
            }

            if (earningsData.getTotalEarnings() != null || earningsData.getTotalEarnings().equals("null")) {
                txtTotalEarnings.setText("Rs." + earningsData.getTotalEarnings() + ".00");
            } else {
                txtTotalEarnings.setText("Rs.0.00");
            }

            if (earningsData.getWithdrawApproval() != null || earningsData.getWithdrawApproval().equals("null")) {
                txtWithdrawEarnings.setText("Rs." + earningsData.getWithdrawApproval() + ".00");
            } else {
                txtWithdrawEarnings.setText("Rs.0.00");
            }

            if (earningsData.getAvailablePayment() != null || earningsData.getAvailablePayment().equals("null")) {
                txtAvailableForPaymentEarnings.setText("Rs." + earningsData.getAvailablePayment() + ".00");
            } else {
                txtAvailableForPaymentEarnings.setText("Rs.0.00");
            }

            if (earningsData.getConfirmedCashBack() != null || earningsData.getConfirmedCashBack().equals("null")) {
                txtCashBackEarnings.setText("Rs." + earningsData.getConfirmedCashBack() + ".00");
            } else {
                txtCashBackEarnings.setText("Rs.0.00");
            }

            if (earningsData.getConfirmedCashBack() != null || earningsData.getConfirmedCashBack().equals("null")) {
                txtCashBackConfirmedReferralEarnings.setText("Rs." + earningsData.getConfirmedCashBack() + ".00");
            } else {
                txtCashBackConfirmedReferralEarnings.setText("Rs.0.00");
            }

            if (earningsData.getPendingCashBack() != null || earningsData.getPendingCashBack().equals("null")) {
                txtCashBackPendingRefferalEarnings.setText("Rs." + earningsData.getPendingCashBack() + ".00");
            } else {
                txtCashBackPendingRefferalEarnings.setText("Rs.0.00");
            }


        } else {
            txtCashBackPendingEarnings.setText("Rs.0.00");
            txtPaidEarnings.setText("Rs.0.00");
            txtTotalEarnings.setText("Rs.0.00");
            txtWithdrawEarnings.setText("Rs.0.00");
            txtAvailableForPaymentEarnings.setText("Rs.0.00");
            txtCashBackEarnings.setText("Rs.0.00");
            txtCashBackConfirmedReferralEarnings.setText("Rs.0.00");
            txtCashBackConfirmedReferralEarnings.setText("Rs.0.00");
            txtCashBackPendingRefferalEarnings.setText("Rs.0.00");
        }
    }

    private void loadTableRows(int start, int end) {
        tblLayout.removeAllViews();

        if (list != null) {
            if (list.size() > 0) {

                if (list.size() >= end) {
                    for (int i = start; i < end; i++) {
                        View view = getLayoutInflater().inflate(R.layout.user_earnings_table_row, null);
                        TableRow tblrowUserEarning = (TableRow) view.findViewById(R.id.tblrowUserEarning);
                        TextView txtUserEarningDate = (TextView) view.findViewById(R.id.txtUserEarningDate);
                        TextView txtUserEarningPurchaseAmount = (TextView) view.findViewById(R.id.txtUserEarningPurchaseAmount);
                        Button btnUserEarningStatus = (Button) view.findViewById(R.id.btnUserEarningStatus);

                        txtUserEarningDate.setText(list.get(i).getTransactionDate());

                        txtUserEarningPurchaseAmount.setText(list.get(i).getTransactionAmount());

                        if (list.get(i).getTransactionStatus().equals("Paid")) {
                            btnUserEarningStatus.setBackgroundColor(Color.parseColor("#449d44"));
                        } else {
                            btnUserEarningStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                        }
                        btnUserEarningStatus.setText(list.get(i).getTransactionStatus());
                        tblLayout.addView(view);
                    }
                    String starting = String.valueOf(start + 1);
                    txtShowingResult.setText("Showing " + starting + " to " + end + " entries");
                } else {
                    for (int i = start; i < list.size(); i++) {
                        View view = getLayoutInflater().inflate(R.layout.user_earnings_table_row, null);
                        TableRow tblrowUserEarning = (TableRow) view.findViewById(R.id.tblrowUserEarning);
                        TextView txtUserEarningDate = (TextView) view.findViewById(R.id.txtUserEarningDate);
                        TextView txtUserEarningPurchaseAmount = (TextView) view.findViewById(R.id.txtUserEarningPurchaseAmount);
                        Button btnUserEarningStatus = (Button) view.findViewById(R.id.btnUserEarningStatus);


                        txtUserEarningDate.setText(list.get(i).getTransactionDate());
                        txtUserEarningPurchaseAmount.setText(list.get(i).getTransactionAmount());

                        if (list.get(i).getTransactionStatus().equals("Paid")) {
                            btnUserEarningStatus.setBackgroundColor(Color.parseColor("#449d44"));
                        } else {
                            btnUserEarningStatus.setBackgroundColor(Color.parseColor("#ED0C0C"));
                        }
                        btnUserEarningStatus.setText(list.get(i).getTransactionStatus());
                        tblLayout.addView(view);
                    }
                    String starting = String.valueOf(start + 1);
                    txtShowingResult.setText("Showing " + starting + " to " + list.size() + " entries");
                }

            } else {
            }
        }


    }

    public void filterOption(int start, int end) {
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void filterOptionByEditText(String text) {
        list = new ArrayList<EarningData>();
        for (int i = 0; i < earningDatas.size(); i++) {
            if (earningDatas.get(i).getRetailerName().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    earningDatas.get(i).getTransactionAmount().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    earningDatas.get(i).getTransactionStatus().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                list.add(earningDatas.get(i));
            }
        }
        createPageNo();
        loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void createPageNo() {
        linearPageNo.removeAllViews();

        int numOfPage = list.size() / rowCount;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        params.setMargins(5, 5, 5, 5);

        for (int i = 0; i < numOfPage; i++) {
            final int j = i;
            TextView textView = new TextView(MyEarnings.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(i + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j + 1;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearPageNo.addView(textView);
        }

        if ((list.size() % rowCount) > 0) {
            final int j = numOfPage + 1;
            TextView textView = new TextView(MyEarnings.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(numOfPage + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearPageNo.addView(textView);
        }
    }

    private void loadCBTableRows(int start, int end) {
        tblCBLayout.removeAllViews();

        Log.e("cblist size", cblist.size() + " Size ");

        if (cblist.size() > 0) {
            if (cblist.size() >= end) {
                for (int i = start; i < end; i++) {
                    View view = getLayoutInflater().inflate(R.layout.user_earnings_cbtable_row, null);
                    TableRow tblrowUserEarning = (TableRow) view.findViewById(R.id.tblrowCBUserEarning);
                    TextView txtCBUserEarningDate = (TextView) view.findViewById(R.id.txtCBUserEarningDate);
                    TextView txtCBUserEarningStore = (TextView) view.findViewById(R.id.txtCBUserEarningStore);
                    TextView txtCBUserEarningPurchaseAmount = (TextView) view.findViewById(R.id.txtCBUserEarningPurchaseAmount);

                    Button btnCBUserEarningStatus = (Button) view.findViewById(R.id.btnCBUserEarningStatus);

                    txtCBUserEarningDate.setText(cblist.get(i).getCashbackDateAdded());
                    txtCBUserEarningStore.setText(cblist.get(i).getCashbackStoreName());
                    txtCBUserEarningPurchaseAmount.setText("Rs. " + cblist.get(i).getCashbackAmount());


                    btnCBUserEarningStatus.setText(cblist.get(i).getCashbackStatus());
                    tblCBLayout.addView(view);
                }
                String starting = String.valueOf(start + 1);
                txtCBShowingResult.setText("Showing " + starting + " to " + end + " entries");
            } else {
                for (int i = start; i < cblist.size(); i++) {
                    View view = getLayoutInflater().inflate(R.layout.user_earnings_cbtable_row, null);
                    TableRow tblrowUserEarning = (TableRow) view.findViewById(R.id.tblrowCBUserEarning);
                    TextView txtCBUserEarningDate = (TextView) view.findViewById(R.id.txtCBUserEarningDate);
                    TextView txtCBUserEarningStore = (TextView) view.findViewById(R.id.txtCBUserEarningStore);
                    TextView txtCBUserEarningPurchaseAmount = (TextView) view.findViewById(R.id.txtCBUserEarningPurchaseAmount);

                    Button btnCBUserEarningStatus = (Button) view.findViewById(R.id.btnCBUserEarningStatus);

                    txtCBUserEarningDate.setText(cblist.get(i).getCashbackDateAdded());
                    txtCBUserEarningStore.setText(cblist.get(i).getCashbackStoreName());
                    txtCBUserEarningPurchaseAmount.setText("Rs. " + cblist.get(i).getCashbackAmount());


                    btnCBUserEarningStatus.setText(cblist.get(i).getCashbackStatus());
                    tblCBLayout.addView(view);
                }
                String starting = String.valueOf(start + 1);
                txtCBShowingResult.setText("Showing " + starting + " to " + cblist.size() + " entries");
            }

        } else {
        }


    }

    public void filterCBOption(int start, int end) {
        createCBPageNo();
        loadCBTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void filterCBOptionByEditText(String text) {
        cblist = new ArrayList<CashbackData>();
        for (int i = 0; i < cashbackDatas.size(); i++) {
            if (cashbackDatas.get(i).getCashbackAmount().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    cashbackDatas.get(i).getCashbackDateAdded().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    cashbackDatas.get(i).getCashbackStatus().toLowerCase().trim().contains(text.toLowerCase().trim()) ||
                    cashbackDatas.get(i).getCashbackStoreName().toLowerCase().trim().contains(text.toLowerCase().trim())) {
                cblist.add(cashbackDatas.get(i));
            }
        }
        createCBPageNo();
        loadCBTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
    }

    public void createCBPageNo() {
        linearCBPageNo.removeAllViews();

        int numOfPage = cblist.size() / rowCount;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        params.setMargins(5, 5, 5, 5);

        for (int i = 0; i < numOfPage; i++) {
            final int j = i;
            TextView textView = new TextView(MyEarnings.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(i + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j + 1;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearCBPageNo.addView(textView);
        }

        if ((cblist.size() % rowCount) > 0) {
            final int j = numOfPage + 1;
            TextView textView = new TextView(MyEarnings.this);
            textView.setTextColor(Color.parseColor("#29BAB0"));
            textView.setText(String.valueOf(numOfPage + 1));
            textView.setLayoutParams(params);
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPageNo = j;
                    loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                }
            });
            linearCBPageNo.addView(textView);
        }
    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        dialog = new ProgressDialog(MyEarnings.this);
        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(this);
        queue.getCache().clear();
        StringRequest cacheRequest = new StringRequest(0, url, new Response.Listener<String>() {
            public JSONObject object;

            @Override
            public void onResponse(String response) {
                earningDatas = new ArrayList<EarningData>();
                cashbackDatas = new ArrayList<CashbackData>();
                if (response.trim().length() > 0) {
                    try {
                        object = new JSONObject(response);
                        if (object.getInt("success") == 1) {
                            noRecordsTxt.setVisibility(View.GONE);
                            JSONArray array = object.getJSONArray("myearnings");
                            if (array.length() > 0) {
                                noRecordsTxt.setVisibility(View.GONE);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    EarningData data = new EarningData();
                                    data.setRetailerName(object1.getString("retailer_name"));
                                    data.setTransactionAmount(object1.getString("transation_amount"));
                                    data.setTransactionDate(object1.getString("transation_date"));
                                    data.setTransactionStatus(object1.getString("transation_status"));
                                    earningDatas.add(data);
                                }

                                list = new ArrayList<EarningData>();
                                list = earningDatas;
                                loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                                createPageNo();
                            }else{
                                //   noRecordsTxt.setVisibility(View.VISIBLE);
                            }
                            JSONArray cbarray = object.getJSONArray("cashbackearnings");
                            if (cbarray.length() > 0) {
                                for (int i = 0; i < cbarray.length(); i++) {
                                    JSONObject object1 = cbarray.getJSONObject(i);
                                    CashbackData data = new CashbackData();
                                    data.setCashbackAmount(object1.getString("cashback_amount"));
                                    data.setCashbackDateAdded(object1.getString("date_added"));
                                    data.setCashbackStatus(object1.getString("status"));
                                    data.setCashbackStoreName(object1.getString("affiliate_id"));
                                    cashbackDatas.add(data);
                                }
                                cblist = new ArrayList<CashbackData>();
                                cblist = cashbackDatas;
                                if (cblist.size() > 0) {
                                    loadCBTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                                    createCBPageNo();
                                    earningCashbackLinear.setVisibility(View.VISIBLE);
                                } else {
                                    earningCashbackLinear.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            noRecordsTxt.setVisibility(View.VISIBLE);
                            //Toast.makeText(MyEarnings.this,"No Data Found..",Toast.LENGTH_LONG).show();

                        }
                        earningsData = new EarningsData();
                        earningsData.setAvailablePayment(object.getString("available_balance"));
                        earningsData.setConfirmedCashBack(object.getString("ref_amount"));
                        earningsData.setPaidEarnings(object.getString("paid_amount"));
                        earningsData.setPendingCashBack(object.getString("pending_cashback"));
                        earningsData.setTotalEarnings(object.getString("total_earnings"));
                        earningsData.setWithdrawApproval(object.getString("waiting_bal"));
                        placeEarningList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MyEarnings.this, "Something goes wrong..",
                            Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        String url=urlLoadEarning+"?user_id="+preferences.getString(SessionManager.KEY_USERID,"");
        volleyJsonObjectRequest(url);
    }

    class LoadEarningDetails extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        JSONObject object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MyEarnings.this);
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("user_id", preferences.getString(SessionManager.KEY_USERID, "")));
            object = parser.makeHttpRequest(MyEarnings.this, urlLoadEarning, "GET", param);
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            earningDatas = new ArrayList<EarningData>();
            cashbackDatas = new ArrayList<CashbackData>();
            if (result.trim().length() > 0) {
                try {
                    object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        noRecordsTxt.setVisibility(View.GONE);
                        JSONArray array = object.getJSONArray("myearnings");
                        if (array.length() > 0) {
                            noRecordsTxt.setVisibility(View.GONE);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                EarningData data = new EarningData();
                                data.setRetailerName(object1.getString("retailer_name"));
                                data.setTransactionAmount(object1.getString("transation_amount"));
                                data.setTransactionDate(object1.getString("transation_date"));
                                data.setTransactionStatus(object1.getString("transation_status"));
                                earningDatas.add(data);
                            }

                            list = new ArrayList<EarningData>();
                            list = earningDatas;
                            loadTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                            createPageNo();
                        }else{
                         //   noRecordsTxt.setVisibility(View.VISIBLE);
                        }
                        JSONArray cbarray = object.getJSONArray("cashbackearnings");
                        if (cbarray.length() > 0) {
                            for (int i = 0; i < cbarray.length(); i++) {
                                JSONObject object1 = cbarray.getJSONObject(i);
                                CashbackData data = new CashbackData();
                                data.setCashbackAmount(object1.getString("cashback_amount"));
                                data.setCashbackDateAdded(object1.getString("date_added"));
                                data.setCashbackStatus(object1.getString("status"));
                                data.setCashbackStoreName(object1.getString("affiliate_id"));
                                cashbackDatas.add(data);
                            }
                            cblist = new ArrayList<CashbackData>();
                            cblist = cashbackDatas;
                            if (cblist.size() > 0) {
                                loadCBTableRows((currentPageNo * rowCount) - rowCount, (currentPageNo * rowCount));
                                createCBPageNo();
                                earningCashbackLinear.setVisibility(View.VISIBLE);
                            } else {
                                earningCashbackLinear.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        noRecordsTxt.setVisibility(View.VISIBLE);
                        //Toast.makeText(MyEarnings.this,"No Data Found..",Toast.LENGTH_LONG).show();

                    }
                    earningsData = new EarningsData();
                    earningsData.setAvailablePayment(object.getString("available_balance"));
                    earningsData.setConfirmedCashBack(object.getString("ref_amount"));
                    earningsData.setPaidEarnings(object.getString("paid_amount"));
                    earningsData.setPendingCashBack(object.getString("pending_cashback"));
                    earningsData.setTotalEarnings(object.getString("total_earnings"));
                    earningsData.setWithdrawApproval(object.getString("waiting_bal"));
                    placeEarningList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(MyEarnings.this, "Something goes wrong..",
                        Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }
}
