package com.application.ads;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.adapter.SubCategoryAdapter;
import com.application.ads.data.ProductDetails;
import com.application.ads.data.SubCategoryData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SubCategoryActivity extends Fragment {

    public static String urlLoadSubCategories = DBConnection.BASEURL + "load_sub_categories.php";
    public static String urlSearchProducts = DBConnection.BASEURL + "search_products.php";
    List<SubCategoryData> subcategorydata;
    JSONParser parser;
    GridView subcatgrid;
    SubCategoryAdapter subcatadapter;
    TextView parenttitle;
    Bundle bundle;
    SharedPreferences preferences;
    SessionManager manager;
    ArrayAdapter<String> prodadapter;
    AutoCompleteTextView searchtext;
    ImageView searchbtn;
    List<ProductDetails> productsdetails;
    List<String> produnctname;
    int status;
    String parenttitletext, parentid;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private String searchProdkey;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_subcategory, null);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        parser = new JSONParser();

        bundle = getArguments();
        parentid = bundle.getString("parentid");
        parenttitletext = bundle.getString("parenttitle");
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);


        preferences = getActivity().getSharedPreferences(
                SessionManager.PREF_NAME, SessionManager.PRIVATE_MODE);

        parenttitle = (TextView) view.findViewById(R.id.parenttitle);
        subcatgrid = (GridView) view.findViewById(R.id.subcatgrid);
        manager = new SessionManager(getActivity());
        parenttitle.setText(parenttitletext);
//      new ExecuteLoadSubCategories(parentid).execute();
//      https://achadiscount.in/android/load_sub_categories.php?parent_id=20002
        String subCategoryUrl = urlLoadSubCategories + "?" + "parent_id=" + parentid;
        volleyJsonObjectRequest(subCategoryUrl);


        subcatgrid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getActivity(),
                        ProductDetailsActivity.class);
                intent.putExtra("prod_parent_name",
                        subcategorydata.get(position).getSubcat_name());
                intent.putExtra("parent_child_id", subcategorydata
                        .get(position).getSubcat_id());
                startActivity(intent);
            }
        });

        produnctname = new ArrayList<String>();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
//                new ExecuteLoadSubCategories(parentid).execute();
                String subCategoryUrl = urlLoadSubCategories + "?" + "parent_id=" + parentid;
                volleyJsonObjectRequest(subCategoryUrl);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        searchtext = (AutoCompleteTextView) view.findViewById(R.id.searchtext);

        searchbtn = (ImageView) view.findViewById(R.id.searchbtn);
        searchbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (searchtext.getText().toString().equals("")
                        || searchtext.getText().toString().equals(null)) {
                    Toast.makeText(getActivity(), "Please provide search term",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String name = searchtext.getText().toString();
                    // Log.e("name", name);

                    for (int i = 0; i < productsdetails.size(); i++) {

                        if (name.equalsIgnoreCase(productsdetails.get(i)
                                .getProduct_name())) {
                            status = 1;
                            // Toast.makeText(getActivity(),
                            // productsdetails.get(i).getProduct_id(),
                            // Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(),
                                    ProductOverView.class);
                            intent.putExtra("prod_id", productsdetails.get(i)
                                    .getProduct_id());
                            searchtext.setText("");
                            startActivity(intent);

                        } else {
                            status = 0;
                        }

                    }
                    if (status == 0) {

                        searchProdkey = searchtext.getText().toString().trim();

                        Intent intent = new Intent(getActivity(), SearchProductDetailsActivity.class);
                        intent.putExtra("prodName", searchProdkey);
                        startActivity(intent);
                     /*   Toast.makeText(getActivity(),
                                "No data found..Try different keyword",
                                Toast.LENGTH_SHORT).show();*/
                        searchtext.setText("");
                    }

                }
            }
        });

        prodadapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                produnctname);
        searchtext.setAdapter(prodadapter);
        searchtext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("Text", s.toString());
                new ExecuteSearchProducts(s.toString()).execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        searchtext.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String name = searchtext.getText().toString();
                for (int i = 0; i < productsdetails.size(); i++) {
                    if (name.equalsIgnoreCase(productsdetails.get(i)
                            .getProduct_name())) {
                        Intent intent = new Intent(getActivity(),
                                ProductOverView.class);
                        intent.putExtra("prod_id", productsdetails.get(i)
                                .getProduct_id());
                        searchtext.setText("");
                        startActivity(intent);
                    }
                }
            }
        });

        return view;
    }

    public void volleyJsonObjectRequest(String url) {

        subcategorydata = new ArrayList<SubCategoryData>();
        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonObject.getInt("success") == 1) {
                        JSONArray subcatarray = jsonObject.getJSONArray("sub_categories");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            SubCategoryData subcatdata = new SubCategoryData();
                            subcatdata.setSubcat_id(subcatobj
                                    .getString("cate_id"));
                            subcatdata.setSubcat_name(subcatobj
                                    .getString("cate_name"));
                            subcatdata.setSubcat_img(subcatobj
                                    .getString("cate_img"));
                            subcategorydata.add(subcatdata);

                        }
                        // placeCategory();
                        subcatadapter = new SubCategoryAdapter(getActivity(),
                                subcategorydata);
                        subcatgrid.setAdapter(subcatadapter);
                        // showSubCatDialog(subcategorydata,parentname);

                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }

//                    Toast.makeText(getActivity(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public class ExecuteLoadSubCategories extends
            AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String parentid;

        public ExecuteLoadSubCategories(String parentid) {
            this.parentid = parentid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(getActivity(), "", "Loading..", true,
                    false);

        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("parent_id", parentid));
            JSONObject object = parser.makeHttpRequest(getActivity(), urlLoadSubCategories,
                    "GET", param);
            Log.e("Object", object.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            dialog.dismiss();
            subcategorydata = new ArrayList<SubCategoryData>();
            // Toast.makeText(getActivity()., result.length()+"",
            // Toast.LENGTH_SHORT).show();
            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {
                        JSONArray subcatarray = object
                                .getJSONArray("sub_categories");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            SubCategoryData subcatdata = new SubCategoryData();
                            subcatdata.setSubcat_id(subcatobj
                                    .getString("cate_id"));
                            subcatdata.setSubcat_name(subcatobj
                                    .getString("cate_name"));
                            subcatdata.setSubcat_img(subcatobj
                                    .getString("cate_img"));
                            subcategorydata.add(subcatdata);

                        }
                        // placeCategory();
                        subcatadapter = new SubCategoryAdapter(getActivity(),
                                subcategorydata);
                        subcatgrid.setAdapter(subcatadapter);
                        // showSubCatDialog(subcategorydata,parentname);

                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    System.out.println(e);
                }
            }
        }
    }

    class ExecuteSearchProducts extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String start_letter;


        public ExecuteSearchProducts(String start_letter) {
            this.start_letter = start_letter;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading..");
            dialog.setCanceledOnTouchOutside(false);
            //	dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("start_letter", start_letter));
            JSONObject object = parser.makeHttpRequest(getActivity(), urlSearchProducts,
                    "GET", param);
            Log.e("Object", urlSearchProducts + param.toString());
            if (object != null) {
                return object.toString();
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //	dialog.dismiss();

            List<SubCategoryData> subcatlist = new ArrayList<SubCategoryData>();

            if (result.trim().length() > 0) {
                try {
                    JSONObject object = new JSONObject(result);
                    if (object.getInt("success") == 1) {

                        productsdetails = new ArrayList<ProductDetails>();

                        JSONArray subcatarray = object.getJSONArray("products");

                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject subcatobj = subcatarray.getJSONObject(j);
                            ProductDetails productdetails = new ProductDetails();
                            productdetails.setProduct_name(subcatobj
                                    .getString("prod_name"));
                            productdetails.setProduct_id(subcatobj
                                    .getString("product_id"));
                            productsdetails.add(productdetails);
                        }

                        List<String> produnctname = new ArrayList<String>();
                        for (int i = 0; i < productsdetails.size(); i++) {

                            produnctname.add(productsdetails.get(i)
                                    .getProduct_name());
                        }

                        prodadapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_list_item_1,
                                produnctname);
                        searchtext.setAdapter(prodadapter);

                    } else {
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT)
                                .show();
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
    }

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
