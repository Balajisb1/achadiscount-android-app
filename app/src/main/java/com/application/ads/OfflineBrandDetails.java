package com.application.ads;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.application.ads.data.MyApplication;
import com.application.ads.data.OfflineStoreData;
import com.application.ads.extra.DBConnection;
import com.application.ads.extra.JSONParser;
import com.application.ads.extra.SessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class OfflineBrandDetails extends Activity {

    public static String urlLoadSingleOfferDetails = "https://achadiscount.in/offline/admin/index.php/json/getsinglebrandforapp";
    public static String urlSubscribeBrand = "https://achadiscount.in/offline/admin/index.php/json/addtofavouriteforapp";
    ImageView currentbrandimage;
    TextView currentbrandname, currentbranddesc, currentoffername,
            currentofferperc, readmore, nocurrentresulttext,
            noupcomingresulttext, noexpiredresulttext;
    LinearLayout currentofferslinear, upcominglinear, expiredlinear;
    Button currentbrandstoredetails;
    JSONParser parser;
    SessionManager manager;
    SharedPreferences preferences;
    boolean status;
    Button currentofferbtn, upcomingbtn, expiredbtn;
    Dialog dialog;
    ImageView offlinefav;
    MyApplication myApplication;
    List<OfflineStoreData> categorydata, storeaddress, currentoffers,
            upcomingoffers, expiredoffers;

    // ?userid=1&brandid=1&name=avinashghare&contactno=8888888899&email=test@gmail.com

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_brand_details);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View toolbarview = getLayoutInflater().inflate(
                R.layout.activity_othertoolbar, null);
        TextView toolbartitle = (TextView) toolbarview
                .findViewById(R.id.toolbartitle);
        toolbartitle.setText("Offline Brand Details");
        getActionBar().setCustomView(toolbarview);
        myApplication = MyApplication.getInstance();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        offlinefav = (ImageView) findViewById(R.id.offlinefav);


        parser = new JSONParser();
        manager = new SessionManager(OfflineBrandDetails.this);
        preferences = getSharedPreferences(SessionManager.PREF_NAME,
                SessionManager.PRIVATE_MODE);

        currentbrandimage = (ImageView) findViewById(R.id.currentbrandimage);
        currentbrandname = (TextView) findViewById(R.id.currentbrandname);
        currentbranddesc = (TextView) findViewById(R.id.currentbranddesc);

        nocurrentresulttext = (TextView) findViewById(R.id.nocurrentresulttext);
        noupcomingresulttext = (TextView) findViewById(R.id.noupcomingresulttext);
        noexpiredresulttext = (TextView) findViewById(R.id.noexpiredresulttext);

        currentofferslinear = (LinearLayout) findViewById(R.id.currentofferslinear);
        expiredlinear = (LinearLayout) findViewById(R.id.expiredlinear);
        upcominglinear = (LinearLayout) findViewById(R.id.upcominglinear);

        expiredbtn = (Button) findViewById(R.id.expiredbtn);
        upcomingbtn = (Button) findViewById(R.id.upcomingbtn);
        currentofferbtn = (Button) findViewById(R.id.currentofferbtn);
        currentbrandstoredetails = (Button) findViewById(R.id.currentbrandstoredetails);

        storeaddress = new ArrayList<OfflineStoreData>();
        categorydata = new ArrayList<OfflineStoreData>();
        expiredoffers = new ArrayList<OfflineStoreData>();
        upcomingoffers = new ArrayList<OfflineStoreData>();
        currentoffers = new ArrayList<OfflineStoreData>();

        offlinefav.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (manager.isLoggedIn()) {
//					new ExecuteAddFavourite().execute();
                /*	List<NameValuePair> param = new ArrayList<NameValuePair>();
					Log.e("id is:", getIntent().getStringExtra("id") + " id");

					// ?=1&=1&=&=8888888899&=test@gmail.com
					param.add(new BasicNameValuePair("brandid", getIntent()
							.getStringExtra("id")));
					param.add(new BasicNameValuePair("email", preferences.getString(
							SessionManager.KEY_EMAIL, "")));
					param.add(new BasicNameValuePair("name", preferences.getString(
							SessionManager.KEY_USERNAME, "")));
					param.add(new BasicNameValuePair("userid", preferences.getString(
							SessionManager.KEY_USERID, "")));
					param.add(new BasicNameValuePair("contactno", preferences
							.getString(SessionManager.KEY_CONTACTNO, "")));*/

                    String url = urlSubscribeBrand + "?brandid=" + getIntent().getStringExtra("id") + "&email=" + preferences.getString(SessionManager.KEY_EMAIL, "")
                            + "&name=" + preferences.getString(SessionManager.KEY_USERNAME, "") + "&userid=" + preferences.getString(SessionManager.KEY_USERID, "") +
                            "&contactno=" + preferences.getString(SessionManager.KEY_CONTACTNO, "");
                    volleyJsonObjectRequest1(url);


                } else {
                    Intent intent = new Intent(OfflineBrandDetails.this,
                            LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        currentbrandstoredetails.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog = new Dialog(OfflineBrandDetails.this);
                dialog.setContentView(R.layout.activity_offline_store_locations);
                LinearLayout addresslinear = (LinearLayout) dialog
                        .findViewById(R.id.addresslinear);
                for (int i = 0; i < storeaddress.size(); i++) {
                    final int m = i;
                    if (storeaddress.get(i).getDist() <= 10) {
                        View view = getLayoutInflater().inflate(
                                R.layout.activity_offline_address_text, null);
                        TextView offlineaddressname = (TextView) view
                                .findViewById(R.id.offlineaddressname);
                        offlineaddressname.setText(storeaddress.get(i)
                                .getLocationaddr());
                        view.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(
                                        OfflineBrandDetails.this,
                                        OfflineOfferMap.class);
                                intent.putExtra("lat", storeaddress.get(m)
                                        .getLocationlat());
                                intent.putExtra("long", storeaddress.get(m)
                                        .getLocationlong());
                                intent.putExtra("address", storeaddress.get(m)
                                        .getLocationaddr());
                                intent.putExtra("city", storeaddress.get(m)
                                        .getLocationcity());
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        });
                        addresslinear.addView(view);
                    }
                }
                dialog.show();

            }
        });

        currentofferbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                currentofferbtn.setBackgroundColor(Color.parseColor("#f32440"));
                currentofferbtn.setTextColor(getResources().getColor(
                        R.color.WHITE));

                upcomingbtn.setBackgroundResource(R.drawable.border_red);
                upcomingbtn.setTextColor(getResources().getColor(R.color.PINK));

                expiredbtn.setBackgroundResource(R.drawable.border_red);
                expiredbtn.setTextColor(getResources().getColor(R.color.PINK));

                currentofferslinear.setVisibility(View.VISIBLE);
                expiredlinear.setVisibility(View.GONE);
                upcominglinear.setVisibility(View.GONE);

            }
        });

        upcomingbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                currentofferbtn.setBackgroundResource(R.drawable.border_red);
                currentofferbtn.setTextColor(getResources().getColor(
                        R.color.PINK));

                upcomingbtn.setBackgroundColor(Color.parseColor("#f32440"));
                upcomingbtn
                        .setTextColor(getResources().getColor(R.color.WHITE));

                expiredbtn.setBackgroundResource(R.drawable.border_red);
                expiredbtn.setTextColor(getResources().getColor(R.color.PINK));

                currentofferslinear.setVisibility(View.GONE);
                expiredlinear.setVisibility(View.GONE);
                upcominglinear.setVisibility(View.VISIBLE);
            }
        });

        expiredbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                currentofferbtn.setBackgroundResource(R.drawable.border_red);
                currentofferbtn.setTextColor(getResources().getColor(
                        R.color.PINK));

                upcomingbtn.setBackgroundResource(R.drawable.border_red);
                upcomingbtn.setTextColor(getResources().getColor(R.color.PINK));

                expiredbtn.setBackgroundColor(Color.parseColor("#f32440"));
                expiredbtn.setTextColor(getResources().getColor(R.color.WHITE));
                currentofferslinear.setVisibility(View.GONE);
                expiredlinear.setVisibility(View.VISIBLE);
                upcominglinear.setVisibility(View.GONE);
            }
        });

        readmore = (TextView) findViewById(R.id.readmore);

        readmore.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!status) {
                    currentbranddesc.setMaxLines(1000);
                    readmore.setText("Read Less");
                    status = true;
                } else {
                    currentbranddesc.setMaxLines(4);
                    readmore.setText("Read More");
                    status = false;
                }
            }
        });

//		new ExecuteLoadAllAddress().execute();
/*
		List<NameValuePair> param = new ArrayList<NameValuePair>();
		//	Log.e("id is:", getIntent().getStringExtra("id") + " id");
		param.add(new BasicNameValuePair("id", getIntent().getStringExtra(
				"id")));
		param.add(new BasicNameValuePair("lat", myApplication
				.getMyLatitude()));
		param.add(new BasicNameValuePair("long", myApplication
				.getMyLongitude()));
		param.add(new BasicNameValuePair("userid", preferences.getString(SessionManager.KEY_USERID, "0")));*/

        String url = urlLoadSingleOfferDetails + "?id=" + getIntent().getStringExtra("id") + "&lat=" + myApplication.getMyLatitude()
                + "&long=" + myApplication.getMyLongitude() + "&userid=" + preferences.getString(SessionManager.KEY_USERID, "0");
        volleyJsonObjectRequest(url);
    }


    public void volleyJsonObjectRequest1(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//					JSONObject jsonObject = new JSONObject(jsonString);

                    if (jsonString.equals("0")) {
                        Toast.makeText(OfflineBrandDetails.this,
                                "Failed to Subscribe of this brand",
                                Toast.LENGTH_SHORT).show();
                        offlinefav.setImageResource(R.drawable.heart);
                    } else {

                        offlinefav.setImageResource(R.drawable.filled_heart);
                        Toast.makeText(OfflineBrandDetails.this,
                                "Subscription of this brand successfully",
                                Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void volleyJsonObjectRequest(String url) {


        Log.i("URL", url);
        final RequestQueue queue = Volley.newRequestQueue(this);
        CacheRequest cacheRequest = new CacheRequest(0, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);

                    storeaddress = new ArrayList<OfflineStoreData>();
                    categorydata = new ArrayList<OfflineStoreData>();
                    expiredoffers = new ArrayList<OfflineStoreData>();
                    upcomingoffers = new ArrayList<OfflineStoreData>();
                    currentoffers = new ArrayList<OfflineStoreData>();

                    if (jsonString.toString().trim().length() > 0) {


                        JSONObject object1 = jsonObject.getJSONObject("brand");
                        OfflineStoreData data = new OfflineStoreData();
                        data.setId(object1.getString("id"));
                        data.setImage(object1.getString("logo"));
                        data.setOfferimage(object1.getString("image"));
                        data.setDescription(object1.getString("description"));
                        data.setBrandname(object1.getString("name"));
                        data.setFavorite(object1.getString("favourite"));
                        categorydata.add(data);

                        JSONArray jsonArray2 = jsonObject.getJSONArray("locations");
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            JSONObject object = jsonArray2.getJSONObject(j);
                            OfflineStoreData data2 = new OfflineStoreData();
                            data2.setLocationid(object.getString("id"));
                            data2.setLocationcity(object.getString("city"));
                            data2.setLocationlat(object.getString("lat"));
                            data2.setLocationlong(object.getString("long"));
                            data2.setLocationaddr(object.getString("address1"));
                            data2.setDist(object.getDouble("dist"));
                            storeaddress.add(data2);
                        }

                        JSONArray jsonArray3 = jsonObject.getJSONArray("active");
                        for (int k = 0; k < jsonArray3.length(); k++) {
                            JSONObject object2 = jsonArray3.getJSONObject(k);
                            OfflineStoreData data3 = new OfflineStoreData();
                            data3.setBrand(object2.getString("brand"));
                            data3.setOffer(object2.getString("offer"));
                            data3.setDescription(object2.getString("description"));
                            data3.setBrandname(object2.getString("brandname"));
                            data3.setDist(object2.getDouble("distance"));
                            currentoffers.add(data3);

                        }

                        JSONArray jsonArray4 = jsonObject.getJSONArray("upcoming");
                        for (int l = 0; l < jsonArray4.length(); l++) {
                            JSONObject object3 = jsonArray4.getJSONObject(l);
                            OfflineStoreData data4 = new OfflineStoreData();
                            data4.setBrand(object3.getString("brand"));
                            data4.setOffer(object3.getString("offer"));
                            data4.setDescription(object3.getString("description"));
                            data4.setBrandname(object3.getString("brandname"));
                            data4.setDist(object3.getDouble("distance"));
                            upcomingoffers.add(data4);

                        }

                        JSONArray jsonArray5 = jsonObject.getJSONArray("expired");
                        for (int k = 0; k < jsonArray5.length(); k++) {
                            JSONObject object4 = jsonArray5.getJSONObject(k);
                            OfflineStoreData data5 = new OfflineStoreData();
                            data5.setBrand(object4.getString("brand"));
                            data5.setOffer(object4.getString("offer"));
                            data5.setDescription(object4.getString("description"));
                            data5.setBrandname(object4.getString("brandname"));
                            data5.setDist(object4.getDouble("distance"));
                            expiredoffers.add(data5);

                        }

                        fillAllBrandDetails(categorydata, currentoffers,
                                upcomingoffers, expiredoffers);
                        // fillAllBrandDetails(categorydata,storeaddress);
                    } else {
                        Toast.makeText(OfflineBrandDetails.this,
                                "No addresses found", Toast.LENGTH_SHORT).show();
                    }


//                    Toast.makeText(getApplicationContext(), "onResponse:\n\n" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getActivity(), "onErrorResponse:\n\n" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(cacheRequest);

    }

    public void fillAllBrandDetails(List<OfflineStoreData> categorydata,
                                    List<OfflineStoreData> currentoffers,
                                    List<OfflineStoreData> upcomingoffers,
                                    List<OfflineStoreData> expiredoffers) {

        for (int i = 0; i < categorydata.size(); i++) {
            currentbrandname.setText(categorydata.get(i).getBrandname());
            currentbranddesc.setText(categorydata.get(i).getDescription());
            if (currentbranddesc.getLineCount() > 4) {
                readmore.setVisibility(View.VISIBLE);
            }

            if (categorydata.get(i).getFavorite().equalsIgnoreCase("0")) {
                offlinefav.setImageResource(R.drawable.heart);
            } else {
                offlinefav.setImageResource(R.drawable.filled_heart);
            }

            Picasso.with(OfflineBrandDetails.this).load(DBConnection.OFFLINE_EXTRAIMG
                    + categorydata.get(i).getOfferimage()).placeholder(R.drawable.heart).resize(144, 0).into(currentbrandimage);

        }

        if (currentoffers.size() > 0) {
            for (int j = 0; j < currentoffers.size(); j++) {
                final int m = j;
                final List<OfflineStoreData> currentoffersdetails = currentoffers;
                View view = getLayoutInflater().inflate(
                        R.layout.activity_offline_brand_offerdetails, null);

                currentoffername = (TextView) view
                        .findViewById(R.id.currentoffername);
                currentofferperc = (TextView) view
                        .findViewById(R.id.currentofferperc);
                currentoffername.setText(currentoffers.get(j).getBrandname()
                        + " - " + currentoffers.get(j).getDescription());
                currentofferperc.setText(currentoffers.get(j).getOffer()
                        + "% Offer");

                view.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OfflineBrandDetails.this,
                                OfflineOfferDetails.class);
                        intent.putExtra("id", currentoffersdetails.get(m)
                                .getBrand());
                        intent.putExtra("desc", currentoffersdetails.get(m)
                                .getDescription());
                        startActivity(intent);
                    }
                });
                currentofferslinear.addView(view);
            }
        } else {
            nocurrentresulttext.setVisibility(View.VISIBLE);
        }

        if (expiredoffers.size() > 0) {
            for (int j = 0; j < expiredoffers.size(); j++) {
                final int m = j;
                final List<OfflineStoreData> currentoffersdetails = expiredoffers;
                View view = getLayoutInflater().inflate(
                        R.layout.activity_offline_brand_offerdetails, null);

                currentoffername = (TextView) view
                        .findViewById(R.id.currentoffername);
                currentofferperc = (TextView) view
                        .findViewById(R.id.currentofferperc);
                currentoffername.setText(expiredoffers.get(j).getBrandname()
                        + " - " + expiredoffers.get(j).getDescription());
                currentofferperc.setText(expiredoffers.get(j).getOffer()
                        + "% Offer");

                view.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OfflineBrandDetails.this,
                                OfflineOfferDetails.class);
                        intent.putExtra("id", currentoffersdetails.get(m)
                                .getBrand());
                        startActivity(intent);
                    }
                });
                expiredlinear.addView(view);
            }
        } else {
            noexpiredresulttext.setVisibility(View.VISIBLE);
        }

        if (upcomingoffers.size() > 0) {
            for (int j = 0; j < upcomingoffers.size(); j++) {
                final int m = j;
                final List<OfflineStoreData> currentoffersdetails = upcomingoffers;

                View view = getLayoutInflater().inflate(
                        R.layout.activity_offline_brand_offerdetails, null);

                currentoffername = (TextView) view
                        .findViewById(R.id.currentoffername);
                currentofferperc = (TextView) view
                        .findViewById(R.id.currentofferperc);
                currentoffername.setText(upcomingoffers.get(j).getBrandname()
                        + " - " + upcomingoffers.get(j).getDescription());
                currentofferperc.setText(upcomingoffers.get(j).getOffer()
                        + "% Offer");

                view.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OfflineBrandDetails.this,
                                OfflineOfferDetails.class);
                        intent.putExtra("id", currentoffersdetails.get(m)
                                .getBrand());
                        startActivity(intent);
                    }
                });
                upcominglinear.addView(view);
            }
        } else {
            noupcomingresulttext.setVisibility(View.VISIBLE);
        }

		/*
		 * for(int j=0;j<storeaddress.size();j++){ final int m=j; final
		 * List<OfflineStoreData> storeaddressdetails=storeaddress;
		 * if(storeaddress.get(j).getDist()<=10){ View
		 * view=getLayoutInflater().inflate
		 * (R.layout.activity_offline_address_text, null); TextView
		 * offlineaddressname
		 * =(TextView)view.findViewById(R.id.offlineaddressname);
		 * offlineaddressname
		 * .setText(storeaddress.get(j).getLocationaddr().toString());
		 * offlineaddressname.setOnClickListener(new OnClickListener() {
		 *
		 * @Override public void onClick(View v) { Intent intent=new
		 * Intent(OfflineBrandDetails.this,OfflineOfferMap.class);
		 * intent.putExtra("lat", storeaddressdetails.get(m).getLocationlat());
		 * intent.putExtra("long",
		 * storeaddressdetails.get(m).getLocationlong());
		 * intent.putExtra("address",
		 * storeaddressdetails.get(m).getLocationaddr());
		 * intent.putExtra("city",
		 * storeaddressdetails.get(m).getLocationcity()); startActivity(intent);
		 *
		 *
		 * } }); addresslinear.addView(view); }
		 *
		 * }
		 */

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // public void fillAllBrandDetails(List<OfflineStoreData>
    // categorydata,List<OfflineStoreData> storeaddress ){

    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }

    class ExecuteLoadAllAddress extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineBrandDetails.this, "",
                    "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            //	Log.e("id is:", getIntent().getStringExtra("id") + " id");
            param.add(new BasicNameValuePair("id", getIntent().getStringExtra(
                    "id")));
            param.add(new BasicNameValuePair("lat", myApplication
                    .getMyLatitude()));
            param.add(new BasicNameValuePair("long", myApplication
                    .getMyLongitude()));
            param.add(new BasicNameValuePair("userid", preferences.getString(SessionManager.KEY_USERID, "0")));
			/*
			 * JSONArray
			 * array=parser.makeHttpRequest1(urlLoadSingleOfferDetails, "GET",
			 * param); return array.toString();
			 */
            JSONObject object = parser.makeHttpRequest(getApplicationContext(),
                    urlLoadSingleOfferDetails, "GET", param);
            return object.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();
            storeaddress = new ArrayList<OfflineStoreData>();
            categorydata = new ArrayList<OfflineStoreData>();
            expiredoffers = new ArrayList<OfflineStoreData>();
            upcomingoffers = new ArrayList<OfflineStoreData>();
            currentoffers = new ArrayList<OfflineStoreData>();
            try {
                if (result.trim().length() > 0) {

                    JSONObject jsonObject = new JSONObject(result);

                    JSONObject object1 = jsonObject.getJSONObject("brand");
                    OfflineStoreData data = new OfflineStoreData();
                    data.setId(object1.getString("id"));
                    data.setImage(object1.getString("logo"));
                    data.setOfferimage(object1.getString("image"));
                    data.setDescription(object1.getString("description"));
                    data.setBrandname(object1.getString("name"));
                    data.setFavorite(object1.getString("favourite"));
                    categorydata.add(data);

                    JSONArray jsonArray2 = jsonObject.getJSONArray("locations");
                    for (int j = 0; j < jsonArray2.length(); j++) {
                        JSONObject object = jsonArray2.getJSONObject(j);
                        OfflineStoreData data2 = new OfflineStoreData();
                        data2.setLocationid(object.getString("id"));
                        data2.setLocationcity(object.getString("city"));
                        data2.setLocationlat(object.getString("lat"));
                        data2.setLocationlong(object.getString("long"));
                        data2.setLocationaddr(object.getString("address1"));
                        data2.setDist(object.getDouble("dist"));
                        storeaddress.add(data2);
                    }

                    JSONArray jsonArray3 = jsonObject.getJSONArray("active");
                    for (int k = 0; k < jsonArray3.length(); k++) {
                        JSONObject object2 = jsonArray3.getJSONObject(k);
                        OfflineStoreData data3 = new OfflineStoreData();
                        data3.setBrand(object2.getString("brand"));
                        data3.setOffer(object2.getString("offer"));
                        data3.setDescription(object2.getString("description"));
                        data3.setBrandname(object2.getString("brandname"));
                        data3.setDist(object2.getDouble("distance"));
                        currentoffers.add(data3);

                    }

                    JSONArray jsonArray4 = jsonObject.getJSONArray("upcoming");
                    for (int l = 0; l < jsonArray4.length(); l++) {
                        JSONObject object3 = jsonArray4.getJSONObject(l);
                        OfflineStoreData data4 = new OfflineStoreData();
                        data4.setBrand(object3.getString("brand"));
                        data4.setOffer(object3.getString("offer"));
                        data4.setDescription(object3.getString("description"));
                        data4.setBrandname(object3.getString("brandname"));
                        data4.setDist(object3.getDouble("distance"));
                        upcomingoffers.add(data4);

                    }

                    JSONArray jsonArray5 = jsonObject.getJSONArray("expired");
                    for (int k = 0; k < jsonArray5.length(); k++) {
                        JSONObject object4 = jsonArray5.getJSONObject(k);
                        OfflineStoreData data5 = new OfflineStoreData();
                        data5.setBrand(object4.getString("brand"));
                        data5.setOffer(object4.getString("offer"));
                        data5.setDescription(object4.getString("description"));
                        data5.setBrandname(object4.getString("brandname"));
                        data5.setDist(object4.getDouble("distance"));
                        expiredoffers.add(data5);

                    }

                    fillAllBrandDetails(categorydata, currentoffers,
                            upcomingoffers, expiredoffers);
                    // fillAllBrandDetails(categorydata,storeaddress);
                } else {
                    Toast.makeText(OfflineBrandDetails.this,
                            "No addresses found", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception exception) {
                Log.e("except", exception.toString());
            }
        }

    }

    class ExecuteAddFavourite extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = ProgressDialog.show(OfflineBrandDetails.this, "",
                    "Loading..");
        }

        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            Log.e("id is:", getIntent().getStringExtra("id") + " id");

            // ?=1&=1&=&=8888888899&=test@gmail.com
            param.add(new BasicNameValuePair("brandid", getIntent()
                    .getStringExtra("id")));
            param.add(new BasicNameValuePair("email", preferences.getString(
                    SessionManager.KEY_EMAIL, "")));
            param.add(new BasicNameValuePair("name", preferences.getString(
                    SessionManager.KEY_USERNAME, "")));
            param.add(new BasicNameValuePair("userid", preferences.getString(
                    SessionManager.KEY_USERID, "")));
            param.add(new BasicNameValuePair("contactno", preferences
                    .getString(SessionManager.KEY_CONTACTNO, "")));
			/*
			 * JSONArray
			 * array=parser.makeHttpRequest1(urlLoadSingleOfferDetails, "GET",
			 * param); return array.toString();
			 */

            Log.e("obj", urlSubscribeBrand + param.toString());
            String object = parser.makeHttpRequest2(OfflineBrandDetails.this, urlSubscribeBrand, "GET",
                    param);

            return object;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog.dismiss();

            try {
                // JSONObject object = new JSONObject(result);
                Log.e("object", result);
                if (result.equals("0")) {
                    Toast.makeText(OfflineBrandDetails.this,
                            "Failed to Subscribe of this brand",
                            Toast.LENGTH_SHORT).show();
                    offlinefav.setImageResource(R.drawable.heart);
                } else {

                    offlinefav.setImageResource(R.drawable.filled_heart);
                    Toast.makeText(OfflineBrandDetails.this,
                            "Subscription of this brand successfully",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.e("err", e.toString());
            }

        }
    }
}
